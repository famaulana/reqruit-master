<?php

Route::group([
    'prefix' => 'auth',
    'as' => 'auth.',
    'namespace' => 'Http\Controllers\Admin'
], function () {
    Route::get('panel-login', 'Auth\LoginController@showLoginForm')->name('admin.login.form');
    Route::post('panel-login', 'Auth\LoginController@login')->name('admin.login.post');
    Route::post('panel-logout', 'Auth\LoginController@logout')->name('admin.logout');

    Route::get('panel-password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('panel-password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::get('panel-password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('admin.password.reset');
    Route::post('panel-password/update', 'Auth\ResetPasswordController@reset')->name('admin.password.update');

    Route::get('panel/verify/{token}', 'Auth\VerificationController@verify')->name('admin.verify');
    Route::get('panel/send-verification', 'Auth\VerificationController@resend')->name('admin.resend-verification');
});

Route::group([
    'prefix' => 'admin',
    'as' => 'admin.',
    'middleware' => ['verified-admin'],
    'guard' => 'admin',
    'namespace' => 'Http\Controllers\Admin'
], function () {
    Route::get('home', 'HomeController@index')->name('home');

    // Category
    // Route::post('/categories/store-content-image', 'CategoryController@storeContentImage')->name('categories.store-content-image');
    // Route::get('/categories/all', 'CategoryController@all')->name('categories.all');
    // Route::post('/categories/bulk-delete-permanent', 'CategoryController@bulkDestroyPermanent')->name('categories.bulk-delete-permanent');
    // Route::get('/categories/{id}/delete-permanent', 'CategoryController@destroyPermanent')->name('categories.delete-permanent');
    // Route::get('/categories/{id}/restore', 'CategoryController@restore')->name('categories.restore');
    // Route::get('/categories/trashed', 'CategoryController@trashed')->name('categories.trashed');
    // Route::get('/categories/search', 'CategoryController@search')->name('categories.search');
    // Route::resource('/categories', 'CategoryController');

    // Pages
    Route::post('pages/bulk-delete', 'PageController@bulkDestroy')->name('pages.bulk-delete');
    Route::post('pages/bulk-delete-permanent', 'PageController@bulkDestroyPermanent')->name('pages.bulk-delete-permanent');
    Route::get('pages/{id}/delete-permanent', 'PageController@destroyPermanent')->name('pages.delete-permanent');
    Route::get('pages/{id}/restore', 'PageController@restore')->name('pages.restore');
    Route::get('pages/trashed', 'PageController@trashed')->name('pages.trashed');
    Route::post('pages/store-content-image', 'PageController@storeContentImage')->name('pages.store-content-image');
    Route::resource('pages', 'PageController');

    // Employers
    Route::get('employers/{employer}/companies', 'EmployerController@companies')->name('employers.companies');
    Route::get('employers/{id}/avatar', 'EmployerController@avatar')->name('employers.avatar');
    Route::patch('employers/{id}/update-password', 'EmployerController@updatePassword')->name('employers.update-password');
    Route::post('employers/bulk-delete', 'EmployerController@bulkDestroy')->name('employers.bulk-delete');
    Route::post('employers/bulk-delete-permanent', 'EmployerController@bulkDestroyPermanent')->name('employers.bulk-delete-permanent');
    Route::get('employers/{id}/delete-permanent', 'EmployerController@destroyPermanent')->name('employers.delete-permanent');
    Route::get('employers/{id}/restore', 'EmployerController@restore')->name('employers.restore');
    Route::get('employers/trashed', 'EmployerController@trashed')->name('employers.trashed');
    Route::resource('employers', 'EmployerController', ['only' => ['index', 'show', 'destroy']]);

    // Head Hunting
    Route::resource('head-huntings', 'HeadHuntingController', ['only' => ['index', 'show', 'update']]);

    // Companies
    Route::post('companies/bulk-delete', 'CompanyController@bulkDestroy')->name('companies.bulk-delete');
    Route::post('companies/bulk-delete-permanent', 'CompanyController@bulkDestroyPermanent')->name('companies.bulk-delete-permanent');
    Route::get('companies/{id}/delete-permanent', 'CompanyController@destroyPermanent')->name('companies.delete-permanent');
    Route::get('companies/{id}/restore', 'CompanyController@restore')->name('companies.restore');
    Route::get('companies/trashed', 'CompanyController@trashed')->name('companies.trashed');
    Route::resource('companies', 'CompanyController', ['only' => ['index', 'destroy', 'show']]);

    // Company reviews
    Route::post('company-reviews/bulk-delete', 'CompanyReviewController@bulkDestroy')->name('company-reviews.bulk-delete');
    Route::post('company-reviews/bulk-delete-permanent', 'CompanyReviewController@bulkDestroyPermanent')->name('company-reviews.bulk-delete-permanent');
    Route::get('company-reviews/{id}/delete-permanent', 'CompanyReviewController@destroyPermanent')->name('company-reviews.delete-permanent');
    Route::get('company-reviews/{id}/restore', 'CompanyReviewController@restore')->name('company-reviews.restore');
    Route::get('company-reviews/trashed', 'CompanyReviewController@trashed')->name('company-reviews.trashed');
    Route::resource('company-reviews', 'CompanyReviewController', ['only' => ['index', 'edit', 'update', 'destroy']]);

    // Industries
    Route::post('industries/bulk-delete', 'IndustryController@bulkDestroy')->name('industries.bulk-delete');
    Route::post('industries/bulk-delete-permanent', 'IndustryController@bulkDestroyPermanent')->name('industries.bulk-delete-permanent');
    Route::get('industries/{id}/delete-permanent', 'IndustryController@destroyPermanent')->name('industries.delete-permanent');
    Route::get('industries/{id}/restore', 'IndustryController@restore')->name('industries.restore');
    Route::get('industries/trashed', 'IndustryController@trashed')->name('industries.trashed');
    Route::resource('industries', 'IndustryController', ['except' => ['show']]);

    // Work Jobs
    Route::post('job/jobs/bulk-delete', 'WorkjobController@bulkDestroy')->name('jobs.bulk-delete');
    Route::post('job/jobs/bulk-delete-permanent', 'WorkjobController@bulkDestroyPermanent')->name('jobs.bulk-delete-permanent');
    Route::get('job/jobs/{id}/delete-permanent', 'WorkjobController@destroyPermanent')->name('jobs.delete-permanent');
    Route::get('job/jobs/{id}/restore', 'WorkjobController@restore')->name('jobs.restore');
    Route::get('job/jobs/trashed', 'WorkjobController@trashed')->name('jobs.trashed');
    Route::resource('job/jobs', 'WorkjobController', ['only' => ['index', 'show', 'destroy']]);

    // Job applications
    Route::resource('job/job-applications', 'JobApplicationController', ['only' => ['index', 'show']]);

    // Skills
    Route::post('job/skills/bulk-delete', 'SkillController@bulkDestroy')->name('skills.bulk-delete');
    Route::post('job/skills/bulk-delete-permanent', 'SkillController@bulkDestroyPermanent')->name('skills.bulk-delete-permanent');
    Route::get('job/skills/{id}/delete-permanent', 'SkillController@destroyPermanent')->name('skills.delete-permanent');
    Route::get('job/skills/{id}/restore', 'SkillController@restore')->name('skills.restore');
    Route::get('job/skills/trashed', 'SkillController@trashed')->name('skills.trashed');
    Route::resource('job/skills', 'SkillController', ['except' => ['show']]);

    // Job Functions
    Route::post('job/job-functions/bulk-delete', 'JobfunctionController@bulkDestroy')->name('job-functions.bulk-delete');
    Route::post('job/job-functions/bulk-delete-permanent', 'JobfunctionController@bulkDestroyPermanent')->name('job-functions.bulk-delete-permanent');
    Route::get('job/job-functions/{id}/delete-permanent', 'JobfunctionController@destroyPermanent')->name('job-functions.delete-permanent');
    Route::get('job/job-functions/{id}/restore', 'JobfunctionController@restore')->name('job-functions.restore');
    Route::get('job/job-functions/trashed', 'JobfunctionController@trashed')->name('job-functions.trashed');
    Route::resource('job/job-functions', 'JobfunctionController', ['except' => ['show']]);

    // Job Roles
    Route::post('job/job-roles/bulk-delete', 'JobroleController@bulkDestroy')->name('job-roles.bulk-delete');
    Route::post('job/job-roles/bulk-delete-permanent', 'JobroleController@bulkDestroyPermanent')->name('job-roles.bulk-delete-permanent');
    Route::get('job/job-roles/{id}/delete-permanent', 'JobroleController@destroyPermanent')->name('job-roles.delete-permanent');
    Route::get('job/job-roles/{id}/restore', 'JobroleController@restore')->name('job-roles.restore');
    Route::get('job/job-roles/trashed', 'JobroleController@trashed')->name('job-roles.trashed');
    Route::resource('job/job-roles', 'JobroleController', ['except' => ['show']]);

    // Job application rejection message
    Route::get('job/jap-rejection-messages', 'JapRejectionMessageController@index')->name('jap-rejection-messages.index');

    // Job Roles
    Route::post('testimonials/bulk-delete', 'TestimonialController@bulkDestroy')->name('testimonials.bulk-delete');
    Route::resource('testimonials', 'TestimonialController', ['except' => ['show']]);

    // Articles
    Route::post('articles/bulk-delete', 'ArticleController@bulkDestroy')->name('articles.bulk-delete');
    Route::post('articles/bulk-delete-permanent', 'ArticleController@bulkDestroyPermanent')->name('articles.bulk-delete-permanent');
    Route::get('articles/{id}/delete-permanent', 'ArticleController@destroyPermanent')->name('articles.delete-permanent');
    Route::get('articles/{id}/restore', 'ArticleController@restore')->name('articles.restore');
    Route::get('articles/trashed', 'ArticleController@trashed')->name('articles.trashed');
    Route::post('articles/store-content-image', 'ArticleController@storeContentImage')->name('articles.store-content-image');
    Route::resource('articles', 'ArticleController');

    // Users
    Route::delete('users/{user}/addresses/{address}', 'UserController@addressesDelete')->name('users.addresses.delete');
    Route::get('users/{user}/addresses', 'UserController@addresses')->name('users.addresses');
    Route::get('users/{id}/avatar', 'UserController@avatar')->name('users.avatar');
    Route::patch('users/{id}/update-password', 'UserController@updatePassword')->name('users.update-password');
    Route::post('users/bulk-delete', 'UserController@bulkDestroy')->name('users.bulk-delete');
    Route::post('users/bulk-delete-permanent', 'UserController@bulkDestroyPermanent')->name('users.bulk-delete-permanent');
    Route::get('users/{id}/delete-permanent', 'UserController@destroyPermanent')->name('users.delete-permanent');
    Route::get('users/{id}/restore', 'UserController@restore')->name('users.restore');
    Route::get('users/trashed', 'UserController@trashed')->name('users.trashed');
    Route::resource('users', 'UserController', ['except' => 'create']);

    // Education Degree
    Route::post('education/education-degrees/bulk-delete', 'EducationDegreeController@bulkDestroy')->name('education-degrees.bulk-delete');
    Route::post('education/education-degrees/bulk-delete-permanent', 'EducationDegreeController@bulkDestroyPermanent')->name('education-degrees.bulk-delete-permanent');
    Route::get('education/education-degrees/{id}/delete-permanent', 'EducationDegreeController@destroyPermanent')->name('education-degrees.delete-permanent');
    Route::get('education/education-degrees/{id}/restore', 'EducationDegreeController@restore')->name('education-degrees.restore');
    Route::get('education/education-degrees/trashed', 'EducationDegreeController@trashed')->name('education-degrees.trashed');
    Route::resource('education/education-degrees', 'EducationDegreeController', ['except' => 'show']);

    // Education Field
    Route::post('education/education-fields/bulk-delete', 'EducationFieldController@bulkDestroy')->name('education-fields.bulk-delete');
    Route::post('education/education-fields/bulk-delete-permanent', 'EducationFieldController@bulkDestroyPermanent')->name('education-fields.bulk-delete-permanent');
    Route::get('education/education-fields/{id}/delete-permanent', 'EducationFieldController@destroyPermanent')->name('education-fields.delete-permanent');
    Route::get('education/education-fields/{id}/restore', 'EducationFieldController@restore')->name('education-fields.restore');
    Route::get('education/education-fields/trashed', 'EducationFieldController@trashed')->name('education-fields.trashed');
    Route::resource('education/education-fields', 'EducationFieldController', ['except' => 'show']);

    // Education Field
    Route::post('question/english-questions/bulk-delete', 'EnglishQuestionController@bulkDestroy')->name('english-questions.bulk-delete');
    Route::post('question/english-questions/bulk-delete-permanent', 'EnglishQuestionController@bulkDestroyPermanent')->name('english-questions.bulk-delete-permanent');
    Route::get('question/english-questions/{id}/delete-permanent', 'EnglishQuestionController@destroyPermanent')->name('english-questions.delete-permanent');
    Route::get('question/english-questions/{id}/restore', 'EnglishQuestionController@restore')->name('english-questions.restore');
    Route::get('question/english-questions/trashed', 'EnglishQuestionController@trashed')->name('english-questions.trashed');
    Route::resource('question/english-questions', 'EnglishQuestionController', ['except' => 'show']);

    // Static Block
    Route::post('static-blocks/bulk-delete', 'StaticBlockController@bulkDestroy')->name('static-blocks.bulk-delete');
    Route::post('static-blocks/bulk-delete-permanent', 'StaticBlockController@bulkDestroyPermanent')->name('static-blocks.bulk-delete-permanent');
    Route::get('static-blocks/{id}/delete-permanent', 'StaticBlockController@destroyPermanent')->name('static-blocks.delete-permanent');
    Route::get('static-blocks/{id}/restore', 'StaticBlockController@restore')->name('static-blocks.restore');
    Route::get('static-blocks/trashed', 'StaticBlockController@trashed')->name('static-blocks.trashed');
    Route::post('static-blocks/store-content-image', 'StaticBlockController@storeContentImage')->name('static-blocks.store-content-image');
    Route::resource('static-blocks', 'StaticBlockController');

    // Menu
    Route::post('menus/bulk-delete', 'MenuController@bulkDestroy')->name('menus.bulk-delete');
    Route::post('menus/upload-image', 'MenuController@uploadImage')->name('menus.upload-image');
    Route::resource('menus', 'MenuController');

    // Main Banner
    Route::post('banners/main-banners/bulk-delete', 'MainBannerController@bulkDestroy')->name('main-banners.bulk-delete');
    Route::resource('banners/main-banners', 'MainBannerController');

    // Admins
    Route::get('admins/{id}/avatar', 'AdminAvatarController')->name('admins.avatar');
    Route::post('admins/{id}/update-password', 'AdminController@updatePassword')->name('admins.update-password');
    Route::post('admins/bulk-delete', 'AdminController@bulkDestroy')->name('admins.bulk-delete');
    Route::post('admins/bulk-delete-permanent', 'AdminController@bulkDestroyPermanent')->name('admins.bulk-delete-permanent');
    Route::get('admins/{id}/delete-permanent', 'AdminController@destroyPermanent')->name('admins.delete-permanent');
    Route::get('admins/{id}/restore', 'AdminController@restore')->name('admins.restore');
    Route::get('admins/trashed', 'AdminController@trashed')->name('admins.trashed');
    Route::resource('admins', 'AdminController');

    //Config
    Route::get('configs/general', 'ConfigController@general')->name('configs.general');
    Route::get('configs/logo', 'ConfigController@logo')->name('configs.logo');
    Route::get('configs/homepage', 'ConfigController@homepage')->name('configs.homepage');
    Route::get('configs/blog', 'ConfigController@blog')->name('configs.blog');
    Route::get('configs/auth', 'ConfigController@auth')->name('configs.auth');
    Route::get('configs/menu', 'ConfigController@menu')->name('configs.menu');
    Route::get('configs/assessment', 'ConfigController@assessment')->name('configs.assessment');
    Route::get('configs/job-filters', 'ConfigController@jobFilters')->name('configs.job-filters');
    Route::get('configs/footer', 'ConfigController@footer')->name('configs.footer');
    Route::get('configs/social', 'ConfigController@social')->name('configs.social');
    Route::get('configs/locale', 'ConfigController@locale')->name('configs.locale');
    Route::get('configs/javascript', 'ConfigController@javascript')->name('configs.javascript');
    Route::get('data-configs', 'ConfigController@dataConfigs')->name('data-configs');
    Route::post('configs/update', 'ConfigController@update')->name('configs.update');
    Route::post('configs/update-file', 'ConfigController@updateFile')->name('configs.update-file');
    Route::post('configs/upload-image', 'ConfigController@uploadImage')->name('configs.upload-image');
});
