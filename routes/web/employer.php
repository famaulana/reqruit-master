<?php

Route::group([
    'prefix' => 'auth',
    'as' => 'auth.',
    'namespace' => 'Http\Controllers\Employer'
], function () {
    Route::get('employer/login', 'Auth\LoginController@showLoginForm')->name('employer.login.form');
    Route::post('employer/login', 'Auth\LoginController@login')->name('employer.login.post');
    Route::post('employer/logout', 'Auth\LoginController@logout')->name('employer.logout');

    Route::get('employer/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('employer.password.request');
    Route::post('employer/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('employer.password.email');
    Route::get('employer/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('employer.password.reset');
    Route::post('employer/password/update', 'Auth\ResetPasswordController@reset')->name('employer.password.update');

    Route::get('employer/register', 'Auth\RegisterController@showRegistrationForm')->name('employer.register.form');
    Route::post('employer/register', 'Auth\RegisterController@register')->name('employer.register.post');
    Route::get('employer/account/verify/{token}', 'Auth\VerificationController@verify')->name('employer.verify');
    Route::get('employer/account/send-verification', 'Auth\VerificationController@resend')->name('employer.resend-verification');
});

Route::group([
    'prefix' => 'employer',
    'as' => 'account-empl.',
    'middleware' => ['verified-employer'],
    'guard' => 'employer',
    'namespace' => 'Http\Controllers\Employer'
], function () {
    // Dashboard
    Route::get('dashboard', 'AccountController@dashboard')->name('dashboard');
    Route::get('recruitment-type', 'AccountController@recruitmentType')->name('recruitment-type');
    Route::post('self-recruitment', 'AccountController@selfRecruitment')->name('self-recruitment');
    Route::get('assist-recruitment', 'AccountController@assistRecruitment')->name('assist-recruitment');
    Route::post('assist-recruitment', 'AccountController@assistRecruitmentPost')->name('assist-recruitment.post');

    // Edit account
    Route::get('edit-account', 'AccountController@editAccount')->name('edit-account');
    Route::post('update-account', 'AccountController@updateAccount')->name('update-account');
    Route::post('update-avatar', 'AccountController@updateAvatar')->name('update-avatar');
    Route::post('update-password', 'AccountController@updatePassword')->name('update-password');

    // Company
    Route::get('company', 'CompanyController@index')->name('company.index');
    Route::post('company', 'CompanyController@store')->name('company.store');
    Route::patch('company/{company}', 'CompanyController@update')->name('company.update');
    Route::post('company/{company}/update-banner', 'CompanyController@updateBanner')->name('update-banner');
    Route::post('company/{company}/update-logo', 'CompanyController@updateLogo')->name('update-logo');
    Route::post('company/{company}/add-image', 'CompanyController@addImageNewCompany')->name('add-image');
    Route::post('company/{company}/update-image', 'CompanyController@updateImage')->name('update-image');
    Route::delete('company/{company}/remove-image/{index}', 'CompanyController@removeImage')->name('remove-image');

    // Company team
    Route::get('team', 'TeamController@index')->name('team.index');
    Route::post('team/invite', 'TeamController@invite')->name('team.invite');
    Route::post('team/{employer}/role-update', 'TeamController@roleUpdate')->name('team.role-update');

    // Work job
    Route::resource('workjob', 'WorkjobController');
    Route::post('workjob/search', 'WorkjobController@search')->name('workjob.search');
    Route::get('workjob/{workjob}/job-applications', 'WorkjobController@jobApplications')->name('workjob.job-applications');

    // Job application
    Route::get('job-applications', 'JobApplicationController@index')->name('job-applications.index');
    Route::get('job-applications/{jobApplication}/profile', 'JobApplicationController@profile')->name('job-applications.profile');
    Route::post('job-applications/{jobApplication}/status-update', 'JobApplicationController@statusUpdate')->name('job-applications.status-update');
});
