<?php

Route::group(['guard' => 'user', 'namespace' => 'Http\Controllers\Common'], function () {
    // Social Auth
    Route::get('auth/{service}/login', 'AuthSocialController@login')->name('auth.social.login');
    Route::get('auth/{service}/callback', 'AuthSocialController@callback')->name('auth.social.callback');

    // // Search
    // Route::get('explore-jobs', 'ExploreJobController')->name('explore-jobs');

    // // Job
    // Route::get('job/{slug}/{identifier}', 'WorkjobController@index')->name('workjob.index');
    // Route::post('job/{slug}/{identifier}/apply', 'WorkjobController@apply')->name('workjob.apply');
    // Route::get('job/{slug}/{identifier}/questions', 'WorkjobController@questions')->name('workjob.questions');
    // Route::post('job/{slug}/{identifier}/answers', 'WorkjobController@answers')->name('workjob.answers');
    // Route::post('job/{slug}/{identifier}/cancel-application', 'WorkjobController@cancelApplication')->name('workjob.cancel-application');

    // // Company
    // Route::get('company/{slug}', 'CompanyController@index')->name('company.index');
    // Route::post('company/search', 'CompanyController@search')->name('company.search');

    // // Page
    // Route::get('page/{slug}', 'PageController')->name('page');

    // // Job Function
    // Route::post('jobfunction/search', 'JobFunctionController@search')->name('jobfunction.search');

    // // Skill
    // Route::post('skill/search', 'SkillController@search')->name('skill.search');

    // // City
    // Route::post('city/search', 'CityController@search')->name('city.search');

    // // Profile
    // Route::get('profile/{identifier}', 'ProfileController')->name('profile');
});

// Route::get('test-email-template', function () {
//     $jobApplication = JobApplication::find(1);
//     $emailTemplate = JobApplicationService::interviewEmailTemplate();


//     return(new JobApplicationUpdate($jobApplication, $emailTemplate))->toMail($jobApplication->user);
// });
