<?php

Route::group(['as' => 'api.', 'namespace' => 'Http\Controllers\Api\Common'], function () {
    Route::get('countries', 'AddressController@countries')->name('countries');
    Route::get('provinces', 'AddressController@provinces')->name('provinces');
    Route::get('cities/{province_id}', 'AddressController@cities')->name('cities');
    Route::get('subdistricts/{city_id}', 'AddressController@subdistricts')->name('subdistricts');
    Route::post('cities-search', 'AddressController@searchCities')->name('cities-search');

    // Site content
    Route::get('site-content', 'SiteContentController@index')->name('site-content.index');

    // Homepage
    Route::get('home', 'HomeController')->name('home');

    // Auth
    Route::get('auth-data', 'AuthDataController')->name('auth-data');

    // Explore jobs
    Route::get('explore-jobs', 'ExploreJobController')->name('explore-jobs');

    // Job Function
    Route::get('jobfunctions', 'JobfunctionController@index')->name('jobfunctions.index');
    Route::post('jobfunctions-search', 'JobFunctionController@search')->name('jobfunctions-search');

    // Skill
    Route::post('skills-search', 'SkillController@search')->name('skills-search');

    // Job Role
    Route::get('jobroles/{jobfunction_id}', 'JobroleController@jobroles')->name('jobroles');

    // Company
    Route::get('company/{slug}', 'CompanyController@index')->name('company.index');
    Route::get('company/{slug}/reviews', 'CompanyController@reviews')->name('company.reviews');
    Route::post('company/{slug}/review', 'CompanyController@submitReview')->name('company.submit-review');
    Route::get('company/{slug}/jobs', 'CompanyController@jobs')->name('company.jobs');
    Route::post('companies-search', 'CompanyController@search')->name('companies-search');

    // Job
    Route::get('job/{slug}/{identifier}', 'WorkjobController@index')->name('workjob.index');
    Route::post('job/{slug}/{identifier}/apply', 'WorkjobController@apply')->name('workjob.apply');
    Route::get('job/{slug}/{identifier}/questions', 'WorkjobController@questions')->name('workjob.questions');
    Route::post('job/{slug}/{identifier}/answers', 'WorkjobController@answers')->name('workjob.answers');
    Route::post('job/{slug}/{identifier}/cancel-application', 'WorkjobController@cancelApplication')->name('workjob.cancel-application');
    Route::get('job/{slug}/{identifier}/applied-status', 'WorkjobController@appliedStatus')->name('workjob.applied-status');
    Route::get('job/search', 'WorkjobController@search')->name('workjob.search');
    // Profile
    Route::get('profile/{identifier}', 'ProfileController')->name('profile');

    // Page
    Route::get('page/{slug}', 'PageController')->name('page');

    // Article
    Route::get('articles', 'ArticleController@index')->name('article.index');
    Route::get('articles/load', 'ArticleController@load')->name('article.load');
    Route::post('articles-search', 'ArticleController@search')->name('article.search');
    Route::get('article/{slug}', 'ArticleController@article')->name('article');

    // Contact
    Route::post('contact', 'ContactController')->name('contact');
});
