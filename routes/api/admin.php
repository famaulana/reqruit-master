<?php

Route::group([
    'prefix' => 'panel',
    'as' => 'api.auth.',
    'namespace' => 'Http\Controllers\Admin\Api'
], function () {
    // Route::post('login', 'AuthController@login')->name('admin.login');
    // Route::post('send-reset-password-link', 'AuthController@sendResetPasswordLink')->name('admin.send-reset-password-link');
    // Route::post('update-password', 'AuthController@updatePassword')->name('admin.update-password');
});

Route::group([
    'prefix' => 'admin',
    'as' => 'api.admin.',
    'middleware' => ['api', 'multiauth:admin-api'],
    'namespace' => 'Http\Controllers\Admin\Api'
], function () {
    // Route::post('logout', 'AuthController@logout')->name('logout');
    // Route::get('admin-data', 'AuthController@adminData')->name('admin-data');

    // // Category
    // Route::post('/categories/store-content-image', 'CategoryController@storeContentImage')->name('categories.store-content-image');
    // Route::get('/categories/all', 'CategoryController@all')->name('categories.all');
    // Route::post('/categories/bulk-delete-permanent', 'CategoryController@bulkDestroyPermanent')->name('categories.bulk-delete-permanent');
    // Route::get('/categories/{id}/delete-permanent', 'CategoryController@destroyPermanent')->name('categories.delete-permanent');
    // Route::get('/categories/{id}/restore', 'CategoryController@restore')->name('categories.restore');
    // Route::get('/categories/trashed', 'CategoryController@trashed')->name('categories.trashed');
    // Route::get('/categories/search', 'CategoryController@search')->name('categories.search');
    // Route::resource('/categories', 'CategoryController');

    // // User
    // Route::post('users/bulk-delete', 'UserController@bulkDestroy')->name('users.bulk-delete');
    // Route::post('users/bulk-delete-permanent', 'UserController@bulkDestroyPermanent')->name('users.bulk-delete-permanent');
    // Route::get('users/{id}/delete-permanent', 'UserController@destroyPermanent')->name('users.delete-permanent');
    // Route::get('users/{id}/restore', 'UserController@restore')->name('users.restore');
    // Route::get('users/trashed', 'UserController@trashed')->name('users.trashed');
    // Route::resource('users', 'UserController');

    // // Admins
    Route::get('admins/{id}/avatar', 'AdminController@avatar')->name('admins.avatar');
    // Route::post('admins/{id}/update-password', 'AdminController@updatePassword')->name('admins.update-password');
    // Route::post('admins/bulk-delete', 'AdminController@bulkDestroy')->name('admins.bulk-delete');
    // Route::post('admins/bulk-delete-permanent', 'AdminController@bulkDestroyPermanent')->name('admins.bulk-delete-permanent');
    // Route::get('admins/{id}/delete-permanent', 'AdminController@destroyPermanent')->name('admins.delete-permanent');
    // Route::get('admins/{id}/restore', 'AdminController@restore')->name('admins.restore');
    // Route::get('admins/trashed', 'AdminController@trashed')->name('admins.trashed');
    // Route::resource('admins', 'AdminController');

    // //Config
    // Route::get('data-configs', 'ConfigController@dataConfigs')->name('data-configs');
    // Route::post('configs/update', 'ConfigController@update')->name('configs.update');
});
