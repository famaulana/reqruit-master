<?php

Route::group(['as' => 'api.user.', 'namespace' => 'Http\Controllers\Api\User'], function () {
    // Auth
    Route::get('available-social-login', 'AuthController@availableSocialLogin')->name('available-social-login');
    Route::post('register', 'AuthController@register')->name('register');
    Route::post('login', 'AuthController@login')->name('login');
    Route::post('send-reset-password-link', 'AuthController@sendResetPasswordLink')->name('send-reset-password-link');
    Route::post('update-password', 'AuthController@updatePassword')->name('update-password');
    Route::post('verify-email', 'AuthController@verifyEmail')->name('verify-email');
});

Route::group([
    'as' => 'api.user.',
    'middleware' => ['api', 'multiauth:user'],
    'guard' => 'user',
    'namespace' => 'Http\Controllers\Api\User'
], function () {
    // Auth
    Route::post('logout', 'AuthController@logout')->name('logout');
    Route::get('user-data', 'AuthController@userData')->name('user-data');

    Route::group(['prefix' => 'account'], function () {
        // Job Applications
        Route::get('job-applications-status/{status}', 'JobApplicationController@jobApplicationsByStatus')->name('job-applications.by-status');
        Route::post('job-applications/interview-action', 'JobApplicationController@interviewAction')->name('job-applications.interview-action');
        Route::get('job-applications', 'JobApplicationController@index')->name('job-applications.index');

        // User
        Route::post('update-password', 'UserController@updatePassword')->name('update-password');

        // Profile
        Route::get('profile', 'UserController@profile')->name('profile');
        Route::get('profile-completion', 'UserController@profileCompletion')->name('profile-completion');
        Route::post('update-avatar', 'UserController@updateAvatar')->name('update-avatar');
        Route::post('update-basic-info', 'UserController@updateBasicInfo')->name('update-basic-info');
        Route::post('update-about', 'UserController@updateAbout')->name('update-about');
        Route::post('update-skills', 'UserController@updateSkills')->name('update-skills');
        Route::post('update-interest', 'UserController@updateInterest')->name('update-interest');
        Route::post('upload-resume', 'UserController@uploadResume')->name('upload-resume');
        Route::post('update-portofolio', 'UserController@uploadPortofolio')->name('upload-portofolio');
        Route::post('upload-video', 'UserController@uploadVideo')->name('upload-video');
        
        // Experience
        Route::post('experiences', 'UserController@addExperience')->name('add-experience');
        Route::patch('experiences/{id}', 'UserController@updateExperience')->name('update-experience');
        Route::delete('experiences/{id}', 'UserController@deleteExperience')->name('delete-experience');

        // Organization experience
        Route::post('organization-experiences', 'UserController@addOrganizationExperience')->name('add-organization-experience');
        Route::patch('organization-experiences/{id}', 'UserController@updateOrganizationExperience')->name('update-organization-experience');
        Route::delete('organization-experiences/{id}', 'UserController@deleteOrganizationExperience')->name('delete-organization-experience');

        // Education
        Route::post('educations', 'UserController@addEducation')->name('add-education');
        Route::patch('educations/{id}', 'UserController@updateEducation')->name('update-education');
        Route::delete('educations/{id}', 'UserController@deleteEducation')->name('delete-education');

        // Award
        Route::post('awards', 'UserController@addAward')->name('add-award');
        Route::patch('awards/{id}', 'UserController@updateAward')->name('update-award');
        Route::delete('awards/{id}', 'UserController@deleteAward')->name('delete-award');

        // Bookmark
        Route::resource('bookmarks', 'BookmarkController')->only(['index', 'store', 'destroy']);

        // Notifications
        Route::get('notifications', 'NotificationController@index')->name('notifications.index');
        Route::get('notifications/{id}/read', 'NotificationController@read')->name('notifications.read');
        Route::get('notifications/mark-all-read', 'NotificationController@markAllRead')->name('notifications.mark-all-read');

        // English Test
        Route::get('assessment/english', 'EnglishAssessmentController@index')->name('assessment.english.index');
        Route::post('assessment/english', 'EnglishAssessmentController@postAnswer')->name('assessment.english.post-answer');

        // Personality
        Route::get('assessment/personality', 'PersonalityTestController@index')->name('assessment.personality.index');
        Route::post('assessment/personality', 'PersonalityTestController@postStatement')->name('assessment.personality.post-statement');

        // Company Review
        Route::post('write-review/company-review', 'WriteReviewController@postCompanyReview')->name('write-review.post-company-review');
        Route::post('write-review/salary-review', 'WriteReviewController@postSalaryReview')->name('write-review.post-salary-review');
        Route::post('write-review/interview-review', 'WriteReviewController@postInterviewReview')->name('write-review.post-interview-review');
        Route::post('write-review/benefits-review', 'WriteReviewController@postBenefitsReview')->name('write-review.post-benefits-review');
        Route::post('write-review/workplace-photos/add-photo', 'WriteReviewController@addPhoto')->name('write-review.add-photo');
        Route::delete('write-review/workplace-photos/remove-photo/{fileName}', 'WriteReviewController@removePhoto')->name('write-review.remove-photo');
        Route::post('write-review/workplace-photos', 'WriteReviewController@postWorkplacePhotos')->name('write-review.post-workplace-photos');
        Route::get('company/{company}/my-review', 'CompanyController@myReview')->name('company.my-review');
    });
});
