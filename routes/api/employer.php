<?php

Route::group([
    'prefix' => 'employer',
    'as' => 'api.employer.',
    'namespace' => 'Http\Controllers\Api\Employer'
], function () {
    // Auth
    Route::post('register', 'AuthController@register')->name('register');
    Route::post('login', 'AuthController@login')->name('login');
    Route::post('send-reset-password-link', 'AuthController@sendResetPasswordLink')->name('send-reset-password-link');
    Route::post('update-password', 'AuthController@updatePassword')->name('update-password');
    Route::post('verify-email', 'AuthController@verifyEmail')->name('verify-email');

    Route::get('recruitment-type', 'RecruitmentTypeController@index')->name('recruitment-type');

    // Head hunting
    Route::get('head-hunting', 'HeadHuntingController@index')->name('head-hunting.index');
    Route::post('head-hunting', 'HeadHuntingController@post')->name('head-hunting.post');
});

Route::group([
    'prefix' => 'employer',
    'as' => 'api.employer.',
    'middleware' => ['api', 'multiauth:employer'],
    'guard' => 'employer',
    'namespace' => 'Http\Controllers\Api\Employer'
], function () {
    // Auth
    Route::post('logout', 'AuthController@logout')->name('logout');
    Route::get('employer-data', 'AuthController@employerData')->name('employer-data');

    // Edit account
    Route::get('edit-account', 'AccountController@editAccount')->name('edit-account');
    Route::post('update-account', 'AccountController@updateAccount')->name('update-account');
    Route::post('update-avatar', 'AccountController@updateAvatar')->name('update-avatar');
    Route::post('update-account-password', 'AccountController@updatePassword')->name('update-account-password');

    // Dashboard
    Route::get('dashboard', 'AccountController@dashboard')->name('dashboard');

    // Company
    Route::get('company', 'CompanyController@index')->name('company.index');
    Route::post('company', 'CompanyController@store')->name('company.store');
    Route::patch('company/{company}', 'CompanyController@update')->name('company.update');
    Route::post('company/{company}/update-banner', 'CompanyController@updateBanner')->name('update-banner');
    Route::post('company/add-logo', 'CompanyController@addLogo')->name('add-logo');
    Route::post('company/{company}/update-logo', 'CompanyController@updateLogo')->name('update-logo');
    Route::post('company/{company}/add-image', 'CompanyController@addImageNewCompany')->name('add-image');
    Route::post('company/{company}/update-image', 'CompanyController@updateImage')->name('update-image');
    Route::delete('company/{company}/remove-image/{index}', 'CompanyController@removeImage')->name('remove-image');

    // Company review
    Route::post('company-reviews/{companyReview}/status-update', 'CompanyReviewController@statusUpdate')->name('company-reviews.status-update');
    Route::resource('company-reviews', 'CompanyReviewController')->only(['index', 'destroy']);

    // Company team
    Route::get('team', 'TeamController@index')->name('team.index');
    Route::post('team/invite', 'TeamController@invite')->name('team.invite');
    Route::post('team/{employer}/role-update', 'TeamController@roleUpdate')->name('team.role-update');

    // Work job
    Route::post('workjob/search', 'WorkjobController@search')->name('workjob.search');
    Route::get('workjob/{workjob}/job-applications', 'WorkjobController@jobApplications')->name('workjob.job-applications');
    Route::resource('workjob', 'WorkjobController');

    // Job application
    Route::get('job-applications', 'JobApplicationController@index')->name('job-applications.index');
    Route::get('job-applications/{jobApplication}/profile', 'JobApplicationController@profile')->name('job-applications.profile');
    Route::post('job-applications/{jobApplication}/status-update', 'JobApplicationController@statusUpdate')->name('job-applications.status-update');
});
