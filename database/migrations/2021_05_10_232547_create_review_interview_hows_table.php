<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewInterviewHowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('review_interview_hows', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('company_review_id');
            $table->string('how_to_get_interview'); // online, university, referral, in-person, recruiter, staffing-agency, other
            $table->integer('how_long_number');
            $table->string('how_long_unit'); // days, weeks, months
            $table->dateTime('interview_time');
            $table->string('location');
            $table->string('type'); // phone-screen, 1-on-1, group-panel,
            $table->string('testing'); // skills-review, personality-quiz, drug-test, iq-test 
            $table->string('other'); // background-check, presentation, other 
            $table->string('how_helpful'); // very-helpful, helpful, not-helpful , not-use
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('review_interview_hows');
    }
}
