<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('identifier')->index()->nullable();
            $table->string('name')->index();
            $table->string('email')->unique();
            $table->string('phone')->unique()->nullable();
            $table->string('photo')->nullable();
            $table->string('gender')->nullable();
            $table->date('dob')->nullable();
            $table->text('address')->nullable();
            $table->unsignedInteger('country_id')->nullable();
            $table->unsignedInteger('province_id')->nullable();
            $table->string('province_name')->nullable();
            $table->unsignedInteger('city_id')->nullable();
            $table->string('city_name')->nullable();
            $table->unsignedInteger('subdistrict_id')->nullable();
            $table->string('pos_code')->nullable();
            $table->string('resume')->nullable();
            $table->string('video')->nullable();
            $table->string('job_status')->nullable();
            $table->string('website')->nullable();
            $table->text('about')->nullable();
            $table->string('relationship')->nullable();
            $table->string('hobby')->nullable();
            $table->string('residence_status')->nullable();
            $table->unsignedInteger('citizenship')->nullable();
            $table->string('when_can_start')->nullable();
            $table->date('can_start_date')->nullable();
            $table->string('yt_video')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('instagram')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('behance')->nullable();
            $table->string('github')->nullable();
            $table->string('codepen')->nullable();
            $table->string('vimeo')->nullable();
            $table->string('youtube')->nullable();
            $table->string('dribbble')->nullable();
            $table->string('verification_token')->nullable();
            $table->boolean('verified')->default(0);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('country_id')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('city_id')->references('id')->on('cities')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('subdistrict_id')->references('id')->on('subdistricts')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('citizenship')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
