<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCompanyReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_reviews', function(Blueprint $table) {
            $table->dropColumn('type');
            $table->dropColumn('pros');
            $table->dropColumn('cons');
            $table->dropColumn('base_salary');
            $table->dropColumn('currency');
            $table->dropColumn('salary_duration');
            $table->dropColumn('get_bonus');
            $table->dropColumn('work_experience');
            $table->dropColumn('location');
            $table->dropColumn('country_id');
            $table->dropColumn('gender');
            $table->dropColumn('overall_exp');
            $table->dropColumn('interview_process');
            $table->dropColumn('interview_difficulty');
            $table->dropColumn('get_offer');
            $table->dropColumn('benefits_review');
            $table->dropColumn('photos');
            $table->renameColumn('advice', 'review');
        });

        Schema::dropIfExists('review_interview_questions');
        Schema::dropIfExists('review_interview_hows');
        Schema::dropIfExists('review_benefits');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('company_reviews', function (Blueprint $table) {
            $table->string('type');
            $table->text('pros')->nullable();
            $table->text('cons')->nullable();
            $table->renameColumn('review', 'advice');
            $table->integer('base_salary')->nullable();
            $table->string('currency')->nullable();
            $table->string('salary_duration')->nullable(); // monthly, yearly, per-project
            $table->boolean('get_bonus')->default(0);
            $table->string('work_experience')->nullable(); // less-then-1-year, 1-3-years, 3-5-years, 5-10-years, 10-years-more, no-preferences
            $table->string('location')->nullable();
            $table->string('country_id')->nullable();
            $table->string('gender')->nullable();
            $table->string('overall_exp')->nullable(); // positive, neutral, negative
            $table->text('interview_process')->nullable();
            $table->string('interview_difficulty')->nullable(); // very-easy, easy, average, difficult, very-difficult
            $table->string('get_offer')->nullable(); // no, yes-declined, yes-accepted
            $table->text('benefits_review')->nullable();
            $table->text('photos')->nullable();
        });

        Schema::create('review_interview_questions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('company_review_id');
            $table->text('question');
            $table->text('answer');
            $table->timestamps();
        });

        Schema::create('review_interview_hows', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('company_review_id');
            $table->string('how_to_get_interview'); // online, university, referral, in-person, recruiter, staffing-agency, other
            $table->integer('how_long_number');
            $table->string('how_long_unit'); // days, weeks, months
            $table->dateTime('interview_time');
            $table->string('location');
            $table->string('type'); // phone-screen, 1-on-1, group-panel,
            $table->string('testing'); // skills-review, personality-quiz, drug-test, iq-test 
            $table->string('other'); // background-check, presentation, other 
            $table->string('how_helpful'); // very-helpful, helpful, not-helpful , not-use
            $table->timestamps();
        });

        Schema::create('review_benefits', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('company_review_id');
            $table->string('employee_discount')->nullable(); // yes, no, unsure
            $table->string('employee_assistance_program')->nullable(); // yes, no, unsure
            $table->string('fertility_assistance')->nullable(); // yes, no, unsure
            $table->string('fsa')->nullable(); // yes, no, unsure
            $table->string('stock_options')->nullable(); // yes, no, unsure
            $table->string('dependent_care')->nullable(); // yes, no, unsure
            $table->string('adoption_assistance')->nullable(); // yes, no, unsure
            $table->string('employee_stock_purchase_plan')->nullable(); // yes, no, unsure
            $table->string('childcare')->nullable(); // yes, no, unsure
            $table->string('job_training')->nullable(); // yes, no, unsure
            $table->string('apprenticeship_program')->nullable(); // yes, no, unsure
            $table->string('professional_development')->nullable(); // yes, no, unsure
            $table->string('performance_bonus')->nullable(); // yes, no, unsure
            $table->string('gym_membership')->nullable(); // yes, no, unsure
            $table->string('hsa')->nullable(); // yes, no, unsure
            $table->string('family_medical_leave')->nullable(); // yes, no, unsure
            $table->string('military_leave')->nullable(); // yes, no, unsure
            $table->string('commuter_checks')->nullable(); // yes, no, unsure
            $table->string('life_assurance')->nullable(); // yes, no, unsure
            $table->string('vision_insurance')->nullable(); // yes, no, unsure
            $table->string('volunteer_time_off')->nullable(); // yes, no, unsure
            $table->string('tuition_assistance')->nullable(); // yes, no, unsure
            $table->string('supplemental_workers_compensation')->nullable(); // yes, no, unsure
            $table->string('healt_care_on_site')->nullable(); // yes, no, unsure
            $table->string('mobile_phone_discount')->nullable(); // yes, no, unsure
            $table->string('company_social_events')->nullable(); // yes, no, unsure
            $table->string('retiree_health')->nullable(); // yes, no, unsure
            $table->string('legal_assistance')->nullable(); // yes, no, unsure
            $table->string('occupational_accident_insurance')->nullable(); // yes, no, unsure
            $table->string('charitable_gift_matching')->nullable(); // yes, no, unsure
            $table->string('supplemntal_life_insurance')->nullable(); // yes, no, unsure
            $table->string('unpaid_extended_leave')->nullable(); // yes, no, unsure
            $table->string('pet_friendly_workplace')->nullable(); // yes, no, unsure
            $table->string('equity_incentive_plan')->nullable(); // yes, no, unsure
            $table->string('travel_concierge')->nullable(); // yes, no, unsure
            $table->string('disability_insurance')->nullable(); // yes, no, unsure
            $table->string('sabbatical')->nullable(); // yes, no, unsure
            $table->string('company_car')->nullable(); // yes, no, unsure
            $table->string('mental_health_care')->nullable(); // yes, no, unsure
            $table->string('bereavement_leave')->nullable(); // yes, no, unsure
            $table->string('accidental_death_and_dismemberment_insurance')->nullable(); // yes, no, unsure
            $table->string('pension_plan')->nullable(); // yes, no, unsure
            $table->string('maternity_and_paternity_leave')->nullable(); // yes, no, unsure
            $table->string('work_from_home')->nullable(); // yes, no, unsure
            $table->string('health_insurance')->nullable(); // yes, no, unsure
            $table->string('four_zero_one_plan')->nullable(); // yes, no, unsure
            $table->string('vacation_and_paid_time_off')->nullable(); // yes, no, unsure
            $table->string('retirement_plan')->nullable(); // yes, no, unsure
            $table->string('free_lunch')->nullable(); // yes, no, unsure
            $table->string('sick_days')->nullable(); // yes, no, unsure
            $table->string('diversity_program')->nullable(); // yes, no, unsure
            $table->string('dental_insurance')->nullable(); // yes, no, unsure
            $table->string('paid_holidays')->nullable(); // yes, no, unsure
            $table->string('reduced_or_flexible_hours')->nullable(); // yes, no, unsure
            $table->timestamps();
        });
    }
}
