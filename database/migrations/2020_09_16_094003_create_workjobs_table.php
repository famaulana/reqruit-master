<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkjobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workjobs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('edited_by');
            $table->unsignedBigInteger('company_id');
            $table->string('identifier')->unique();
            $table->string('title');
            $table->string('slug');
            $table->string('type'); // full-time, part-time, internship, freelance
            $table->unsignedInteger('country_id');
            $table->unsignedInteger('province_id');
            $table->unsignedInteger('city_id')->nullable();
            $table->string('city_name')->nullable();
            $table->integer('number_of_vacancies');
            $table->unsignedBigInteger('jobfunction_id');
            $table->string('work_experience'); // less-then-1-year, 1-3-years, 3-5-years, 5-10-years, 10-years-more, no-preferences
            $table->string('salary_currency');
            $table->integer('salary_min')->nullable();
            $table->integer('salary_max')->nullable();
            $table->string('salary_duration'); // monthly, yearly, per-project
            $table->boolean('bonus_salary');
            $table->integer('bonus_salary_min')->nullable();
            $table->integer('bonus_salary_max')->nullable();
            $table->string('bonus_salary_duration'); // monthly, yearly, per-project
            $table->longText('description');
            $table->boolean('has_question');
            $table->string('attachment')->nullable();
            $table->string('job_posting_status')->default('open'); // open, closed
            $table->string('status')->default('draft'); // draft, published
            $table->boolean('hide_company')->default(0);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('created_by')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('edited_by')->references('id')->on('employers')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('company_id')->references('id')->on('companies')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('country_id')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('city_id')->references('id')->on('cities')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('jobfunction_id')->references('id')->on('jobfunctions')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workjobs');
    }
}
