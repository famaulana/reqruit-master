<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewBenefitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('review_benefits', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('company_review_id');
            $table->string('employee_discount')->nullable(); // yes, no, unsure
            $table->string('employee_assistance_program')->nullable(); // yes, no, unsure
            $table->string('fertility_assistance')->nullable(); // yes, no, unsure
            $table->string('fsa')->nullable(); // yes, no, unsure
            $table->string('stock_options')->nullable(); // yes, no, unsure
            $table->string('dependent_care')->nullable(); // yes, no, unsure
            $table->string('adoption_assistance')->nullable(); // yes, no, unsure
            $table->string('employee_stock_purchase_plan')->nullable(); // yes, no, unsure
            $table->string('childcare')->nullable(); // yes, no, unsure
            $table->string('job_training')->nullable(); // yes, no, unsure
            $table->string('apprenticeship_program')->nullable(); // yes, no, unsure
            $table->string('professional_development')->nullable(); // yes, no, unsure
            $table->string('performance_bonus')->nullable(); // yes, no, unsure
            $table->string('gym_membership')->nullable(); // yes, no, unsure
            $table->string('hsa')->nullable(); // yes, no, unsure
            $table->string('family_medical_leave')->nullable(); // yes, no, unsure
            $table->string('military_leave')->nullable(); // yes, no, unsure
            $table->string('commuter_checks')->nullable(); // yes, no, unsure
            $table->string('life_assurance')->nullable(); // yes, no, unsure
            $table->string('vision_insurance')->nullable(); // yes, no, unsure
            $table->string('volunteer_time_off')->nullable(); // yes, no, unsure
            $table->string('tuition_assistance')->nullable(); // yes, no, unsure
            $table->string('supplemental_workers_compensation')->nullable(); // yes, no, unsure
            $table->string('healt_care_on_site')->nullable(); // yes, no, unsure
            $table->string('mobile_phone_discount')->nullable(); // yes, no, unsure
            $table->string('company_social_events')->nullable(); // yes, no, unsure
            $table->string('retiree_health')->nullable(); // yes, no, unsure
            $table->string('legal_assistance')->nullable(); // yes, no, unsure
            $table->string('occupational_accident_insurance')->nullable(); // yes, no, unsure
            $table->string('charitable_gift_matching')->nullable(); // yes, no, unsure
            $table->string('supplemntal_life_insurance')->nullable(); // yes, no, unsure
            $table->string('unpaid_extended_leave')->nullable(); // yes, no, unsure
            $table->string('pet_friendly_workplace')->nullable(); // yes, no, unsure
            $table->string('equity_incentive_plan')->nullable(); // yes, no, unsure
            $table->string('travel_concierge')->nullable(); // yes, no, unsure
            $table->string('disability_insurance')->nullable(); // yes, no, unsure
            $table->string('sabbatical')->nullable(); // yes, no, unsure
            $table->string('company_car')->nullable(); // yes, no, unsure
            $table->string('mental_health_care')->nullable(); // yes, no, unsure
            $table->string('bereavement_leave')->nullable(); // yes, no, unsure
            $table->string('accidental_death_and_dismemberment_insurance')->nullable(); // yes, no, unsure
            $table->string('pension_plan')->nullable(); // yes, no, unsure
            $table->string('maternity_and_paternity_leave')->nullable(); // yes, no, unsure
            $table->string('work_from_home')->nullable(); // yes, no, unsure
            $table->string('health_insurance')->nullable(); // yes, no, unsure
            $table->string('four_zero_one_plan')->nullable(); // yes, no, unsure
            $table->string('vacation_and_paid_time_off')->nullable(); // yes, no, unsure
            $table->string('retirement_plan')->nullable(); // yes, no, unsure
            $table->string('free_lunch')->nullable(); // yes, no, unsure
            $table->string('sick_days')->nullable(); // yes, no, unsure
            $table->string('diversity_program')->nullable(); // yes, no, unsure
            $table->string('dental_insurance')->nullable(); // yes, no, unsure
            $table->string('paid_holidays')->nullable(); // yes, no, unsure
            $table->string('reduced_or_flexible_hours')->nullable(); // yes, no, unsure
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('review_benefits');
    }
}
