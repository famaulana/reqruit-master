<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_reviews', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('company_id');
            $table->boolean('anonymous')->default(1);
            $table->string('job_status')->nullable(); // current, former
            $table->string('employment_status')->nullable(); // full-time, part-time, internship, freelance
            $table->string('job_title')->nullable();
            $table->integer('rating')->nullable();
            $table->string('review_headline')->nullable();
            $table->text('pros')->nullable();
            $table->text('cons')->nullable();
            $table->text('advice')->nullable();
            $table->integer('base_salary')->nullable();
            $table->string('currency')->nullable();
            $table->string('salary_duration')->nullable(); // monthly, yearly, per-project
            $table->boolean('get_bonus')->default(0);
            $table->string('work_experience')->nullable(); // less-then-1-year, 1-3-years, 3-5-years, 5-10-years, 10-years-more, no-preferences
            $table->string('location')->nullable();
            $table->string('country_id')->nullable();
            $table->string('gender')->nullable();
            $table->string('overall_exp')->nullable(); // positive, neutral, negative
            $table->text('interview_process')->nullable();
            $table->string('interview_difficulty')->nullable(); // very-easy, easy, average, difficult, very-difficult
            $table->string('get_offer')->nullable(); // no, yes-declined, yes-accepted
            $table->text('benefits_review')->nullable();
            $table->text('photos')->nullable();
            $table->boolean('published')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('company_id')->references('id')->on('companies')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_reviews');
    }
}
