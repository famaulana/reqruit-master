<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->char('country_code', 2);
            $table->string('name')->index();
            $table->char('currency_code', 3);
            $table->char('fips_code', 2);
            $table->char('iso_numeric', 4);
            $table->string('north', 30);
            $table->string('south', 30);
            $table->string('east', 30);
            $table->string('west', 30);
            $table->string('capital', 30);
            $table->string('continent_name');
            $table->char('continent', 2);
            $table->string('languages');
            $table->char('iso_alpha_3', 3);
            $table->integer('geo_name_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
