<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEducationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('education', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('institution');
            $table->unsignedInteger('province_id');
            $table->unsignedInteger('city_id');
            $table->unsignedBigInteger('education_degree_id');
            $table->unsignedBigInteger('education_field_id');
            $table->decimal('gpa', 10, 2);
            $table->date('start_time')->nullable();
            $table->date('end_time')->nullable();
            $table->boolean('still_studying')->default(0);
            $table->text('more_info')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('education_degree_id')->references('id')->on('education_degrees')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('education_field_id')->references('id')->on('education_fields')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('education');
    }
}
