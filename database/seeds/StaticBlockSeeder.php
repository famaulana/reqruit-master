<?php

use App\Models\StaticBlock;
use Illuminate\Database\Seeder;

class StaticBlockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // About seeder
        StaticBlock::create([
            'name' => 'About',
            'content' => '<p>Quisque velit nisi, pretium ut lacinia in, elementum id enim. Cras ultricies ligula sed magna dictum porta. Vivamus suscipit tortor eget felis porttitor volutpat. Proin eget tortor risus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus.</p>',
            'active' => 1
        ]);

        // Self recruitment details seeder
        StaticBlock::create([
            'name' => 'Self Recruitment Details',
            'content' => '<ul><li>Cras ultricies ligula sed magna dictum porta.</li><li>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi.</li><li>Pellentesque in ipsum id orci porta dapibus.</li><li>Vivamus suscipit tortor eget felis porttitor volutpat.</li></ul>',
            'active' => 1
        ]);

        // Assist recruitment details seeder
        StaticBlock::create([
            'name' => 'Head Hunting Details',
            'content' => '<ul><li>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi.</li><li>Quisque velit nisi, pretium ut lacinia in, elementum id enim.</li><li>Donec sollicitudin molestie malesuada.</li><li>Sed porttitor lectus nibh.</li></ul>',
            'active' => 1
        ]);
    }
}
