<?php

use Illuminate\Database\Seeder;

class EnglishQuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\EnglishQuestion::class, 20)->create();
    }
}
