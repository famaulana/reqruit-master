<?php

use App\Models\Jobrole;
use App\Models\Skill;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class WorkjobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        factory(App\Models\Workjob::class, 60)->create()->each(function ($job) use ($faker) {
            $jobRoles = Jobrole::where('jobfunction_id', $job->jobfunction_id)
                ->inRandomOrder()
                ->skip(0)
                ->take(rand(2, 5))
                ->pluck('id')
                ->toArray();

            $mustSkills = Skill::inRandomOrder()
                ->skip(0)
                ->take(rand(1, 3))
                ->pluck('id')
                ->toArray();

            $niceSkills = Skill::inRandomOrder()
                ->skip(0)
                ->take(rand(1, 2))
                ->pluck('id')
                ->toArray();

            
            $job->job_roles()->attach($jobRoles);
            $job->must_skills()->attach($mustSkills);
            $job->nice_skills()->attach($niceSkills);

            if ($job->has_question) {
                $questionCount = rand(1, 5);

                for ($i=0; $i < $questionCount; $i++) {
                    $job->job_questions()->create([
                        'question' => $faker->sentence(rand(5, 10)),
                        'sort_order' => $i
                    ]);
                }
            }
        });
    }
}
