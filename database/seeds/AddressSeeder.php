<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Countries
        $country = __DIR__ . '/address/countries.sql';
        DB::unprepared(file_get_contents($country));
        $this->command->info('Countries table seeded!');
        
        // Province
        $province = __DIR__ . '/address/provinces.sql';
        DB::unprepared(file_get_contents($province));
        $this->command->info('Province table seeded!');

        // City
        $city = __DIR__ . '/address/cities.sql';
        DB::unprepared(file_get_contents($city));
        $this->command->info('City table seeded!');

        // Subdistrict
        $subdistrict = __DIR__ . '/address/subdistricts.sql';
        DB::unprepared(file_get_contents($subdistrict));
        $this->command->info('Subdistrict table seeded!');
    }
}
