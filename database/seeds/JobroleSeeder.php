<?php

use Illuminate\Database\Seeder;

class JobroleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Jobrole::class, 75)->create();
    }
}
