<?php

use App\Helpers\PathHelper;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Article::class, 10)->create()->each(function ($article) {
            // Create path if not exists
            $path = 'public/' . PathHelper::article_featured_image_path($article->id);
            $imgPath = str_replace('/', DIRECTORY_SEPARATOR, $path);
            if (!Storage::exists($imgPath)) {
                Storage::makeDirectory($imgPath, 0775, true);
            }

            // Saving article featured image
            $ImageNumber = rand(1, 3);
            $fileName = 'image-' . $ImageNumber . '.png';
            $newImage = Image::make(storage_path('faker-images' . DIRECTORY_SEPARATOR . 'article' . DIRECTORY_SEPARATOR . $fileName));
            $newImage->save(storage_path('app' . DIRECTORY_SEPARATOR . $imgPath . $fileName));
            $article->featured_image = $fileName;
            $article->save();
        });
    }
}
