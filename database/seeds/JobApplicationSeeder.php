<?php

use App\Models\User;
use App\Models\Employer;
use App\Services\UserService;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class JobApplicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $employer = Employer::first();
        $company = $employer->company;
        $job = $company->work_jobs()->first();
        $users = User::take(20)->get();
        
        foreach ($users as $user) {
            if (!$user->job_applications()->where('workjob_id', $job->id)->exists()) {
                $haveCoverLetter = rand(0, 1);
                $coverLetter = $haveCoverLetter === 1 ? $faker->sentences(rand(4, 10), true) : '';
                $answers = [];

                if ($job->has_question && $job->job_questions()->exists()) {
                    foreach ($job->job_questions as $q) {
                        $answers[] = [
                            'question' => $q->question,
                            'answer' => $faker->sentences(rand(1, 3), true),
                        ];
                    }
                }

                $service = new UserService();
                $service->applyJob($user, $job->id, $answers, $coverLetter);
            }
        }
    }
}
