<?php

use App\Models\Category;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        // Fashion
        $rawParagraphs = $faker->paragraphs(4, false);
        $paragraphs = '';
        foreach ($rawParagraphs as $rawParagraph) {
            $paragraphs .= '<p>' . $rawParagraph . '</p>';
        }
        $fashion = Category::create([
            'name' => 'Fashion',
            'slug' => 'fashion',
            'page_title' => 'Fashion',
            'content' => $paragraphs,
            'meta_title' => 'Fashion',
            'meta_description' => $faker->sentence(7),
        ]);

        // Gadget
        $rawParagraphs = $faker->paragraphs(4, false);
        $paragraphs = '';
        foreach ($rawParagraphs as $rawParagraph) {
            $paragraphs .= '<p>' . $rawParagraph . '</p>';
        }
        $gadget = Category::create([
            'name' => 'Gadget',
            'slug' => 'gadget',
            'page_title' => 'Gadget',
            'content' => $paragraphs,
            'meta_title' => 'Gadget',
            'meta_description' => $faker->sentence(7),
        ]);

        // Elektronik
        $rawParagraphs = $faker->paragraphs(4, false);
        $paragraphs = '';
        foreach ($rawParagraphs as $rawParagraph) {
            $paragraphs .= '<p>' . $rawParagraph . '</p>';
        }
        $elektronik = Category::create([
            'name' => 'Elektronik',
            'slug' => 'elektronik',
            'page_title' => 'Elektronik',
            'content' => $paragraphs,
            'meta_title' => 'Elektronik',
            'meta_description' => $faker->sentence(7),
        ]);

        // Rumah Tangga
        $rawParagraphs = $faker->paragraphs(4, false);
        $paragraphs = '';
        foreach ($rawParagraphs as $rawParagraph) {
            $paragraphs .= '<p>' . $rawParagraph . '</p>';
        }
        $rumahTangga = Category::create([
            'name' => 'Rumah Tangga',
            'slug' => 'rumah-tangga',
            'page_title' => 'Rumah Tangga',
            'content' => $paragraphs,
            'meta_title' => 'Rumah Tangga',
            'meta_description' => $faker->sentence(7),
        ]);
    }
}
