<?php

use App\Models\Admin;
use App\Models\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Admin::create([
            'name' => 'Admin',
            'email' => 'admin@example.com',
            'password' => Hash::make('secret'),
            'primary' => 1,
            'verified' => 1,
            'email_verified_at' => date('Y-m-d H:i:s')
        ]);

        $permissions = Permission::all()->pluck('id')->toArray();

        $admin->permissions()->sync($permissions);
    }
}
