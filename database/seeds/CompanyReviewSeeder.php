<?php

use Illuminate\Database\Seeder;

class CompanyReviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\CompanyReview::class, 50)->create();
    }
}
