<?php

use Illuminate\Database\Seeder;

class JobfunctionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Jobfunction::class, 15)->create();
    }
}
