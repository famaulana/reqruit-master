<?php

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Permission Manage category
        $manage_category = new Permission();
        $manage_category->name = 'manage-category';
        $manage_category->display_name ='Manage category';
        $manage_category->save();

        // Permission Manage user
        $manage_user = new Permission();
        $manage_user->name = 'manage-user';
        $manage_user->display_name ='Manage user';
        $manage_user->save();

        // Permission Manage education degree
        $manage_education_degree = new Permission();
        $manage_education_degree->name = 'manage-education-degree';
        $manage_education_degree->display_name ='Manage education degree';
        $manage_education_degree->save();

        // Permission Manage education field
        $manage_education_field = new Permission();
        $manage_education_field->name = 'manage-education-field';
        $manage_education_field->display_name ='Manage education field';
        $manage_education_field->save();

        // Permission Manage question
        $manage_question = new Permission();
        $manage_question->name = 'manage-question';
        $manage_question->display_name ='Manage question';
        $manage_question->save();

        // Permission Manage employer
        $manage_employer = new Permission();
        $manage_employer->name = 'manage-employer';
        $manage_employer->display_name ='Manage employer';
        $manage_employer->save();

        // Permission Manage head hunting
        $manage_head_hunting = new Permission();
        $manage_head_hunting->name = 'manage-head-hunting';
        $manage_head_hunting->display_name ='Manage head hunting';
        $manage_head_hunting->save();

        // Permission Manage company
        $manage_company = new Permission();
        $manage_company->name = 'manage-company';
        $manage_company->display_name ='Manage company';
        $manage_company->save();

        // Permission Manage company review
        $manage_company_review = new Permission();
        $manage_company_review->name = 'manage-company-review';
        $manage_company_review->display_name ='Manage company review';
        $manage_company_review->save();

        // Permission Manage industry
        $manage_industry = new Permission();
        $manage_industry->name = 'manage-industry';
        $manage_industry->display_name ='Manage industry';
        $manage_industry->save();

        // Permission Manage job
        $manage_job = new Permission();
        $manage_job->name = 'manage-job';
        $manage_job->display_name ='Manage job';
        $manage_job->save();

        // Permission Manage job application
        $manage_job_application = new Permission();
        $manage_job_application->name = 'manage-job-application';
        $manage_job_application->display_name ='Manage job application';
        $manage_job_application->save();

        // Permission Manage skill
        $manage_skill = new Permission();
        $manage_skill->name = 'manage-skill';
        $manage_skill->display_name ='Manage skill';
        $manage_skill->save();

        // Permission Manage job function
        $manage_job_function = new Permission();
        $manage_job_function->name = 'manage-job-function';
        $manage_job_function->display_name ='Manage job function';
        $manage_job_function->save();

        // Permission Manage job role
        $manage_job_role = new Permission();
        $manage_job_role->name = 'manage-job-role';
        $manage_job_role->display_name ='Manage job role';
        $manage_job_role->save();

        // Permission Manage testimonial
        $manage_testimonial = new Permission();
        $manage_testimonial->name = 'manage-testimonial';
        $manage_testimonial->display_name ='Manage testimonial';
        $manage_testimonial->save();

        // Permission Manage article
        $manage_article = new Permission();
        $manage_article->name = 'manage-article';
        $manage_article->display_name ='Manage article';
        $manage_article->save();

        // Permission Manage admin
        $manage_admin = new Permission();
        $manage_admin->name = 'manage-admin';
        $manage_admin->display_name ='Manage admin';
        $manage_admin->save();

        // Permission Manage page
        $manage_page = new Permission();
        $manage_page->name = 'manage-page';
        $manage_page->display_name ='Manage page';
        $manage_page->save();

        // Permission Manage banner
        $manage_banner = new Permission();
        $manage_banner->name = 'manage-banner';
        $manage_banner->display_name ='Manage banner';
        $manage_banner->save();

        // Permission Manage static block
        $manage_static_block = new Permission();
        $manage_static_block->name = 'manage-static-block';
        $manage_static_block->display_name ='Manage static block';
        $manage_static_block->save();

        // Permission Manage menu
        $manage_menu = new Permission();
        $manage_menu->name = 'manage-menu';
        $manage_menu->display_name ='Manage menu';
        $manage_menu->save();

        // Permission Manage config
        $manage_config = new Permission();
        $manage_config->name = 'manage-config';
        $manage_config->display_name ='Manage config';
        $manage_config->save();
    }
}
