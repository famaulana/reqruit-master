<?php

use App\Helpers\PathHelper;
use Illuminate\Database\Seeder;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class TestimonialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Testimonial::class, 15)->create()->each(function ($testimonial) {
            // Create path if not exists
            $path = 'public/' . PathHelper::testimonial_avatar_path($testimonial->id);
            $imgPath = str_replace('/', DIRECTORY_SEPARATOR, $path);
            if (!Storage::exists($imgPath)) {
                Storage::makeDirectory($imgPath, 0775, true);
            }

            // Saving avatar logo
            $imageNumber = rand(1, 8);
            $fileName = 'avatar-' . $imageNumber . '.jpg';
            $newAvatar = Image::make(storage_path('faker-images' . DIRECTORY_SEPARATOR . 'avatar' . DIRECTORY_SEPARATOR . $fileName));
            $newAvatar->save(storage_path('app' . DIRECTORY_SEPARATOR . $imgPath . $fileName));

            $testimonial->avatar = $fileName;
            $testimonial->save();
        });
    }
}
