<?php

use App\Models\PersonalAnalysisStatement;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AddressSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(IndustrySeeder::class);
        $this->call(EmployerSeeder::class);
        $this->call(PageSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(EducationDegreeSeeder::class);
        $this->call(EducationFieldSeeder::class);
        $this->call(SkillSeeder::class);
        $this->call(JobfunctionSeeder::class);
        $this->call(JobroleSeeder::class);
        $this->call(WorkjobSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(JobApplicationSeeder::class);
        $this->call(TestimonialSeeder::class);
        $this->call(StaticBlockSeeder::class);
        $this->call(EnglishQuestionSeeder::class);
        $this->call(ArticleSeeder::class);
        $this->call(CompanyReviewSeeder::class);
    }
}
