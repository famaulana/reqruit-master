<?php

use Carbon\Carbon;
use App\Models\City;
use App\Models\Skill;
use App\Models\Jobrole;
use App\Models\Province;
use App\Enums\WorkjobType;
use App\Helpers\PathHelper;
use App\Models\Jobfunction;
use Faker\Factory as Faker;
use App\Models\EducationDegree;
use App\Models\EducationField;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        factory(App\Models\User::class, 50)->create()->each(function ($user) use ($faker) {
            // Working Experience
            $randWorkExp = rand(1, 3);
            $stillWorking = rand(0, 1);
            $stillWorking = $stillWorking == 1 ? true : false;

            $workExps = [];
            for ($i=0; $i < $randWorkExp; $i++) {
                $now = Carbon::now();
                $startNumberExp = rand(2, 30);
                $endNumberExp = rand(0, $startNumberExp);
                $startTimeExp = $now->subMonths($startNumberExp);
                $endTimeExp = $now->subMonths($endNumberExp);

                $workExps[] = [
                    'position' => $faker->jobTitle,
                    'company' => $faker->company,
                    'start_time' => $startTimeExp,
                    'end_time' => $stillWorking ? null : $endTimeExp,
                    'still_working' => $stillWorking,
                    'more_info' => $faker->sentence(20),
                ];
            }

            $user->work_experiences()->createMany($workExps);

            // Organization Experience
            $randOrgExp = rand(1, 3);
            $stillInvolved = rand(0, 1);
            $stillInvolved = $stillInvolved == 1 ? true : false;

            $orgExps = [];
            for ($i=0; $i < $randOrgExp; $i++) {
                $now = Carbon::now();
                $startNumberExp = rand(2, 30);
                $endNumberExp = rand(0, $startNumberExp);
                $startTimeExp = $now->subMonths($startNumberExp);
                $endTimeExp = $now->subMonths($endNumberExp);

                $orgExps[] = [
                    'name' => $faker->words(rand(2, 5), true),
                    'position' => $faker->jobTitle,
                    'start_time' => $startTimeExp,
                    'end_time' => $stillInvolved ? null : $endTimeExp,
                    'still_involved' => $stillInvolved,
                    'more_info' => $faker->sentence(20),
                ];
            }

            $user->organization_experiences()->createMany($orgExps);

            // Education
            $randEdu = rand(1, 3);
            $stillStudying = rand(0, 1);
            $stillStudying = $stillStudying == 1 ? true : false;

            $edus = [];
            for ($i=0; $i < $randEdu; $i++) {
                $now = Carbon::now();
                $startNumberEdu = rand(2, 30);
                $endNumberEdu = rand(0, $startNumberEdu);
                $startTimeEdu = $now->subMonths($startNumberEdu);
                $endTimeEdu = $now->subMonths($endNumberEdu);
                $province = Province::inRandomOrder()->first();
                $city = City::inRandomOrder()->first();
                $field = EducationField::inRandomOrder()->first();

                $edus[] = [
                    'institution' => $faker->company,
                    'province_id' => $province->id,
                    'city_id' => $city->id,
                    'education_degree_id' => EducationDegree::inRandomOrder()->first()->id,
                    'education_field_id' => $field->id,
                    'gpa' => rand(50, 100),
                    'start_time' => $startTimeEdu,
                    'end_time' => $stillStudying ? null : $endTimeEdu,
                    'still_studying' => $stillStudying,
                    'more_info' => $faker->sentence(20),
                ];
            }

            $user->educations()->createMany($edus);

            // Award
            $randAward = rand(1, 3);
            $awards = [];
            for ($i=0; $i < $randAward; $i++) {
                $awards[] = [
                    'title' => $faker->sentence(rand(2, 4)),
                    'description' => $faker->sentence(rand(5, 10)),
                    'awarded_at' => Carbon::now()->subYears(rand(1, 15)),
                    'more_info' => $faker->sentence(20),
                ];
            }

            $user->awards()->createMany($awards);

            // Skills
            $skills = Skill::inRandomOrder()
                ->skip(0)
                ->take(rand(1, 4))
                ->pluck('id')
                ->toArray();

            $user->skills()->attach($skills);

            // Job interest
            $jobFunctionIds = Jobfunction::inRandomOrder()
                ->skip(0)
                ->take(rand(1, 2))
                ->pluck('id')
                ->toArray();

            $jobRoleIds = [];
            foreach ($jobFunctionIds as $jobFunctionId) {
                $jobRoleIds[] = Jobrole::where('jobfunction_id', $jobFunctionId)
                    ->inRandomOrder()
                    ->skip(0)
                    ->take(rand(1, 5))
                    ->pluck('id')
                    ->toArray();
            }

            $cities = City::inRandomOrder()
                ->skip(0)
                ->take(rand(1, 5))
                ->pluck('id')
                ->toArray();
            
            $jobTypes = WorkjobType::getValues();

            $user->job_interest()->create([
                'jobfunction_ids' => $jobFunctionIds,
                'jobrole_ids' => array_reduce($jobRoleIds, 'array_merge', array()),
                'job_types' => $faker->randomElements($jobTypes, rand(1, 3)),
                'current_salary' => rand(1, 20) * 1000000,
                'expected_salary' => rand(1, 20) * 1000000,
                'cities' => $cities,
                'relocated' => rand(0, 1),
            ]);

            // Resume
            $addResume = rand(0, 1);
            if ($addResume == 1) {
                // Create path if not exists
                $path = 'public/' . PathHelper::user_resume_path($user->id);
                $filePath = str_replace('/', DIRECTORY_SEPARATOR, $path);
                if (!Storage::exists($filePath)) {
                    Storage::makeDirectory($filePath, 0775, true);
                }

                // Saving resume file
                $fileName = 'resume.pdf';
                $resumeFile = storage_path('faker-files' . DIRECTORY_SEPARATOR . 'user' . DIRECTORY_SEPARATOR . $fileName);
                $destinationFile = storage_path('app' . DIRECTORY_SEPARATOR . $filePath.$fileName);
                File::copy($resumeFile, $destinationFile);

                $user->resume = $fileName;
                $user->save();
            }
        });
    }
}
