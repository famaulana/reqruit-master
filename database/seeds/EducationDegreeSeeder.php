<?php

use App\Models\EducationDegree;
use Illuminate\Database\Seeder;

class EducationDegreeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $degrees = ['Junior High School', 'Senior High School', 'Bachelor'];
        foreach ($degrees as $degree) {
            EducationDegree::create(['name' => $degree]);
        }
    }
}
