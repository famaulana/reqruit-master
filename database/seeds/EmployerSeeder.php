<?php

use App\Models\City;
use App\Models\Country;
use App\Helpers\PathHelper;
use App\Models\Industry;
use App\Models\Province;
use App\Models\Subdistrict;
use Illuminate\Database\Seeder;
use App\Services\CompanyService;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class EmployerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $country = Country::where('country_code', 'ID')->first();

        factory(App\Models\Employer::class, 10)->create()->each(function ($employer) use ($country) {
            $faker = \Faker\Factory::create();
            $name = $faker->unique()->company;
            $companyService = new CompanyService();
            $slug = $companyService->createSlug($name);
            $province = Province::inRandomOrder()->first();
            $city = City::where('province_id', $province->id)->inRandomOrder()->first();
            $subdistrict = Subdistrict::where('city_id', $city->id)->inRandomOrder()->first();

            $company = $employer->companies()->create([
                'name' => $name,
                'slug' => $slug,
                'short_description' => $faker->sentence(10),
                'address' => $faker->address,
                'country_id' => $country->id,
                'province_id' => $province->id,
                'city_id' => $city->id,
                'subdistrict_id' => $subdistrict->id,
                'pos_code' => $faker->postcode,
                'phone' => $faker->unique()->phoneNumber,
                'email' => $faker->unique()->email,
                'website_url' => $faker->url,
                'instagram' => $faker->url,
                'facebook' => $faker->url,
                'linkedin' => $faker->url,
                'twitter' => $faker->url,
                'description' => $faker->paragraphs(4, true)
            ]);

            // Create path if not exists
            $path = 'public/' . PathHelper::company_images_path($company->id);
            $imgPath = str_replace('/', DIRECTORY_SEPARATOR, $path);
            if (!Storage::exists($imgPath)) {
                Storage::makeDirectory($imgPath, 0775, true);
            }

            // Saving company logo
            $logoImageNumber = rand(1, 3);
            $logoFileName = 'logo-' . $logoImageNumber . '.jpg';
            $newLogo = Image::make(storage_path('faker-images' . DIRECTORY_SEPARATOR . 'company' . DIRECTORY_SEPARATOR . $logoFileName));
            $newLogo->save(storage_path('app' . DIRECTORY_SEPARATOR . $imgPath . $logoFileName));

            // Saving company banner
            $bannerImageNumber = rand(1, 3);
            $bannerFileName = 'banner-' . $bannerImageNumber . '.jpg';
            $newBanner = Image::make(storage_path('faker-images' . DIRECTORY_SEPARATOR . 'company' . DIRECTORY_SEPARATOR . $bannerFileName));
            $newBanner->save(storage_path('app' . DIRECTORY_SEPARATOR . $imgPath . $bannerFileName));

            // Saving company images
            $companyImages = [];
            for ($i=0; $i < 3; $i++) {
                $imageNumber = rand(1, 6);
                $imageFileName = 'company-image-' . $imageNumber . '.jpg';
                $companyImages[] = [
                    'file_name' => $imageFileName,
                    'description' => $faker->words(rand(2, 7), true),
                ];
                $newImage = Image::make(storage_path('faker-images' . DIRECTORY_SEPARATOR . 'company' . DIRECTORY_SEPARATOR . $imageFileName));
                $newImage->save(storage_path('app' . DIRECTORY_SEPARATOR . $imgPath . $imageFileName));
            }

            $company->logo = $logoFileName;
            $company->banner = $bannerFileName;
            $company->images = $companyImages;
            $company->save();

            // Saving industries
            $industryIds = Industry::take(rand(1, 4))->pluck('id')->toArray();
            $company->industries()->attach($industryIds);
        });
    }
}
