<?php

use Illuminate\Database\Seeder;

class EducationFieldSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\EducationField::class, 20)->create();
    }
}
