<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Testimonial;
use Faker\Generator as Faker;

$factory->define(Testimonial::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'profession' => $faker->jobTitle,
        'content' => $faker->sentences(rand(3, 6), true),
        'rating' => rand(1, 10),
    ];
});
