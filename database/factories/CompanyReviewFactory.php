<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Enums\WorkjobType;
use App\Models\Company;
use App\Models\CompanyReview;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(CompanyReview::class, function (Faker $faker) {
    $user = User::inRandomOrder()->first();
    $company = Company::inRandomOrder()->first();

    return [
        'user_id' => $user->id,
        'company_id' => $company->id,
        'anonymous' => 0,
        'job_status' => $faker->randomElement(['current', 'former']),
        'employment_status' => $faker->randomElement(WorkjobType::getValues()),
        'job_title' => $faker->sentence(rand(3, 6)),
        'rating' => rand(1, 5),
        'review_headline' => $faker->sentence(rand(3, 6)),
        'review' => $faker->paragraphs(rand(1, 3), true),
        'published' => rand(0, 1)
    ];
});
