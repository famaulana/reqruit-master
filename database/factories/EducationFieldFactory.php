<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\EducationField;
use Faker\Generator as Faker;

$factory->define(EducationField::class, function (Faker $faker) {
    return [
        'name' => $faker->words(rand(1, 3), true)
    ];
});
