<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Carbon\Carbon;
use App\Models\City;
use App\Models\User;
use App\Models\Country;
use App\Models\Province;
use App\Models\Subdistrict;
use App\Enums\UserJobStatus;
use App\Services\UserService;
use Faker\Generator as Faker;
use App\Enums\ResidenceStatusEnum;
use App\Enums\UserRelationshipEnum;
use App\Enums\WhenCanStartWorkEnum;
use Illuminate\Support\Facades\Hash;

$factory->define(User::class, function (Faker $faker) {
    $userService = new UserService();
    $country = Country::where('country_code', 'ID')->first();
    $province = Province::inRandomOrder()->first();
    $city = City::where('province_id', $province->id)->inRandomOrder()->first();
    $subdistrict = Subdistrict::where('city_id', $city->id)->inRandomOrder()->first();
    $whenCanStart = $faker->randomElement(WhenCanStartWorkEnum::getValues());

    return [
        'identifier' => $userService->generateUserIdentifier(),
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->e164PhoneNumber,
        'dob' => "$faker->year-$faker->month-$faker->dayOfMonth",
        'address' => $faker->address,
        'country_id' => $country->id,
        'province_id' => $province->id,
        'city_id' => $city->id,
        'subdistrict_id' => $subdistrict->id,
        'pos_code' => rand(11111, 50000),
        'about' => $faker->sentences(rand(4, 10), true),
        'job_status' => $faker->randomElement(UserJobStatus::getValues()),
        'hobby' => $faker->word(),
        'relationship' => $faker->randomElement(UserRelationshipEnum::getValues()),
        'residence_status' => $faker->randomElement(ResidenceStatusEnum::getValues()),
        'citizenship' => $country->id,
        'when_can_start' => $whenCanStart,
        'can_start_date' => $whenCanStart === 'choose-date' ? Carbon::now()->addDays(rand(1, 100))->format('d F Y') : null,
        'website' => $faker->randomElement(array($faker->url, '')),
        'facebook' => $faker->randomElement(array($faker->url, '')),
        'twitter' => $faker->randomElement(array($faker->url, '')),
        'instagram' => $faker->randomElement(array($faker->url, '')),
        'linkedin' => $faker->randomElement(array($faker->url, '')),
        'behance' => $faker->randomElement(array($faker->url, '')),
        'github' => $faker->randomElement(array($faker->url, '')),
        'codepen' => $faker->randomElement(array($faker->url, '')),
        'vimeo' => $faker->randomElement(array($faker->url, '')),
        'youtube' => $faker->randomElement(array($faker->url, '')),
        'dribbble' => $faker->randomElement(array($faker->url, '')),
        'verified' => 1,
        'gender' => $faker->randomElement(['male', 'female']),
        'password' => Hash::make('secret'),
    ];
});
