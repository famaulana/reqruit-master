<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Article;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Article::class, function (Faker $faker) {
    $title = $faker->sentence(rand(3, 6));

    return [
        'title' => $title,
        'slug' => Str::slug($title, '-'),
        'content' => implode('. ', $faker->paragraphs),
        'excerpt' => $faker->paragraph(6),
        'meta_title' => $title,
        'meta_description' => $faker->sentence,
    ];
});
