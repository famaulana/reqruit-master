<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Employer;
use Faker\Generator as Faker;
use App\Services\EmployerService;
use Illuminate\Support\Facades\Hash;

$factory->define(Employer::class, function (Faker $faker) {
    $employerService = new EmployerService();

    return [
        'identifier' => $employerService->generateEmployerIdentifier(),
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->e164PhoneNumber,
        'dob' => "$faker->year-$faker->month-$faker->dayOfMonth",
        'job_position' => $faker->jobTitle,
        'verified' => 1,
        'gender' => $faker->randomElement(['male', 'female']),
        'password' => Hash::make('secret'),
    ];
});
