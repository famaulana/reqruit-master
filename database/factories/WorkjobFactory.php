<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Enums\WorkjobExperience;
use App\Enums\WorkjobSalaryDuration;
use App\Enums\WorkjobType;
use App\Models\City;
use App\Models\Country;
use App\Models\Workjob;
use App\Models\Employer;
use App\Models\Jobfunction;
use App\Models\Province;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use App\Services\WorkjobService;

$factory->define(Workjob::class, function (Faker $faker) {
    $employer = Employer::inRandomOrder()->first();
    $company = $employer->companies()->inRandomOrder()->first();
    $title = $faker->sentence(6);
    $country = Country::where('country_code', 'ID')->first();
    $province = Province::inRandomOrder()->first();
    $city = City::where('province_id', $province->id)->inRandomOrder()->first();
    $jobFunction = Jobfunction::inRandomOrder()->first();
    $salaryMin = $faker->numberBetween(300, 500);
    $salaryMax = $faker->numberBetween($salaryMin + 1, 1000);
    $bonusSalaryMin = $faker->numberBetween(50, 100);
    $bonusSalaryMax = $faker->numberBetween($bonusSalaryMin, 200);

    return [
        'created_by' => $employer->id,
        'edited_by' => $employer->id,
        'company_id' => $company->id,
        'identifier' => WorkjobService::generateIdentifier(),
        'title' => $title,
        'slug' => Str::slug($title),
        'type' => $faker->randomElement(WorkjobType::toArray()),
        'country_id' => $country->id,
        'province_id' => $province->id,
        'city_id' => $city->id,
        'number_of_vacancies' => rand(1, 5),
        'jobfunction_id' => $jobFunction->id,
        'work_experience' => $faker->randomElement(WorkjobExperience::getValues()),
        'salary_currency' => 'IDR',
        'salary_min' => $salaryMin * 10000,
        'salary_max' => $salaryMax * 10000,
        'salary_duration' => $faker->randomElement(WorkjobSalaryDuration::getValues()),
        'bonus_salary' => rand(0, 1),
        'bonus_salary_min' => $bonusSalaryMin * 10000,
        'bonus_salary_max' => $bonusSalaryMax * 10000,
        'bonus_salary_max' => $bonusSalaryMax * 10000,
        'bonus_salary_duration' => $faker->randomElement(WorkjobSalaryDuration::getValues()),
        'description' => $faker->paragraphs(4, true),
        'has_question' => rand(0, 1),
        'status' => 'published'
    ];
});
