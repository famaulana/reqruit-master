<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Page;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Page::class, function (Faker $faker) {
    $title = $faker->sentence(3);

    return [
        'title' => $title,
        'slug' => Str::slug($title, '-'),
        'content' => implode('. ', $faker->paragraphs),
        'meta_title' => $title,
        'meta_description' => $faker->sentence,
    ];
});
