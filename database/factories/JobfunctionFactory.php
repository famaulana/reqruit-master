<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Jobfunction;
use Faker\Generator as Faker;

$factory->define(Jobfunction::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->words(2, true)
    ];
});
