<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Jobfunction;
use App\Models\Jobrole;
use Faker\Generator as Faker;

$factory->define(Jobrole::class, function (Faker $faker) {
    $jobFunction = Jobfunction::inRandomOrder()->first();

    return [
        'name' => $faker->unique()->words(2, true),
        'jobfunction_id' => $jobFunction->id
    ];
});
