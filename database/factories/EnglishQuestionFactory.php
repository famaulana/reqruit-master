<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\EnglishQuestion;
use Faker\Generator as Faker;

$factory->define(EnglishQuestion::class, function (Faker $faker) {
    $answers = [];
    while (count($answers) < 4) {
        $randomString = Str::random(10);
        if (!in_array($randomString, $answers)) {
            $answers[] = [
                'id' => $randomString,
                'answer' => $faker->words(rand(1, 4), true)
            ];
        }
    }

    $correctAnswerId = rand(0, 3);

    return [
        'question' => $faker->sentences(rand(2, 4), true),
        'answers' => $answers,
        'correct_answer' => $answers[$correctAnswerId]['id'],
        'timer' => 200
    ];
});
