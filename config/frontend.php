<?php

return [
    'frontend_url' => env('FRONTEND_URL', 'http://localhost:3000'),
    'employer_url' => env('EMPLOYER_URL', 'http://localhost:3001')
];
