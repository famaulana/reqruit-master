<?php

$configFile = app_path("Themes/".config('active_theme.domain')."/etc/configs.php");

if (file_exists($configFile)) {
    return require_once $configFile;
}
