<?php

return [
    // General
    'site_name' => 'Website Name',
    'site_email' => 'admin@example.com',
    'site_contact_email' => 'contact@example.com',

    // Logo
    'logo1' => '',
    'logo2' => '',
    'logo3' => '',
    'logo_square' => '',
    'favicon' => '',
    'admin_logo' => 'logo1',
    'frontend_logo' => 'logo1',
    'email_logo' => 'logo1',
    'invoice_logo' => 'logo1',
    'light_logo' => 'logo1',
    'dark_logo' => 'logo1',

    // Homepage
    'home_meta_title' => '',
    'home_meta_description' => '',
    'home_banner' => '',
    'home_banner_mobile' => '',
    'intro_right_image' => '',
    'intro_left_image' => '',
    'section_about_btn_link' => '',
    'section_about_yt_link' => '',
    'section_about_video_image' => '',
    'home_join_banner' => '',
    'home_count_1_value' => '',
    'home_count_1_label' => '',
    'home_count_2_value' => '',
    'home_count_2_label' => '',
    'home_count_3_value' => '',
    'home_count_3_label' => '',
    'home_count_4_value' => '',
    'home_count_4_label' => '',

    // Blog
    'featured_posts' => [],
    'popular_posts' => [],

    // Authentication
    'auth_image' => '',
    'auth_image_employer' => '',

    // Formatting
    'locale' => 'id-ID',

    // Menu
    'top_menu' => '', // Menu ID
    'footer_menu' => '', // Menu ID
    'top_menu_employer' => '', // Menu ID

    // Job filters
    'jf_companies' => [],

    // Footer
    'footer_content' => '',
    'footer_bg_image_enable' => 'no',
    'footer_bg_image' => '',

    // Social
    'youtube_link' => '',
    'instagram_link' => '',
    'twitter_link' => '',
    'facebook_link' => '',

    // Job application
    'jap_rejection_messages' => [],

    // English test
    'english_test_q_count' => 20,
    'english_test_can_retest_after_days' => 30,
    'english_test_session_2_timer' => 60,

    // Personal test
    'personal_test_can_retest_after_days' => 30,
    
    // Script
    'javascript' => ''
];
