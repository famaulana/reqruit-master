<?php

$themes = glob('../app/Themes' . '/*', GLOB_ONLYDIR);

$paths = [resource_path('views')];
foreach ($themes as $themeDir) {
    $configFile = "$themeDir/etc/configs.php";
    $registrationFile = "$themeDir/registration.php";

    $themeDomain = explode('/', $themeDir);
    $themeDomain = end($themeDomain);

    $dir = realpath(base_path('app/Themes/'.$themeDomain.'/resources/views'));
    if (file_exists($dir) && is_dir($dir)) {
        $paths[] = $dir;
    }
}

return $paths;
