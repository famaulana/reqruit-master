<?php

namespace App\Themes\Reqruit;

use App\Theming\Interfaces\Common\RegistrationInterface;

class Registration implements RegistrationInterface
{
    /**
     * Theme name
     *
     * @return String
     */
    public function themeName()
    {
        return 'Reqruit';
    }

    /**
     * Theme domain name
     *
     * @return String
     */
    public function themeDomain()
    {
        return 'reqruit';
    }

    /**
     * Theme root namespace
     *
     * @return String
     */
    public function themeNamespace()
    {
        return 'Reqruit';
    }
}
