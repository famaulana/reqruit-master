@extends('common.reqruit.layouts.app', ['header_type' => 'light'])

@section('content')
    <div class="container">
        <div class="auth-page auth-page--register">
            <div class="auth-form auth-form--compact">
                <div class="auth-form__wrapper">
                    <h1>Daftar Perusahaan</h1>
                    <div class="auth-form__cta">Daftar akun perusahaan.</div>
                        <employer-register></employer-register>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
