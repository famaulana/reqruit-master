@extends('common.reqruit.layouts.app', ['header_type' => 'light'])

@section('content')
    <div class="container">
        <div class="auth-page auth-page--login">
            <div class="auth-form auth-form--compact">
                <div class="auth-form__wrapper">
                    <h1>Login Perusahaan</h1>
                    <div class="auth-form__cta">Login akun perusahaan.</div>
                        <form method="POST" action="{{ route('auth.employer.login.post') }}" class="form">
                            @csrf
                    
                            <div class="form__group">
                                <label class="sr-only" for="email">{{ __('E-Mail Address') }}</label>
                                <div class="input-group input-group--no-append">
                                    <div
                                        class="input-group__prepend input-group__prepend--icon-primary"
                                    >
                                        <i class="icon-profile"></i>
                                    </div>
                                    <div class="input-group__main">
                                        <input id="email" type="email" class="form__control" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email" autofocus>
                                    </div>
                                </div>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                @enderror
                            </div>
                    
                            <div class="form__group">
                                <label class="sr-only" for="password">{{ __('Password') }}</label>
                                <div class="input-group input-group--no-append">
                                    <div
                                        class="input-group__prepend input-group__prepend--icon-primary"
                                    >
                                        <i class="icon-lock"></i>
                                    </div>
                                    <div class="input-group__main">
                                        <input id="password" type="password" class="form__control" name="password" required autocomplete="current-password" placeholder="Kata Sandi">
                                    </div>
                                </div>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                @enderror
                            </div>

                            @if (Route::has('auth.employer.password.request'))
                                <a href="{{ route('auth.employer.password.request') }}" title="Lupa password" class="auth-form-text">
                                    Lupa Password?
                                </a>
                            @endif

                            <button type="submit" class="btn btn--primary btn--block btn--shadow mt-3">Masuk</button>

                            <div class="center mt-3 mb-3">Atau</div>

                            <a href="{{ route('auth.employer.register.form') }}" class="btn btn-outline btn-outline--primary btn--block">
                                Daftar
                            </a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
