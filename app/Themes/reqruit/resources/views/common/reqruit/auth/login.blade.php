@extends('common.reqruit.layouts.app', ['header_type' => 'light'])

@section('content')
    <div class="container auth-page auth-page--login">

        <div class="auth-form auth-form--login">
            <div class="auth-form__msg">
                <div class="auth-form__msg-text">
                    <h2>Halo, Teman</h2>
                    <p>Belum punya akun nih?</p>
                    <p>
                        Yuk, Masukkan info pribadi Anda dan
                        mulai bergabung dengan kami
                    </p>
                </div>
                <div class="auth-form__msg-btn">
                    <a
                        href="{{ route('auth.user.register.form') }}"
                        class="btn btn--lg btn--long btn-outline btn-outline--white btn-outline--color-light"
                        >Daftar</a
                    >
                </div>
            </div>

            <div class="auth-form__wrapper">
                <h1>Masuk</h1>
                <div class="auth-form__cta">Yuk, Masukkan akun Reqruit Anda</div>

                <form method="POST" action="{{ route('auth.user.login.post') }}" class="form">
                    @csrf
                    <div class="form__group">
                        <label class="sr-only" for="email">{{ __('E-Mail Address') }}</label>
                        <div class="input-group input-group--no-append">
                            <div
                                class="input-group__prepend input-group__prepend--icon-primary"
                            >
                                <i class="icon-profile"></i>
                            </div>
                            <div class="input-group__main">
                                <input id="email" type="email" class="form__control" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email" autofocus>
                            </div>
                        </div>
                        @error('email')
                            <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
            
                    <div class="form__group">
                        <label class="sr-only" for="password">{{ __('Password') }}</label>
                        <div class="input-group input-group--no-append">
                            <div
                                class="input-group__prepend input-group__prepend--icon-primary"
                            >
                                <i class="icon-lock"></i>
                            </div>
                            <div class="input-group__main">
                                <input id="password" type="password" class="form__control" name="password" required autocomplete="current-password" placeholder="Kata Sandi">
                            </div>
                        </div>
                        @error('password')
                            <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
            
                    @if (Route::has('auth.user.password.request'))
                        <a href="{{ route('auth.user.password.request') }}" title="Lupa password" class="auth-form-text">
                            Lupa Password?
                        </a>
                    @endif
            
                    <button type="submit" class="btn btn--primary btn--block btn--shadow mt3">Masuk</button>

                    @if (config('services.google.client_id', '') != '' || config('services.facebook.client_id', '') != '' || config('services.linkedin.client_id', '') != '')  
                        <div class="auth-form__or">
                            <span class="text">Atau</span>
                            <span class="line">&nbsp;</span>
                        </div>
                        <p class="auth-form__cta-social">Akun Sosial</p>
            
                        <div class="auth-form__social">
                            @if (config('services.google.client_id', '') != '')
                                <div class="auth-form__social-item">
                                    <a href="{{ route('auth.user.social.login', ['service' => 'google']) }}" title="Google">
                                        <svg>
                                            <image
                                                xlink:href="{{ url('images/reqruit/icons-color/google.svg') }}"
                                                alt="Google"
                                            />
                                        </svg>
                                    </a>
                                </div>
                            @endif
                            @if (config('services.facebook.client_id', '') != '')
                                <div class="auth-form__social-item">
                                    <a href="{{ route('auth.user.social.login', ['service' => 'facebook']) }}" title="Facebook">
                                        <svg>
                                            <image
                                                xlink:href="{{ url('images/reqruit/icons-color/facebook.svg') }}"
                                                alt="Facebook"
                                            />
                                        </svg>
                                    </a>
                                </div>
                            @endif
                            @if (config('services.linkedin.client_id', '') != '')
                                <div class="auth-form__social-item">
                                    <a href="{{ route('auth.user.social.login', ['service' => 'linkedin']) }}" title="Linkedin">
                                        <svg>
                                            <image
                                                xlink:href="{{ url('images/reqruit/icons-color/linkedin.svg') }}"
                                                alt="Linkedin"
                                            />
                                        </svg>
                                    </a>
                                </div>
                            @endif
                        </div>
                    @endif
                </form>

            </div>
        </div>

        <div class="auth-image auth-image--login">
            <img src="{{ url('images/reqruit/content-images/male-person.png') }}" alt="Login" class="img-fluid">
        </div>

    </div>
@endsection
