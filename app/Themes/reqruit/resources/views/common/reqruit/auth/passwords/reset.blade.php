@extends('common.reqruit.layouts.app', ['header_type' => 'light'])

@section('content')
    <div class="container">
        <div class="auth-page auth-page--forgot-password">
            <div class="auth-form auth-form--compact">
                <div class="auth-form__wrapper">
                    <h1>Reset Password</h1>
                    <div class="auth-form__cta">Atur ulang password Anda.</div>
                    
                    <form method="POST" action="{{ route('auth.user.password.update') }}" class="form">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form__group">
                            <label class="sr-only" for="email">{{ __('E-Mail Address') }}</label>
                            <div class="input-group input-group--no-append">
                                <div
                                    class="input-group__prepend input-group__prepend--icon-primary"
                                >
                                    <i class="icon-message"></i>
                                </div>
                                <div class="input-group__main">
                                    <input id="email" type="email" class="form__control" name="email" value="{{ $email ?? old('email') }}" placeholder="Email" required autocomplete="email" autofocus>
                                </div>
                            </div>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form__group">
                            <label class="sr-only" for="password">{{ __('Password') }}</label>
                            <div class="input-group input-group--no-append">
                                <div
                                    class="input-group__prepend input-group__prepend--icon-primary"
                                >
                                    <i class="icon-lock"></i>
                                </div>
                                <div class="input-group__main">
                                    <input id="password" type="password" class="form__control" name="password" placeholder="Password" required autocomplete="new-password">
                                </div>
                            </div>
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form__group">
                            <label class="sr-only" for="password-confirm">{{ __('Confirm Password') }}</label>
                            <div class="input-group input-group--no-append">
                                <div
                                    class="input-group__prepend input-group__prepend--icon-primary"
                                >
                                    <i class="icon-lock"></i>
                                </div>
                                <div class="input-group__main">
                                    <input id="password-confirm" type="password" class="form__control" name="password_confirmation" placeholder="Konfirmasi password" required autocomplete="new-password">
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn--primary btn--block btn--shadow mt3">Reset Password</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
