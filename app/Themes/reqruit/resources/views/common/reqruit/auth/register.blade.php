@extends('common.reqruit.layouts.app', ['header_type' => 'light'])

@section('content')
    <div class="container">
        <div class="auth-page auth-page--register">
            <user-register
                :google_client_id="'{{ config('services.google.client_id', '') }}'"    
                :facebook_client_id="'{{ config('services.facebook.client_id', '') }}'"    
                :linkedin_client_id="'{{ config('services.linkedin.client_id', '') }}'"    
            ></user-register>
            <div class="auth-image auth-image--register">
                <img src="{{ url('images/reqruit/content-images/male-person.png') }}" alt="Register" class="img-fluid">
            </div>
        </div>
    </div>
@endsection
