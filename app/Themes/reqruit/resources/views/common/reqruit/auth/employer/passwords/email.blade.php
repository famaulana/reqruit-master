@extends('common.reqruit.layouts.app', ['header_type' => 'light'])

@section('content')
<div class="container">
    <div class="auth-page auth-page--forgot-password">
        <div class="auth-form auth-form--compact">
            <div class="auth-form__wrapper">
                <h1>Lupa Password</h1>
                <div class="auth-form__cta">Lupa password akun perusahaan.</div>

                <form method="POST" action="{{ route('auth.employer.password.email') }}" class="form">
                    @csrf
            
                    <div class="form__group">
                        <label class="sr-only" for="email">{{ __('E-Mail Address') }}</label>
                        <div class="input-group input-group--no-append">
                            <div
                                class="input-group__prepend input-group__prepend--icon-primary"
                            >
                                <i class="icon-profile"></i>
                            </div>
                            <div class="input-group__main">
                                <input id="email" type="email" class="form__control" name="email" value="{{ old('email') }}" placeholder="Masukkan email" required autocomplete="email" autofocus>
                            </div>
                        </div>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
            
                    <button type="submit" class="btn btn--primary btn--block btn--shadow mt3">Kirim Link Reset Password</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
