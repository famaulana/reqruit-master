@include('common.reqruit.layouts._head', ['faviconUrl' => $faviconUrl])
<body>
    <div id="app" class="common auth">

        <header class="header header--auth">
            <div class="logo">
                <a href="{{ route('home') }}" title="Homepage">
                    <img src="{{ $frontendLogoUrl }}" alt="{{ $siteName }}" class="img-fluid">
                </a>
            </div>
        </header>

        <div class="container my-3">
            @include('common.reqruit.share.flash-alert')
        </div>

        @yield('content')
    </div>
</body>
</html>
