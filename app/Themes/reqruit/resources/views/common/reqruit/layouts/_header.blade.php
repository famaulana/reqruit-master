@php
    $menuService = new \App\Services\MenuService();
    $menuRepository = new \App\Repositories\MenuRepository();
    $appConfig = new \App\Repositories\AppConfigRepository();

    $topMenuId = $appConfig->getThemeConfig('top_menu');
    if($topMenuId != '') {
        $topMenu = json_decode($menuRepository->getMenu($topMenuId, true), true);
    }
@endphp

<header class="header {{ $header_type }} flex items-center px3 clearfix">
    <div class="header__logo col col-11 sm-col-11 md-col-11 lg-col-2">
        <h1>
            @if (\Route::current()->getName() == 'home')
                <a href="{{ route('home') }}" title="{{ $siteName }}">
                    @if ($lightLogoUrl == '')
                        {{ $siteName }}
                    @else
                        <img src="{{ $lightLogoUrl }}" alt="{{ $siteName }}" class="img-fluid" />
                    @endif
                </a>
            @else
                <a href="{{ route('home') }}" title="{{ $siteName }}">
                    @if ($logo_url == '')
                        {{ $siteName }}
                    @else
                        <img src="{{ $logo_url }}" alt="{{ $siteName }}" class="img-fluid" />
                    @endif
                </a>
            @endif
        </h1>
    </div>
    <div class="header__top-menu col col-10">
        <nav class="top-menu-links flex items-center">
            {{-- @if (\Route::current()->getName() == 'home')
                <ul>
                    <li class="nav-item">
                        <a href="{{ route('explore-jobs') }}" class="nav-link" title="Cari Pekerjaan">Cari Pekerjaan</a>
                        <div class="line">&nbsp;</div>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('auth.employer.login.form') }}" class="nav-link" title="Login">Untuk Perusahaan</a>
                        <div class="line">&nbsp;</div>
                    </li>
                </ul>
            @else
                @isset ($topMenu['links'])
                    <ul>
                        @foreach ($topMenu['links'] as $menu)
                            <li class="nav-item">
                                <a href="{{ $menuService::getUrl($menu) }}" class="nav-link">{{ $menu['text'] }}</a>
                                <div class="line">&nbsp;</div>
                            </li>
                        @endforeach
                    </ul>
                @endisset
            @endif --}}
            @isset ($topMenu['links'])
                <ul>
                    @foreach ($topMenu['links'] as $menu)
                        <li class="nav-item">
                            <a href="{{ $menuService::getUrl($menu) }}" class="nav-link">{{ $menu['text'] }}</a>
                            <div class="line">&nbsp;</div>
                        </li>
                    @endforeach
                </ul>
            @endisset
        </nav>
        <div class="header__auth">
            @if (\Request::is('employer/*'))
                @if (auth()->guard('employer')->check())
                    <div class="header__authenticated">
                        <div class="header__avatar">
                            <img src="{{ auth()->guard('employer')->user()->avatar_url }}" alt="{{ auth()->guard('employer')->user()->name }}" class="img-fluid">
                        </div>
                        <div class="header__auth-menu-trigger"><i class="icon-arrow-down-2"></i></div>
                        <div class="header__auth-menu-wrapper">
                            <nav class="header__auth-menu">
                                <ul>
                                    <li><a href="{{ route('account-empl.dashboard') }}" title="Dashboard Perusahaan">Dashboard</a></li>
                                    <li><a href="{{ route('account-empl.edit-account') }}" title="Edit Akun">Edit Akun</a></li>
                                    <li id="logout-link"><a href="#" title="Logout">Logout <i class="icon-logout"></i></a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    
                    <form id="logout-form" action="{{ route('auth.employer.logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                @else
                    <a href="{{ route('auth.user.register.form') }}" title="Daftar" class="btn btn--warning btn--lg col">Daftar</a>
                    <a href="{{ route('auth.user.login.form') }}" title="Masuk" class="btn btn--lg btn-outline btn-outline--warning btn-outline--color-{{ $header_type == 'dark' ? 'light' : 'dark' }} col">Masuk</a>
                @endif
            @else
                @if (auth()->guard('user')->check())
                    <div class="header__authenticated">
                        <div class="header__avatar">
                            <img src="{{ auth()->guard('user')->user()->avatar_url }}" alt="{{ auth()->guard('user')->user()->name }}" class="img-fluid">
                        </div>
                        <div class="header__auth-menu-trigger"><i class="icon-arrow-down-2"></i></div>
                        <div class="header__auth-menu-wrapper">
                            <nav class="header__auth-menu">
                                <ul>
                                    <li><a href="{{ route('account.job-applications.index') }}" title="Lamaran Saya">Lamaran Saya</a></li>
                                    <li><a href="{{ route('account.index') }}" title="Edit Akun">Edit Akun</a></li>
                                    <li><a href="{{ route('account.overview') }}" title="Edit Profil">Edit Profil</a></li>
                                    <li id="logout-link"><a href="#" title="Logout">Logout <i class="icon-logout"></i></a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    
                    <form id="logout-form" action="{{ route('auth.user.logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                @else
                    <a href="{{ route('auth.user.register.form') }}" title="Daftar" class="btn btn--warning btn--lg col">Daftar</a>
                    <a href="{{ route('auth.user.login.form') }}" title="Masuk" class="btn btn--lg btn-outline btn-outline--warning btn-outline--color-{{ $header_type == 'dark' ? 'light' : 'dark' }} col">Masuk</a>
                @endif
            @endif
            
        </div>
    </div>

    {{-- <div class="header__search col col-2 sm-col-1 md-col-1 flex justify-end">
        <i class="icon-search"></i>
    </div> --}}

    <div id="mobile-nav-toggle" class="col col-1 sm-col-1 md-col-1">
        <span class="dot">&nbsp;</span>
        <span class="dot">&nbsp;</span>
        <span class="dot">&nbsp;</span>
    </div>
</header>