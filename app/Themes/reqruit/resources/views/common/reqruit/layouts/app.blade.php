@include('common.reqruit.layouts._head', ['faviconUrl' => $faviconUrl])
<body class="@yield('body_class')">
    <div id="app" class="common">
        @include('common.reqruit.layouts._header', [
            'logo_url' => $frontendLogoUrl,
            'logo_url_light' => $lightLogoUrl,
            'header_type' => $header_type
        ])

        <div class="container">
            @include('common.reqruit.share.flash-alert')
        </div>

        <div class="content">
            @yield('content')
        </div>

        @include('common.reqruit.layouts._footer', [
            'logo_url' => $lightLogoUrl,
        ])
        <loading-block></loading-block>
    </div>
</body>
</html>
