<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {!! SEO::generate() !!}

    <link rel="icon" type="image/png" href="{{ $faviconUrl }}">

    <script type="text/javascript">
        var APP_BASE_URL = {!! json_encode(url('/')) !!};
    </script>

    <!-- Scripts -->
    <script src="{{ asset('themes/reqruit/js/common.js') }}" defer></script>

    @stack('scripts')

    @if ($javascript != '')
        <script>
            {!! $javascript !!}
        </script>
    @endif

    @stack('styles')
    
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&family=Quicksand&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('themes/reqruit/css/common.css') }}" rel="stylesheet">
</head>