@php
    $menuService = new \App\Services\MenuService();
    $menuRepository = new \App\Repositories\MenuRepository();
    $appConfig = new \App\Repositories\AppConfigRepository();

    $footerMenuId = $appConfig->getThemeConfig('footer_menu');
    if($footerMenuId != '') {
        $footerMenu = json_decode($menuRepository->getMenu($footerMenuId, true), true);
    }
@endphp

<footer class="footer">
    <div class="container">
      
        <div class="footer__top clearfix">
            <div class="col col-12 md-col-8 lg-col-8 footer__widget">
                <div class="footer__logo">
                    <a href="{{ route('home') }}" title="{{ $siteName }}">
                        @if ($logo_url == '')
                            {{ $siteName }}
                        @else
                            <img src="{{ $logo_url }}" alt="{{ $siteName }}" class="img-fluid" />
                        @endif
                    </a>
                </div>
                <div class="footer__about">{{ config('theme.reqruit.footer_content', '') }}</div>
            </div>

            <div class="col col-12 md-col-4 lg-col-4 footer__widget">
                @isset ($footerMenu['links'])
                    <h4 class="footer__widget-title">Informasi</h4>
                    <nav class="footer__widget-nav">
                        <ul>
                            @foreach ($footerMenu['links'] as $menu)
                                <li>
                                    <a href="{{ $menuService::getUrl($menu) }}" class="nav-link">{{ $menu['text'] }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </nav>
                @endisset
            </div>
        </div>

        <div class="footer__social">
            <div class="footer__social-cta">Ikuti Sosial Media Kami</div>
            <nav class="footer__social-links">
                <ul>
                    @if (config('theme.reqruit.youtube_link') != '')
                        <li>
                            <a href="{{ config('theme.reqruit.youtube_link') }}" title="Youtube" target="_blank">
                                <svg><image xlink:href="{{ url('images/reqruit/icons/social/youtube.svg') }}" alt="Youtube" /></svg>
                            </a>
                        </li>
                    @endif
                    @if (config('theme.reqruit.instagram_link') != '')
                        <li>
                            <a href="{{ config('theme.reqruit.instagram_link') }}" title="Instagram" target="_blank">
                                <svg><image xlink:href="{{ url('images/reqruit/icons/social/instagram.svg') }}" alt="Instagram" /></svg>
                            </a>
                        </li>
                    @endif
                    @if (config('theme.reqruit.twitter_link') != '')
                        <li>
                            <a href="{{ config('theme.reqruit.twitter_link') }}" title="Twitter" target="_blank">
                                <svg><image xlink:href="{{ url('images/reqruit/icons/social/twitter.svg') }}" alt="Twitter" /></svg>
                            </a>
                        </li>
                    @endif
                    @if (config('theme.reqruit.facebook_link') != '')
                        <li>
                            <a href="{{ config('theme.reqruit.facebook_link') }}" title="Facebook" target="_blank">
                                <svg><image xlink:href="{{ url('images/reqruit/icons/social/facebook.svg') }}" alt="Facebook" /></svg>
                            </a>
                        </li>
                    @endif
                </ul>
            </nav>
        </div>

        <div class="footer__copyright">
            &copy; {{ date('Y') }} {{ $siteName }}. All rights reserved.
        </div>
    </div>
</footer>