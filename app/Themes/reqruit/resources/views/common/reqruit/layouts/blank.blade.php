@include('common.reqruit.layouts._head', ['faviconUrl' => $faviconUrl])
<body>
    <div id="app" class="common">
        @yield('content')
    </div>
</body>
</html>
