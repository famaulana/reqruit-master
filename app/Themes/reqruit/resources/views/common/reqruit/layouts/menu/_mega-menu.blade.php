@php
    $menuService = new \App\Services\MenuService();
@endphp

<div class="mega-menu child-nav" id="sub-menu-{{ $key }}">
    @if ($mega_menu['mega_menu_image'] !== null)
        <div class="mm-image">
            <img src="{{ $mega_menu['mega_menu_image_url'] }}" alt="{{ $mega_menu['text'] }}" class="img-fluid">
        </div>
    @endif
    <div class="nav-item d-lg-none close-sub-menu">
        <a href="#" class="nav-link">
            <i class="fas fa-angle-left"></i> Back
        </a>
    </div>
    <div class="mm-content">
        @foreach ($mega_menu['childs'] as $menuWrapper)
            <div class="mm-item">
                <div class="mm-sub-title">{{ $menuWrapper['text'] }}</div>
                <ul class="mm-list">
                    @if (count($menuWrapper['childs']) > 0)
                        @foreach ($menuWrapper['childs'] as $menu)
                            <li class="nav-item">
                                <a href="{{ $menuService::getUrl($menu) }}" class="nav-link">{{ $menu['text'] }}</a>
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>
        @endforeach
    </div>
</div>