@extends('common.reqruit.layouts.app', ['header_type' => 'light'])

@section('body_class', 'content-no-padding-top')

@section('content')
<div class="p-company">
    <div class="p-company__banner">
        <div class="logo-sq">
            <img src="{{ $company->logo_url }}" alt="{{ $company->name }}" class="img-fluid">
        </div>
        <img src="{{ $company->banner_url }}" alt="{{ $company->name }}" class="img-fluid">
    </div>

    <div class="container">
        <section class="p-company__top">
            <div class="p-company__logo">
                <img src="{{ $company->logo_url }}" alt="{{ $company->name }}" class="img-fluid">
            </div>

            <div class="p-company__details">
                <h1>{{ $company->name }}</h1>
                <div class="p-company__short-desc">{{ $company->short_description }}</div>
                <div class="p-company__list">
                    <div class="list-detail">
                        <dl>
                            <dt>Lokasi</dt>
                            <dd>
                                {{ $company->address }}<br />
                                Kecamatan {{ $company->subdistrict->name }},<br />
                                {{ $company->city_name }} - {{ $company->province->name }}<br />
                                {{ $company->pos_code }}
                            </dd>

                            <dt>Telepon</dt>
                            <dd>{{ $company->phone }}</dd>
    
                            @if (!is_null($company->website_url))
                                <dt>Website</dt>
                                <dd><a href="{{ $company->website_url }}" title="{{ $company->name }}">{{ $company->website_url }}</a></dd>
                            @endif

                            <dt>Industri</dt>
                            <dd>
                                <div class="flex flex-wrap">
                                    @foreach ($company->industries as $ind)
                                        <div class="badge mr2 mb2"><span>{{ $ind->name }}</span></div>
                                    @endforeach
                                </div>
                            </dd>
                        </dl>
                    </div>
                    <div class="list-socials">
                        <ul>
                            @if ($company->linkedin != '')
                                <li>
                                    <a href="{{ $company->linkedin }}" title="Linkedin">
                                        <svg><image xlink:href="{{ url('images/reqruit/icons/social/linkedin.svg') }}" alt="Linkedin" /></svg>
                                    </a>
                                </li>
                            @endif
                            @if ($company->instagram != '')
                                <li>
                                    <a href="{{ $company->instagram }}" title="Instagram">
                                        <svg><image xlink:href="{{ url('images/reqruit/icons/social/instagram.svg') }}" alt="Instagram" /></svg>
                                    </a>
                                </li>
                            @endif
                            @if ($company->twitter != '')
                                <li>
                                    <a href="{{ $company->twitter }}" title="Twitter">
                                        <svg><image xlink:href="{{ url('images/reqruit/icons/social/twitter.svg') }}" alt="Twitter" /></svg>
                                    </a>
                                </li>
                            @endif
                            @if ($company->facebook != '')
                                <li>
                                    <a href="{{ $company->facebook }}" title="Facebook">
                                        <svg><image xlink:href="{{ url('images/reqruit/icons/social/facebook.svg') }}" alt="Facebook" /></svg>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <article class="p-company__description">
            <h2 class="block-title">Overview</h2>
            <div class="description">
                {!! $company->description !!}
            </div>
        </article>

        <article class="p-company__images">
            <h2 class="block-title">Gallery</h2>
            <silent-box :gallery="{{ json_encode($images) }}"></silent-box>
        </article>

        <section class="p-company__jobs">
            <h2 class="block-title">Pekerjaan</h2>

            <div class="job-grid">
                @foreach ($company->work_jobs as $job)
                    @include('common.reqruit.workjob._workjob-card', ['job' => $job])
                @endforeach
            </div>
        </section>
    </div>
</div>
    
@endsection
