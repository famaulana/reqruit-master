<div class="rating-stars">
    <svg class="rating-star"><image xlink:href="{{ url('images/reqruit/icons/rating-star/rating-star-light.svg') }}" alt="rating-full" /></svg>
    <svg class="rating-star"><image xlink:href="{{ url('images/reqruit/icons/rating-star/rating-star-half.svg') }}" alt="rating-half" /></svg>
    @for ($i = 0; $i < 3; $i++)
        <svg class="rating-star"><image xlink:href="{{ url('images/reqruit/icons/rating-star/rating-star-dimmed.svg') }}" alt="rating-dimmed" /></svg>
    @endfor
</div>