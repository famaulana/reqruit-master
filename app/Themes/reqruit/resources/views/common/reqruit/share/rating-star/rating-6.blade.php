<div class="rating-stars">
    @for ($i = 0; $i < 3; $i++)
        <svg class="rating-star"><image xlink:href="{{ url('images/reqruit/icons/rating-star/rating-star-light.svg') }}" alt="rating-full" /></svg>
    @endfor
    @for ($i = 0; $i < 2; $i++)
        <svg class="rating-star"><image xlink:href="{{ url('images/reqruit/icons/rating-star/rating-star-dimmed.svg') }}" alt="rating-dimmed" /></svg>
    @endfor
</div>