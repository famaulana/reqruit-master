<div class="rating-stars">
    <svg class="rating-star"><image xlink:href="{{ url('images/reqruit/icons/rating-star/rating-star-light.svg') }}" alt="rating-full" /></svg>
    @for ($i = 0; $i < 4; $i++)
        <svg class="rating-star"><image xlink:href="{{ url('images/reqruit/icons/rating-star/rating-star-dimmed.svg') }}" alt="rating-dimmed" /></svg>
    @endfor
</div>