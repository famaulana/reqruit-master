@if (session()->has('flash_alert'))
    <div class="alert alert--{{ session()->get('flash_alert.level') }}" role="alert">
        {!! session()->get('flash_alert.message') !!}
    </div>
@endif