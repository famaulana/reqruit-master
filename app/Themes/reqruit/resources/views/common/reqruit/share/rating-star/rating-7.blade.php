<div class="rating-stars">
    @for ($i = 0; $i < 3; $i++)
        <svg class="rating-star"><image xlink:href="{{ url('images/reqruit/icons/rating-star/rating-star-light.svg') }}" alt="rating-full" /></svg>
    @endfor
    <svg class="rating-star"><image xlink:href="{{ url('images/reqruit/icons/rating-star/rating-star-half.svg') }}" alt="rating-half" /></svg>
    <svg class="rating-star"><image xlink:href="{{ url('images/reqruit/icons/rating-star/rating-star-dimmed.svg') }}" alt="rating-dimmed" /></svg>
</div>