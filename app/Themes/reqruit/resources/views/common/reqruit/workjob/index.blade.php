@extends('common.reqruit.layouts.app', ['header_type' => 'light'])

@section('content')
    <div class="container">
        <div class="job">
            <div class="job__details">
                <div class="job__main-info">
                    <div class="logo-sq job__company-logo">
                        <img src="{{ $job->company->logo_url }}" alt="{{ $job->company->name }}" class="img-fluid">
                    </div>
                    <div class="job__main-info-inner">
                        <h1 class="job__title">{{ $job->title }}</h1>
                        <div class="job__company-name">
                            <a href="{{ route('company.index', $job->company->slug) }}" title="{{ $job->company->name }}">{{ $job->company->name }}</a>
                        </div>
                    </div>
                </div>

                <div class="job__detail-2">
                    <div class="job__detail-2-item">
                        <dl>
                            <dt>Lokasi</dt>
                            <dd>{{ $job->city_name }}</dd>
                        
                            <dt>Tipe Pekerjaan</dt>
                            <dd>{{ $job->type->description }}</dd>
                        </dl>
                    </div>
                    <div class="job__detail-2-item">
                        <dl>
                            <dt>Kategory</dt>
                            <dd>{{ $job->job_function->name }}</dd>

                            <dt>Pengalaman Kerja</dt>
                            <dd>{{ $job->work_experience->description }}</dd>
                        </dl>
                    </div>
                </div>

                <div class="job__detail-acc">
                    <button class="accordion"><span class="text">Skills</span><span class="icon"><i class="icon-arrow-down-2"></i></span></button>
                    <div class="accordion__panel">
                        <div class="job__skills">
                            <div class="job__skills-item must-skills">
                                <p>Skill Wajib:</p>
                                <ul>
                                    @foreach ($job->must_skills as $skill)
                                        <li>{{ $skill->name }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="job__skills-item nice-skills">
                                <p>Skill Opsional:</p>
                                <ul>
                                    @foreach ($job->nice_skills as $skill)
                                        <li>{{ $skill->name }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        
                    </div>
                    
                    <button class="accordion"><span class="text">Gaji</span><span class="icon"><i class="icon-arrow-down-2"></i></span></button>
                    <div class="accordion__panel">
                        <div class="job__salary">
                            <div class="job__salary-item common-salary">
                                <dl>
                                    <dt>Gaji Pokok</dt>
                                    <dd>{{ $job->salary_currency }} {{ $job->formatted_salary_min }} - {{ $job->formatted_salary_max }}</dd>

                                    <dt>Durasi Gaji</dt>
                                    <dd>{{ $job->salary_duration->description }}</dd>
                                </dl>
                            </div>
                            <div class="job__salary-item bonus-salary">
                                <dl>
                                    <dt>Gaji Bonus</dt>
                                    <dd>
                                        @if ($job->bonus_salary)
                                            {{ $job->salary_currency }} {{ $job->formatted_salary_min }} - {{ $job->formatted_salary_max }}
                                        @else
                                            Tidak ada
                                        @endif
                                    </dd>

                                    @if ($job->bonus_salary)
                                        <dt>Durasi Gaji Bonus</dt>
                                        <dd>{{ $job->bonus_salary_duration->description }}</dd>
                                    @endif
                                </dl>
                            </div>
                        </div>
                        
                    </div>
                    
                    <button class="accordion"><span class="text">Deskripsi</span><span class="icon"><i class="icon-arrow-down-2"></i></span></button>
                    <div class="accordion__panel">
                        {!! $job->description !!}
                    </div>
                </div>
            </div>

            <div class="job-company">
                <div class="job-company__inner">
                    <div class="job-company__bookmark"><i class="icon-bookmark"></i></div>
                    <div class="job-company__logo">
                        <div class="logo-sq">
                            <img src="{{ $job->company->logo_url }}" alt="{{ $job->company->name }}" class="img-fluid">
                        </div>
                    </div>
                    <div class="job-company__name">
                        <a href="{{ route('company.index', $job->company->slug) }}" title="{{ $job->company->name }}">{{ $job->company->name }}</a>
                    </div>
                    <div class="job-company__city">{{ $job->city_name }}</div>
                    <div class="job-company__website">
                        <a href="{{ $job->company->website_url }}" title="{{ $job->company->name }}">
                            {{ $job->company->website_url }}
                        </a>
                    </div>
                    @if ($jap['applied'])
                        <div class="center mt-2">
                            <h3>Sudah Melamar</h3>
                            <p>Status: {{ $jap['status']->description }}</p>
                        </div>
                        <div class="job-company__button">
                            <form method="POST" id="cancel-job-form" action="{{ route('workjob.cancel-application', ['slug' => $job->slug, 'identifier' => $job->identifier]) }}">
                                @csrf
                                <button 
                                    type="submit" 
                                    class="btn btn--primary btn--block submit-confirm"
                                    data-title="Batalkan Lamaran?"
                                    data-description="Anda akan membatalkan lamaran pekerjaan."
                                    data-accept-text="Ya, Batalkan!"
                                    data-form-id="cancel-job-form" 
                                    title="Apply"
                                >Batalkan Lamaran</button>
                            </form>
                        </div>
                    @else
                        <div class="job-company__button">
                            <form method="POST" id="apply-job-form" action="{{ route('workjob.apply', ['slug' => $job->slug, 'identifier' => $job->identifier]) }}">
                                @csrf
                                <button 
                                    type="submit" 
                                    class="btn btn--primary btn--block submit-confirm" 
                                    data-title="Lamar Pekerjaan?" 
                                    data-description="Anda akan melamar pekerjaan ini."
                                    data-accept-text="Ya, Lanjutkan" 
                                    data-form-id="apply-job-form" 
                                    title="Apply"
                                >Apply</button>
                            </form>
                        </div>
                    @endif
                    
                </div>
            </div>
        </div>

        
    </div>
@endsection
