@extends('common.reqruit.layouts.app', ['header_type' => 'light'])

@section('content')
    <div class="container">
        <div class="clearfix mxn2">
            <div class="sm-col-12 md-col-10 lg-col-8 px2 mx-auto">
                <h1 class="page-title center">Jawab pertanyaan dibawah ini!</h1>
                <workjob-questions 
                    :questions="{{ json_encode($questions) }}"
                    :job="{{ json_encode($job) }}"
                ></workjob-questions>
            </div>
        </div>
    </div>
@endsection
