<div class="job-card">
    <div class="job-card__main">
        <div class="job-card__logo">
            <div>&nbsp;</div>
            <img
                src="{{ $job->company->logo_url }}"
                alt="{{ $job->company->name }}"
                class="img-fluid"
            />
        </div>
        <div class="job-card__info">
            <div class="job-card__title">
                <a
                    href="{{ route('workjob.index', ['slug' => $job->slug, 'identifier' => $job->identifier]) }}"
                    title="{{ $job->title }}"
                >
                    {{ $job->title }}
                </a>
            </div>
            <div class="job-card__company">
                <a
                    href="{{ route('company.index', ['slug' => $job->company->slug]) }}"
                    title="{{ $job->company->name }}"
                >
                    {{ $job->company->name }}
                </a>
            </div>
            <div class="job-card__location">
                <i class="icon-location"></i>
                <span>{{ $job->city_name }}</span>
            </div>
            <div class="job-card__salary">
                <span
                    >{{ $job->salary_currency }}
                    {{ $job->formatted_salary_min  }}
                    -
                    {{ $job->formatted_salary_max }}</span
                >
            </div>
        </div>
    </div>
    <div class="job-card__created-time">
        <i class="icon-time-circle"></i>
        <span>{{ $job->created_at }}</span>
    </div>
    {{-- <div class="job-card__bookmark">
        <i class="icon-bookmark"></i>
    </div> --}}
</div>