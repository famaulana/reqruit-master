@extends('common.reqruit.layouts.app', ['header_type' => 'light'])

@section('content')
    <profile-view 
        :user="{{ json_encode($user) }}"
    ></profile-view>
@endsection
