@extends('common.reqruit.layouts.app', ['header_type' => 'light'])

@section('content')
    {{-- <nav id="breadcrumb" aria-label="breadcrumb" class="d-none d-lg-block">
        <ol class="breadcrumb container">
            <li class="breadcrumb-item">
                <a href="{{ url('/') }}" title="Home">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <a href="{{ route('page', $page->slug) }}" title="{{ $page->title }}">{{ $page->title }}</a>
            </li>
        </ol>
    </nav> --}}

    <main class="container page py-4">
        <div class="title">
            <h1>{{ $page->title }}</h1>
        </div>

        <article>{!! $page->content !!}</article>
    </main>
@endsection