@extends('common.reqruit.layouts.app', ['header_type' => 'dark'])

@section('body_class', 'homepage')

@section('content')
    <main class="home">
        <section class="home__top">
            <div class="home__job-search job-search flex flex-column">
                <h2 class="job-search__headline">
                    Temukan pekerjaan <span class="color-secondary">favorit Anda</span> <br>
                    dan buat hidupmu bahagia
                </h2>
                <p class="job-search__small-text">
                    Dengan berbagai macam pekerjaan dan berbagai kategori <br>
                    yang bisa Anda temukan
                </p>
                <div class="job-search__form">
                    <form action="{{ route('explore-jobs') }}" method="GET" class="form">
                        <div class="form__group">
                            <div class="input-group input-group--white input-group--lg">
                                <div class="input-group__prepend input-group__prepend--icon">
                                    <i class="icon-search"></i>
                                </div>
                                <div class="input-group__main">
                                    <input type="text" class="form__control form__control--bg-white form__control--lg" name="keyword" placeholder="Cari Pekerjaan, Perusahaan atau Kata Kunci...">
                                </div>
                                <div class="input-group__append">
                                    <button type="submit" class="btn btn--lg btn--warning btn--bold">Cari Pekerjaan</button>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="form__group-inline">
                            <div class="form__inline">
                                <div class="select form__control form__control--bg-white">
                                    <select name="job-category" id="job-category">
                                        <option value="">Kategori</option>
                                    </select>
                                    <span class="focus"></span>
                                </div>
                            </div>
                            <div class="form__inline">
                                <div class="select form__control form__control--bg-white">
                                    <select name="job-location" id="job-location">
                                        <option value="">Lokasi</option>
                                    </select>
                                    <span class="focus"></span>
                                </div>
                            </div>
                            <div class="form__inline">
                                <div class="select form__control form__control--bg-white">
                                    <select name="job-salary" id="job-salary">
                                        <option value="">Gaji</option>
                                    </select>
                                    <span class="focus"></span>
                                </div>
                            </div>
                        </div> --}}
                    </form>

                    <a href="{{ route('explore-jobs') }}" class="job-search__button-mobile btn btn--lg btn--long btn--white" title="Cari Pekerjaan">Cari Pekerjaan</a>
                </div>
            </div>
            <div class="home__top-picture">
                <div class="home__top-picture-inner">
                    <img src="{{ url('images/reqruit/hero-banner.png') }}" alt="Temukan pekerjaan favorit" class="img-fluid">
                </div>
            </div>
        </section>

        <section class="home__secondary home-sec">
            <div class="home-sec__item flex flex-column items-center">
                <div class="home-sec__icon sprite-icons sprite-icons--laptop">&nbsp;</div>
                <div class="home-sec__text center">Web Programming</div>
            </div>
            <div class="home-sec__item flex flex-column items-center">
                <div class="home-sec__icon sprite-icons sprite-icons--phone">&nbsp;</div>
                <div class="home-sec__text center">Mobile Programming</div>
            </div>
            <div class="home-sec__item flex flex-column items-center">
                <div class="home-sec__icon sprite-icons sprite-icons--monitor">&nbsp;</div>
                <div class="home-sec__text center">Grafis & Desain</div>
            </div>
            <div class="home-sec__item flex flex-column items-center">
                <div class="home-sec__icon sprite-icons sprite-icons--typewriter">&nbsp;</div>
                <div class="home-sec__text center">Penulisan & Penerjemahan</div>
            </div>
        </section>

        <section class="section home__develop home-develop">
            <div class="container">
                <h2 class="section__title">Kembangkan Karirmu Disini Bersama Kami</h2>
                <div class="section__inner">
                    <div class="home-develop__item">
                        <div class="flex-auto home-develop__text">
                            <h3>Cari Pekerjaan <br> Hanya dengan Satu Klik</h3>
                            <p>Dengan adanya fitur cari beserta filter yang akurat, membuat anda mudah dan efisien dalam pemanfaatan waktu dalam mencari pekerjaan</p>
                        </div>
                        <div class="home-develop__image">
                            <img src="{{ url('images/reqruit/content-images/homepage-male-ppl.png') }}" class="img-fluid" alt="Male">
                        </div>
                    </div>

                    <div class="home-develop__item">
                        <div class="home-develop__image">
                            <img src="{{ url('images/reqruit/content-images/homepage-female-ppl.png') }}" class="img-fluid" alt="Female">
                        </div>
                        <div class="flex-auto home-develop__text right-align">
                            <h3>Beragam Kategori Tersedia <br> Untuk Memudahkan Pencarianmu</h3>
                            <p>Dengan beragam kategori yang kami sediakan anda mudah untuk menyesuaikan dengan keahlian anda</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section home__service home-service">
            <div class="container">
                <h2 class="section__title">Layanan Kami</h2>

                <div class="section__inner">
                    
                    <div class="home-service__item-wrapper">
                        <div class="home-service__item">
                            <div class="home-service__text">
                                <h3>Tolak Ukur Gaji</h3>
                                <p>Fitur untuk memeriksa dan membandingkan kisaran gaji di berbagai lokasi, fungsi pekerjaan, dan industri.</p>
                            </div>
                            <div class="home-service__image">
                                <img src="{{ url('images/reqruit/icons-color/chart-board.png') }}" alt="Chart Board" class="img-fluid">
                            </div>
                        </div>
                        <div class="home-service__item">
                            <div class="home-service__text">
                                <h3>Branding Perusahaan</h3>
                                <p>Untuk memperkaya pengalaman pengguna dan menarik lebih banyak kandidat, kami membuat halaman khusus untuk setiap perusahaan.</p>
                            </div>
                            <div class="home-service__image">
                                <img src="{{ url('images/reqruit/icons-color/rocket-lamp.png') }}" alt="Rocket Lamp" class="img-fluid">
                            </div>
                        </div>
                    </div>

                    <div class="home-service__item-wrapper">
                        <div class="home-service__item">
                            <div class="home-service__image">
                                <img src="{{ url('images/reqruit/icons-color/chart-board.png') }}" alt="Chart Board" class="img-fluid">
                            </div>
                            <div class="home-service__text">
                                <h3>Tolak Ukur Gaji</h3>
                                <p>Fitur untuk memeriksa dan membandingkan kisaran gaji di berbagai lokasi, fungsi pekerjaan, dan industri.</p>
                            </div>
                        </div>
                        <div class="home-service__item">
                            <div class="home-service__image">
                                <img src="{{ url('images/reqruit/icons-color/rocket-lamp.png') }}" alt="Rocket Lamp" class="img-fluid">
                            </div>
                            <div class="home-service__text">
                                <h3>Branding Perusahaan</h3>
                                <p>Untuk memperkaya pengalaman pengguna dan menarik lebih banyak kandidat, kami membuat halaman khusus untuk setiap perusahaan.</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        @if ($about_content != '')
            <section class="section home__about home-about">
                <div class="container">
                    <h2 class="section__title section__title--left">Tentang Kami</h2>

                    <div class="section__inner">
                        
                        <div class="home-about__text">
                            {!! $about_content !!}
                            @if ($about_btn_link != '')
                                <a href="{{ $about_btn_link }}" class="btn btn--lg btn--long btn--primary"><span>Selengkapnya</span> <i class="icon-arrow-right-circle"></i></a>
                            @endif
                        </div>

                        @if ($about_yt_embed != '' && $about_video_image_url != '')
                            <div class="home-about__video-wrapper">
                                <div class="home-about__video">
                                    <img src="{{ $about_video_image_url }}" alt="About us video" class="img-fluid">
                                    <div class="home-about__play-video" data-embed="{{ $about_yt_embed }}">
                                        <div class="icon-wrapper"><span>&#9658;</span></div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </section>
        @endif

        @if ($job_applications_count > 10 && $companies_count > 9 && $jobs_count > 15)
            <section class="section home__numbers home-numbers">
                <div class="container">
                    <div class="section__inner">
                        
                        <div class="home-numbers__item">
                            <div class="home-numbers__big-text">{{ $job_applications_count }}+</div>
                            <div class="home-numbers__small-text">Pelamar Kerja</div>
                        </div>

                        <div class="home-numbers__item">
                            <div class="home-numbers__big-text">{{ $companies_count }}+</div>
                            <div class="home-numbers__small-text">Perusahaan</div>
                        </div>

                        <div class="home-numbers__item">
                            <div class="home-numbers__big-text">{{ $jobs_count }}+</div>
                            <div class="home-numbers__small-text">Pekerjaan</div>
                        </div>

                    </div>
                </div>
            </section>
        @endif

        @if (!is_null($featured_jobs))
            <section class="section home__ft-jobs home-ft-jobs">
                <div class="container">
                    <h2 class="section__title section__title--full">Pekerjaan Yang Mungkin Kamu Tertarik</h2>

                    <div class="section__inner">
                        @foreach ($featured_jobs as $job)
                            <div class="job-list">
                                <div class="job-list__top">
                                    <div class="job-list__top-right">
                                        <div class="flex items-center">
                                            <div class="logo-sq mr2">
                                                <img src="{{ $job->company->logo_url }}" alt="{{ $job->company->name }}" class="img-fluid">
                                            </div>
                                            <div class="job-list__details">
                                                <div class="job-list__title">{{ $job->title }}</div>
                                                <div class="job-list__company">{{ $job->company->name }}</div>
                                                <div class="job-list__published-time">Posted {{ $job->created_at->diffForHumans() }}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="job-list__top-left">
                                        <div class="job-list__info-wrapper">
                                            <div class="job-list__info">
                                                <div class="job-list__info-title">Kategori</div>
                                                <div class="job-list__info-desc">{{ $job->job_function->name }}</div>
                                            </div>
                                            <div class="job-list__info">
                                                <div class="job-list__info-title">Lokasi</div>
                                                <div class="job-list__info-desc">{{ $job->city->name }}</div>
                                            </div>
                                        </div>
                                        <div class="job-list__button">
                                            <a href="{{ route('workjob.index', ['slug' => $job->slug, 'identifier' => $job->identifier]) }}" class="btn btn--primary btn--block" title="Info Lanjutan">Info Lanjutan</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </section>
        @endif

        {{-- <section class="section home__blog-posts home-blog-posts">
            <div class="container">
                <h2 class="section__title section__title--full">Blog Karir</h2>

                <div class="section__inner">
                    
                    <div class="blog-list clearfix mxn1">
                        <div class="blog-list__item col sm-col-12 md-col-4 px1">
                            <div class="blog-list__image">
                                <a href="#">
                                    <img src="https://images.pexels.com/photos/2188870/pexels-photo-2188870.png?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" alt="" class="img-fluid">
                                </a>
                            </div>
                            <h3 class="blog-list__title">
                                <a href="#">Bagaimana membangun startup sendiri ?</a>
                            </h3>
                            <div class="blog-list__excerpt">
                                Memiliki sebuah bisnis startup sendiri menjadi
                                salah satu impian banyak orang. Hanya saja, 
                                masih banyak orang, termasuk kamu dan calon entrepreneur muda.
                            </div>
                            <div class="blog-list__button">
                                <a href="#" class="btn btn--primary"><span class="mr1">Selengkapnya</span> <i class="icon-arrow-right-circle"></i></a>
                            </div>
                        </div>

                        <div class="blog-list__item col sm-col-12 md-col-4 px1">
                            <div class="blog-list__image">
                                <a href="#">
                                    <img src="https://images.pexels.com/photos/3439818/pexels-photo-3439818.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" alt="" class="img-fluid">
                                </a>
                            </div>
                            <h3 class="blog-list__title">
                                <a href="#">Bagaimana menjadi pekerja professional ?</a>
                            </h3>
                            <div class="blog-list__excerpt">
                                Bekerja secara preofesional memberikan banyak keuntungan. Tidak hanya dihormati oleh rekan dan atasan Anda, sikap profesional membantu peningkatan karir dalam pekerjaan.
                            </div>
                            <div class="blog-list__button">
                                <a href="#" class="btn btn--primary"><span class="mr1">Selengkapnya</span> <i class="icon-arrow-right-circle"></i></a>
                            </div>
                        </div>

                        <div class="blog-list__item col sm-col-12 md-col-4 px1">
                            <div class="blog-list__image">
                                <a href="#">
                                    <img src="https://images.pexels.com/photos/5326967/pexels-photo-5326967.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" alt="" class="img-fluid">
                                </a>
                            </div>
                            <h3 class="blog-list__title">
                                <a href="#">Relate mana antara freelance & Businessman ?</a>
                            </h3>
                            <div class="blog-list__excerpt">
                                Ketika orang berpikir tentang seorang wirausahawan, sering kali mereka membayangkan pendiri startup yang baru diluncurkan, pemilik bisnis veteran, atau seseorang yang menjalankan...
                            </div>
                            <div class="blog-list__button">
                                <a href="#" class="btn btn--primary"><span class="mr1">Selengkapnya</span> <i class="icon-arrow-right-circle"></i></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section> --}}

        <section class="section home__join home-join">
            <div class="home-join__image">
                <img src="{{ url('images/reqruit/content-images/join-us-banner.png') }}" alt="Join us" class="img-fluid">
            </div>
            <div class="home-join__cta center">
                <h3>Ayo Bergabung Bersama Kami!</h3>
                <a href="{{ route('auth.user.register.form') }}" class="btn btn--lg btn---long btn--primary" title="Daftar">Daftar</a>
            </div>
        </section>

        @if (!is_null($testimonials))
            <section class="section home__testi home-testi">
                <div class="container">
                    <h2 class="section__title section__title--full">Apa Saja Kata Klien Kami</h2>

                    <div class="section__inner">
                        <div id="home-testi-slider" class="glide">
                            <div class="glide__track" data-glide-el="track">
                                <ul class="glide__slides">
                                    @foreach ($testimonials as $testi)
                                        <li class="glide__slide">
                                            <div class="testi p3">
                                                <div class="testi__top flex items-center">
                                                    <div class="testi__avatar">
                                                        <img src="{{ $testi->avatar_url }}" alt="{{ $testi->name }}" class="img-fluid">
                                                    </div>
                                                    <div class="testi__user-detail">
                                                        <div class="testi__user-name">{{ $testi->name }}</div>
                                                        <div class="testi__user-job">{{ $testi->profession }}</div>
                                                    </div>
                                                </div>
                                                <div class="testi__content">{{ $testi->content }}</div>
                                                <div class="testi__rating flex items-center">
                                                    @include('common.reqruit.share.rating-star.rating-'.$testi->rating)
                                                    <div class="testi__rating-score">{{ $testi->rating }}/10</div>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>

                            <div class="glide__arrows" data-glide-el="controls">
                                <button class="glide__arrow glide__arrow--left" data-glide-dir="<">
                                    <svg>
                                        <image xlink:href="{{ url('images/reqruit/icons/slider/slide-arrow-left.svg') }}" alt="rating-dimmed" />
                                    </svg>
                                </button>
                                <button class="glide__arrow glide__arrow--right" data-glide-dir=">">
                                    <svg>
                                        <image xlink:href="{{ url('images/reqruit/icons/slider/slide-arrow-right.svg') }}" alt="rating-dimmed" />
                                    </svg>
                                </button>
                            </div>

                            <div class="glide__bullets" data-glide-el="controls[nav]">
                                @for ($i = 0; $i < count($testimonials); $i++)
                                    <button class="glide__bullet" data-glide-dir="={{ $i }}"></button>
                                @endfor
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif
        

        <section class="section home__pre-footer home-pre-footer">
            <div class="container">
                <div class="section__inner">
                    <div class="home-pre-footer__cta">
                        <span>Ayo, Bangun</span><br>
                        <span class="text-strong">Karir Favoritmu</span>
                    </div>
                    <div class="home-pre-footer__button flex justify-center">
                        <a href="{{ route('explore-jobs') }}" class="btn btn--lg btn--long btn--primary" title="Cari Kerja">Cari Kerja</a>
                    </div>
                </div>
            </div>
        </section>

    </main>
@endsection