@extends('common.reqruit.employer.account.account-layout')

@section('account-content')
    <h1 class="page-title">Kelola Pekerjaan</h1>
    <div class="mb-4">
        <a href="{{ route('account-empl.workjob.create') }}" class="btn btn-outline btn-outline--primary" title="Pekerjaan Baru"><i class="icon-plus"></i> Pekerjaan Baru</a>
    </div>
    <employer-company-jobs :data="{{ json_encode($data) }}"></employer-company-jobs>
@endsection
