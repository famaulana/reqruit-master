@extends('common.reqruit.employer.account.account-layout')

@section('account-content')
    <h1 class="page-title">Kelola Tim Perusahaan</h1>
    <company-team :team="{{ json_encode($team) }}"></company-team>
@endsection
