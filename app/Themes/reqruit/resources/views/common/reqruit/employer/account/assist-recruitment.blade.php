@extends('common.reqruit.layouts.app', ['header_type' => 'light'])

@section('content')
    <div class="container">
        <h1 class="center"></h1>
        <div class="clearfix mxn2">
            <div class="md-col-10 lg-col-7 px2 mx-auto">
                @if (is_null($employer->self_recruitment))
                    <div class="card">
                        <div class="card__header">
                            <h2 class="card__header-title">What position do you need?</h2>
                        </div>
                        <div class="card__body">
                            <form action="{{ route('account-empl.assist-recruitment.post') }}" method="POST" class="form">
                                @csrf
                                <div class="form__group">
                                    <label for="position" class="form__label">Position</label>
                                    <input type="text" class="form__control" name="position" id="position" value="{{ old('position') }}">
                                    {!! $errors->first('position', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                                </div>
                                <div class="form__group">
                                    <label for="more_info" class="form__label">More Informations</label>
                                    <textarea rows="5" class="form__control" name="more_info" id="more_info" value="{{ old('more_info') }}"></textarea>
                                    {!! $errors->first('more_info', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                                </div>
                                <button type="submit" class="btn btn--block btn--primary">Submit</button>
                            </form>                        
                        </div>
                    </div>
                @else
                    <div class="card card--success">
                        <div class="card__body">
                            Thank you, please wait our consultant will contact you.
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
