@extends('common.reqruit.layouts.app', ['header_type' => 'light'])

@section('content')
    <div class="employer-page">
        <div class="container">
            <div class="clearfix">
                <nav class="simple-side-nav employer-page__nav col col-12 sm-col-12 md-col-3 lg-col-3">
                    <ul>
                        <li class="nav-link {{ (Request::is('employer/dashboard*')) ? 'active' : '' }}">
                            <a href="{{ route('account-empl.dashboard') }}" title="Dashboard"><i class="icon-category"></i> Dashboard</a>
                        </li>
                        <li class="nav-link {{ (Request::is('employer/company*')) ? 'active' : '' }}">
                            <a href="{{ route('account-empl.company.index') }}" title="Perusahaan"><i class="icon-home"></i> Perusahaan</a>
                        </li>
                        <li class="nav-link {{ (Request::is('employer/team*')) ? 'active' : '' }}">
                            <a href="{{ route('account-empl.team.index') }}" title="Team"><i class="icon-3-user"></i> Tim Perusahaan</a>
                        </li>
                        <li class="nav-link {{ (Request::is('employer/workjob*')) ? 'active' : '' }}">
                            <a href="{{ route('account-empl.workjob.index') }}" title="Pekerjaan"><i class="icon-work"></i> Pekerjaan</a>
                        </li>
                        <li class="nav-link {{ (Request::is('employer/job-applications*')) ? 'active' : '' }}">
                            <a href="{{ route('account-empl.job-applications.index') }}" title="Pelamar"><i class="icon-document"></i> Pelamar</a>
                        </li>
                    </ul>
                </nav>
                <div class="employer-page__content col col-12 sm-col-12 md-col-9 lg-col-9">
                    @yield('account-content')
                </div>
            </div>
        </div>

        <div class="employer-page__bg">
            <img src="{{ url('/images/reqruit/content-images/employer-page-bg.png') }}" alt="Building" class="img-fluid">
        </div>
    </div>
@endsection
