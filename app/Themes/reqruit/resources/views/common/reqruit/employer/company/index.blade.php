@extends('common.reqruit.employer.account.account-layout')

@section('account-content')
    <h1 class="page-title">Kelola Perusahaan</h1>
    <manage-company 
        :company="{{ json_encode($company) }}"
        :industries="{{ json_encode($industries) }}"
    ></manage-company>
@endsection
