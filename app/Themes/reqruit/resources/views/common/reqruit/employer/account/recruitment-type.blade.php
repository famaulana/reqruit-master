@extends('common.reqruit.layouts.app', ['header_type' => 'light'])

@section('content')
    <div class="container">
        <h1 class="center">How you recruit?</h1>
        <div class="req-type">
            <div class="card card--border-info req-type__item">
                <div class="card__body">
                    <h2 class="req-type__title">Do Recruitment by Yourself</h2>
                    <div class="req-type__content">{!! $selfReqBlock->content !!}</div>
                    <div class="req-type__cta">
                        <form action="{{ route('account-empl.self-recruitment') }}" method="POST">
                            @csrf
                            <button type="submit" class="btn btn--block btn--info">Start Now</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="card card--border-success req-type__item">
                <div class="card__body">
                    <h2 class="req-type__title">Let Us Help You to Do the Recruitment</h2>
                    <div class="req-type__content">{!! $assistReqBlock->content !!}</div>
                    <div class="req-type__cta">
                        <a href="{{ route('account-empl.assist-recruitment') }}" class="btn btn--block btn--success">Start Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
