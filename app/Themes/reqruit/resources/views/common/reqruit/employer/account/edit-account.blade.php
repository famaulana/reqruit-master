@extends('common.reqruit.layouts.app', ['header_type' => 'light'])

@section('content')
    <employer-account 
        :employer="{{ json_encode($employer) }}"
    ></employer-account>
@endsection
