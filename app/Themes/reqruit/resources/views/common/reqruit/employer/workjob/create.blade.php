@extends('common.reqruit.layouts.app', ['header_type' => 'light'])

@section('content')
    <div class="container">
        <h1 class="page-title">Pekerjaan Baru</h1>
        <employer-create-job :option_select="{{ json_encode($optionSelect) }}"></employer-create-job>
    </div>
@endsection
