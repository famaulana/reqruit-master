@extends('common.reqruit.layouts.app', ['header_type' => 'light'])

@section('content')
    <div class="container">
        <h1 class="page-title">Edit Pekerjaan</h1>
        <employer-edit-job 
            :workjob="{{ json_encode($workjob) }}"
            :option_select="{{ json_encode($optionSelect) }}"
        ></employer-edit-job>
    </div>
@endsection
