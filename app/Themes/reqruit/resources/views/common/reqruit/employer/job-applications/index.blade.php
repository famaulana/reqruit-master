@extends('common.reqruit.layouts.app', ['header_type' => 'light'])

@section('content')
    <div class="container">
        <h1 class="page-title">Kelola Pelamar</h1>
        <div class="mb-4">
            <a href="{{ route('account-empl.dashboard') }}" class="btn btn--primary" title="Dashboard"><i class="icon-arrow-left"></i> Dashboard</a>
        </div>
        <employer-job-applications 
            :option_select="{{ json_encode($option_select) }}"
            :job_applications_data="{{ json_encode($jobApplications) }}"
            :workjob="{{ json_encode($workjob) }}"
        ></employer-job-applications>
    </div>
@endsection
