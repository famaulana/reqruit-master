@extends('common.reqruit.layouts.app', ['header_type' => 'light'])

@section('content')
    <div class="container">
        <explore-jobs :data="{{ json_encode($data) }}"></explore-jobs>
    </div>
@endsection
