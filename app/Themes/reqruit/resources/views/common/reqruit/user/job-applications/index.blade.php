@extends('common.reqruit.layouts.app', ['header_type' => 'light'])

@section('content')
    <user-job-applications 
        :data="{{ json_encode($data) }}"
    ></user-job-applications>
@endsection
