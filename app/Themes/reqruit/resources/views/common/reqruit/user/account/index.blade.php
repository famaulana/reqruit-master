@extends('common.reqruit.layouts.app', ['header_type' => 'light'])

@section('content')
    <user-profile 
        :user="{{ json_encode($user) }}"
    ></user-profile>
@endsection
