@extends('common.reqruit.user.account.account-layout')

@section('account-content')
    <div class="title-section">
        <div class="title">
            <h1>Edit Profile</h1>
        </div>
        <div class="line">&nbsp;</div>
    </div>

    <div class="account-content">
        <user-profile :user="{{ json_encode($user) }}"></user-profile>
    </div>
@endsection
