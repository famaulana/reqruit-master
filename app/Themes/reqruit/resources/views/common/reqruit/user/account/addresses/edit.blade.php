@extends('common.reqruit.user.account.account-layout')

@section('account-content')
    <div class="title-section">
        <div class="title">
            <h1>Edit Alamat</h1>
        </div>
        <div class="line">&nbsp;</div>
    </div>

    <div class="account-content">
        <address-edit :address="{{ json_encode($address) }}"></address-edit>
    </div>
@endsection
