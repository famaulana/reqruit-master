@extends('common.reqruit.layouts.app', ['header_type' => 'light'])

@section('content')
    <user-overview 
        :user="{{ json_encode($user) }}"
        :job_statuses="{{ json_encode($jobStatuses) }}"
        :relationships="{{ json_encode($relationships) }}"
        :job_types="{{ json_encode($jobTypes) }}"
        :jobfunctions="{{ json_encode($jobfunctions) }}"
        :education_degrees="{{ json_encode($educationDegrees) }}"
        :education_fields="{{ json_encode($educationFields) }}"
        :skills="{{ json_encode($skills) }}"
        :all_cities="{{ json_encode($allCities) }}"
    ></user-overview>
@endsection
