Vue.component("UserOverview", require("./UserOverview.vue").default);
Vue.component("UserProfile", require("./UserProfile.vue").default);
Vue.component(
    "UserJobApplications",
    require("./UserJobApplications.vue").default
);
