// Layout
require("./Layout/layout");

// Auth
require("./Auth/auth");

// Account
require("./Account/account");

// Employer
require("./Employer/employer");

// Explore Jobs
require("./ExploreJobs/explore-jobs");

// Profile
require("./Profile/profile");

// Workjob
require("./Workjob/workjob");

// Share
require("./Share/share");
