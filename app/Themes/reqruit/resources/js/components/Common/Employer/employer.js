Vue.component("EmployerAccount", require("./EmployerAccount.vue").default);
Vue.component("ManageCompany", require("./Company/ManageCompany.vue").default);
Vue.component("CompanyTeam", require("./Team/CompanyTeam.vue").default);
Vue.component(
    "EmployerCompanyJobs",
    require("./Workjob/EmployerCompanyJobs.vue").default
);
Vue.component(
    "EmployerCreateJob",
    require("./Workjob/EmployerCreateJob.vue").default
);
Vue.component(
    "EmployerEditJob",
    require("./Workjob/EmployerEditJob.vue").default
);
Vue.component(
    "EmployerJobApplications",
    require("./JobApplication/EmployerJobApplications.vue").default
);
