// window._ = require("lodash");
window.axios = require("axios");
window.Vue = require("vue");
require("./actions/slider");
require("./actions/header");
require("./actions/accordion");
require("./actions/modal");
require("./actions/smooth-scroll");
require("./actions/tab");
require("./actions/homepage");
require("./actions/form");

import VueSweetalert2 from "vue-sweetalert2";
import Vuelidate from "vuelidate";
// import Vuex from "vuex";
import DatePick from "vue-date-pick";
import vSelect from "vue-select";
import CKEditor from "@ckeditor/ckeditor5-vue";
import draggable from "vuedraggable";
import VueSilentbox from "vue-silentbox";

// Add CSRF to axios
window.axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";

// lodash
// Vue.prototype._ = _;

Vue.use(VueSweetalert2);
Vue.use(Vuelidate);
// Vue.use(Vuex);
Vue.use(CKEditor);
Vue.component("date-pick", DatePick);
Vue.component("v-select", vSelect);
Vue.component("draggable", draggable);
Vue.use(VueSilentbox);

// Components
require("./components/Common/common");

// filters
require("../../../../../resources/js/filters");

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: "#app"
});
