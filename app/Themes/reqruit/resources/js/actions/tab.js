import $ from "jquery";

$(document).ready(function() {
    $(document).on("click", ".tab__nav-item", function() {
        $(this)
            .parent("ul")
            .children("li")
            .removeClass("active");
        $(this)
            .parent("ul")
            .parent(".tab__nav")
            .next(".tab__content")
            .children(".tab__content-item")
            .removeClass("active");
        $(this).addClass("active");
        $($(this).data("target")).addClass("active");
    });
});
