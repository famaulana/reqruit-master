import $ from "jquery";

$(document).ready(function() {
    $(".modal-trigger").on("click", function(e) {
        e.preventDefault();
        $($(this).data("target")).addClass("show");
    });

    $(".close-modal").on("click", function() {
        $(this)
            .closest(".c-modal")
            .removeClass("show");
    });

    $(".c-modal").on("click", function(e) {
        if (e.target !== this) {
            return;
        }

        $(this).removeClass("show");
    });
});
