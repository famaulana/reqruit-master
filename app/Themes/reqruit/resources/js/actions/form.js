import $ from "jquery";
import Swal from "sweetalert2";

$(document).ready(function() {
    $(".submit-confirm").on("click", function(e) {
        e.preventDefault();

        Swal.fire({
            title: $(this).data("title"),
            text: $(this).data("description"),
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: $(this).data("accept-text")
        }).then(result => {
            if (result.isConfirmed) {
                let form_id = $(this).data("form-id");
                $(`#${form_id}`).submit();
            }
        });
    });
});
