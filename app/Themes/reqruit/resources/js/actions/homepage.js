import $ from "jquery";
import tingle from "tingle.js";

$(document).ready(function() {
    // Modal video about
    // instanciate new modal
    var modal = new tingle.modal({
        closeMethods: ["overlay", "button", "escape"],
        closeLabel: "Close",
        cssClass: ["youtube-embed"]
    });

    // open modal
    $(".home-about__play-video").on("click", function() {
        modal.setContent($(this).data("embed"));
        modal.open();
    });
});
