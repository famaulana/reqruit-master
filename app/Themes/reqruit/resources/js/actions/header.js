import $ from "jquery";

$(document).ready(function() {
    // Show hide search form
    // $(".icon__search").click(function() {
    //     $(".search-block").toggleClass("active");
    //     $(this)
    //         .children(".fa-search")
    //         .toggle();
    //     $(this)
    //         .children(".fa-times")
    //         .toggle();
    // });

    // Show hide mobile navigation
    $("#mobile-nav-toggle").click(function() {
        $(".header__top-menu").toggleClass("active");
        $(this).toggleClass("active");
    });

    // Show hide sub menu
    // $(".open-sub-menu").click(function() {
    //     var target = $(this).attr("data-target");
    //     $("#" + target).addClass("active");
    // });

    // $(".close-sub-menu").click(function() {
    //     $(this)
    //         .parent(".child-nav")
    //         .removeClass("active");
    // });

    // Auth menu in mobile device
    $(".header__avatar").on("click", function() {
        $(".header__auth-menu-wrapper").addClass("show");
    });

    $(".header__auth-menu-wrapper").on("click", function(e) {
        if (e.target !== this) {
            return;
        }

        $(this).removeClass("show");
    });

    // Logout
    $("#logout-link").on("click", function(e) {
        e.preventDefault();
        $("#logout-form").submit();
    });
});
