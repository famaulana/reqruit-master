import Glide from "@glidejs/glide";
import $ from "jquery";

$(document).ready(function() {
    if ($("#home-testi-slider").length) {
        new Glide("#home-testi-slider", {
            type: "carousel",
            startAt: 0,
            perView: 2,
            gap: 20,
            autoplay: 6000,
            hoverpause: true,
            breakpoints: {
                992: {
                    perView: 1,
                    peek: {
                        before: 150,
                        after: 150
                    }
                },
                768: {
                    perView: 1
                }
            }
        }).mount();
    }
});
