<?php

namespace App\Repositories;

use App\Helpers\PathHelper;
use App\Services\ConfigService;

class AppConfigRepository
{
    /**
     * Get app config
     *
     * @param String $option
     * @return String
     */
    public function getConfig($option)
    {
        $default = config('app_configs_default.' . $option, '');

        return config('app_configs.' . $option, $default);
    }

    /**
     * Get theme config
     *
     * @param String $option
     * @return String
     */
    public function getThemeConfig($option)
    {
        $theme = config('active_theme.domain');
        $default = config('theme_configs.' . $option, '');

        return config('theme.' . $theme . '.' . $option, $default);
    }

    /**
     * get and generate file url
     *
     * @param String $option
     * @return String
     */
    public function getFileUrl(String $option)
    {
        $file = $this->getConfig($option);
        $fileUrl = '';
        if ($file != '') {
            $fileUrl = $this->getUrl($file);
        }

        return $fileUrl;
    }

    // /**
    //  * get and generate file url from theme configuration
    //  *
    //  * @param String $option
    //  * @return String
    //  */
    // public function getThemeFileUrl(String $option)
    // {
    //     $file = $this->getThemeConfig($option);
    //     $fileUrl = '';
    //     if ($file != '') {
    //         $fileUrl = $this->getUrl($file);
    //     }

    //     return $fileUrl;
    // }

    /**
     * Get configuration image file url
     *
     * @param String $file
     * @return String
     */
    public function getUrl(String $file)
    {
        if ($file == '') {
            return;
        }

        $imgPath = PathHelper::config_image_path();
        return url('storage/' . $imgPath . $file);
    }

    /**
     * get and generate all configuration file urls
     *
     * @return String
     */
    public function getFileUrls()
    {
        $configImages = ConfigService::configImages();

        $files = [];
        foreach ($configImages as $option) {
            $file = $this->getConfig($option);
            $fileUrl = '';
            if ($file != '') {
                $fileUrl = $this->getUrl($file);
            }

            $files[$option] = $fileUrl;
        }

        return $files;
    }

    /**
     * Configuration file url that has been assigned
     *
     * @return void
     */
    public function configFileUrls()
    {
        $adminLogo = $this->getConfig('admin_logo');
        $frontendLogo = $this->getConfig('frontend_logo');
        $emailLogo = $this->getConfig('email_logo');
        $invoiceLogo = $this->getConfig('invoice_logo');
        $lightLogo = $this->getConfig('light_logo');
        $darkLogo = $this->getConfig('dark_logo');

        $fileUrls = $this->getFileUrls();

        return [
            'admin_logo' =>  $fileUrls[$adminLogo],
            'frontend_logo' =>  $fileUrls[$frontendLogo],
            'email_logo' =>  $fileUrls[$emailLogo],
            'invoice_logo' =>  $fileUrls[$invoiceLogo],
            'light_logo' =>  $fileUrls[$lightLogo],
            'dark_logo' =>  $fileUrls[$darkLogo],
            'footer_bg_image' =>  $fileUrls['footer_bg_image'],
            'favicon' => $this->getUrl($this->getConfig('favicon')),
            'logo1' => $this->getUrl($this->getConfig('logo1')),
            'logo2' => $this->getUrl($this->getConfig('logo2')),
            'logo3' => $this->getUrl($this->getConfig('logo3')),
            'logo_square' => $this->getUrl($this->getConfig('logo_square'))
        ];
    }
}
