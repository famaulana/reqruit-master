<?php

namespace App\Repositories;

use App\Enums\JobApplicationStatus;
use App\Repositories\Interfaces\RepositoryInterface;
use Illuminate\Http\Request;

class JobApplicationRepository implements RepositoryInterface
{
    /**
     * Admin list request validation rules
     *
     * @param Request $request
     * @return void
     */
    public static function adminListRequestValidation(Request $request)
    {
        $statuses = implode(',', JobApplicationStatus::toArray());

        return $request->validate([
            'keyword' => 'nullable|string',
            'user_name' => 'nullable|string',
            'user_email' => 'nullable|email',
            'job_title' => 'nullable|string',
            'company_name' => 'nullable|string',
            'status' => 'nullable|string|in:'.$statuses,
            'start_date' => 'required_with:end_date|string|date_format:Y-m-d',
            'end_date' => 'required_with:start_date|string|date_format:Y-m-d'
        ]);
    }

    /**
     * Search skill
     *
     * @param Eloquent Model $model
     * @param Array $filters
     * @param String $orderBy
     * @param String $sortOrder
     * @return Eloquent Skill model
     */
    public function search($model, array $filters, string $orderBy = 'created_at', string $sortOrder = 'DESC')
    {
        if (isset($filters['keyword'])) {
            $keyword = $filters['keyword'];

            $model = $model->whereHas('user', function ($query) use ($keyword) {
                $query->where('name', 'LIKE', '%'. $keyword . '%')
                    ->where('email', 'LIKE', '%' . $keyword . '%');
            });

            $model = $model->whereHas('workjob', function ($query) use ($keyword) {
                $query->where('title', 'LIKE', '%'. $keyword . '%');

                $query->whereHas('company', function ($queryCompany) use ($keyword) {
                    $queryCompany->where('name', 'LIKE', '%'.$keyword.'%');
                });
            });
        }

        if (isset($filters['start_date']) && isset($filters['end_date'])) {
            $model = $model->where('created_at', '>=', $filters['start_date'] . ' 00:00:00')
                ->where('created_at', '<=', $filters['end_date'] . ' 23:59:59');
        }

        if (isset($filters['user_name'])) {
            $model = $model->whereHas('user', function ($query) use ($filters) {
                $query->where('name', 'LIKE', '%'. $filters['user_name'] . '%');
            });
        }

        if (isset($filters['user_email'])) {
            $model = $model->whereHas('user', function ($query) use ($filters) {
                $query->where('email', 'LIKE', '%'. $filters['user_email'] . '%');
            });
        }

        if (isset($filters['job_title'])) {
            $model = $model->whereHas('workjob', function ($query) use ($filters) {
                $query->where('title', 'LIKE', '%'. $filters['job_title'] . '%');
            });
        }

        if (isset($filters['company_name'])) {
            $model = $model->whereHas('workjob', function ($query) use ($filters) {
                $query->whereHas('company', function ($queryCompany) use ($filters) {
                    $queryCompany->where('name', 'LIKE', '%'.$filters['company_name'].'%');
                });
            });
        }

        if (isset($filters['status'])) {
            $model = $model->where('status', $filters['status']);
        }

        $model = $model->orderBy($orderBy, $sortOrder);
        
        return $model;
    }
}
