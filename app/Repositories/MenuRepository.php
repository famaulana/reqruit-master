<?php

namespace App\Repositories;

use App\Models\Menu;
use Illuminate\Http\Request;
use App\Http\Resources\MenuResource;
use Illuminate\Support\Facades\Cache;
use App\Repositories\Interfaces\RepositoryInterface;

class MenuRepository implements RepositoryInterface
{
    /**
     * Admin list request validation rules
     *
     * @param Request $request
     * @return void
     */
    public static function adminListRequestValidation(Request $request)
    {
        return $request->validate([
            'keyword' => 'nullable|string',
            'name' => 'nullable|string',
        ]);
    }

    /**
     * Search skill
     *
     * @param Eloquent Model $model
     * @param Array $filters
     * @param String $orderBy
     * @param String $sortOrder
     * @return Eloquent Skill model
     */
    public function search($model, array $filters, string $orderBy = 'created_at', string $sortOrder = 'DESC')
    {
        if (isset($filters['keyword'])) {
            $model = $model->where('name', 'like', '%' . $filters['keyword'] . '%');
        }

        if (isset($filters['name'])) {
            $model = $model->where('name', 'like', '%' . $filters['name'] . '%');
        }

        $model = $model->orderBy($orderBy, $sortOrder);
        
        return $model;
    }

    /**
     * Get menu from cache
     *
     * @param Integer $id
     * @param boolean $resource
     * @return App/Models/Menu or Array
     */
    public function getMenu(Int $id, Bool $resource = false)
    {
        $cacheName = $resource ? 'menu_'.$id.'_resource' : 'menu_'.$id;

        $menu = Cache::remember($cacheName, 22*60, function () use ($id, $resource) {
            $menu = Menu::find($id);
            
            if ($resource) {
                if ($menu === null) {
                    return json_encode([]);
                }

                $menu = new MenuResource($menu);
                return json_encode($menu);
            }

            if ($menu === null) {
                return [];
            }

            return $menu;
        });

        return $menu;
    }

    /**
     * Cache menu
     *
     * @param App\Models\Menu $menu
     * @return void
     */
    public function cacheMenu(Menu $menu)
    {
        $cacheName = 'menu_'.$menu->id;
        Cache::put($cacheName, $menu, 22*60);

        $cacheNameResource = 'menu_'.$menu->id.'_resource';
        $menu = new MenuResource($menu);
        Cache::put($cacheNameResource, json_encode($menu), 22*60);

        return;
    }
}
