<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Repositories\Interfaces\RepositoryInterface;

class HeadHuntingRepository implements RepositoryInterface
{
    /**
    * Admin list request validation rules
    *
    * @param Request $request
    * @return void
    */
    public static function adminListRequestValidation(Request $request)
    {
        return $request->validate([
            'keyword' => 'nullable|string',
            'position' => 'nullable|string',
            'employer_name' => 'nullable|string',
            'employer_email' => 'nullable|email',
            'contacted' => 'nullable|numeric|in:0,1',
            'start_date' => 'required_with:end_date|string|date_format:Y-m-d',
            'end_date' => 'required_with:start_date|string|date_format:Y-m-d'
        ]);
    }

    /**
     * Search employer
     *
     * @param Eloquent Model $model
     * @param Array $filters
     * @param String $orderBy
     * @param String $sortOrder
     * @return Eloquent HeadHunting model
     */
    public function search($model, array $filters, string $orderBy = 'created_at', string $sortOrder = 'DESC')
    {
        if (isset($filters['keyword'])) {
            $keyword = $filters['keyword'];

            // $model =$model->where('position', 'like', '%' . $keyword . '%')
            //     ->whereHas('employer', function ($query) use ($keyword) {
            //         $query->where('name', 'LIKE', '%'. $keyword . '%')
            //         ->where('email', 'LIKE', '%' . $keyword . '%');
            //     });

            $model =$model->where('job_position', 'like', '%' . $keyword . '%')
                ->orWhere('name', 'like', '%' . $filters['employer_name'] . '%')
                ->orWhere('email', 'like', '%' . $filters['employer_email'] . '%');
        }

        if (isset($filters['start_date']) && isset($filters['end_date'])) {
            $model = $model->where('created_at', '>=', $filters['start_date'] . ' 00:00:00')
                ->where('created_at', '<=', $filters['end_date'] . ' 23:59:59');
        }

        if (isset($filters['position'])) {
            $model = $model->where('position', 'like', '%' . $filters['position'] . '%');
        }

        if (isset($filters['employer_name'])) {
            // $model = $model->whereHas('employer', function ($query) use ($filters) {
            //     $query->where('name', 'LIKE', '%'. $filters['employer_name'] . '%');
            // });
            $model = $model->where('name', 'like', '%' . $filters['employer_name'] . '%');
        }

        if (isset($filters['employer_email'])) {
            // $model = $model->whereHas('employer', function ($query) use ($filters) {
            //     $query->where('email', 'LIKE', '%'. $filters['employer_email'] . '%');
            // });
            $model = $model->where('name', 'email', $filters['employer_email']);
        }

        if (isset($filters['contacted'])) {
            $model = $model->where('contacted', $filters['contacted']);
        }

        $model = $model->orderBy($orderBy, $sortOrder);
        
        return $model;
    }
}
