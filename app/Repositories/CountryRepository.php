<?php

namespace App\Repositories;

use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Http\Resources\CountryResource;
use App\Repositories\Interfaces\RepositoryInterface;

class CountryRepository implements RepositoryInterface
{
    const FORM_DATA_CACHE_NAME = 'country_form_data_cache';

    /**
     * Admin list request validation rules
     *
     * @param Request $request
     * @return void
     */
    public static function adminListRequestValidation(Request $request)
    {
        return $request->validate([
            'keyword' => 'nullable|string',

            'name' => 'nullable|string',

            'start_date' => 'required_with:end_date|string|date_format:Y-m-d',
            'end_date' => 'required_with:start_date|string|date_format:Y-m-d'
        ]);
    }

    /**
     * Search skill
     *
     * @param Eloquent Model $model
     * @param Array $filters
     * @param String $orderBy
     * @param String $sortOrder
     * @return Eloquent Skill model
     */
    public function search($model, array $filters, string $orderBy = 'created_at', string $sortOrder = 'DESC')
    {
        if (isset($filters['keyword'])) {
            $model = $model->where('name', 'like', '%' . $filters['keyword'] . '%');
        }

        if (isset($filters['start_date']) && isset($filters['end_date'])) {
            $model = $model->where('created_at', '>=', $filters['start_date'] . ' 00:00:00')
                ->where('created_at', '<=', $filters['end_date'] . ' 23:59:59');
        }

        if (isset($filters['name'])) {
            $model = $model->where('name', 'like', '%' . $filters['name'] . '%');
        }

        $model = $model->orderBy($orderBy, $sortOrder);
        
        return $model;
    }

    /**
    * Get all countries for form data option
    *
    * @return Array
    */
    public static function formDataCache()
    {
        return Cache::remember(self::FORM_DATA_CACHE_NAME, 604800, function () {
            return CountryResource::collection(Country::orderBy('name', 'ASC')->get());
        });
    }

    /**
     * Delete countries form data cache
     *
     * @return void
     */
    public static function deleteFormDataCache()
    {
        Cache::forget(self::FORM_DATA_CACHE_NAME);
    }
}
