<?php

namespace App\Repositories;

use Cache;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Resources\UserResource;
use App\Repositories\Interfaces\RepositoryInterface;

class UserRepository implements RepositoryInterface
{
    /**
     * Admin list request validation rules
     *
     * @param Request $request
     * @return void
     */
    public static function adminListRequestValidation(Request $request)
    {
        return $request->validate([
            'keyword' => 'nullable|string',
            'identifier' => 'nullable|string',
            'name' => 'nullable|string',
            'email' => 'nullable|email',
            'phone' => 'nullable|string',
            'gender' => 'nullable|string|in:male,female',
            'start_date' => 'nullable|string|date_format:Y-m-d',
            'end_date' => 'nullable|string|date_format:Y-m-d'
        ]);
    }

    /**
     * Search model data
     *
     * @param Eloquent Model $model
     * @param Array $filters
     * @param String $orderBy
     * @param String $sortOrder
     * @return Eloquent model
     */
    public function search($model, array $filters, String $orderBy = 'created_at', String $sortOrder = 'DESC')
    {
        if (isset($filters['keyword'])) {
            $model = $model->where('name', 'like', '%' . $filters['keyword'] . '%')
                ->orWhere('email', 'like', '%' . $filters['keyword'] . '%')
                ->orWhere('identifier', $filters['keyword']);
        }

        if (isset($filters['start_date']) && isset($filters['end_date'])) {
            $model = $model->where('created_at', '>=', $filters['start_date'] . ' 00 : 00:00')
                ->where('created_at', '<=', $filters['end_date'] . ' 23 : 59:59');
        }

        if (isset($filters['name'])) {
            $model = $model->where('name', 'like', '%' . $filters['name'] . '%');
        }

        if (isset($filters['email'])) {
            $model = $model->where('email', $filters['email']);
        }

        if (isset($filters['phone'])) {
            $model = $model->where('phone', $filters['phone']);
        }

        if (isset($filters['gender'])) {
            $model = $model->where('gender', $filters['gender']);
        }

        $model = $model->orderBy($orderBy, $sortOrder);
        
        return $model;
    }

    /**
     * Get user profile completion
     *
     * @param User $user
     * @return Array
     */
    public function profileCompletion(User $user)
    {
        return Cache::remember('user_'.$user->id.'_profile_completion', now()->addDays(7), function () use ($user) {
            return $this->generateProfileCompletion($user);
        });
    }

    /**
     * Update user profile completion cache
     *
     * @param User $user
     * @return Array
     */
    public function updateProfileCompletionCache(User $user)
    {
        $completion = $this->generateProfileCompletion($user);
        Cache::put('user_'.$user->id.'_profile_completion', $completion, now()->addDays(7));

        return $completion;
    }

    /**
     * Delete user profile completion cache
     *
     * @param User $user
     * @return void
     */
    public static function deleteProfileCompletionCache(User $user)
    {
        Cache::forget('user_'.$user->id.'_profile_completion');
    }

    /**
     * Get user basic info
     *
     * @param User $user
     * @return Array
     */
    public function basicInfo(User $user)
    {
        return [
            'name' => $user->name ?? '',
            'email' => $user->email ?? '',
            'gender' => $user->gender ?? '',
            'dob' => $user->dob ?? '',
            'phone' => $user->phone ?? '',
            'city_id' => $user->city_id ?? '',
            'province_id' => $user->province_id ?? '',
            'job_status' => $user->job_status ?? '',
            'hobby' => $user->hobby ?? '',
            'relationship' => $user->relationship ?? '',
        ];
    }

    /**
     * Is user basic info completed
     *
     * @param User $user
     * @return boolean
     */
    public function isBasicInfoComplete(User $user)
    {
        return in_array('', $this->basicInfo($user)) ? false : true;
    }

    /**
     * Generate user profile completion
     *
     * @param User $user
     * @return Array
     */
    public function generateProfileCompletion(User $user)
    {
        $basicInfo = $this->basicInfo($user);

        $about = [
            'about' => $user->about ?? '',
        ];

        $experiences = [
            'experiences' => $user->work_experiences()->exists() ? 'exists' : '',
        ];

        $organizationExperiences = [
            'organization_experiences' => $user->organization_experiences()->exists() ? 'exists' : '',
        ];

        $educations = [
            'educations' => $user->educations()->exists() ? 'exists' : '',
        ];

        $skills = [
            'skills' => $user->skills()->exists() ? 'exists' : '',
        ];

        $awards = [
            'awards' => $user->awards()->exists() ? 'exists' : '',
        ];

        $interest = [
            'interest' => $user->job_interest()->exists() ? 'exists' : '',
        ];

        $resume = [
            'skills' => $user->resume ?? '',
        ];

        $webAndSosmed = [
            'yt_video' => $user->yt_video ?? '',
            'website' => $user->website ?? '',
            'facebook' => $user->facebook ?? '',
            'twitter' => $user->twitter ?? '',
            'instagram' => $user->instagram ?? '',
            'linkedin' => $user->linkedin ?? '',
            'behance' => $user->behance ?? '',
            'github' => $user->github ?? '',
            'codepen' => $user->codepen ?? '',
            'vimeo' => $user->vimeo ?? '',
            'youtube' => $user->youtube ?? '',
            'dribbble' => $user->dribbble ?? '',
        ];

        $portofolio = [
            'portofolio' => count(array_unique($webAndSosmed)) === 1 ? '' : 'filled',
        ];

        $video = [
            'video' => $user->video ?? ''
        ];

        $englishAssessment = [
            'english_assessment' => $user->english_assessment()->exists() ? 'exists' : '',
        ];

        $personalityTest = [
            'personality_test' => $user->personality_test()->exists() ? 'exists' : '',
        ];

        $allFields = array_merge(
            $basicInfo,
            $about,
            $experiences,
            $organizationExperiences,
            $educations,
            $skills,
            $awards,
            $interest,
            $resume,
            $portofolio,
            $video,
            $englishAssessment,
            $personalityTest
        );

        $count = count($allFields);
        $filled = count(array_filter($allFields, function ($field) {
            return $field != '';
        }));

        return [
            'basic_info' => $this->isBasicInfoComplete($user),
            'about' => in_array('', $about) ? false : true,
            'experiences' => in_array('', $experiences) ? false : true,
            'organization_experiences' => in_array('', $organizationExperiences) ? false : true,
            'educations' => in_array('', $educations) ? false : true,
            'skills' => in_array('', $skills) ? false : true,
            'awards' => in_array('', $awards) ? false : true,
            'interest' => in_array('', $interest) ? false : true,
            'resume' => in_array('', $resume) ? false : true,
            'portofolio' => in_array('', $portofolio) ? false : true,
            'video' => in_array('', $video) ? false : true,
            'english_assessment' => in_array('', $englishAssessment) ? false : true,
            'personality_test' => in_array('', $personalityTest) ? false : true,
            'percentage' => ceil(($filled * 100) / $count)
        ];
    }

    /**
     * User  resource cache
     *
     * @param User $user
     * @return Array
     */
    public static function UserResourceCache(User $user)
    {
        // Store the cache for 7 days
        return Cache::remember('user_'.$user->id.'_resource', now()->addDays(7), function () use ($user) {
            return new UserResource($user);
        });
    }

    /**
     * Update user resource cache
     *
     * @param User $user
     * @return Array
     */
    public static function updateUserResourceCache(User $user)
    {
        $userResource = new UserResource($user);
        Cache::put('user_'.$user->id.'_resource', $userResource, now()->addDays(7));

        return $userResource;
    }
}
