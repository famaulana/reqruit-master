<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\EducationField;
use Illuminate\Support\Facades\Cache;
use App\Http\Resources\EducationFieldResource;
use App\Repositories\Interfaces\RepositoryInterface;

class EducationFieldRepository implements RepositoryInterface
{
    const FORM_DATA_CACHE_NAME = 'education_field_form_data_cache';

    /**
     * Admin list request validation rules
     *
     * @param Request $request
     * @return void
     */
    public static function adminListRequestValidation(Request $request)
    {
        return $request->validate([
            'keyword' => 'nullable|string',

            'name' => 'nullable|string',

            'start_date' => 'required_with:end_date|string|date_format:Y-m-d',
            'end_date' => 'required_with:start_date|string|date_format:Y-m-d'
        ]);
    }

    /**
     * Search job function
     *
     * @param Eloquent Model $model
     * @param Array $filters
     * @param String $orderBy
     * @param String $sortOrder
     * @return Eloquent Jobfunction model
     */
    public function search($model, array $filters, string $orderBy = 'created_at', string $sortOrder = 'DESC')
    {
        if (isset($filters['keyword'])) {
            $model = $model->where('name', 'like', '%' . $filters['keyword'] . '%');
        }

        if (isset($filters['start_date']) && isset($filters['end_date'])) {
            $model = $model->where('created_at', '>=', $filters['start_date'] . ' 00:00:00')
                ->where('created_at', '<=', $filters['end_date'] . ' 23:59:59');
        }

        if (isset($filters['name'])) {
            $model = $model->where('name', 'like', '%' . $filters['name'] . '%');
        }

        $model = $model->orderBy($orderBy, $sortOrder);
        
        return $model;
    }

    /**
     * Get all education fields for form data option
     *
     * @return Array
     */
    public static function formDataCache()
    {
        return Cache::remember(self::FORM_DATA_CACHE_NAME, 604800, function () {
            return EducationFieldResource::collection(EducationField::orderBy('name', 'ASC')->get());
        });
    }

    /**
     * Delete education fields form data cache
     *
     * @return void
     */
    public static function deleteFormDataCache()
    {
        Cache::forget(self::FORM_DATA_CACHE_NAME);
    }
}
