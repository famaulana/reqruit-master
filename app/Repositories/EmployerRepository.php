<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Repositories\Interfaces\RepositoryInterface;

class EmployerRepository implements RepositoryInterface
{
    /**
    * Admin list request validation rules
    *
    * @param Request $request
    * @return void
    */
    public static function adminListRequestValidation(Request $request)
    {
        return $request->validate([
            'keyword' => 'nullable|string',
            'identifier' => 'nullable|string',
            'name' => 'nullable|string',
            'email' => 'nullable|email',
            'phone' => 'nullable|string',
            'gender' => 'nullable|string|in:male,female',
            'start_date' => 'required_with:end_date|string|date_format:Y-m-d',
            'end_date' => 'required_with:start_date|string|date_format:Y-m-d'
        ]);
    }

    /**
     * Search employer
     *
     * @param Eloquent Model $model
     * @param Array $filters
     * @param String $orderBy
     * @param String $sortOrder
     * @return Eloquent Employee model
     */
    public function search($model, array $filters, string $orderBy = 'created_at', string $sortOrder = 'DESC')
    {
        if (isset($filters['keyword'])) {
            $model = $model->where('name', 'like', '%' . $filters['keyword'] . '%');
        }

        if (isset($filters['start_date']) && isset($filters['end_date'])) {
            $model = $model->where('created_at', '>=', $filters['start_date'] . ' 00:00:00')
                ->where('created_at', '<=', $filters['end_date'] . ' 23:59:59');
        }

        if (isset($filters['identifier'])) {
            $model = $model->where('identifier', $filters['identifier']);
        }

        if (isset($filters['name'])) {
            $model = $model->where('name', 'like', '%' . $filters['name'] . '%');
        }

        if (isset($filters['email'])) {
            $model = $model->where('email', $filters['email']);
        }

        if (isset($filters['phone'])) {
            $model = $model->where('phone', $filters['phone']);
        }

        if (isset($filters['gender'])) {
            $model = $model->where('gender', $filters['gender']);
        }

        $model = $model->orderBy($orderBy, $sortOrder);
        
        return $model;
    }
}
