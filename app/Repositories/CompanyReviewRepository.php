<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Repositories\Interfaces\RepositoryInterface;

class CompanyReviewRepository implements RepositoryInterface
{
    /**
    * Admin list request validation rules
    *
    * @param Request $request
    * @return void
    */
    public static function adminListRequestValidation(Request $request)
    {
        return $request->validate([
            'user_name' => 'nullable|string',
            'company_name' => 'nullable|string',
            'rating' => 'nullable|numeric|min:1|max:5',
            'published' => 'nullable|string|in:true,false',
            'start_date' => 'required_with:end_date|string|date_format:Y-m-d',
            'end_date' => 'required_with:start_date|string|date_format:Y-m-d'
        ]);
    }

    /**
         * Search model data
         *
         * @param Eloquent Model $model
         * @param Array $filters
         * @param String $orderBy
         * @param String $sortOrder
         * @return Eloquent model
         */
    public function search($model, array $filters, string $orderBy = 'created_at', string $sortOrder = 'DESC')
    {
        if (isset($filters['user_name'])) {
            $model = $model->whereHas('user', function ($q) use ($filters) {
                $q->where('name', $filters['user_name']);
            });
        }

        if (isset($filters['user_email'])) {
            $model = $model->whereHas('user', function ($q) use ($filters) {
                $q->where('email', $filters['user_email']);
            });
        }

        if (isset($filters['company_name'])) {
            $model = $model->whereHas('company', function ($q) use ($filters) {
                $q->where('name', $filters['company_name']);
            });
        }

        if (isset($filters['start_date']) && isset($filters['end_date'])) {
            $model = $model->where('created_at', '>=', $filters['start_date'] . ' 00:00:00')
                ->where('created_at', '<=', $filters['end_date'] . ' 23:59:59');
        }

        if (isset($filters['rating'])) {
            $model = $model->where('rating', intval($filters['rating']));
        }

        if (isset($filters['published'])) {
            $published = $filters['published'] == 'true' ? true : false;
            $model = $model->where('published', $published);
        }

        $model = $model->orderBy($orderBy, $sortOrder);
        
        return $model;
    }
}
