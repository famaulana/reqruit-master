<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Repositories\Interfaces\RepositoryInterface;

class MainBannerRepository implements RepositoryInterface
{
    /**
     * Admin list request validation rules
     *
     * @param Request $request
     * @return void
     */
    public static function adminListRequestValidation(Request $request)
    {
        return $request->validate([
            'keyword' => 'nullable|string',
            'name' => 'nullable|string',
        ]);
    }

    /**
     * Search skill
     *
     * @param Eloquent Model $model
     * @param Array $filters
     * @param String $orderBy
     * @param String $sortOrder
     * @return Eloquent Skill model
     */
    public function search($model, array $filters, string $orderBy = 'created_at', string $sortOrder = 'DESC')
    {
        if (isset($filters['keyword'])) {
            $model = $model->where('name', 'like', '%' . $filters['keyword'] . '%');
        }

        if (isset($filters['name'])) {
            $model = $model->where('name', 'like', '%' . $filters['name'] . '%');
        }

        $model = $model->orderBy($orderBy, $sortOrder);
        
        return $model;
    }
}