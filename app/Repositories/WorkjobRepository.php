<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Repositories\Interfaces\RepositoryInterface;

class WorkjobRepository implements RepositoryInterface
{
    /**
     * Admin list request validation rules
     *
     * @param Request $request
     * @return void
     */
    public static function adminListRequestValidation(Request $request)
    {
        return $request->validate([
            'keyword' => 'nullable|string',

            'identifier' => 'nullable|string',
            'title' => 'nullable|string',

            'type' => 'nullable|string|in:full-time,part-time,internship,freelance',
            'job_types' => 'nullable|string',

            'country_id' => 'nullable|numeric',
            'province_id' => 'nullable|numeric',
            'city_id' => 'nullable|numeric',
            'cities' => 'nullable|string',

            'jobfunction_id' => 'nullable|numeric',
            'jobfunctions' => 'nullable|string',

            'company_id' => 'nullable|numeric',
            'companies' => 'nullable|string',

            'skills' => 'nullable|string',

            'salary_min' => 'nullable|numeric|min:0',
            'salary_max' => 'nullable|numeric',
            'salary_duration' => 'nullable|string|in:monthly,yearly,per-project',

            'work_experience' => 'nullable|string',

            'bonus_salary' => 'nullable|numeric|in:0,1',
            'bonus_salary_min' => 'nullable|numeric|min:0',
            'bonus_salary_max' => 'nullable|numeric|min:0',
            'bonus_salary_duration' => 'nullable|string|in:monthly,yearly,per-project',

            'job_posting_status' => 'nullable|string|in:open,closed',
            'status' => 'nullable|string|in:draft,published',

            'skip' => 'nullable|numeric|min:0',
            'take' => 'nullable|numeric|min:1',
            'per_page' => 'nullable|numeric|min:1',
            'paginate' => 'nullable|string|in:true,false',

            'order_by' => 'nullable|string',
            'sort_order' => 'nullable|string',

            'start_date' => 'required_with:end_date|string|date_format:Y-m-d',
            'end_date' => 'required_with:start_date|string|date_format:Y-m-d',

            'last_update' => 'nullable|string',
            'remote' => 'nullable|in:true,false',
        ]);
    }

    /**
     * Search job
     *
     * @param Eloquent model $model
     * @param Array $filters
     * @param String $orderBy
     * @param String $sortOrder
     * @return Eloquent Workjob model
     */
    public function search($model, array $filters, string $orderBy = 'id', string $sortOrder = 'DESC')
    {
        if (isset($filters['keyword'])) {
            $model = $model->where('title', 'like', '%' . $filters['keyword'] . '%');
        }

        if (isset($filters['start_date']) && isset($filters['end_date'])) {
            $model = $model->where('created_at', '>=', $filters['start_date'] . ' 00:00:00')
                ->where('created_at', '<=', $filters['end_date'] . ' 23:59:59');
        }

        if (isset($filters['identifier'])) {
            $model = $model->where('identifier', $filters['identifier']);
        }

        if (isset($filters['title'])) {
            $model = $model->where('title', 'like', '%' . $filters['title'] . '%');
        }

        if (isset($filters['type'])) {
            $model = $model->where('type', $filters['type']);
        }

        if (isset($filters['job_types'])) {
            $model = $model->whereIn('type', explode(',', $filters['job_types']));
        }

        if (isset($filters['country_id'])) {
            $model = $model->where('country_id', $filters['country_id']);
        }

        if (isset($filters['province_id'])) {
            $model = $model->where('province_id', $filters['province_id']);
        }

        if (isset($filters['city_id'])) {
            $model = $model->where('city_id', $filters['city_id']);
        }

        if (isset($filters['cities'])) {
            $model = $model->whereIn('city_id', explode(',', $filters['cities']));
        }

        if (isset($filters['jobfunction_id'])) {
            $model = $model->where('jobfunction_id', $filters['jobfunction_id']);
        }

        if (isset($filters['jobfunctions'])) {
            $model = $model->whereIn('jobfunction_id', explode(',', $filters['jobfunctions']));
        }

        if (isset($filters['company_id'])) {
            $model = $model->where('company_id', $filters['company_id']);
        }

        if (isset($filters['companies'])) {
            $model = $model->whereIn('company_id', explode(',', $filters['companies']));
        }

        if (isset($filters['skills'])) {
            $skillsFilter = $filters['skills'];
            $model = $model->whereHas('must_skills', function ($query) use ($skillsFilter) {
                $query->whereIn('id', explode(',', $skillsFilter));
            });
        }

        if (isset($filters['salary_min'])) {
            $model = $model->where('salary_min', '>=', $filters['salary_min']);
        }

        if (isset($filters['salary_max'])) {
            $model = $model->where('salary_max', '<=', $filters['salary_max']);
        }

        if (isset($filters['work_experience'])) {
            $model = $model->whereIn('work_experience', explode(',', $filters['work_experience']));
        }

        if (isset($filters['salary_duration'])) {
            $model = $model->where('salary_duration', $filters['salary_duration']);
        }

        if (isset($filters['bonus_salary'])) {
            $model = $model->where('bonus_salary', $filters['bonus_salary']);
        }

        if (isset($filters['bonus_salary_min'])) {
            $model = $model->where('bonus_salary_min', '>=', $filters['bonus_salary_min']);
        }

        if (isset($filters['bonus_salary_max'])) {
            $model = $model->where('bonus_salary_max', '<=', $filters['bonus_salary_max']);
        }

        if (isset($filters['bonus_salary_duration'])) {
            $model = $model->where('bonus_salary_duration', $filters['bonus_salary_duration']);
        }

        if (isset($filters['job_posting_status'])) {
            $model = $model->where('job_posting_status', $filters['job_posting_status']);
        }

        if (isset($filters['status'])) {
            $model = $model->where('status', $filters['status']);
        }

        if (isset($filters['last_update'])) {
            $lastUpdate = $filters['last_update'];
            if ($lastUpdate != 'anytime') {
                $startDate = $lastUpdate == 1 ? now()->subDay()->format('Y-m-d') : now()->subDays($lastUpdate)->format('Y-m-d 00:00:00');
                $model = $model->where('updated_at', '>=', $startDate)
                    ->where('updated_at', '<=', now()->format('Y-m-d H:i:s'));
            }
        }

        if (isset($filters['remote'])) {
            $remote = $filters['remote'] == 'true' ? 1 : 0;
            $model = $model->where('remote', $remote);
        }

        if (isset($filters['skip'])) {
            $model = $model->skip($filters['skip']);
        }

        if (isset($filters['take'])) {
            $model = $model->take($filters['take']);
        }

        $model = $orderBy === 'random' ? $model->inRandomOrder() : $model->orderBy($orderBy, $sortOrder);
        
        return $model;
    }

    /**
     * Search filters
     *
     * @param Request $request
     * @return Array
     */
    public static function filters(Request $request)
    {
        $rawFilters = [
            'keyword' => $request->keyword,

            'identifier' => $request->identifier,
            'title' => $request->title,

            'type' => $request->type,
            'job_types' => $request->job_types,

            'country_id' => $request->country_id,
            'province_id' => $request->province_id,
            'city_id' => $request->city_id,
            'cities' => $request->cities,

            'jobfunction_id' => $request->jobfunction_id,
            'jobfunctions' => $request->jobfunctions,

            'company_id' => $request->company_id,
            'companies' => $request->companies,

            'skills' => $request->skills,

            'salary_min' => $request->salary_min,
            'salary_max' => $request->salary_max,
            'salary_duration' => $request->salary_duration,

            'work_experience' => $request->work_experience,

            'bonus_salary' => $request->bonus_salary,
            'bonus_salary_min' => $request->bonus_salary_min,
            'bonus_salary_max' => $request->bonus_salary_max,
            'bonus_salary_duration' => $request->bonus_salary_duration,

            'job_posting_status' => $request->job_posting_status,
            'status' => $request->status,

            'skip' => $request->skip,
            'take' => $request->take,
            'per_page' => $request->per_page,
            'paginate' => $request->paginate,

            'order_by' => $request->order_by,
            'sort_order' => $request->sort_order,

            'start_date' => $request->start_date,
            'end_date' => $request->end_date,

            'last_update' => $request->last_update,
            'remote' => $request->remote,
        ];

        $filters = [];
        foreach ($rawFilters as $name => $value) {
            if ($value !== '' && !is_null($value)) {
                $filters[$name] = $value;
            }
        }

        return $filters;
    }
}
