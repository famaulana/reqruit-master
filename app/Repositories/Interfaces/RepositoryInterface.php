<?php

namespace App\Repositories\Interfaces;

use Illuminate\Http\Request;

interface RepositoryInterface
{
    /**
     * Admin list request validation rules
     *
     * @param Request $request
     * @return void
     */
    public static function adminListRequestValidation(Request $request);

    /**
     * Search model data
     *
     * @param Eloquent Model $model
     * @param Array $filters
     * @param String $orderBy
     * @param String $sortOrder
     * @return Eloquent model
     */
    public function search($model, array $filters, String $orderBy = 'created_at', String $sortOrder = 'DESC');
}
