<?php

namespace App\Repositories;

/**
 * Personal analys repository
 */
class PersonalAnalysRepository
{
    /**
     * Get statements
     *
     * @return Array
     */
    public static function statements()
    {
        return [
            // 1
            [
                'statements' => [
                    'A' => 'Mudah bergaul, ramah, mudah setuju',
                    'B' => 'Mempercayai, percaya pada orang lain',
                    'C' => 'Petualang, suka mengambil resiko',
                    'D' => 'Penuh toleransi, menghormati orang lain',
                ],
                'char_most' => [
                    'A' => 'S',
                    'B' => 'I',
                    'C' => 'C',
                    'D' => 'X'
                ],
                'char_least' => [
                    'A' => 'S',
                    'B' => 'X',
                    'C' => 'C',
                    'D' => 'D'
                ],
            ],
            // 2
            [
                'statements' => [
                    'A' => 'Yang penting adalah hasil',
                    'B' => 'Kerjakan dengan benar, ketepatan sangat penting',
                    'C' => 'Buat agar menyenangkan',
                    'D' => 'Kerjakan bersama-sama',
                ],
                'char_most' => [
                    'A' => 'I',
                    'B' => 'C',
                    'C' => 'D',
                    'D' => 'X'
                ],
                'char_least' => [
                    'A' => 'I',
                    'B' => 'C',
                    'C' => 'D',
                    'D' => 'S'
                ],
            ],
            // 3
            [
                'statements' => [
                    'A' => 'Pendidikan, Kebudayaan',
                    'B' => 'Prestasi, penghargaan',
                    'C' => 'Keselamatan, keamanan',
                    'D' => 'Sosial, pertemuan kelompok',
                ],
                'char_most' => [
                    'A' => 'X',
                    'B' => 'D',
                    'C' => 'S',
                    'D' => 'I'
                ],
                'char_least' => [
                    'A' => 'C',
                    'B' => 'D',
                    'C' => 'X',
                    'D' => 'I'
                ],
            ],
            // 4
            [
                'statements' => [
                    'A' => 'Lembut, tertutup',
                    'B' => 'Visionary/pandangan ke masa depan',
                    'C' => 'Pusat perhatian, suka bersosialisasi',
                    'D' => 'Pendamai, membawa ketenangan',
                ],
                'char_most' => [
                    'A' => 'C',
                    'B' => 'S',
                    'C' => 'X',
                    'D' => 'I'
                ],
                'char_least' => [
                    'A' => 'X',
                    'B' => 'S',
                    'C' => 'D',
                    'D' => 'I'
                ],
            ],
            // 5
            [
                'statements' => [
                    'A' => 'Menahan diri, bisa hidup tanpa memiliki',
                    'B' => 'Membeli karena dorongan hasrat/impulse',
                    'C' => 'Akan menunggu, tanpa tekanan',
                    'D' => 'Akan membeli apa yang diinginkan',
                ],
                'char_most' => [
                    'A' => 'X',
                    'B' => 'C',
                    'C' => 'X',
                    'D' => 'S'
                ],
                'char_least' => [
                    'A' => 'I',
                    'B' => 'C',
                    'C' => 'D',
                    'D' => 'S'
                ],
            ],
            // 6
            [
                'statements' => [
                    'A' => 'Mengambil kendali, bersikap langsung/direct',
                    'B' => 'Suka bergaul, antusias',
                    'C' => 'Mudah ditebak, konsisten',
                    'D' =>  'Waspada, berhati-hati',
                ],
                'char_most' => [
                    'A' => 'D',
                    'B' => 'S',
                    'C' => 'X',
                    'D' => 'X'
                ],
                'char_least' => [
                    'A' => 'D',
                    'B' => 'S',
                    'C' => 'I',
                    'D' => 'C'
                ],
            ],
            // 7
            [
                'statements' => [
                    'A' => 'Menyemangati orang lain',
                    'B' => 'Berusaha mencapai kesempurnaan',
                    'C' => 'Menjadi bagian dari tim/kelompok',
                    'D' => 'Ingin menetapkan goal/tujuan',
                ],
                'char_most' => [
                    'A' => 'X',
                    'B' => 'S',
                    'C' => 'D',
                    'D' => 'I'
                ],
                'char_least' => [
                    'A' => 'C',
                    'B' => 'X',
                    'C' => 'D',
                    'D' => 'I'
                ],
            ],
            // 8
            [
                'statements' => [
                    'A' => 'Bersahabat, mudah bergaul',
                    'B' => 'Unik, bosan pada rutinitas',
                    'C' => 'Aktif melakukan perubahan',
                    'D' => 'Ingin segala sesuatu akurat dan pasti',
                ],
                'char_most' => [
                    'A' => 'D',
                    'B' => 'I',
                    'C' => 'X',
                    'D' => 'X'
                ],
                'char_least' => [
                    'A' => 'X',
                    'B' => 'X',
                    'C' => 'S',
                    'D' => 'C'
                ],
            ],
            [
            // 9
                'statements' => [
                    'A' => 'Sulit dikalahkan/ditundukkan',
                    'B' => 'Melaksanakan sesuai perintah',
                    'C' => 'Bersemangat, riang',
                    'D' => 'Ingin keteraturan, rapi',
                ],
                'char_most' => [
                    'A' => 'I',
                    'B' => 'S',
                    'C' => 'D',
                    'D' => 'C'
                ],
                'char_least' => [
                    'A' => 'I',
                    'B' => 'S',
                    'C' => 'D',
                    'D' => 'X'
                ],
            ],
            // 10
            [
                'statements' => [
                    'A' => 'Menjadi frustasi',
                    'B' => 'Memendam perasaan dalam hati',
                    'C' => 'Menyampaikan sudut pandang pribadi',
                    'D' => 'Berani menghadapi oposisi',
                ],
                'char_most' => [
                    'A' => 'D',
                    'B' => 'C',
                    'C' => 'X',
                    'D' => 'S'
                ],
                'char_least' => [
                    'A' => 'D',
                    'B' => 'X',
                    'C' => 'I',
                    'D' => 'S'
                ],
            ],
            // 11
            [
                'statements' => [
                    'A' => 'Mengalah, tidak suka pertentangan',
                    'B' => 'Penuh dengan hal-hal kecil/detail',
                    'C' => 'Berubah pada menit-menit terakhir',
                    'D' => 'Mendesak/memaksa, agak kasar',
                ],
                'char_most' => [
                    'A' => 'I',
                    'B' => 'S',
                    'C' => 'X',
                    'D' => 'D'
                ],
                'char_least' => [
                    'A' => 'I',
                    'B' => 'S',
                    'C' => 'C',
                    'D' => 'D'
                ],
            ],
            // 12
            [
                'statements' => [
                    'A' => 'Saya akan pimpin mereka',
                    'B' => 'Saya akan ikut/mengikuti',
                    'C' => 'Saya akan pengaruhi/bujuk mereka',
                    'D' => 'Saya akan mendapatkan fakta-faktanya',
                ],
                'char_most' => [
                    'A' => 'X',
                    'B' => 'D',
                    'C' => 'C',
                    'D' => 'S'
                ],
                'char_least' => [
                    'A' => 'I',
                    'B' => 'D',
                    'C' => 'X',
                    'D' => 'S'
                ],
            ],
            // 13
            [
                'statements' => [
                    'A' => 'Hidup/lincah, banyak bicara',
                    'B' => 'Cepat, penuh keyakinan',
                    'C' => 'Berusaha menjaga keseimbangan',
                    'D' => 'Berusaha patuh pada peraturan',
                ],
                'char_most' => [
                    'A' => 'D',
                    'B' => 'I',
                    'C' => 'S',
                    'D' => 'X'
                ],
                'char_least' => [
                    'A' => 'X',
                    'B' => 'I',
                    'C' => 'S',
                    'D' => 'C'
                ],
            ],
            // 14
            [
                'statements' => [
                    'A' => 'Ingin kemajuan/peningkatan',
                    'B' => 'Puas dengan keadaan. Tenang/mudah puas',
                    'C' => 'Menunjukkan perasaan dengan terbuka',
                    'D' => 'Rendah hati, sederhana',
                ],
                'char_most' => [
                    'A' => 'C',
                    'B' => 'D',
                    'C' => 'I',
                    'D' => 'S'
                ],
                'char_least' => [
                    'A' => 'C',
                    'B' => 'X',
                    'C' => 'I',
                    'D' => 'X'
                ],
            ],
            // 15
            [
                'statements' => [
                    'A' => 'Memikirkan orang lain dahulu',
                    'B' => 'Suka bersaing/kompetitif, suka tantangan',
                    'C' => 'Optimis, berpikir positif',
                    'D' => 'Sistimatis, berpikir logis',
                ],
                'char_most' => [
                    'A' => 'S',
                    'B' => 'X',
                    'C' => 'C',
                    'D' => 'X'
                ],
                'char_least' => [
                    'A' => 'X',
                    'B' => 'I',
                    'C' => 'C',
                    'D' => 'D'
                ],
            ],
            // 16
            [
                'statements' => [
                    'A' => 'Mengelola waktu dengan efisien',
                    'B' => 'Sering terburu-buru merasa ditekan',
                    'C' => 'Hal-hal sosial adalah penting',
                    'D' => 'Suka menyelesaikan hal yang sudah dimulai',
                ],
                'char_most' => [
                    'A' => 'I',
                    'B' => 'X',
                    'C' => 'X',
                    'D' => 'D'
                ],
                'char_least' => [
                    'A' => 'X',
                    'B' => 'S',
                    'C' => 'C',
                    'D' => 'D'
                ],
            ],
            // 17
            [
                'statements' => [
                    'A' => 'Tenang, Pendiam, Tertutup',
                    'B' => 'Gembira, bebas, riang',
                    'C' => 'Menyenangkan, baik hati',
                    'D' => 'Menyolok, berani',
                ],
                'char_most' => [
                    'A' => 'C',
                    'B' => 'S',
                    'C' => 'X',
                    'D' => 'D'
                ],
                'char_least' => [
                    'A' => 'X',
                    'B' => 'S',
                    'C' => 'I',
                    'D' => 'D'
                ],
            ],
            // 18
            [
                'statements' => [
                    'A' => 'Menyenangkan orang lain, ramah, penurut',
                    'B' => 'Tertawa lepas, hidup',
                    'C' => 'Pemberani, tegas',
                    'D' => 'Pendiam, tertutup, tenang',
                ],
                'char_most' => [
                    'A' => 'I',
                    'B' => 'S',
                    'C' => 'X',
                    'D' => 'D'
                ],
                'char_least' => [
                    'A' => 'X',
                    'B' => 'X',
                    'C' => 'C',
                    'D' => 'D'
                ],
            ],
            // 19
            [
                'statements' => [
                    'A' => 'Menolak perubahan yang mendadak',
                    'B' => 'Cenderung terlalu banyak berjanji',
                    'C' => 'Mundur apabila berada dibawah tekanan',
                    'D' => 'Tidak takut untuk berkelahi',
                ],
                'char_most' => [
                    'A' => 'C',
                    'B' => 'D',
                    'C' => 'I',
                    'D' => 'S'
                ],
                'char_least' => [
                    'A' => 'X',
                    'B' => 'D',
                    'C' => 'I',
                    'D' => 'S'
                ],
            ],
            // 20
            [
                'statements' => [
                    'A' => 'Menyediakan waktu untuk orang lain',
                    'B' => 'Merencanakan masa depan, bersiap-siap',
                    'C' => 'Menuju petualangan baru',
                    'D' => 'Menerima penghargaan atas pencapaian target',
                ],
                'char_most' => [
                    'A' => 'D',
                    'B' => 'C',
                    'C' => 'X',
                    'D' => 'I'
                ],
                'char_least' => [
                    'A' => 'D',
                    'B' => 'X',
                    'C' => 'S',
                    'D' => 'I'
                ],
            ],
            // 21
            [
                'statements' => [
                    'A' => 'Ingin wewenang/kekuasaan lebih',
                    'B' => 'Ingin kesempatan baru',
                    'C' => 'Menghindari perselisihan/konflik apapun',
                    'D' => 'Ingin arahan yang jelas',
                ],
                'char_most' => [
                    'A' => 'S',
                    'B' => 'X',
                    'C' => 'D',
                    'D' => 'C'
                ],
                'char_least' => [
                    'A' => 'I',
                    'B' => 'S',
                    'C' => 'D',
                    'D' => 'C'
                ],
            ],
            // 22
            [
                'statements' => [
                    'A' => 'Penyemangat/pendukung yang baik',
                    'B' => 'Pendengar yang baik',
                    'C' => 'Penganalisa yang baik',
                    'D' => 'Pendelegasian yang baik/pandai membagi tugas',
                ],
                'char_most' => [
                    'A' => 'I',
                    'B' => 'X',
                    'C' => 'D',
                    'D' => 'S'
                ],
                'char_least' => [
                    'A' => 'I',
                    'B' => 'C',
                    'C' => 'D',
                    'D' => 'S'
                ],
            ],
            // 23
            [
                'statements' => [
                    'A' => 'Peraturan perlu diuji',
                    'B' => 'Peraturan membuat menjadi adil',
                    'C' => 'Peraturan membuat menjadi membosankan',
                    'D' => 'Peraturan membuat menjadi aman',
                ],
                'char_most' => [
                    'A' => 'I',
                    'B' => 'C',
                    'C' => 'D',
                    'D' => 'X'
                ],
                'char_least' => [
                    'A' => 'I',
                    'B' => 'X',
                    'C' => 'D',
                    'D' => 'S'
                ],
            ],
            // 24
            [
                'statements' => [
                    'A' => 'Dapat dipercaya dan diandalkan',
                    'B' => 'Kreatif, unik',
                    'C' => 'Berorientasi pada hasil/profit/untung',
                    'D' => 'Memegang teguh standar yang tinggi, akurat',
                ],
                'char_most' => [
                    'A' => 'D',
                    'B' => 'S',
                    'C' => 'I',
                    'D' => 'C'
                ],
                'char_least' => [
                    'A' => 'D',
                    'B' => 'S',
                    'C' => 'I',
                    'D' => 'C'
                ],
            ]
        ];
    }

    /**
     * Conversions data
     *
     * @return Array
     */
    public static function conversions()
    {
        return [
            'most' => [
                'D' => [
                    20 => 100,
                    16 => 97,
                    15 => 93,
                    14 => 95,
                    13 => 83,
                    12 => 79,
                    11 => 76,
                    10 => 73,
                    9 => 65,
                    8 => 59,
                    7 => 53,
                    6 => 48,
                    5 => 43,
                    4 => 38,
                    3 => 33,
                    2 => 24,
                    1 => 15,
                    0 => 3,
                ],
                'I' => [
                    17 => 100,
                    16 => 100,
                    15 => 100,
                    14 => 100,
                    13 => 100,
                    12 => 100,
                    11 => 100,
                    10 => 97,
                    9 => 92,
                    8 => 88,
                    7 => 83,
                    6 => 73,
                    5 => 68,
                    4 => 56,
                    3 => 43,
                    2 => 35,
                    1 => 20,
                    0 => 8,
                ],
                'S' => [
                    19 => 100,
                    16 => 100,
                    15 => 100,
                    14 => 100,
                    13 => 100,
                    12 => 97,
                    11 => 89,
                    10 => 85,
                    9 => 78,
                    8 => 74,
                    7 => 67,
                    6 => 61,
                    5 => 55,
                    4 => 45,
                    3 => 38,
                    2 => 30,
                    1 => 22,
                    0 => 11,
                ],
                'C' => [
                    15 => 100,
                    14 => 100,
                    13 => 100,
                    12 => 100,
                    11 => 100,
                    10 => 100,
                    9 => 96,
                    8 => 89,
                    7 => 84,
                    6 => 73,
                    5 => 66,
                    4 => 54,
                    3 => 40,
                    2 => 29,
                    1 => 16,
                    0 => 0,
                ],
            ],
            'least' => [
                'D' => [
                    0 => 100,
                    1 => 87,
                    2 => 75,
                    3 => 67,
                    4 => 59,
                    5 => 53,
                    6 => 48,
                    7 => 42,
                    8 => 38,
                    9 => 31,
                    10 => 28,
                    11 => 25,
                    12 => 21,
                    13 => 15,
                    14 => 11,
                    15 => 8,
                    16 => 5,
                    21 => 1,
                ],
                'I' => [
                    0 => 100,
                    1 => 86,
                    2 => 75,
                    3 => 67,
                    4 => 55,
                    5 => 46,
                    6 => 37,
                    7 => 28,
                    8 => 22,
                    9 => 15,
                    10 => 10,
                    11 => 7,
                    19 => 0,
                ],
                'S' => [
                    0 => 100,
                    1 => 96,
                    2 => 85,
                    3 => 75,
                    4 => 67,
                    5 => 59,
                    6 => 53,
                    7 => 42,
                    8 => 37,
                    9 => 29,
                    10 => 23,
                    11 => 15,
                    12 => 8,
                    13 => 4,
                    19 => 1,
                ],
                'C' => [
                    0 => 100,
                    1 => 95,
                    2 => 82,
                    3 => 74,
                    4 => 65,
                    5 => 58,
                    6 => 52,
                    7 => 47,
                    8 => 39,
                    9 => 33,
                    10 => 23,
                    11 => 14,
                    12 => 7,
                    13 => 3,
                    16 => 0,
                ]
            ]
        ];
    }

    /**
     * Behavioral codes data
     *
     * @return Array
     */
    public static function behavioralCodes()
    {
        return [
            'HCLDLILS' => [
                'name' => 'Analyzer #7',
                'type' => 'C'
            ],
            'HCLILDLS' => [
                'name' => 'Analyzer #7',
                'type' => 'C'
            ],
            'HCHSLDLI' => [
                'name' => 'Coordinator #21',
                'type' => 'C'
            ],
            'HCHSLILD' => [
                'name' => 'Coordinator #21',
                'type' => 'C'
            ],
            'HCHDLILS' => [
                'name' => 'Implementor #24',
                'type' => 'C'
            ],
            'HCHDLSLI' => [
                'name' => 'Implementor #24',
                'type' => 'C'
            ],
            'HCHILSLD' => [
                'name' => 'Analyzer #60',
                'type' => 'C'
            ],
            'HCHILDLS' => [
                'name' => 'Analyzer #60',
                'type' => 'C'
            ],
            'HCHIHDLS' => [
                'name' => 'Analyzer #55',
                'type' => 'C'
            ],
            'HCHDHILS' => [
                'name' => 'Analyzer #55',
                'type' => 'C'
            ],
            'HCHSHDLI' => [
                'name' => 'Analyzer #38',
                'type' => 'C'
            ],
            'HCHDHSLI' => [
                'name' => 'Analyzer #38',
                'type' => 'C'
            ],
            'HDLILCLS' => [
                'name' => 'Conductor #1',
                'type' => 'D'
            ],
            'HDLILCLS' => [
                'name' => 'Conductor #1',
                'type' => 'D'
            ],
            'HDHILSLC' => [
                'name' => 'Persuader #12',
                'type' => 'D'
            ],
            'HDHILCLS' => [
                'name' => 'Persuader #12',
                'type' => 'D'
            ],
            'HDHCLILS' => [
                'name' => 'Implementor #9',
                'type' => 'D'
            ],
            'HDHCLSLI' => [
                'name' => 'Implementor #9',
                'type' => 'D'
            ],
            'HDHSLILC' => [
                'name' => 'Conductor #57',
                'type' => 'D'
            ],
            'HDHSLCLI' => [
                'name' => 'Conductor #57',
                'type' => 'D'
            ],
            'HDHIHCLS' => [
                'name' => 'Conductor #27',
                'type' => 'D'
            ],
            'HDHCHILS' => [
                'name' => 'Conductor #27',
                'type' => 'D'
            ],
            'HDHSHCLI' => [
                'name' => 'Conductor #42',
                'type' => 'D'
            ],
            'HDHCHSLI' => [
                'name' => 'Conductor #42',
                'type' => 'D'
            ],
            'HSLDLCLI' => [
                'name' => 'Supporter #5',
                'type' => 'S'
            ],
            'HSLDLILC' => [
                'name' => 'Supporter #5',
                'type' => 'S'
            ],
            'HSLCLDLI' => [
                'name' => 'Supporter #5',
                'type' => 'S'
            ],
            'HSLCLILD' => [
                'name' => 'Supporter #5',
                'type' => 'S'
            ],
            'HSLILDLC' => [
                'name' => 'Supporter #5',
                'type' => 'S'
            ],
            'HSLILCLD' => [
                'name' => 'Supporter #5',
                'type' => 'S'
            ],
            'HSHCLDLI' => [
                'name' => 'Coordinator #20',
                'type' => 'S'
            ],
            'HSHCLILD' => [
                'name' => 'Coordinator #20',
                'type' => 'S'
            ],
            'HSHILDLC' => [
                'name' => 'Relater #17',
                'type' => 'S'
            ],
            'HSHILCLD' => [
                'name' => 'Relater #17',
                'type' => 'S'
            ],
            'HSHDLILC' => [
                'name' => 'Supporter #59',
                'type' => 'S'
            ],
            'HSHDLCLI' => [
                'name' => 'Supporter #59',
                'type' => 'S'
            ],
            'HSHIHCLD' => [
                'name' => 'Supporter #35',
                'type' => 'S'
            ],
            'HSHCHILD' => [
                'name' => 'Supporter #35',
                'type' => 'S'
            ],
            'HSHDHILC' => [
                'name' => 'Supporter #50',
                'type' => 'S'
            ],
            'HSHIHDLC' => [
                'name' => 'Supporter #50',
                'type' => 'S'
            ],
            'HILDLCLS' => [
                'name' => 'Promoter #3',
                'type' => 'I'
            ],
            'HILDLSLC' => [
                'name' => 'Promoter #3',
                'type' => 'I'
            ],
            'HILCLDLS' => [
                'name' => 'Promoter #3',
                'type' => 'I'
            ],
            'HILCLSLD' => [
                'name' => 'Promoter #3',
                'type' => 'I'
            ],
            'HILSLDLC' => [
                'name' => 'Promoter #3',
                'type' => 'I'
            ],
            'HILSLCLD' => [
                'name' => 'Promoter #3',
                'type' => 'I'
            ],
            'HIHDLSLC' => [
                'name' => 'Persuader #13',
                'type' => 'I'
            ],
            'HIHDLCLS' => [
                'name' => 'Persuader #13',
                'type' => 'I'
            ],
            'HIHSLDLC' => [
                'name' => 'Relater #16',
                'type' => 'I'
            ],
            'HIHSLCLD' => [
                'name' => 'Relater #16',
                'type' => 'I'
            ],
            'HIHCLDLS' => [
                'name' => 'Promoter #58',
                'type' => 'I'
            ],
            'HIHCLSLD' => [
                'name' => 'Promoter #58',
                'type' => 'I'
            ],
            'HIHSHCLD' => [
                'name' => 'Promoter #47',
                'type' => 'I'
            ],
            'HIHCHSLD' => [
                'name' => 'Promoter #47',
                'type' => 'I'
            ],
            'HIHDHSLC' => [
                'name' => 'Promoter #30',
                'type' => 'I'
            ],
            'HIHSHDLC' => [
                'name' => 'Promoter #30',
                'type' => 'I'
            ],
        ];
    }

    /**
     * Conclusions data
     *
     * @return Array
     */
    public static function conclusions()
    {
        return [
            'Analyzer #7' => [
                'key_strengths' => [
                    'Ability to do tough assignments right the first time',
                    'Alert and sensitive to errors where precision and accuracy are required',
                    'Professional and disciplined in their approach as it relates to an area of their expertise',
                    'Organization skills, wise use of time'
                ],
                'improve_effectiveness_by' => [
                    'Being less of a perfesionist',
                    'Going "by the book" less often',
                    'More enthusiasm, less reliance on data'
                ],
                'tendencies' => [
                    'goal' => 'Accuracy and quality',
                    'judges' => 'Correct results proff and facts presented',
                    'influence' => 'Use of data and exactness',
                    'value_to_org' => 'High standards for self and subordinates, well-disciplined',
                    'overuses' => 'Rules and regulations',
                    'when_under_stress' => 'Becomes overly critical of self and others',
                    'fears' => 'High-risk decisions'
                ]
            ],
            'Coordinator #21' => [
                'key_strengths' => [
                    'Ability to set and accomplish high standards of conduct and work',
                    'Alert and sensitive to problems, rules, errors and procedures',
                    'Ability to make tough decisions without letting emotions interfere',
                    'Ability to understand and preserve the need for quality systems'
                ],
                'improve_effectiveness_by' => [
                    'Stating true feelings on issues',
                    'Less concern that change may damage relationships or quality',
                    'More confidence, interdependency'
                ],
                'tendencies' => [
                    'goal' => 'Security and neatness',
                    'judges' => 'Precise standards',
                    'influence' => 'Dependability, attention to detail',
                    'value_to_org' => 'Conscientiousness, maintain standards',
                    'overuses' => 'Dependency on standard operating procedure',
                    'when_under_stress' => 'Becomes introverted, obstinate',
                    'fears' => 'Antagonism'
                ]
            ],
            'Implementor #24' => [
                'key_strengths' => [
                    'Ability to do quality work while exploring new ways to increase quantity',
                    'Ability to make tough decisions, using insight and facts, while remaining unemotional',
                    'Ability to push hard to discover correct acceptable solutions to problems',
                    'Experts and challenges team to higher performance standards'
                ],
                'improve_effectiveness_by' => [
                    'Being sensitive to the feeling of others',
                    'Being less blunt and direct',
                    'Showing more sincerity'
                ],
                'tendencies' => [
                    'goal' => 'Designing systems',
                    'judges' => 'Their own high standards',
                    'influence' => 'Setting a pace in developing systems',
                    'value_to_org' => 'Precise, conscientious worker',
                    'overuses' => 'Facts and figures',
                    'when_under_stress' => 'Takes on too much',
                    'fears' => 'Disorganization'
                ]
            ],
            'Analyzer #60' => [
                'key_strengths' => [
                    'A promoter of quality systems',
                    'A good sense of urgency balanced with maintaining high standards',
                    'Organized, even in relationships. Appreciates company of people with similar ideas, likes being organized and quality-conscious',
                    'Sensitive to change in the social and work environment'
                ],
                'improve_effectiveness_by' => [
                    'Being more accepting of other\'s ideas and beliefs',
                    'Setting realistic goals',
                    'Not being overly sensitive to other people\'s comments'
                ],
                'tendencies' => [
                    'goal' => 'Diplomatic',
                    'judges' => 'Who they know, prestige and accomplishments',
                    'influence' => 'Strategy in good relations',
                    'value_to_org' => 'Creates a good working environment',
                    'overuses' => 'Tactfulness',
                    'when_under_stress' => 'Becomes too suave',
                    'fears' => 'Having to trade quality for good relationships'
                ]
            ],
            'Analyzer #55' => [
                'key_strengths' => [
                    'Ability to complete many projects with high standards',
                    'Ability to defend strong feelings on certain issues',
                    'Ability to get results through people',
                    'Maintains standards by following proven procedures'
                ],
                'improve_effectiveness_by' => [
                    'Not over controlling the situation',
                    'Being realistic when appraising people',
                    'Becoming more adaptable under pressure'
                ],
                'tendencies' => [
                    'goal' => 'Many challenges',
                    'judges' => 'Their skills and commitment',
                    'influence' => 'Being optimistic',
                    'value_to_org' => 'Combined tasks and people skills',
                    'overuses' => 'Standard procedures',
                    'when_under_stress' => 'Becomes controlling',
                    'fears' => 'Missing deadlines'
                ]
            ],
            'Analyzer #38' => [
                'key_strengths' => [
                    'Ability to fight hard for results and/or procedures to ensure quality and correctness',
                    'Ability to ask the right questions to uncover hidden facts',
                    'Avoids favoritism when evaluating personnel',
                    'Will combine analytical and intuitive skills when dealing with complex issues'
                ],
                'improve_effectiveness_by' => [
                    'Being less analytical in trying to achieve correctness',
                    'Not hiding emotion and expressing more thoughts to others',
                    'Sharing information, team cooperation'
                ],
                'tendencies' => [
                    'goal' => 'Problem solver',
                    'judges' => 'Their use of data',
                    'influence' => 'Facts and figures',
                    'value_to_org' => 'Independently accepts analytically challenging assignments',
                    'overuses' => 'Perfectionism',
                    'when_under_stress' => 'Becomes blunt',
                    'fears' => 'People contact, high risks and lack of privacy'
                ]
            ],
            'Conductor #1' => [
                'key_strengths' => [
                    'Ability to tackle tough problems dealing with many issues',
                    'Forward-looking, aggressive and competitive',
                    'Ability to work in an environment that has variety and change',
                    'Initiates activity and sets a pace to achieve desired results'
                ],
                'improve_effectiveness_by' => [
                    'Being less intense, opinionated and blunt',
                    'Not coercing others who may not be as committed to a project as they are',
                    'Patience, concern for people, humility'
                ],
                'tendencies' => [
                    'goal' => 'Dominance and independence',
                    'judges' => 'Their ability for getting the task done quickly',
                    'influence' => 'Force of character, persistence',
                    'value_to_org' => 'Show \'em attitude',
                    'overuses' => 'Challenge and contest',
                    'when_under_stress' => 'Becomes quiet and analytical',
                    'fears' => 'Losing control'
                ]
            ],
            'Persuader #12' => [
                'key_strengths' => [
                    'Results-oriented with a sense of urgency to accomplish goals and meet deadlines',
                    'Decisive and aggressive when presented with challenges',
                    'Initiates activity through other people to get desired results',
                    'Extroverted and actively seeks relationships with a variety of people'
                ],
                'improve_effectiveness_by' => [
                    'Becoming less irritated if deadlines are delayed or missed',
                    'Not taking on too many responsibilities at one time, more consistency',
                    'More follow-through, directness, lower expectations'
                ],
                'tendencies' => [
                    'goal' => 'Aggressive and confident to win',
                    'judges' => 'Ability to communicate and to think',
                    'influence' => 'Friendliness and desire for results',
                    'value_to_org' => 'Good planner, problem solver and resourceful',
                    'overuses' => 'Position and their way',
                    'when_under_stress' => 'Becomes restless, impatient and insensitive',
                    'fears' => 'Losing and failing'
                ]
            ],
            'Implementor #9' => [
                'key_strengths' => [
                    'Sets high standards for self and others, expecting performance and teamwork.',
                    'Aware and sensitive to the cost of errors and mistakes',
                    'Structured in use of time',
                    'Systematically solves problems without letting emotions influence decisions'
                ],
                'improve_effectiveness_by' => [
                    'More warmth and appreciation of other team members',
                    'Being more consistent with decisions-quality versus quality issues.',
                    'Not being so blunt and critical of people who do not meet their standards'
                ],
                'tendencies' => [
                    'goal' => 'Dominance and pioneering',
                    'judges' => 'Their own standards, progressive ideas',
                    'influence' => 'Competition and unique challenges',
                    'value_to_org' => 'Initiate change on their own',
                    'overuses' => 'Bluntness, overly critical',
                    'when_under_stress' => 'Driving and demanding',
                    'fears' => 'Not being influential'
                ]
            ],
            'Conductor #57' => [
                'key_strengths' => [
                    'Ability to come up with new ideas and follow them through to completion',
                    'Appreciattes others who are team players',
                    'Ability to see the "big picture" along with the small details',
                    'Determination and persistence'
                ],
                'improve_effectiveness_by' => [
                    'Not being overly focused on one issue and missing other opportunities',
                    'Being less concerned with personal standards',
                    'checking priorites with others'
                ],
                'tendencies' => [
                    'goal' => 'Determined',
                    'judges' => 'amount of work completed',
                    'influence' => 'Tenacity and persistence',
                    'value_to_org' => 'Results oriented with a sense of consistency',
                    'overuses' => 'Reliance on self',
                    'when_under_stress' => 'Stubborn, quiet and non demonstrative',
                    'fears' => 'Involvement with too many people'
                ]
            ],
            'Conductor #27' => [
                'key_strengths' => [
                    'Results through people',
                    'Faces obstacles and challenges with optimism',
                    'High personal goals',
                    'Sense of urgency to make things happen'
                ],
                'improve_effectiveness_by' => [
                    'Being more careful with details',
                    'slowing down when delegating complete projects',
                    'Developing consistency when disciplining others'
                ],
                'tendencies' => [
                    'goal' => 'Aggressively works through people to achieve results'    ,
                    'judges' => 'Team participation',
                    'influence' => 'Persuasion',
                    'value_to_org' => 'Innovated and futuristic',
                    'overuses' => 'Strong will',
                    'when_under_stress' => 'Impatient and driving',
                    'fears' => 'Not achieveing desired goals'
                ]
            ],
            'Conductor #42' => [
                'key_strengths' => [
                    'Ability to express ideas without getting emotinally attached',
                    'Single minded concentration on goals and vital issues',
                    'Careful scruinizing of others who may impact your performance',
                    'Ability to explain technical data clearly and translate theory into wrokable solutions'
                ],
                'improve_effectiveness_by' => [
                    'Sharing knowledge, thoughts and emotions with others',
                    'Not hestitating to act under heavy pressure',
                    'Developing more people skills and verbalization'
                ],
                'tendencies' => [
                    'goal' => 'Self sufficient',
                    'judges' => 'Comprehension and power of reasoning',
                    'influence' => 'Rational, indirect manner',
                    'value_to_org' => 'Self starter, goal oriented',
                    'overuses' => 'Self reliance',
                    'when_under_stress' => 'Ambivalent andpessimistic',
                    'fears' => 'Dealines without time for quality assurances'
                ]
            ],
            'Supporter #5' => [
                'key_strengths' => [
                    'Ability to present self in a calm and controlled manner, using the ability toconcentrate as a means to listen and learn',
                    'Ability to stay with a task that provides meaningful contribution to the organization',
                    'A team member who can be open, patient and tolerant of differences',
                    'Enjoys prasing others'
                ],
                'improve_effectiveness_by' => [
                    'Projecting a senses of urgency when the need arise',
                    'Less reliance on routines',
                    'More iniyiatives, adaptability to change'
                ],
                'tendencies' => [
                    'goal' => 'Dependability and stability',
                    'judges' => 'Their consistency',
                    'influence' => 'A congenial disposition, serving others',
                    'value_to_org' => 'Stabilizes the environment in a friendly manner',
                    'overuses' => 'Composure',
                    'when_under_stress' => 'Non-expressive',
                    'fears' => 'Not being apprecaited, and the unknown'
                ]
            ],
            'Coordinator #20' => [
                'key_strengths' => [
                    'Abilityto begin a project and follow through to completion',
                    'Willing to work for a leader and a cause',
                    'Excels in seeking solutions to problems through logic that will be comprehensive and pleasing to all involved',
                    'Demonstrates positive leadership through the consideration shown to the feelings of the others on the team'
                ],
                'improve_effectiveness_by' => [
                    'Learning to be self promoting',
                    'Using a direct approach',
                    'Displaying concerns and feelings'
                ],
                'tendencies' => [
                    'goal' => 'Achieve high standards set for self',
                    'judges' => 'Use of Knowledge',
                    'influence' => 'Ability to follow through',
                    'value_to_org' => 'Adding focus and logic to existing needs',
                    'overuses' => 'Resistance to change',
                    'when_under_stress' => 'Becomes determined, stubborn',
                    'fears' => 'Not meeting specific requirements'
                ]
            ],
            'Relater #17' => [
                'key_strengths' => [
                    'Good listening skills with the baility to emphatize with ppeople',
                    'Skilled at helping and supporting others achieve goals and aspirations',
                    'Gifted at accepting people\'s sentiments, beliefs and values',
                    'Ability to create an environment where people feel significant'
                ],
                'improve_effectiveness_by' => [
                    'Being assertive and decisiev',
                    'Being less accepting of the status quo',
                    'More strenghth, firmness and self assertion'
                ],
                'tendencies' => [
                    'goal' => 'Acceptance',
                    'judges' => 'Loyalty,sincereity, dependability',
                    'influence' => 'Offering understanding, firenship',
                    'value_to_org' => 'Supports, harmonizes and offers stability under pressure',
                    'overuses' => 'Kindness, compassion',
                    'when_under_stress' => 'Withdrawn',
                    'fears' => 'Dissension, conflict, not being killed'
                ]
            ],
            'Supporter #59' => [
                'key_strengths' => [
                    'Ability to take a problem and follow through to successful completion',
                    'Persistent, determined, tenacious and logical in the pursuit of results',
                    'Excels at maintaning relationships both on and off the job',
                    'Team player who will display leadership skills and stand up aggressively for what they believe in'
                ],
                'improve_effectiveness_by' => [
                    'Demonstrating less passive behavior even if it affects their security',
                    'Using new and creative thinking when problems solving',
                    'Not resisting new situations that may be out of their comfort zone'
                ],
                'tendencies' => [
                    'goal' => 'Personal achievement',
                    'judges' => 'Their accomplishments and successes',
                    'influence' => 'Perserverance',
                    'value_to_org' => 'Works independently and like challenge',
                    'overuses' => 'Bluntness, overly critical',
                    'when_under_stress' => 'Obstinate, inflexible, relentless',
                    'fears' => 'Not attending desired results'
                ]
            ],
            'Supporter #35' => [
                'key_strengths' => [
                    'Ability to be supportive, friendly and optimistic in any relationship',
                    'Sociable with the ability to enjoy the uniqueness of each human being',
                    'Ability to use balanced judgement, bringing stability to the entire team',
                    'Good at analyzing situations that can be felt, touched, seen, heard, personally observed or experienced'
                ],
                'improve_effectiveness_by' => [
                    'Staying focused on roles and expectations to be effective',
                    'Having a sense of urgency ',
                    'Sincere appreciation of shortcut methods'
                ],
                'tendencies' => [
                    'goal' => 'Status quo',
                    'judges' => 'Friendship',
                    'influence' => 'Consistency of performance, accomodation',
                    'value_to_org' => 'Planner, consistency, maintains pace',
                    'overuses' => 'Modesty, conservatism',
                    'when_under_stress' => 'A grudgeholder',
                    'fears' => 'Conflict, losin face'
                ]
            ],
            'Supporter #50' => [
                'key_strengths' => [
                    'Ability to emphathize with others feelings while maintaining their own independence',
                    'Excels at projects that require a determination and persistence to win',
                    'Positive influence on uncooperative or negative team members',
                    'Good at bringing people to the negoatitaion process and listening to opposing views'
                ],
                'improve_effectiveness_by' => [
                    'Prioritizing daily activities',
                    'More weighing of pros and cons',
                    'Being more firm and consistent in their convictions'
                ],
                'tendencies' => [
                    'goal' => 'Success through consistency',
                    'judges' => 'Loyal friendships',
                    'influence' => 'Persuasive people skills',
                    'value_to_org' => 'solves problems creatively and works to innovate through people',
                    'overuses' => 'Intensity',
                    'when_under_stress' => 'Passionate, foreceful',
                    'fears' => 'Not being supported by the team and change'
                ]
            ],
            'Promoter #3' => [
                'key_strengths' => [
                    'Very optimistic with a positive sense of humour',
                    'Place focus on people and high trust in relationships',
                    'Develops friendships quickly, enjoys networking',
                    'Uses a consensus approach to decision making'
                ],
                'improve_effectiveness_by' => [
                    'Keeping sight of career goals',
                    'Being less concerned with the feelings of others',
                    'Being organized and having a realistic attitude'
                ],
                'tendencies' => [
                    'goal' => 'Obliging and accomodating',
                    'judges' => 'Their warmth',
                    'influence' => 'Friendliness and interpersonal skills',
                    'value_to_org' => 'Communicates the "big dream", ability to bring team together',
                    'overuses' => 'Dependency on others and optimism',
                    'when_under_stress' => 'emotional, too trusting',
                    'fears' => 'Not being liked enough'
                ]
            ],
            'Persuader #13' => [
                'key_strengths' => [
                    'Ability to influence people to their way of thinking',
                    'Communicates in a very open manner',
                    'Ability to calm connflicts situations',
                    'Ability to promote new ideas and products'
                ],
                'improve_effectiveness_by' => [
                    'Making decisions based less on emotions',
                    'Being willling to confront when required',
                    'Having realitic deadlines and praticing good time management'
                ],
                'tendencies' => [
                    'goal' => 'Maintain firendships',
                    'judges' => 'Influential contacts, commitment',
                    'influence' => 'Inspirtaion and charisma',
                    'value_to_org' => 'Stable, dependable, wide range of friendships',
                    'overuses' => 'Enthusiasm',
                    'when_under_stress' => 'Overly verbal ',
                    'fears' => 'Failure'
                ]
            ],
            'Relater #16' => [
                'key_strengths' => [
                    'Ability to help others using warmth, empathy and understanding',
                    'Protects and values both people and thigs',
                    'Good listener and talker'
                ],
                'improve_effectiveness_by' => [
                    'Being assertive and decisive in certain situations',
                    'Not avoding confrontation, even when risky',
                    'More initiative, sense of urgency'
                ],
                'tendencies' => [
                    'goal' => 'Maintain long term relationship',
                    'judges' => 'Thie loyalty to the realtionship',
                    'influence' => 'Personal realtionships, setting a good example',
                    'value_to_org' => 'Good listener, patient with others',
                    'overuses' => 'Tolerance',
                    'when_under_stress' => 'A grudge-holder, uneasy under stressful situations',
                    'fears' => 'Confrontation'
                ]
            ],
            'Promoter #58' => [
                'key_strengths' => [
                    'Ability to handle difficult situations tactfully, sensistive to the needs of people',
                    'Ability to create a pleasant, comfortable atmosphere',
                    'Ability to promote ideas effectively',
                    'Prefers a fast-paced environment'
                ],
                'improve_effectiveness_by' => [
                    'Being less analytical in trying to achieve correctness',
                    'Communicating less information when selling products or ideas',
                    'Being more assertive'
                ],
                'tendencies' => [
                    'goal' => 'Approval and acceptance',
                    'judges' => 'Their ability to read verbal and nonverbal clues',
                    'influence' => 'Poise and confidence',
                    'value_to_org' => 'Relieves tension and promotes people and projects',
                    'overuses' => 'Control of conversation',
                    'when_under_stress' => 'Verbal, caustic with others',
                    'fears' => 'Loss of uniqueness'
                ]
            ],
            'Promoter #47' => [
                'key_strengths' => [
                    'Ability to be adaptable in many situations',
                    'An optimistic,sociable and cooperative team player',
                    'will attempt to bring the team together in a well-organized manner',
                    'Patience to listen to what others are saying'
                ],
                'improve_effectiveness_by' => [
                    'Being less accomodating of the others',
                    'More consistent display of assertiveness',
                    'More directness and better time management'
                ],
                'tendencies' => [
                    'goal' => 'Systematic results through others',
                    'judges' => 'Their ability to communicate and yo think',
                    'influence' => 'Diplomacy',
                    'value_to_org' => 'Careful and personable',
                    'overuses' => 'Position and their standard',
                    'when_under_stress' => 'Possessive and overly sensistive',
                    'fears' => 'Not being part of the team'
                ]
            ],
            'Promoter #30' => [
                'key_strengths' => [
                    'Ability to be persiasive, assertive',
                    'Ability to be independent when the need arises',
                    'Ability to create and promote an idea',
                    'ability to present ideas in a positive and womewhat direct manner'
                ],
                'improve_effectiveness_by' => [
                    'Being less opinionated',
                    'Gathering enough information before acting',
                    'More attention to details, organization'
                ],
                'tendencies' => [
                    'goal' => 'Sociable and convincing',
                    'judges' => 'Their dedication and tenacity',
                    'influence' => 'Taking repsonsibility',
                    'value_to_org' => 'Enthusiasm and directness with new ideas and opinions',
                    'overuses' => 'Ambitiousness',
                    'when_under_stress' => 'Superfiacial',
                    'fears' => 'Not being seen as a team player'
                ]
            ],
        ];
    }
}
