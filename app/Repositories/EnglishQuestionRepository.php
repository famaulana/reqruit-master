<?php

namespace App\Repositories;

use App\Repositories\AppConfigRepository;

/**
 * English question repository class
 */
class EnglishQuestionRepository
{
    /**
     * Get vocabulary question
     *
     * @return Array
     */
    public static function vocabularyQuestion()
    {
        $config = new AppConfigRepository();

        $story = '<p>My name [_1_] Steven Parker. I am [_2_] Los Angeles. I was [_3_] on 23rd July 1980. I am an [_4_] I work in a construction company. I am [_5_] for conducting land surveys, making maps and preparing detailed plans for [_6_] projects. I work from Monday to Friday. I don’t go to work on Saturdays and Sundays.</p>
<p>I am [_7_] and have 2 children; a son and a daughter. My son, Mike, is 10 years old and my daughter, Katie, is 8 years old. My wife is a teacher. She [_8_] in an elementary school. She really likes her job. She has been working at the school for 5 years.</p>
<p>I finish work at 5 P.M. and usually arrive home around 6 P.M. My wife always [_9_] dinner for our family. After dinner, my wife and I help Mike and Katie do their [_10_]. We go to bed at 9.30 P.M.</p>';

        $choices = [
            [
                'ans_no' => 1,
                'answer' => 'is',
            ],
            [
                'ans_no' => 2,
                'answer' => 'from',
            ],
            [
                'ans_no' => 3,
                'answer' => 'born',
            ],
            [
                'ans_no' => 4,
                'answer' => 'engineer',
            ],
            [
                'ans_no' => 5,
                'answer' => 'responsible',
            ],
            [
                'ans_no' => 6,
                'answer' => 'construction',
            ],
            [
                'ans_no' => 7,
                'answer' => 'married',
            ],
            [
                'ans_no' => 8,
                'answer' => 'teaches',
            ],
            [
                'ans_no' => 9,
                'answer' => 'prepares',
            ],
            [
                'ans_no' => 10,
                'answer' => 'homework',
            ],
        ];

        return [
            'q_group_no' => 1,
            'story' => $story,
            'choices' => $choices,
            'timer' => $config->getConfig('english_test_session_2_timer')
        ];
    }

    /**
     * Get listening questions
     *
     * @return Array
     */
    public static function listeningQuestions()
    {
        $config = new AppConfigRepository();

        return [
            [
                'q_group_no' => 1,
                'audio' => url('/audio/english-test/CONVERSATION-1.mp3'),
                'questions' => [
                    [
                        'q_no' => 1,
                        'question' => 'What is the woman\'s name?',
                        'choices' => [
                            'a' => 'Annie',
                            'b' => 'Carrie',
                            'c' => 'Rudy',
                            'd' => 'Sally',
                        ],
                        'correct' => 'a',
                        'timer' => 15
                    ],
                    [
                        'q_no' => 2,
                        'question' => 'What is the man’s name?',
                        'choices' => [
                            'a' => 'Jerry',
                            'b' => 'Rudy',
                            'c' => 'Annie',
                            'd' => 'Perry',
                        ],
                        'correct' => 'b',
                        'timer' => 15
                    ],
                    [
                        'q_no' => 3,
                        'question' => 'Who is the new student?',
                        'choices' => [
                            'a' => 'Annie',
                            'b' => 'Sally',
                            'c' => 'Jerry',
                            'd' => 'Rudy',
                        ],
                        'correct' => 'd',
                        'timer' => 15
                    ],
                    [
                        'q_no' => 4,
                        'question' => 'When did Rudy join the English course?',
                        'choices' => [
                            'a' => 'Next week',
                            'b' => 'A few week',
                            'c' => 'Last week',
                            'd' => 'This week',
                        ],
                        'correct' => 'c',
                        'timer' => 15
                    ],
                    [
                        'q_no' => 5,
                        'question' => 'How many people are there in the conversation?',
                        'choices' => [
                            'a' => '1',
                            'b' => '2',
                            'c' => '3',
                            'd' => '4',
                        ],
                        'correct' => 'b',
                        'timer' => 15
                    ],
                ]
            ],
            [
                'q_group_no' => 2,
                'audio' => url('/audio/english-test/CONVERSATION-2.mp3'),
                'questions' => [
                    [
                        'q_no' => 1,
                        'question' => 'Who has just come back from Bali?',
                        'choices' => [
                            'a' => 'Sally',
                            'b' => 'Jimmy',
                            'c' => 'Kevin',
                            'd' => 'Sally\'s sister',
                        ],
                        'correct' => 'b',
                        'timer' => 15
                    ],
                    [
                        'q_no' => 2,
                        'question' => 'What did Kevin go to Bali for?',
                        'choices' => [
                            'a' => 'Family meeting',
                            'b' => 'Family issue',
                            'c' => 'Family business',
                            'd' => 'Family vacation',
                        ],
                        'correct' => 'd',
                        'timer' => 15
                    ],
                    [
                        'q_no' => 3,
                        'question' => 'How long did Kevin stay in Bali?',
                        'choices' => [
                            'a' => '3 days',
                            'b' => '4 days',
                            'c' => '2 days',
                            'd' => '5 days',
                        ],
                        'correct' => 'a',
                        'timer' => 15
                    ],
                    [
                        'q_no' => 4,
                        'question' => 'How many children does Kevin have?',
                        'choices' => [
                            'a' => '5',
                            'b' => '4',
                            'c' => '3',
                            'd' => '2',
                        ],
                        'correct' => 'd',
                        'timer' => 15
                    ],
                    [
                        'q_no' => 5,
                        'question' => 'Who lives in Bali?',
                        'choices' => [
                            'a' => 'Sally',
                            'b' => 'Sally\'s sister',
                            'c' => 'Kevin\'s family',
                            'd' => 'Jimmy',
                        ],
                        'correct' => 'b',
                        'timer' => 15
                    ],
                ]
            ],
            [
                'q_group_no' => 3,
                'audio' => url('/audio/english-test/CONVERSATION-3.mp3'),
                'questions' => [
                    [
                        'q_no' => 1,
                        'question' => 'What does Sarah need help for?',
                        'choices' => [
                            'a' => 'Someone to copy some documents for her.',
                            'b' => 'Someone who has been looking for her all over the place.',
                            'c' => 'Someone to teach her how to operate the photocopy machine.',
                            'd' => 'Someone who can tell her where the new photocopy machine is.',
                        ],
                        'correct' => 'c',
                        'timer' => 25
                    ],
                    [
                        'q_no' => 2,
                        'question' => 'What color indicates that the machine is on?',
                        'choices' => [
                            'a' => 'Yellow',
                            'b' => 'Red',
                            'c' => 'Green',
                            'd' => 'Black',
                        ],
                        'correct' => 'c',
                        'timer' => 25
                    ],
                    [
                        'q_no' => 3,
                        'question' => 'According to Donnie’s instructions, what do we need to do after putting down the document on the glass?',
                        'choices' => [
                            'a' => 'Press the yellow button',
                            'b' => 'Turn on the power button',
                            'c' => 'Press start',
                            'd' => 'Close the lids',
                        ],
                        'correct' => 'd',
                        'timer' => 25
                    ],
                    [
                        'q_no' => 4,
                        'question' => 'Which button do you need to use if you want to have the copy result in color?',
                        'choices' => [
                            'a' => 'Yellow button',
                            'b' => 'Red button',
                            'c' => 'Green button',
                            'd' => 'Black button',
                        ],
                        'correct' => 'a',
                        'timer' => 25
                    ],
                    [
                        'q_no' => 5,
                        'question' => 'What is the last step that you have to do before the machine start working?',
                        'choices' => [
                            'a' => 'Press the start button',
                            'b' => 'Turn on the machine',
                            'c' => 'Close the lids',
                            'd' => 'Turn on the tiny green light',
                        ],
                        'correct' => 'a',
                        'timer' => 25
                    ],
                ]
            ],
            [
                'q_group_no' => 4,
                'audio' => url('/audio/english-test/CONVERSATION-4.mp3'),
                'questions' => [
                    [
                        'q_no' => 1,
                        'question' => 'What is Albert in charge of?',
                        'choices' => [
                            'a' => 'Schedule Department',
                            'b' => 'Procurement Division',
                            'c' => 'Delivery Department',
                            'd' => 'Labor Issues Department',
                        ],
                        'correct' => 'b',
                        'timer' => 30
                    ],
                    [
                        'q_no' => 2,
                        'question' => 'What is the main problem with the suppliers?',
                        'choices' => [
                            'a' => 'Domestic political issues',
                            'b' => 'Bad weather',
                            'c' => 'Labor issues',
                            'd' => 'Shipments delays',
                        ],
                        'correct' => 'd',
                        'timer' => 30
                    ],
                    [
                        'q_no' => 3,
                        'question' => 'According to Albert\'s explanation, when will the products reach them at the latest?',
                        'choices' => [
                            'a' => 'Two months after the bad weather',
                            'b' => 'As soon as possible',
                            'c' => 'Two months behind schedule',
                            'd' => 'Two months after the issues',
                        ],
                        'correct' => 'c',
                        'timer' => 30
                    ],
                    [
                        'q_no' => 4,
                        'question' => 'What is causing the delays to the shipments from China?',
                        'choices' => [
                            'a' => 'Bad weather',
                            'b' => 'Labor issues',
                            'c' => 'Domestic political issues',
                            'd' => 'The production\'s rates',
                        ],
                        'correct' => 'a',
                        'timer' => 30
                    ],
                    [
                        'q_no' => 5,
                        'question' => 'Which suppliers are affected by domestic political issue?',
                        'choices' => [
                            'a' => 'Local',
                            'b' => 'Vietnam',
                            'c' => 'China',
                            'd' => 'Regional',
                        ],
                        'correct' => 'b',
                        'timer' => 30
                    ],
                    [
                        'q_no' => 6,
                        'question' => 'What does Albert suggest them to do about the clients?',
                        'choices' => [
                            'a' => 'To expect the products to arrive on schedule',
                            'b' => 'To get down to business',
                            'c' => 'To inform the clients about the delay',
                            'd' => 'To break down what is really going on',
                        ],
                        'correct' => 'c',
                        'timer' => 30
                    ],
                ]
            ],
        ];
    }
}
