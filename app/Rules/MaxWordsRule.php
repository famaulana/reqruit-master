<?php

namespace App\Rules;

use Illuminate\Validation\Validator;
use Illuminate\Contracts\Validation\Rule;

class MaxWordsRule implements Rule
{
    /**
     * Maximum words
     *
     * @var Integer
     */
    private $max_words;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($max_words = 28)
    {
        $this->max_words = $max_words;
    }

    /**
     * Validation handle name
     *
     * @return String
     */
    public static function handle()
    {
        return 'max_words';
    }

    public function validate(string $attribute, $value, $params, Validator $validator)
    {
        $handle = $this->handle();


        $validator->setCustomMessages([
        $handle => $this->message(),
    ]);

        return $this->passes($attribute, $value);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return str_word_count($value) <= $this->max_words;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute cannot be longer than '.$this->max_words.' words.';
    }
}
