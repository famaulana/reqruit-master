<?php

namespace App\Console\Commands;

use App\Models\Page;
use App\Models\Brand;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;

class SitemapGenerate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate sitemap xml';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // create new sitemap object
        $sitemap = App::make("sitemap");

        $sitemap = $this->sitemapProduct($sitemap);
        $sitemap = $this->sitemapBrand($sitemap);
        $sitemap = $this->sitemapCategory($sitemap);
        $sitemap = $this->sitemapPage($sitemap);

        // generate new sitemapindex that will contain all generated sitemaps above
        $sitemap->addSitemap(url('sitemaps/product/sitemap-product.xml'));
        $sitemap->addSitemap(url('sitemaps/brand/sitemap-brand.xml'));
        $sitemap->addSitemap(url('sitemaps/category/sitemap-category.xml'));
        $sitemap->addSitemap(url('sitemaps/page/sitemap-page.xml'));
        $sitemap->store('sitemapindex', 'sitemap');
    }

    /**
     * Generate sitemap files
     *
     * @param Object $sitemap
     * @param String $type
     * @param Integer $sitemapCounter
     * @return Object $sitemap
     */
    private function sitemapFile(Object $sitemap, String $type, Int $sitemapCounter)
    {
        // you need to check for unused items
        if (!empty($sitemap->model->getItems())) {
            // generate sitemap with last items
            $sitemap->store('xml', 'sitemaps/'.$type.'/sitemap-'.$type.'-' . $sitemapCounter);
            // add sitemap to sitemaps array
            $sitemap->addSitemap(url('sitemaps/'.$type.'/sitemap-'.$type.'-' . $sitemapCounter . '.xml'));
            // reset items array
            $sitemap->model->resetItems();
        }

        return $sitemap;
    }

    /**
     * Product Sitemap
     *
     * @param Object $sitemap
     * @return Object $sitemap
     */
    private function sitemapProduct(Object $sitemap)
    {
        // counters
        $sitemapCounter = 0;

        // get all products from db
        Product::orderBy('created_at', 'desc')->chunk(500, function ($products) use ($sitemap, &$sitemapCounter) {
            foreach ($products as $p) {
                // add product to items array
                $sitemap->add(route('product', $p->slug), $p->updated_at, '1.0', 'weekly');
            }

            // Generate sitemap files
            $sitemap = $this->sitemapFile($sitemap, 'product', $sitemapCounter);

            // count generated sitemap
            $sitemapCounter++;
        });

        // Store sitemap product index
        $sitemap->store('sitemapindex', 'sitemaps/product/sitemap-product');

        return $sitemap;
    }

    /**
     * Brand Sitemap
     *
     * @param Object $sitemap
     * @return Object $sitemap
     */
    private function sitemapBrand(Object $sitemap)
    {
        // counters
        $sitemapCounter = 0;

        // get all brands from db
        Brand::orderBy('created_at', 'desc')->chunk(500, function ($brands) use ($sitemap, &$sitemapCounter) {
            foreach ($brands as $brand) {
                // add brand to items array
                $sitemap->add(route('brand', $brand->slug), $brand->updated_at, '1.0', 'weekly');
            }

            // Generate sitemap files
            $sitemap = $this->sitemapFile($sitemap, 'brand', $sitemapCounter);

            // count generated sitemap
            $sitemapCounter++;
        });

        // Store sitemap brand index
        $sitemap->store('sitemapindex', 'sitemaps/brand/sitemap-brand');

        return $sitemap;
    }

    /**
     * Category Sitemap
     *
     * @param Object $sitemap
     * @return Object $sitemap
     */
    private function sitemapCategory(Object $sitemap)
    {
        // counters
        $sitemapCounter = 0;

        // get all category from db
        Category::orderBy('created_at', 'desc')->chunk(500, function ($categories) use ($sitemap, &$sitemapCounter) {
            foreach ($categories as $category) {
                // add category to items array
                $sitemap->add(route('category', $category->slug), $category->updated_at, '1.0', 'weekly');
            }

            // Generate sitemap files
            $sitemap = $this->sitemapFile($sitemap, 'category', $sitemapCounter);

            // count generated sitemap
            $sitemapCounter++;
        });

        // Store sitemap category index
        $sitemap->store('sitemapindex', 'sitemaps/category/sitemap-category');

        return $sitemap;
    }

    /**
     * Page Sitemap
     *
     * @param Object $sitemap
     * @return Object $sitemap
     */
    private function sitemapPage(Object $sitemap)
    {
        // counters
        $sitemapCounter = 0;

        // get all Page from db
        Page::orderBy('created_at', 'desc')->chunk(500, function ($pages) use ($sitemap, &$sitemapCounter) {
            foreach ($pages as $page) {
                // add page to items array
                $sitemap->add(route('page', $page->slug), $page->updated_at, '1.0', 'weekly');
            }

            // Generate sitemap files
            $sitemap = $this->sitemapFile($sitemap, 'page', $sitemapCounter);

            // count generated sitemap
            $sitemapCounter++;
        });

        // Store sitemap page index
        $sitemap->store('sitemapindex', 'sitemaps/page/sitemap-page');

        return $sitemap;
    }
}
