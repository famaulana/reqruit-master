<?php

namespace App\Helpers;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class AppHelper
{
    /**
     * Format number
     *
     * @param Float $n
     * @param String $locale
     * @return String
     */
    public static function formatNumber(Float $n, String $locale = '')
    {
        if ($locale == 'id-ID') {
            return number_format($n, 0, ',', '.');
        }

        return number_format($n, 2, '.', ',');
    }

    /**
     * Get plain number
     *
     * @param Float $n
     * @return Int
     */
    public static function plainNumber(Float $n)
    {
        return (int)number_format($n, 0, '.', '');
    }

    /**
     * Generate unique random string
     *
     * @param String $table
     * @param String $col
     * @param Int $chars
     * @param String $prefix
     * @param Bool $uppercase
     * @return String
     */
    public static function uniqueRandomString(String $table, String $col, Int $chars = 16, String $prefix = '', Bool $uppercase = false)
    {
        $unique = false;

        // Store tested results in array to not test them again
        $tested = [];

        do {
            // Generate random string of characters
            if ($prefix == '') {
                $random = ($uppercase) ? strtoupper(Str::random($chars)) : Str::random($chars);
            } else {
                $random = ($uppercase) ? strtoupper($prefix . Str::random($chars)) : $prefix . Str::random($chars);
            }

            // Check if it's already testing
            // If so, don't query the database again
            if (in_array($random, $tested)) {
                continue;
            }

            // Check if it is unique in the database
            $count = DB::table($table)->where($col, '=', $random)->count();

            // Store the random character in the tested array
            // To keep track which ones are already tested
            $tested[] = $random;

            // String appears to be unique
            if ($count == 0) {
                // Set unique to true to break the loop
                $unique = true;
            }

            // If unique is still false at this point
            // it will just repeat all the steps until
            // it has generated a random string of characters
        } while (!$unique);


        return $random;
    }

    /**
     * Convert youtube link to youtube embed code
     *
     * @param String $string
     * @return String
     */
    public static function convertYoutubeEmbed(String $string, String $height = null)
    {
        $height = is_null($height) ? 'auto' : $height;

        return preg_replace(
            "/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
            "<iframe width=\"100%\" height=\"$height\" src=\"//www.youtube.com/embed/$2\" allowfullscreen></iframe>",
            $string
        );
    }
}
