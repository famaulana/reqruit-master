<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class PathHelper
{
    // ADMIN
    public static function admin_path($adminId)
    {
        return "admin/$adminId/";
    }

    public static function admin_photo_path($adminId)
    {
        return self::admin_path($adminId) . 'photo/';
    }

    // USER
    public static function user_path($userId)
    {
        return "user/$userId/";
    }

    public static function user_photo_path($userId)
    {
        return self::user_path($userId) . 'photo/';
    }

    public static function user_resume_path($userId)
    {
        return self::user_path($userId) . 'resume/';
    }

    public static function user_video_path($userId)
    {
        return self::user_path($userId) . 'video/';
    }

    public static function user_workplace_photo_path($userId)
    {
        return self::user_path($userId) . 'workplace-photo/';
    }

    // EMPLOYER
    public static function employer_path($employerId)
    {
        return "employer/$employerId/";
    }

    public static function employer_photo_path($employerId)
    {
        return self::employer_path($employerId) . 'photo/';
    }

    // COMPANY
    public static function company_path($companyId)
    {
        return "company/$companyId/";
    }

    public static function company_temp_path()
    {
        return "company/tmp/";
    }

    public static function company_images_path($companyId)
    {
        return self::company_path($companyId) . 'images/';
    }

    // CATEGORY
    public static function category_path($categoryId)
    {
        return "category/$categoryId/";
    }

    public static function category_icon_path($categoryId)
    {
        return self::category_path($categoryId) . "icon/";
    }

    public static function category_featured_image_path($categoryId)
    {
        return self::category_path($categoryId) . "featured-image/";
    }

    public static function category_content_image_path()
    {
        return "category/content-images/";
    }

    // STATIC BLOCK
    public static function static_block_path($staticBlockId)
    {
        return "static-block/$staticBlockId/";
    }

    public static function static_block_content_image_path()
    {
        return "static-block/content-images/";
    }

    // WORK JOB
    public static function workjob_path($workjobId)
    {
        return "workjob/$workjobId/";
    }

    public static function workjob_attachment_path($workjobId)
    {
        return self::workjob_path($workjobId)."attachment/";
    }

    public static function workjob_content_image_path()
    {
        return "workjob/content-images/";
    }

    // PAGE
    public static function page_content_image_path()
    {
        return "page/content-images/";
    }

    // TESTIMONIAL
    public static function testimonial_path($id)
    {
        return "testimonial/$id/";
    }

    // ARTICLE
    public static function article_content_image_path()
    {
        return "article/content-images/";
    }

    public static function article_featured_image_path($id)
    {
        return "article/$id/featured-image/";
    }

    // TESTIMONIAL
    public static function testimonial_avatar_path($id)
    {
        return self::testimonial_path($id)."avatar/";
    }

    // BANNER
    public static function main_banner_path()
    {
        return "banners/main-banner/";
    }

    // CONFIG
    public static function config_image_path()
    {
        return "configs/images/";
    }

    // Menu
    public static function menu_path()
    {
        return "menu/";
    }

    public static function mega_menu_image_path()
    {
        return self::menu_path() . "mega-menu/";
    }

    public static function icon_image_path()
    {
        return self::menu_path() . "icon/";
    }

    /**
     * Delete file
     *
     * @param String $pathToFile
     * @param String $disk
     * @return void
     */
    public static function deleteFile(String $pathToFile, String $disk = '')
    {
        if ($disk == '') {
            $disk = config('filesystems.default');
        }

        if ($disk == 's3') {
            $pathToFile = 'app/public/' . $pathToFile;
        } elseif ($disk == 'local') {
            $pathToFile = 'public/' . $pathToFile;
        }

        try {
            Storage::disk($disk)->delete($pathToFile);
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
        }
    }

    /**
     * Delete directory
     *
     * @param String $path
     * @param String $disk
     * @return void
     */
    public static function deleteDirectory(String $path, String $disk = '')
    {
        if ($disk == '') {
            $disk = config('filesystems.default');
        }

        if ($disk == 's3') {
            $path = 'app/public/' . $path;
        } elseif ($disk == 'local') {
            $path = 'public/' . $path;
        }

        try {
            Storage::disk($disk)->deleteDirectory($path);
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
        }
    }

    /**
     * Move
     *
     * @param String $oldPath
     * @param String $newPath
     * @param String $disk
     * @return void
     */
    public static function moveFile(String $oldPath, String $newPath, String $disk = '')
    {
        if ($disk == '') {
            $disk = config('filesystems.default');
        }

        if ($disk == 's3') {
            $oldPath = 'app/public/' . $oldPath;
            $newPath = 'app/public/' . $newPath;
        } elseif ($disk == 'local') {
            $oldPath = 'public/' . $oldPath;
            $newPath = 'public/' . $newPath;
        }

        try {
            Storage::disk($disk)->move($oldPath, $newPath);
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
        }
    }
}
