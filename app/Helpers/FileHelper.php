<?php

namespace App\Helpers;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

class FileHelper
{
    /**
     * Storing file
     *
     * @param File $file
     * @param String $filePath
     * @param String $prefix
     * @return String
     */
    public static function storeFile($file, String $filePath, String $prefix = '')
    {
        // Create path if not exists
        $filePath = 'public'.DIRECTORY_SEPARATOR.str_replace('/', DIRECTORY_SEPARATOR, $filePath);
        if (!Storage::exists($filePath)) {
            Log::debug('here');
            Storage::makeDirectory($filePath, 0775, true);
        }

        $extension = $file->extension();
        $fileName = $prefix != '' ? $prefix.'-'.md5(time()) : md5(time());
        $fileName = $fileName.'.'.$extension;

        Storage::putFileAs(
            $filePath,
            $file,
            $fileName
        );
       
        return $fileName;
    }

    /**
     * Get file url
     *
     * @param String $filePath
     * @param String $fileName
     * @return String
     */
    public static function getFileUrl(String $filePath, String $fileName)
    {
        if (config('filesystems.default') == 'local') {
            return url(Storage::url($filePath.$fileName));
        }

        return Storage::url('app/public/'.$filePath.$fileName);
    }
}
