<?php

namespace App\Helpers;

use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;

class ImageHelper
{
    /**
     * Store image
     *
     * @param $img image file
     * @param String $imgPath
     * @param Array $resize
     * @param String $prefix
     * @return String of stored file name
     */
    public static function processImg($img, String $imgPath, array $resize = [], String $prefix = '')
    {
        // Create path if not exists
        $imgPath = str_replace('/', DIRECTORY_SEPARATOR, $imgPath);
        if (!Storage::exists($imgPath)) {
            Storage::makeDirectory('public'.DIRECTORY_SEPARATOR.$imgPath, 0775, true);
        }

        $filename = null;
        $extension = $img->getClientOriginalExtension();
        if ($prefix != '') {
            $filename = $prefix.'-'.md5(time()) . '.' . $extension;
        } else {
            $filename = md5(time()) . '.' . $extension;
        }

        $filePath = storage_path('app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.$imgPath.$filename);
        if (count($resize) > 0) {
            if ($resize['type'] == 'fit') {
                $resizeImg = Image::make($img)->fit($resize['size']);
            } elseif ($resize['type'] == 'resize') {
                $resizeImg = Image::make($img)->resize($resize['width'], $resize['height'], function ($constraint) {
                    $constraint->aspectRatio();
                });
            }
            $resizeImg->save($filePath);
        } else {
            $newImage = Image::make($img);
            $newImage->save($filePath);
        }
        ImageOptimizer::optimize($filePath);

        return $filename;
    }

    public static function showImage($path, $dirType)
    {
        if ($dirType == 'storage') {
            $path = storage_path('app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.$path);
        } else {
            $path = public_path($path);
        }
        
        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    }

    public static function getImageUrl($imgPath, $fileName)
    {
        if (config('filesystems.default') == 'local') {
            return url(Storage::url($imgPath.$fileName));
        }

        return Storage::url('app/public/'.$imgPath.$fileName);
    }
}
