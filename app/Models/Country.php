<?php

namespace App\Models;

use App\Repositories\CountryRepository;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = [
        'country_code',
        'name',
        'currency_code',
        'fips_code',
        'iso_numeric',
        'north',
        'south',
        'east',
        'west',
        'capital',
        'continent_name',
        'continent',
        'languages',
        'iso_alpha_3',
        'geo_name_id',
    ];

    public static function booted()
    {
        static::created(function ($jobFunction) {
            CountryRepository::deleteFormDataCache();
        });

        static::updated(function ($jobFunction) {
            CountryRepository::deleteFormDataCache();
        });

        static::deleting(function ($jobFunction) {
            CountryRepository::deleteFormDataCache();
        });
    }

    public function companies()
    {
        return $this->hasMany('App\Models\Company');
    }

    public function work_jobs()
    {
        return $this->hasMany('App\Models\Workjob');
    }

    public function users()
    {
        return $this->hasMany('App\Models\User');
    }

    public function citizen_users()
    {
        return $this->hasMany('App\Model\User', 'citizenship');
    }
}
