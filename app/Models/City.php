<?php

namespace App\Models;

use App\Repositories\CityRepository;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = [
        'province_id',
        'name'
    ];

    public static function booted()
    {
        static::created(function ($jobFunction) {
            CityRepository::deleteFormDataCache();
        });

        static::updated(function ($jobFunction) {
            CityRepository::deleteFormDataCache();
        });

        static::deleting(function ($jobFunction) {
            CityRepository::deleteFormDataCache();
        });
    }

    public function province()
    {
        return $this->belongsTo('App\Models\Province');
    }

    public function subdistricts()
    {
        return $this->hasMany('App\Models\Subdistrict');
    }

    public function companies()
    {
        return $this->hasMany('App\Models\Company');
    }

    public function work_jobs()
    {
        return $this->hasMany('App\Models\Workjob');
    }

    public function users()
    {
        return $this->hasMany('App\Models\User');
    }

    public function educations()
    {
        return $this->hasMany('App\Models\Education');
    }
}
