<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subdistrict extends Model
{
    protected $fillable = [
        'city_id',
        'name'
    ];

    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    public function users()
    {
        return $this->hasMany('App\Models\User');
    }

    public function companies()
    {
        return $this->hasMany('App\Models\Company');
    }
}
