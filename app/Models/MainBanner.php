<?php

namespace App\Models;

use Carbon\Carbon;
use App\Helpers\PathHelper;
use App\Helpers\ImageHelper;
use Illuminate\Database\Eloquent\Model;

class MainBanner extends Model
{
    protected $fillable = [
        'image',
        'alt_image',
        'link',
        'open_new_tab',
        'start_date',
        'end_date',
        'enable',
        'sort_order',
    ];

    protected $casts = [
        'open_new_tab' => 'boolean',
        'enable' => 'boolean'
    ];

    protected $dates = [
        'start_date',
        'end_date',
    ];

    protected $appends = [
        'image_url'
    ];

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($banner) {
            $path = PathHelper::main_banner_path();
            PathHelper::deleteFile($path . $banner->image);
        });
    }

    public function scopeActive($query)
    {
        return $query->where('enable', 1)
            ->where('start_date', '<=', now()->startOfDay())
            ->where('end_date', '>=', now()->endOfDay());
    }

    public function scopeSort($query)
    {
        return $query->orderBy('sort_order', 'asc');
    }

    public function getImageUrlAttribute()
    {
        $imgPath = PathHelper::main_banner_path();
        return ImageHelper::getImageUrl($imgPath, $this->image);
    }

    public function getDisplayingAttribute()
    {
        $now = Carbon::now();
        $start = $this->start_date->diffInMinutes($now, false);
        $end = $this->end_date->diffInMinutes($now, false);

        $displaying = true;
        if ($start > 0 && $end < 0 && $this->enable) {
            $displaying = true;
        } else {
            $displaying = false;
        }

        return $displaying;
    }
}
