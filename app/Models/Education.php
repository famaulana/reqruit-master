<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    protected $fillable = [
        'user_id',
        'institution',
        'province_id',
        'city_id',
        'education_degree_id',
        'education_field_id',
        'gpa',
        'start_time',
        'end_time',
        'still_studying',
        'more_info',
    ];

    protected $casts = [
        'still_studying' => 'boolean'
    ];

    protected $dates = [
        'start_time',
        'end_time',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function degree()
    {
        return $this->belongsTo('App\Models\EducationDegree', 'education_degree_id');
    }

    public function province()
    {
        return $this->belongsTo('App\Models\Province');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    public function education_field()
    {
        return $this->belongsTo('App\Models\EducationField');
    }
}
