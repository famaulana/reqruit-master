<?php

namespace App\Models;

use App\Repositories\SkillRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Skill extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name'
    ];

    public static function booted()
    {
        static::created(function ($jobFunction) {
            SkillRepository::deleteFormDataCache();
        });

        static::updated(function ($jobFunction) {
            SkillRepository::deleteFormDataCache();
        });

        static::deleting(function ($jobFunction) {
            SkillRepository::deleteFormDataCache();
        });

        static::restoring(function ($jobFunction) {
            SkillRepository::deleteFormDataCache();
        });
    }

    public function must_work_jobs()
    {
        return $this->belongsToMany('App\Models\Workjob', 'skill_must_workjob', 'skill_id', 'workjob_id');
    }

    public function nice_work_jobs()
    {
        return $this->belongsToMany('App\Models\Workjob', 'skill_nice_workjob', 'skill_id', 'workjob_id');
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User');
    }
}
