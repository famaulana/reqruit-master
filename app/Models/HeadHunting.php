<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HeadHunting extends Model
{
    protected $fillable = [
        'name',
        'email',
        'phone',
        'position',
        'company',
        'job_position',
        'salary_min',
        'salary_max',
        'location',
        'description',
    ];

    protected $casts = [
        'contacted' => 'boolean'
    ];
}
