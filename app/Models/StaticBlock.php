<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StaticBlock extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'content',
        'active',
    ];

    protected $casts = [
        'active' => 'boolean'
    ];
}
