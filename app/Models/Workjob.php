<?php

namespace App\Models;

use App\Enums\WorkjobExperience;
use App\Enums\WorkjobPostingStatus;
use App\Enums\WorkjobSalaryDuration;
use App\Enums\WorkjobStatus;
use App\Enums\WorkjobType;
use App\Helpers\AppHelper;
use App\Repositories\AppConfigRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Workjob extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'created_by',
        'edited_by',
        'company_id',
        'identifier',
        'title',
        'slug',
        'type',
        'country_id',
        'province_id',
        'city_id',
        'city_name',
        'number_of_vacancies',
        'jobfunction_id',
        'work_experience',
        'salary_currency',
        'salary_min',
        'salary_max',
        'salary_duration',
        'bonus_salary',
        'bonus_salary_min',
        'bonus_salary_max',
        'bonus_salary_duration',
        'description',
        'has_question',
        'attachment',
        'job_posting_status',
        'status',
        'hide_company',
        'remote'
    ];

    protected $casts = [
        'type' => WorkjobType::class,
        'work_experience' => WorkjobExperience::class,
        'salary_duration' => WorkjobSalaryDuration::class,
        'bonus_salary_duration' => WorkjobSalaryDuration::class,
        'bonus_salary' => 'boolean',
        'has_question' => 'boolean',
        'job_posting_status' => WorkjobPostingStatus::class,
        'status' => WorkjobStatus::class,
        'hide_company' => 'boolean',
        'remote' => 'boolean'
    ];

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($workjob) {
            if ($workjob->forceDeleting) {
                $workjob->job_roles()->detach();
                $workjob->must_skills()->detach();
                $workjob->nice_skills()->detach();

                $workjob->job_applications()->delete();
                $workjob->job_questions()->delete();
                $workjob->job_question_answers()->delete();
            }
        });
    }

    public function creator()
    {
        return $this->belongsTo('App\Models\Employer', 'created_by');
    }

    public function editor()
    {
        return $this->belongsTo('App\Models\Employer', 'edited_by');
    }

    public function company()
    {
        return $this->belongsTo('App\Models\Company');
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    public function province()
    {
        return $this->belongsTo('App\Models\Province');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    public function job_function()
    {
        return $this->belongsTo('App\Models\Jobfunction', 'jobfunction_id');
    }

    public function job_roles()
    {
        return $this->belongsToMany('App\Models\Jobrole', 'jobrole_workjob', 'workjob_id', 'jobrole_id');
    }

    public function must_skills()
    {
        return $this->belongsToMany('App\Models\Skill', 'skill_must_workjob', 'workjob_id', 'skill_id');
    }

    public function nice_skills()
    {
        return $this->belongsToMany('App\Models\Skill', 'skill_nice_workjob', 'workjob_id', 'skill_id');
    }

    public function job_applications()
    {
        return $this->hasMany('App\Models\JobApplication');
    }

    public function job_questions()
    {
        return $this->hasMany('App\Models\JobQuestion');
    }

    public function job_question_answers()
    {
        return $this->hasMany('App\Models\JobQuestionAnswer');
    }

    public function scopePublished($query)
    {
        return $query->where('status', WorkjobStatus::Published);
    }

    public function scopeDraft($query)
    {
        return $query->where('status', WorkjobStatus::Draft);
    }

    public function scopeOpen($query)
    {
        return $query->where('job_posting_status', WorkjobPostingStatus::Open);
    }

    public function scopeClosed($query)
    {
        return $query->where('job_posting_status', WorkjobPostingStatus::Closed);
    }

    public function scopeContract($query)
    {
        return $query->where('type', WorkjobType::Contract);
    }

    public function scopeContractEmployee($query)
    {
        return $query->where('type', WorkjobType::ContractEmployee);
    }

    public function scopeFreelance($query)
    {
        return $query->where('type', WorkjobType::Freelance);
    }

    public function scopeFulltime($query)
    {
        return $query->where('type', WorkjobType::FullTime);
    }

    public function scopeInternship($query)
    {
        return $query->where('type', WorkjobType::Internship);
    }

    public function getCityNameAttribute()
    {
        return is_null($this->city_id) ? $this->city_name : $this->city->name;
    }

    public function getFormattedSalaryMinAttribute()
    {
        $configs = new AppConfigRepository();
        return AppHelper::formatNumber($this->salary_min, $configs->getConfig('locale'));
    }

    public function getFormattedSalaryMaxAttribute()
    {
        $configs = new AppConfigRepository();
        return AppHelper::formatNumber($this->salary_max, $configs->getConfig('locale'));
    }

    public function getFormattedBonusSalaryMinAttribute()
    {
        $configs = new AppConfigRepository();
        return AppHelper::formatNumber($this->bonus_salary_min, $configs->getConfig('locale'));
    }

    public function getFormattedBonusSalaryMaxAttribute()
    {
        $configs = new AppConfigRepository();
        return AppHelper::formatNumber($this->bonus_salary_max, $configs->getConfig('locale'));
    }
}
