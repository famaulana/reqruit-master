<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobQuestion extends Model
{
    protected $fillable = [
        'workjob_id',
        'question',
        'sort_order',
    ];

    public function workjob()
    {
        return $this->belongsTo('App\Models\Workjob');
    }
}
