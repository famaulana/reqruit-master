<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $fillable = [
        'name'
    ];

    public function cities()
    {
        return $this->hasMany('App\Models\City');
    }

    public function users()
    {
        return $this->hasMany('App\Models\User');
    }

    public function companies()
    {
        return $this->hasMany('App\Models\Company');
    }

    public function work_jobs()
    {
        return $this->hasMany('App\Models\Workjob');
    }

    public function educations()
    {
        return $this->hasMany('App\Models\Education');
    }
}
