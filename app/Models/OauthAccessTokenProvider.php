<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OauthAccessTokenProvider extends Model
{
    protected $primaryKey = 'oauth_access_token_id';

    public function token()
    {
        return $this->belongsTo('App\Models\OauthAccessToken');
    }
}
