<?php

namespace App\Models;

use App\Models\CompanyReview;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ReviewBenefit extends Model
{
    protected $fillable = [
        'employee_discount',
        'employee_assistance_program',
        'fertility_assistance',
        'fsa',
        'stock_options',
        'dependent_care',
        'adoption_assistance',
        'employee_stock_purchase_plan',
        'childcare',
        'job_training',
        'apprenticeship_program',
        'professional_development',
        'performance_bonus',
        'gym_membership',
        'hsa',
        'family_medical_leave',
        'military_leave',
        'commuter_checks',
        'life_assurance',
        'vision_insurance',
        'volunteer_time_off',
        'tuition_assistance',
        'supplemental_workers_compensation',
        'healt_care_on_site',
        'mobile_phone_discount',
        'company_social_events',
        'retiree_health',
        'legal_assistance',
        'occupational_accident_insurance',
        'charitable_gift_matching',
        'supplemntal_life_insurance',
        'unpaid_extended_leave',
        'pet_friendly_workplace',
        'equity_incentive_plan',
        'travel_concierge',
        'disability_insurance',
        'sabbatical',
        'company_car',
        'mental_health_care',
        'bereavement_leave',
        'accidental_death_and_dismemberment_insurance',
        'pension_plan',
        'maternity_and_paternity_leave',
        'work_from_home',
        'health_insurance',
        'four_zero_one_plan',
        'vacation_and_paid_time_off',
        'retirement_plan',
        'free_lunch',
        'sick_days',
        'diversity_program',
        'dental_insurance',
        'paid_holidays',
        'reduced_or_flexible_hours',
    ];

    /**
     * Get the company_review that owns the ReviewBenefit
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company_review(): BelongsTo
    {
        return $this->belongsTo(CompanyReview::class);
    }
}
