<?php

namespace App\Models;

use App\Helpers\PathHelper;
use App\Helpers\ImageHelper;
use App\Models\CompanyReview;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Company extends Model
{
    use SoftDeletes, Notifiable;
    
    protected $fillable = [
        'name',
        'slug',
        'logo',
        'banner',
        'short_description',
        'address',
        'country_id',
        'province_id',
        'city_id',
        'city_name',
        'subdistrict_id',
        'pos_code',
        'phone',
        'email',
        'website_url',
        'instagram',
        'facebook',
        'linkedin',
        'twitter',
        'description',
        'images',
    ];

    protected $casts = [
        'images' => 'array'
    ];

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($company) {
            if ($company->forceDeleting) {
                // Delete directory
                $path = PathHelper::company_path($company->id);
                PathHelper::deleteDirectory($path);

                $company->work_jobs()->forceDelete();
                $company->industries()->forceDelete();
                $company->company_reviews()->forceDelete();
            } else {
                $company->work_jobs()->delete();
                $company->industries()->delete();
                $company->company_reviews()->delete();
            }
        });

        static::restored(function ($company) {
            $company->work_jobs()->restore();
            $company->industries()->restore();
            $company->company_reviews()->restore();
        });
    }

    public function employers()
    {
        return $this->belongsToMany('App\Models\Employer');
    }

    public function work_jobs()
    {
        return $this->hasMany('App\Models\Workjob', 'company_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    public function province()
    {
        return $this->belongsTo('App\Models\Province');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    public function subdistrict()
    {
        return $this->belongsTo('App\Models\Subdistrict');
    }

    public function industries()
    {
        return $this->belongsToMany('App\Models\Industry');
    }

    /**
     * Get all of the company_reviews for the Company
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function company_reviews(): HasMany
    {
        return $this->hasMany(CompanyReview::class);
    }

    public function getCityNameAttribute()
    {
        return is_null($this->city_id) ? $this->city_name : $this->city->name;
    }

    public function getLogoUrlAttribute()
    {
        $url = '';
        if (!is_null($this->logo)) {
            $imgPath = PathHelper::company_images_path($this->id);
            $url = ImageHelper::getImageUrl($imgPath, $this->logo);
        }

        return $url;
    }

    public function getBannerUrlAttribute()
    {
        $url = url('images/reqruit/content-images/company-banner.png');
        if (!is_null($this->banner)) {
            $imgPath = PathHelper::company_images_path($this->id);
            $url = ImageHelper::getImageUrl($imgPath, $this->banner);
        }

        return $url;
    }

    public function getCompanyImagesAttribute()
    {
        $imgPath = PathHelper::company_images_path($this->id);
        $result = [];

        if (is_null($this->images)) {
            return $result;
        }

        foreach ($this->images as $image) {
            $result[] = [
                'new' => false,
                'file_name' => $image['file_name'],
                'src' => ImageHelper::getImageUrl($imgPath, $image['file_name']),
                'description' => $image['description']
            ];
        }

        return $result;
    }
}
