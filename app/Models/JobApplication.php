<?php

namespace App\Models;

use App\Enums\InterviewStatusEnum;
use App\Enums\JobApplicationStatus;
use Illuminate\Database\Eloquent\Model;

class JobApplication extends Model
{
    protected $fillable = [
        'user_id',
        'workjob_id',
        'cover_letter',
        'status',
        'data',
        'interview_time',
        'interview_location',
        'interview_status'
    ];

    protected $casts = [
        'status' => JobApplicationStatus::class,
        'data' => 'array',
        'interview_status' => InterviewStatusEnum::class
    ];

    protected $dates = [
        'interview_time'
    ];

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($jap) {
            foreach ($jap->job_question_answers as $answer) {
                $answer->delete();
            }
        });
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function workjob()
    {
        return $this->belongsTo('App\Models\Workjob');
    }

    public function job_question_answers()
    {
        return $this->hasMany('App\Models\JobQuestionAnswer');
    }
}
