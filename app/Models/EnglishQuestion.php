<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EnglishQuestion extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'question',
        'answers',
        'correct_answer',
        'timer'
    ];

    protected $casts = [
        'answers' => 'array'
    ];
}
