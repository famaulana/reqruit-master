<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Repositories\JobfunctionRepository;
use Illuminate\Database\Eloquent\SoftDeletes;

class Jobfunction extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name'
    ];

    public static function booted()
    {
        static::created(function ($jobFunction) {
            JobfunctionRepository::deleteFormDataCache();
        });

        static::updated(function ($jobFunction) {
            JobfunctionRepository::deleteFormDataCache();
        });

        static::deleting(function ($jobFunction) {
            JobfunctionRepository::deleteFormDataCache();

            if ($jobFunction->forceDeleting) {
                // Permanent delete job roles
                $jobFunction->job_roles()->onlyTrashed()->get()->each(function ($jobRole) {
                    $jobRole->forceDelete();
                });
            } else {
                // Send job roles into trash
                foreach ($jobFunction->job_roles as $jobRole) {
                    $jobRole->delete();
                }
            }
        });

        static::restoring(function ($jobFunction) {
            JobfunctionRepository::deleteFormDataCache();
            // Restore job roles
            $jobFunction->job_roles()->onlyTrashed()->get()->each(function ($jobRole) {
                $jobRole->restore();
            });
        });
    }

    public function work_jobs()
    {
        return $this->hasMany('App\Models\Workjob');
    }

    public function job_roles()
    {
        return $this->hasMany('App\Models\Jobrole', 'jobfunction_id');
    }
}
