<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobQuestionAnswer extends Model
{
    protected $fillable = [
        'job_application_id',
        'user_id',
        'workjob_id',
        'question',
        'answer',
    ];

    public function job_application()
    {
        return $this->belongsTo('App\Models\JobApplication');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function workjob()
    {
        return $this->belongsTo('App\Models\Workjob');
    }
}
