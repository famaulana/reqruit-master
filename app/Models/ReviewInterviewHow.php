<?php

namespace App\Models;

use App\Models\CompanyReview;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ReviewInterviewHow extends Model
{
    protected $fillable = [
        'company_review_id',
        'how_to_get_interview', // online, university, referral, in-person, recruiter, staffing-agency, other
        'how_long_number',
        'how_long_unit', // days, weeks, months
        'interview_time',
        'location',
        'type', // phone-screen, 1-on-1, group-panel,
        'testing', // skills-review, personality-quiz, drug-test, iq-test
        'other', // background-check, presentation, other
        'how_helpful',
    ];

    protected $casts = [
        'type' => 'array',
        'testing' => 'array',
        'other' => 'array',
    ];

    /**
     * Get the company_review that owns the ReviewInterviewHow
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company_review(): BelongsTo
    {
        return $this->belongsTo(CompanyReview::class);
    }
}
