<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'customer_id',
        'name',
        'street',
        'province_id',
        'city_id',
        'subdistrict_id',
        'post_code',
        'phone',
    ];

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }

    public function subdistrict()
    {
        return $this->belongsTo('App\Models\Subdistrict');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    public function province()
    {
        return $this->belongsTo('App\Models\Province');
    }

    public function scopeRelation($query)
    {
        $query->with(['province', 'city', 'subdistrict']);
    }
}
