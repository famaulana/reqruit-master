<?php

namespace App\Models;

use App\Repositories\EducationDegreeRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EducationDegree extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'name'
    ];

    public function educations()
    {
        return $this->hasMany('App\Models\Education', 'education_degree_id');
    }

    public static function booted()
    {
        static::created(function ($jobFunction) {
            EducationDegreeRepository::deleteFormDataCache();
        });

        static::updated(function ($jobFunction) {
            EducationDegreeRepository::deleteFormDataCache();
        });

        static::deleting(function ($jobFunction) {
            EducationDegreeRepository::deleteFormDataCache();
        });

        static::restoring(function ($jobFunction) {
            EducationDegreeRepository::deleteFormDataCache();
        });
    }
}
