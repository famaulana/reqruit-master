<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkExperience extends Model
{
    protected $fillable = [
        'user_id',
        'position',
        'company',
        'start_time',
        'end_time',
        'still_working',
        'more_info',
    ];

    protected $casts = [
        'still_working' => 'boolean'
    ];

    protected $dates = [
        'start_time',
        'end_time',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
