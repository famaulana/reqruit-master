<?php

namespace App\Models;

use App\Helpers\PathHelper;
use App\Helpers\ImageHelper;
use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    protected $fillable = [
        'name',
        'avatar',
        'profession',
        'content',
        'rating',
    ];

    protected $appends = ['avatar_url'];

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($testimonial) {
            // Delete directory
            $path = PathHelper::testimonial_path($testimonial->id);
            PathHelper::deleteDirectory($path);
        });
    }

    public function getAvatarUrlAttribute()
    {
        if ($this->avatar == '') {
            return url('images/default/default-avatar.png');
        }

        $imgPath = PathHelper::testimonial_avatar_path($this->id);
        return ImageHelper::getImageUrl($imgPath, $this->avatar);
    }
}
