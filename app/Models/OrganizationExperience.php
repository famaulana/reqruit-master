<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrganizationExperience extends Model
{
    protected $fillable = [
        'user_id',
        'name',
        'position',
        'start_time',
        'end_time',
        'still_involved',
        'more_info',
    ];

    protected $casts = [
        'still_involved' => 'boolean'
    ];

    protected $dates = [
        'start_time',
        'end_time',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
