<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OauthAccessToken extends Model
{
    public function provider()
    {
        return $this->hasOne('App\Models\OauthAccessTokenProvider');
    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($token) {
            if(!is_null($token->provider)) {
                $token->provider->delete();
            }
        });
    }
}
