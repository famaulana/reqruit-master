<?php

namespace App\Models;

use App\Enums\EmployerRole;
use App\Helpers\PathHelper;
use App\Helpers\ImageHelper;
use App\Models\OauthAccessToken;
use App\Services\EmployerService;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use SMartins\PassportMultiauth\HasMultiAuthApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Employer extends Authenticatable
{
    use HasMultiAuthApiTokens, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'identifier',
        'name',
        'email',
        'phone',
        'photo',
        'gender',
        'dob',
        'job_position',
        'role',
        'self_recruitment',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'verified' => 'boolean',
        'self_recruitment' => 'boolean',
        'role' => EmployerRole::class
    ];

    protected $dates = [
        'email_verified_at',
        'dob'
    ];

    protected $appends = ['avatar_url', 'idn_gender'];

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($employer) {
            if ($employer->forceDeleting) {
                // Delete directory
                $path = PathHelper::employer_path($employer->id);
                PathHelper::deleteDirectory($path);

                if (!is_null($employer->assist_recruitment)) {
                    $employer->assist_recruitment->delete();
                }
            } else {
            }
        });
    }

    public function companies()
    {
        return $this->belongsToMany('App\Models\Company');
    }

    public function getCompanyAttribute()
    {
        return $this->companies()->first();
    }

    public function sendPasswordResetNotification($token)
    {
        $employer = $this;
        $employerService = new EmployerService();
        $employerService->sendResetPasswordLink($employer, $token);
    }

    public function getIdnGenderAttribute()
    {
        return $this->gender == 'male' ? 'Laki-laki' : 'Perempuan';
    }

    public function getAvatarUrlAttribute()
    {
        if ($this->photo == '') {
            return url('images/default/default-avatar.png');
        }

        $imgPath = PathHelper::employer_photo_path($this->id);
        return ImageHelper::getImageUrl($imgPath, $this->photo);
    }

    public function generateToken()
    {
        $tokens = OauthAccessToken::where('user_id', $this->id)->where('name', 'Employer')->get();
        if ($tokens !== null) {
            foreach ($tokens as $token) {
                $token->delete();
            }
        }

        return $this->createToken('Employer')->accessToken;
    }

    /**
     * Get First and Last name
     *
     * @return Array
     */
    public function getFirstLastNameAttribute()
    {
        $fullName = explode(' ', $this->name);
        $firstName = $fullName[0];
        $lastName = implode(' ', array_slice($fullName, 1));
        return [
            'first_name' => $firstName,
            'last_name' => $lastName
        ];
    }
}
