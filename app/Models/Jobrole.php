<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Jobrole extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'jobfunction_id'
    ];

    public function job_function()
    {
        return $this->belongsTo('App\Models\Jobfunction', 'jobfunction_id')->withTrashed();
    }

    public function work_jobs()
    {
        return $this->belongsToMany('App\Models\Workjob');
    }
}
