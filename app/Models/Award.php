<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Award extends Model
{
    protected $fillable = [
        'user_id',
        'title',
        'description',
        'awarded_at',
        'more_info',
    ];

    protected $casts = [
        'awarded_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
