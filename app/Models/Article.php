<?php

namespace App\Models;

use App\Helpers\PathHelper;
use App\Helpers\ImageHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'title',
        'slug',
        'content',
        'excerpt',
        'featured_image',
        'meta_title',
        'meta_description',
    ];

    public function getFeaturedImageUrlAttribute()
    {
        $imgPath = PathHelper::article_featured_image_path($this->id);
        return ImageHelper::getImageUrl($imgPath, $this->featured_image);
    }
}
