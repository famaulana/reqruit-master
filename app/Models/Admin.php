<?php

namespace App\Models;

use App\Helpers\PathHelper;
use Illuminate\Support\Str;
use App\Helpers\ImageHelper;
use App\Repositories\AppConfigRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use SMartins\PassportMultiauth\HasMultiAuthApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use HasMultiAuthApiTokens, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'photo', 'password', 'primary', 'verified'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'primary' => 'boolean',
        'verified' => 'boolean'
    ];

    protected $appends = ['web_avatar_url'];

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($admin) {
            if ($admin->forceDeleting) {
                $admin->permissions()->detach();
            }
        });
    }

    public function permissions()
    {
        return $this->belongsToMany('App\Models\Permission');
    }

    public static function createAdmin($name, $email, $pass)
    {
        $admin = Admin::create([
            'name' => $name,
            'email' => $email,
            'password' => Hash::make($pass),
        ]);
        return $admin;
    }

    public function sendVerification()
    {
        $configs = new AppConfigRepository();
        $token = $this->generateVerificationToken();
        $admin = $this;
        Mail::send('admin.emails.email-verification', compact('admin', 'token'), function ($m) use ($configs, $admin) {
            $m->to($admin->email, $admin->name)->subject('Account Verification - ' . $configs->getConfig('site_name'));
        });

        $link = route('auth.admin.resend-verification') . '?email=' . urlencode($this->email);
        Session::flash('flash_alert', [
            'level' => 'warning',
            'message' => "Klik link aktivasi yang telah terkirim ke email Anda. <a class='alert-link' href='$link'>Kirim ulang</a>.",
        ]);

        return $token;
    }

    public function generateVerificationToken()
    {
        $token = Str::random(40);
        $this->verify_token = $token;
        $this->save();

        return $token;
    }

    public function verify()
    {
        $this->verified = 1;
        $this->verify_token = null;
        $this->email_verified_at = now();
        $this->save();
    }

    public function sendPasswordResetNotification($token)
    {
        $configs = new AppConfigRepository();
        $admin = $this;
        Mail::send('admin.emails.reset-password-link', compact('admin', 'token'), function ($m) use ($configs, $admin) {
            $m->to($admin->email, $admin->name)->subject('Reset Password - ' . $configs->getConfig('site_name'));
        });
    }

    public function getAvatarUrlAttribute()
    {
        return route('api.admin.admins.avatar', $this->id);
    }

    public function getWebAvatarUrlAttribute()
    {
        return route('admin.admins.avatar', $this->id);
    }

    public function getPermissionIdsAttribute()
    {
        return $this->permissions->pluck('id')->toArray();
    }

    public function getPermissionNamesAttribute()
    {
        return $this->permissions->pluck('name')->toArray();
    }
}
