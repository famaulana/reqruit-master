<?php

namespace App\Models;

use App\Models\CompanyReview;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ReviewInterviewQuestion extends Model
{
    protected $fillable = [
        'company_review_id',
        'question',
        'answer'
    ];

    /**
     * Get the company_review that owns the ReviewInterviewQuestion
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company_review(): BelongsTo
    {
        return $this->belongsTo(CompanyReview::class);
    }
}
