<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $casts = [
        'links' => 'array'
    ];

    protected $fillable = [
        'name',
        'links'
    ];
}
