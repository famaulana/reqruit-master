<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PersonalityTest extends Model
{
    protected $fillable = [
        'user_id',
        'statements',
        'result',
    ];

    protected $casts = [
        'statements' => 'array',
        'result' => 'array',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
