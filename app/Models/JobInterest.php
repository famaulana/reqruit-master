<?php

namespace App\Models;

use App\Enums\WorkjobType;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;

class JobInterest extends Model
{
    protected $fillable = [
        'user_id',
        'jobfunction_ids',
        'jobrole_ids',
        'job_types',
        'current_salary',
        'expected_salary',
        'cities',
        'relocated',
    ];

    protected $casts = [
        'jobfunction_ids' => 'array',
        'jobrole_ids' => 'array',
        'job_types' => 'array',
        'cities' => 'array',
        'relocated' => 'boolean',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function getFieldOfInterestAttribute()
    {
        $jobfunctions = Jobfunction::whereIn('id', $this->jobfunction_ids)->get();
        
        $result = [];
        foreach ($jobfunctions as $jobfunction) {
            $allJobRoles = $jobfunction->job_roles()->pluck('name', 'id')->toArray();
            
            $jobroles = [];
            foreach ($allJobRoles as $id => $name) {
                if (in_array($id, $this->jobrole_ids)) {
                    $jobroles[] = [
                        'id' => $id,
                        'name' => $name
                    ];
                }
            }

            $result[] = [
                'jobfunction' => [
                    'id' => $jobfunction->id,
                    'name' => $jobfunction->name
                ],
                'jobroles' => $jobroles
            ];
        }

        return $result;
    }

    public function getFormattedJobTypesAttribute()
    {
        $jobTypes = WorkjobType::toArray();

        $result = [];
        foreach ($jobTypes as $desc => $value) {
            if (in_array($value, $this->job_types)) {
                $result[] = [
                    'value' => $value,
                    'name' => WorkjobType::getDescription($value)
                ];
            }
        }

        return $result;
    }
}
