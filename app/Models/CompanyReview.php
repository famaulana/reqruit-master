<?php

namespace App\Models;

use App\Models\User;
use App\Models\Company;
use App\Models\ReviewBenefit;
use App\Models\ReviewInterviewHow;
use App\Models\ReviewInterviewQuestion;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CompanyReview extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'company_id',
        'anonymous',
        'job_status',
        'employment_status',
        'job_title',
        'rating',
        'review_headline',
        'advice',
        'published',
    ];

    protected $casts = [
        'anonymous' => 'boolean',
        'published' => 'boolean',
    ];

    /**
     * Get the user that owns the CompanyReview
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the user that owns the CompanyReview
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * Query published reviews
     *
     * @param Object $query
     * @return void
     */
    public function scopePublished(Object $query)
    {
        return $query->where('published', 1);
    }
}
