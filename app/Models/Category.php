<?php

namespace App\Models;

use App\Helpers\PathHelper;
use App\Helpers\ImageHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'parent_id',
        'name',
        'slug',
        'page_title',
        'icon',
        'content',
        'featured_image',
        'meta_title',
        'meta_description'
    ];

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($category) {
            if ($category->forceDeleting) {
                // Delete directory
                $path = PathHelper::category_path($category->id);
                PathHelper::deleteDirectory($path);

                foreach ($category->searchable_attributes as $searchable_attribute) {
                    $searchable_attribute->delete();
                }
            }
        });
    }

    public function childs()
    {
        return $this->hasMany('App\Models\Category', 'parent_id', 'id');
    }

    public function products()
    {
        return $this->morphedByMany('App\Models\Product', 'categoriable');
    }

    public function searchable_attributes()
    {
        return $this->morphedByMany('App\Models\Attribute', 'categoriable');
    }

    public function getIconUrlAttribute()
    {
        $imgPath = PathHelper::category_icon_path($this->id);
        return ImageHelper::getImageUrl($imgPath, $this->icon);
    }

    public function getFeaturedImageUrlAttribute()
    {
        $imgUrl = '';
        if ($this->featured_image !== null) {
            $imgPath = PathHelper::category_featured_image_path($this->id);
            $imgUrl =  ImageHelper::getImageUrl($imgPath, $this->featured_image);
        }

        return $imgUrl;
    }
}
