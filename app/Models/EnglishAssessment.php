<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class EnglishAssessment extends Model
{
    protected $fillable = [
        'user_id',
        'assessment',
        'score',
    ];

    protected $casts = [
        'assessment' => 'array'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
