<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bookmark extends Model
{
    protected $fillable = [
        'user_id',
        'workjob_id'
    ];

    public function workjob()
    {
        return $this->belongsTo('App\Models\Workjob');
    }
}
