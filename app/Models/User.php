<?php

namespace App\Models;

use App\Models\CompanyReview;
use App\Helpers\FileHelper;
use App\Helpers\PathHelper;
use App\Enums\UserJobStatus;
use App\Helpers\ImageHelper;
use App\Services\UserService;
use App\Models\OauthAccessToken;
use App\Enums\ResidenceStatusEnum;
use App\Enums\UserRelationshipEnum;
use App\Enums\WhenCanStartWorkEnum;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;
use SMartins\PassportMultiauth\HasMultiAuthApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasMultiAuthApiTokens, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'identifier',
        'name',
        'email',
        'phone',
        'photo',
        'gender',
        'dob',
        'about',
        'address',
        'country_id',
        'province_id',
        'province_name',
        'city_id',
        'city_name',
        'subdistrict_id',
        'pos_code',
        'resume',
        'video',
        'job_status',
        'relationship',
        'hobby',
        'residence_status',
        'citizenship',
        'when_can_start',
        'can_start_date',
        'yt_video',
        'website',
        'facebook',
        'twitter',
        'instagram',
        'linkedin',
        'behance',
        'github',
        'codepen',
        'vimeo',
        'youtube',
        'dribbble',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'verified' => 'boolean',
        'job_status' => UserJobStatus::class,
        'relationship' => UserRelationshipEnum::class,
        'residence_status' => ResidenceStatusEnum::class,
        'when_can_start' => WhenCanStartWorkEnum::class
    ];

    protected $dates = [
        'email_verified_at',
        'dob',
        'can_start_date'
    ];

    protected $appends = ['avatar_url', 'idn_gender'];

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($user) {
            if ($user->forceDeleting) {
                // Delete directory
                $path = PathHelper::user_path($user->id);
                PathHelper::deleteDirectory($path);

                $user->work_experiences()->delete();
                $user->educations()->delete();
                $user->skills()->detach();
                if (!is_null($user->job_interest)) {
                    $user->job_interest->delete();
                }
                $user->job_applications()->delete();
                $user->job_question_answers()->delete();
                $user->bookmarks()->delete();
                $user->company_reviews()->forceDelete();
                $user->notifications()->delete();
                $user->awards()->delete();
                if (!is_null($user->english_assessment)) {
                    $user->english_assessment()->delete();
                }
            } else {
                $user->company_reviews()->delete();
            }
        });

        static::restored(function ($user) {
            $user->company_reviews()->restore();
        });
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    public function province()
    {
        return $this->belongsTo('App\Models\Province');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    public function subdistrict()
    {
        return $this->belongsTo('App\Models\Subdistrict');
    }

    public function work_experiences()
    {
        return $this->hasMany('App\Models\WorkExperience');
    }

    public function organization_experiences()
    {
        return $this->hasMany('App\Models\OrganizationExperience');
    }

    public function educations()
    {
        return $this->hasMany('App\Models\Education');
    }

    public function skills()
    {
        return $this->belongsToMany('App\Models\Skill');
    }

    public function job_interest()
    {
        return $this->hasOne('App\Models\JobInterest');
    }

    public function job_applications()
    {
        return $this->hasMany('App\Models\JobApplication');
    }

    public function job_question_answers()
    {
        return $this->hasMany('App\Models\JobQuestionAnswer');
    }

    public function bookmarks()
    {
        return $this->hasMany('App\Models\Bookmark');
    }

    public function citizenship_data()
    {
        return $this->belongsTo('App\Models\Country', 'citizenship');
    }

    public function awards()
    {
        return $this->hasMany('App\Models\Award');
    }

    public function english_assessment()
    {
        return $this->hasOne('App\Models\EnglishAssessment');
    }

    public function personality_test()
    {
        return $this->hasOne('App\Models\PersonalityTest');
    }

    /**
     * Get all of the company_reviews for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function company_reviews(): HasMany
    {
        return $this->hasMany(CompanyReview::class);
    }

    public function sendPasswordResetNotification($token)
    {
        $user = $this;
        $userService = new UserService();
        $userService->sendResetPasswordLink($user, $token);
    }

    public function getAgeAttribute()
    {
        $age = is_null($this->dob) ? '' : $this->dob->age;
        return $age;
    }

    public function getIdnGenderAttribute()
    {
        return $this->gender == 'male' ? 'Laki-laki' : 'Perempuan';
    }

    public function getGenderTextAttribute()
    {
        return $this->gender == 'male' ? 'Male' : 'Female';
    }

    public function getAvatarUrlAttribute()
    {
        if ($this->photo == '') {
            return url('images/default/default-avatar.png');
        }

        $imgPath = PathHelper::user_photo_path($this->id);
        return ImageHelper::getImageUrl($imgPath, $this->photo);
    }

    public function getResumeUrlAttribute()
    {
        $url = '';
        if (!is_null($this->resume)) {
            $imgPath = PathHelper::user_resume_path($this->id);
            $url = FileHelper::getFileUrl($imgPath, $this->resume);
        }

        return $url;
    }

    public function getVideoUrlAttribute()
    {
        $url = '';
        if (!is_null($this->video)) {
            $videoPath = PathHelper::user_video_path($this->id);
            $url = FileHelper::getFileUrl($videoPath, $this->video);
        }

        return $url;
    }

    public function generateToken()
    {
        $tokens = OauthAccessToken::where('user_id', $this->id)->where('name', 'User')->get();
        if ($tokens !== null) {
            foreach ($tokens as $token) {
                $token->delete();
            }
        }

        return $this->createToken('User')->accessToken;
    }

    /**
     * Get First and Last name
     *
     * @return Array
     */
    public function getFirstLastNameAttribute()
    {
        $fullName = explode(' ', $this->name);
        $firstName = $fullName[0];
        $lastName = implode(' ', array_slice($fullName, 1));
        return [
            'first_name' => $firstName,
            'last_name' => $lastName
        ];
    }
}
