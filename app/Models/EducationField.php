<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Repositories\EducationFieldRepository;

class EducationField extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'name'
    ];

    public static function booted()
    {
        static::created(function ($jobFunction) {
            EducationFieldRepository::deleteFormDataCache();
        });

        static::updated(function ($jobFunction) {
            EducationFieldRepository::deleteFormDataCache();
        });

        static::deleting(function ($jobFunction) {
            EducationFieldRepository::deleteFormDataCache();
        });

        static::restoring(function ($jobFunction) {
            EducationFieldRepository::deleteFormDataCache();
        });
    }

    public function educations()
    {
        return $this->hasMany('App\Models\Education');
    }
}
