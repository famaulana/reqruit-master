<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use App\Models\JobApplication;
use App\Enums\InterviewStatusEnum;
use App\Repositories\AppConfigRepository;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class InterviewUpdate extends Notification
{
    use Queueable;

    public $configs;
    public $siteName;
    public $jobApplication;
    public $logoUrl;
    public $status;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(JobApplication $jobApplication, String $status)
    {
        $this->configs = new AppConfigRepository();

        $this->jobApplication = $jobApplication;
        $this->status = $status;
        $this->siteName = $this->configs->getConfig('site_name');
        $this->logoUrl = $this->configs->configFileUrls()['email_logo'];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        switch ($this->status) {
            case 'accept':
                $subjectPrefix = 'Accepted Interview';
                break;
            case 'decline':
                $subjectPrefix = 'Declined Interview';
                break;
            case 'time-not-suit':
                $subjectPrefix = 'Interview Time Not Suitable';
                break;
            default:
                $subjectPrefix = 'Interview Update';
                break;
        }

        return (new MailMessage)->from($this->configs->getConfig('site_email'), $this->configs->getConfig('site_name'))
            ->subject($subjectPrefix . ' - ' . $this->jobApplication->workjob->title)
            ->markdown('emails.job-application.interview-update', [
                'jobApplication' => $this->jobApplication,
                'siteName' => $this->siteName,
                'logoUrl' => $this->logoUrl,
                'status' => $this->status
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
