<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use App\Models\JobApplication;
use App\Repositories\AppConfigRepository;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class JobApplicationUpdate extends Notification
{
    use Queueable;

    public $configs;
    public $siteName;
    public $company;
    public $jobApplication;
    public $logoUrl;
    public $emailTemplate;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(JobApplication $jobApplication, String $emailTemplate = '')
    {
        $this->configs = new AppConfigRepository();

        $this->jobApplication = $jobApplication;
        $this->company = $jobApplication->workjob->company;
        $this->siteName = $this->configs->getConfig('site_name');
        $this->logoUrl = $this->configs->configFileUrls()['email_logo'];
        $this->emailTemplate = $emailTemplate;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $emailTemplate = $this->emailTemplate;
        if ($emailTemplate != '') {
            $emailTemplate = str_replace('%name%', $notifiable->name, $emailTemplate);
            $emailTemplate = str_replace('%job_title%', $this->jobApplication->workjob->title, $emailTemplate);
            $emailTemplate = str_replace('%company%', $this->company->name, $emailTemplate);
        }

        return (new MailMessage)->from($this->configs->getConfig('site_email'), $this->configs->getConfig('site_name'))
            ->subject('Status Lamaran Anda di '.$this->company->name.' Telah Diperbarui')
            ->markdown('emails.job-application.status-update', [
                'jobApplication' => $this->jobApplication,
                'company' => $this->company,
                'siteName' => $this->siteName,
                'logoUrl' => $this->logoUrl,
                'user' => $notifiable,
                'emailTemplate' => $emailTemplate
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $status = $this->jobApplication->status->description;

        return [
            'text' => 'Status lamaran kerja Anda pada '. $this->company->name.' telah berubah menjadi '.$status,
            'link' => '/account/job-applications?status='.$this->jobApplication->status->value
        ];
    }
}
