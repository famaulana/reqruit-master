<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EducationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'institution' => $this->institution,
            'province' => [
                'id' => $this->province_id,
                'name' => $this->province->name,
            ],
            'city' => [
                'id' => $this->city_id,
                'name' => $this->city->name,
            ],
            'degree' => [
                'id' => is_null($this->degree) ? '' : $this->degree->id,
                'name' => is_null($this->degree) ? '' : $this->degree->name,
            ],
            'field_of_study' => [
                'id' => is_null($this->education_field) ? '' : $this->education_field->id,
                'name' => is_null($this->education_field) ? '' : $this->education_field->name,
            ],
            'gpa' => $this->gpa,
            'start_time' => is_null($this->start_time) ? '' : $this->start_time->format('Y-m-d'),
            'start_year' => is_null($this->start_time) ? '' : $this->start_time->format('Y'),
            'end_time' => is_null($this->end_time) ? '' : $this->end_time->format('Y-m-d'),
            'end_year' => is_null($this->end_time) ? '' : $this->end_time->format('Y'),
            'still_studying' => $this->still_studying,
            'more_info' => $this->more_info,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
