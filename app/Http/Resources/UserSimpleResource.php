<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserSimpleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $genderText = '';
        if ($this->gender == 'male') {
            $genderText = 'Laki-laki';
        } elseif ($this->gender == 'female') {
            $genderText = 'Perempuan';
        }
        
        return [
            'id' => $this->id,
            'identifier' => $this->identifier,
            'name' => $this->name,
            'avatar_url' => $this->avatar_url,
            'email' => $this->email,
            'phone' => $this->phone,
            'gender' => [
                'value' => $this->gender ?? '',
                'text' => $genderText,
            ],
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ];
    }
}
