<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class JobApplicationSimpleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => [
                'id' => $this->user_id,
                'name' => $this->user->name,
                'avatar_url' => $this->user->avatar_url,
                'province' => [
                    'id' => $this->user->province_id ?? '',
                    'name' => is_null($this->user->province) ? '' : $this->user->province->name
                ],
                'city' => [
                    'id' => $this->user->city_id ?? '',
                    'name' => is_null($this->user->city) ? '' : $this->user->city->name
                ]
            ],
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
