<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BookmarkResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'job' => new WorkjobSimpleResource($this->workjob),
            'created_at' => $this->created_at,
            'created_at_diff' => $this->created_at->diffForHumans(),
            'updated_at' => $this->updated_at,
        ];
    }
}
