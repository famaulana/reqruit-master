<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class JobApplicationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => [
                'id' => $this->user_id,
                'name' => $this->user->name,
                'email' => $this->user->email
            ],
            'workjob' => new WorkjobResource($this->workjob),
            'cover_letter' => $this->cover_letter ?? '',
            'status' => $this->status,
            'data' => $this->data ?? [],
            'interview_time' => $this->interview_time ?? '',
            'interview_location' => $this->interview_location ?? '',
            'interview_status' => $this->interview_status ?? '',
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
