<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class WorkExperienceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'position' => $this->position,
            'company' => $this->company,
            'start_time' => is_null($this->start_time) ? '' : $this->start_time->format('Y-m-d'),
            'start_month' => is_null($this->start_time) ? '' : $this->start_time->format('n'),
            'start_year' => is_null($this->start_time) ? '' : $this->start_time->format('Y'),
            'end_time' => is_null($this->end_time) ? '' : $this->end_time->format('Y-m-d'),
            'end_month' => is_null($this->end_time) ? '' : $this->end_time->format('n'),
            'end_year' => is_null($this->end_time) ? '' : $this->end_time->format('Y'),
            'still_working' => $this->still_working,
            'more_info' => $this->more_info,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
