<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class JobQuestionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'workjob' => [
                'id' => $this->workjob_id
            ],
            'question' => $this->question,
            'sort_order' => $this->sort_order
        ];
    }
}
