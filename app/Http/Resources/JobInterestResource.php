<?php

namespace App\Http\Resources;

use App\Models\City;
use App\Http\Resources\Address\CityResource;
use Illuminate\Http\Resources\Json\JsonResource;

class JobInterestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'field_of_interests' => $this->field_of_interest,
            'job_types' => $this->formatted_job_types,
            'current_salary' => $this->current_salary,
            'expected_salary' => $this->expected_salary,
            'cities' => CityResource::collection(City::whereIn('id', $this->cities)->get()),
            'relocated' => $this->relocated,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
