<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrganizationExperienceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'position' => $this->position,
            'start_time' => is_null($this->start_time) ? '' : $this->start_time->format('Y-m-d'),
            'start_month' => is_null($this->start_time) ? '' : $this->start_time->format('n'),
            'start_year' => is_null($this->start_time) ? '' : $this->start_time->format('Y'),
            'end_time' => is_null($this->end_time) ? '' : $this->end_time->format('Y-m-d'),
            'end_month' => is_null($this->end_time) ? '' : $this->end_time->format('n'),
            'end_year' => is_null($this->end_time) ? '' : $this->end_time->format('Y'),
            'still_involved' => $this->still_involved,
            'more_info' => $this->more_info,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
