<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'logo_file' => $this->logo,
            'logo_url' => $this->logo_url,
            'banner_url' => $this->banner_url,
            'industry_ids' => $this->industries()->pluck('id')->toArray(),
            'industries' => IndustryResource::collection($this->industries()->orderBy('name', 'ASC')->get()),
            'short_description' => $this->short_description,
            'address' => $this->address,
            'country' => [
                'id' => $this->country_id,
                'name' => $this->country->name
            ],
            'province' => [
                'id' => $this->province_id,
                'name' => $this->province->name
            ],
            'city' => [
                'id' => $this->city_id ?? '',
                'name' => is_null($this->city_id) ? $this->city_name : $this->city->name
            ],
            'subdistrict' => [
                'id' => $this->subdistrict_id ?? '',
                'name' => is_null($this->subdistrict_id) ? '' : $this->subdistrict->name
            ],
            'pos_code' => $this->pos_code ?? '',
            'phone' => $this->phone ?? '',
            'email' => $this->email ?? '',
            'website_url' => $this->website_url ?? '',
            'instagram' => $this->instagram ?? '',
            'facebook' => $this->facebook ?? '',
            'linkedin' => $this->linkedin ?? '',
            'twitter' => $this->twitter ?? '',
            'description' => $this->description,
            'company_images' => $this->company_images,
            'created_at' => $this->created_at,
        ];
    }
}
