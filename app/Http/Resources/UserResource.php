<?php

namespace App\Http\Resources;

use App\Helpers\AppHelper;
use App\Services\EnglishAssessmentService;
use App\Services\PersonalAnalysService;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $genderText = '';
        if ($this->gender == 'male') {
            $genderText = 'Male';
        } elseif ($this->gender == 'female') {
            $genderText = 'Female';
        }

        $bookmarks = [];
        foreach ($this->bookmarks as $bm) {
            $bookmarks[] = [
                'id' => $bm->id,
                'job_id' => $bm->workjob_id,
            ];
        }

        $provinceName = $this->province_name;
        $cityName = $this->city_name;
        if ($this->country_id !== null) {
            $countryCode = $this->country->country_code;
            if ($countryCode == 'ID') {
                $provinceName = is_null($this->province) ? '' : $this->province->name;
                $cityName = is_null($this->city) ? '' : $this->city->name;
            }
        }

        return [
            'id' => $this->id,
            'identifier' => $this->identifier,
            'name' => $this->name,
            'avatar_url' => $this->avatar_url,
            'email' => $this->email,
            'phone' => $this->phone ?? '',
            'gender' => [
                'value' => $this->gender ?? '',
                'text' => $genderText,
            ],
            'dob' => is_null($this->dob) ? '' : $this->dob->format('Y-m-d'),
            'age' => $this->age,
            'about' => $this->about ?? '',
            'address' => $this->address ?? '',
            'country' => [
                'id' => $this->country_id ?? '',
                'code' => is_null($this->country) ? '' : $this->country->country_code,
                'name' => is_null($this->country) ? '' : $this->country->name,
            ],
            'province' => [
                'id' => $this->province_id ?? '',
                'name' => $provinceName,
            ],
            'city' => [
                'id' => $this->city_id ?? '',
                'name' => $cityName,
            ],
            'subdistrict' => [
                'id' => $this->subdistrict_id ?? '',
                'name' => is_null($this->subdistrict) ? '' : $this->subdistrict->name,
            ],
            'pos_code' => $this->pos_code ?? '',
            'resume' => $this->resume_url,
            'job_status' => is_null($this->job_status) ? '' : $this->job_status,
            'relationship' => is_null($this->relationship) ? '' : $this->relationship,
            'hobby' => $this->hobby ?? '',
            'residence_status' => $this->residence_status ?? '',
            'citizenship' => [
                'id' => $this->citizenship ?? '',
                'name' => is_null($this->citizenship) ? '' : $this->citizenship_data->name,
            ],
            'yt_video' => $this->yt_video ?? '',
            'yt_video_embed' => is_null($this->yt_video) ? '' : AppHelper::convertYoutubeEmbed($this->yt_video, '250px'),
            'website' => $this->website ?? '',
            'facebook' => $this->facebook ?? '',
            'twitter' => $this->twitter ?? '',
            'instagram' => $this->instagram ?? '',
            'linkedin' => $this->linkedin ?? '',
            'behance' => $this->behance ?? '',
            'github' => $this->github ?? '',
            'codepen' => $this->codepen ?? '',
            'vimeo' => $this->vimeo ?? '',
            'youtube' => $this->youtube ?? '',
            'dribbble' => $this->dribbble ?? '',
            'verified' => $this->verified,
            // 'addresses' => AddressResource::collection($this->addresses),
            'experiences' => WorkExperienceResource::collection($this->work_experiences()->orderBy('start_time', 'DESC')->get()),
            'organization_experiences' => OrganizationExperienceResource::collection($this->organization_experiences()->orderBy('start_time', 'DESC')->get()),
            'educations' => EducationResource::collection($this->educations()->orderBy('start_time', 'DESC')->get()),
            'skills' => SkillResource::collection($this->skills),
            'interest' => is_null($this->job_interest) ? '' : new JobInterestResource($this->job_interest),
            'resume' => [
                'file_name' => $this->resume ?? '',
                'file_url' => $this->resume_url
            ],
            'video' => [
                'file_name' => $this->video ?? '',
                'file_url' => $this->video_url
            ],
            'awards' => AwardResource::collection($this->awards),
            'bookmarks' => $bookmarks,
            'has_unread_notifications' => $this->unreadNotifications()->exists(),
            'when_can_start' => $this->when_can_start ?? '',
            'can_start_date' => is_null($this->can_start_date) ? '' : $this->can_start_date->format('Y-m-d'),
            'english_test' => [
                'has_data' => $this->english_assessment()->exists(),
                'score' => is_null($this->english_assessment) ? '' : $this->english_assessment->score,
                'can_redo' => EnglishAssessmentService::canRedo($this->resource),
                'test_date' => is_null($this->english_assessment) ? '' : $this->english_assessment->updated_at,
            ],
            'personal_test' => [
                'has_data' => $this->personality_test()->exists(),
                'can_redo' => PersonalAnalysService::canRedo($this->resource),
                'test_date' => is_null($this->personality_test) ? '' : $this->personality_test->updated_at,
            ],
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at ?? '',
        ];
    }
}
