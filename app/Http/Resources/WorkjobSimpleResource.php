<?php

namespace App\Http\Resources;

use App\Helpers\AppHelper;
use Illuminate\Http\Resources\Json\JsonResource;

class WorkjobSimpleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $mustSkills = [];
        foreach ($this->must_skills as $mustSkill) {
            $mustSkills[] = [
                'id' => $mustSkill->id,
                'name' => $mustSkill->name,
            ];
        }

        $niceSkills = [];
        foreach ($this->nice_skills as $niceSkill) {
            $niceSkills[] = [
                'id' => $niceSkill->id,
                'name' => $niceSkill->name,
            ];
        }

        return [
            'id' => $this->id,
            'company' => [
                'id' => $this->company_id,
                'name' => !is_null($this->company) ? $this->company->name : '',
                'logo_url' => !is_null($this->company) ? $this->company->logo_url: '',
                'slug' => !is_null($this->company) ? $this->company->slug: ''
            ],
            'identifier' => $this->identifier,
            'title' => $this->title,
            'slug' => $this->slug,
            'type' => $this->type,
            'city' => [
                'id' => $this->city_id ?? '',
                'name' => is_null($this->city_id) ? $this->city_name : $this->city->name,
            ],
            'number_of_vacancies' => $this->number_of_vacancies,
            'job_function' => [
                'id' => $this->jobfunction_id,
                'name' => !is_null($this->job_function) ? $this->job_function->name : '',
            ],
            'must_skills' => $mustSkills,
            'nice_skills' => $niceSkills,
            'work_experience' => $this->work_experience,
            'salary_currency' => $this->salary_currency,
            'salary_min' => is_null($this->salary_min) ? 0 : $this->salary_min,
            'salary_max' => is_null($this->salary_max)  ? 0 : $this->salary_max,
            'salary_duration' => $this->salary_duration,
            'bonus_salary' => $this->bonus_salary,
            'bonus_salary_min' => is_null($this->bonus_salary_min)  ? 0 : $this->bonus_salary_min,
            'bonus_salary_max' => is_null($this->bonus_salary_max)  ? 0 : $this->bonus_salary_max,
            'bonus_salary_duration' => $this->bonus_salary_duration,
            'description' => $this->description,
            'attachment' => $this->attachment,
            'job_posting_status' => $this->job_posting_status,
            'status' => $this->status,
            'hide_company' => $this->hide_company,
            'remote' => $this->remote,
            'applicants_count' => $this->job_applications()->count(),
            'has_question' => $this->has_question,
            'created_at' => $this->created_at,
            'created_at_diff_for_human' => $this->created_at->diffForHumans(),
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at ?? '',
        ];
    }
}
