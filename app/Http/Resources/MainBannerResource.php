<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MainBannerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'image_url' => $this->image_url,
            'alt_image' => $this->alt_image,
            'link' => $this->link,
            'open_new_tab' => $this->open_new_tab,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'enable' => $this->enable,
            'sort_order' => $this->sort_order,
            'displaying' => $this->displaying,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
