<?php

namespace App\Http\Resources;

use App\Models\Page;
use App\Models\Category;
use App\Services\MenuService;
use Illuminate\Http\Resources\Json\JsonResource;

class MenuResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'links' => $this->_getMenu($this->links),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }

    private function _getMenu($linksData)
    {
        $menuService = new MenuService();
        $links = [];
        foreach ($linksData as $link) {
            $url = $link['url'];
            if ($link['type'] == 'page') {
                $page = Page::find($link['id']);
                $url = $page !== null ? $page->slug : '';
            }
            if ($link['type'] == 'category') {
                $category = Category::find($link['id']);
                $url = $category !== null ? $category->slug : '';
            }
            
            if ($url != '') {
                $links[] = [
                    'type' => $link['type'],
                    'id' => $link['id'],
                    'text' => $link['text'],
                    'mega_menu' => $link['mega_menu'],
                    'mega_menu_image' => $link['mega_menu_image'],
                    'mega_menu_image_url' => ($link['mega_menu'] == 1 && $link['mega_menu_image'] != '') ? $menuService->getImageFileUrl('mega_menu_image', $link['mega_menu_image']) : '',
                    'icon_font' => $link['icon_font'],
                    'icon_image' => $link['icon_image'],
                    'icon_image_url' => ($link['icon_font'] == 0 && $link['icon_image'] != '') ? $menuService->getImageFileUrl('icon_image', $link['icon_image']) : '',
                    'url' => $url,
                    'childs' => $this->_getMenu($link['childs']),
                    'show_detail' => false,
                ];
            }
        }

        return $links;
    }
}
