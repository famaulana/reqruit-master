<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'customer' => [
                'name' => $this->customer->name,
                'email' => $this->customer->email,
            ],
            'name' => $this->name,
            'street' => $this->street,
            'province' => [
                'id' => $this->province_id,
                'name' => $this->province->name
            ],
            'city' => [
                'id' => $this->city_id,
                'name' => $this->city->name
            ],
            'subdistrict' => [
                'id' => $this->subdistrict_id,
                'name' => $this->subdistrict->name
            ],
            'post_code' => $this->post_code,
            'phone' => $this->phone,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
