<?php

namespace App\Http\Resources;

use App\Repositories\PersonalAnalysRepository;
use Illuminate\Http\Resources\Json\JsonResource;

class PersonalityTestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $conclusions = PersonalAnalysRepository::conclusions();

        $mostBehave = [
            'total' => $this->result['most']['total'],
            'name' => $this->result['most']['behave']['name'],
            'type' => $this->result['most']['behave']['type'],
        ];
        $leastBehave = [
            'total' => $this->result['least']['total'],
            'name' => $this->result['least']['behave']['name'],
            'type' => $this->result['least']['behave']['type'],
        ];

        return [
            'id' => $this->id,
            'result' => $this->result,
            'behaviour' => [
                'most' => [
                    'total' => $mostBehave['total'],
                    'name' => $mostBehave['name'],
                    'type' => $mostBehave['type'],
                    'char' => $mostBehave['name'] != 'N/A' ? $conclusions[$mostBehave['name']] : []
                ],
                'least' => [
                    'total' => $leastBehave['total'],
                    'name' => $leastBehave['name'],
                    'type' => $leastBehave['type'],
                    'char' => $leastBehave['name'] != 'N/A' ? $conclusions[$leastBehave['name']] : []
                ]
            ],
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
