<?php

namespace App\Http\Resources;

use App\Enums\JobStatusEnum;
use App\Enums\WorkjobType;
use Illuminate\Http\Resources\Json\JsonResource;

class CompanyReviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => [
                'id' => is_null($this->user) ? '' : $this->user->id,
                'name' => is_null($this->user) ? '' : $this->user->name,
                'email' => is_null($this->user) ? '' : $this->user->email,
            ],
            'company' => [
                'name' => $this->company->name
            ],
            'anonymous' => $this->anonymous,
            'job_status' => JobStatusEnum::fromValue($this->job_status),
            'employment_status' => WorkjobType::fromValue($this->employment_status),
            'job_title' => $this->job_title ?? '',
            'rating' => $this->rating ?? '',
            'review_headline' => $this->review_headline ?? '',
            'review' => $this->review ?? '',
            'published' => $this->published,
            'created_at_for_human' => $this->created_at->diffForHumans(),
            'created_at' => $this->created_at,
            'deleted_at' => $this->deleted_at,
        ];
    }
}
