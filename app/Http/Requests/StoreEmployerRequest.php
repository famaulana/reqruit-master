<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreEmployerRequest extends FormRequest
{
    /**
     * Determine if the employer is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => 'required|email|unique:employers',
            'phone' => 'required|string|unique:employers',
            'job_position' => 'required|string',
            // 'gender' => 'required|string|in:male,female',
            // 'dob' => 'required|string|date_format:Y-m-d',
            'password' => 'required|string|min:8|confirmed',
            'photo_profile' => 'nullable|mimes:jpeg,png',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'job_position.required' => 'Jabatan wajib diisi!',
        ];
    }
}
