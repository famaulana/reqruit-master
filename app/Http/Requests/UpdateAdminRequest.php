<?php

namespace App\Http\Requests;

use App\Http\Requests\StoreAdminRequest;

class UpdateAdminRequest extends StoreAdminRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules['email'] = 'required|unique:admins,email,' . $this->route('admin.id');
        $rules['password'] = 'nullable';
        return $rules;
    }
}
