<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrganizationExperienceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => "required|string",
            'position' => "required|string",
            'start_month' => "required|numeric|min:1|max:12",
            'start_year' => "required|numeric|min:1945|max:".date('Y'),
            'end_month' => "nullable|required_if:still_working,false|numeric|min:1|max:12",
            'end_year' => "nullable|required_if:still_working,false|numeric|min:1945|max:".date('Y'),
            'still_involved' => 'required|boolean',
            'more_info' => "required|string",
        ];
    }
}
