<?php

namespace App\Http\Requests;

use App\Http\Requests\StoreProductRequest;

class UpdateProductRequest extends StoreProductRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules['name'] = 'required|unique:products,name,' . $this->route('product.id');
        // $rules['slug'] = 'required|unique:products,slug,' . $this->route('product.id');
        $rules['sku'] = 'required|unique:products,sku,' . $this->route('product.id');
        $rules['short_description'] = 'required|unique:products,short_description,' . $this->route('product.id');
        $rules['description'] = 'required|unique:products,description,' . $this->route('product.id');
        $rules['meta_title'] = 'required|unique:products,meta_title,' . $this->route('product.id');
        $rules['meta_description'] = 'required|unique:products,meta_description,' . $this->route('product.id');
        return $rules;
    }
}
