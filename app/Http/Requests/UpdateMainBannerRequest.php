<?php

namespace App\Http\Requests;

use App\Http\Requests\StoreMainBannerRequest;

class UpdateMainBannerRequest extends StoreMainBannerRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules['image'] = 'nullable|image|max:500';
        return $rules;
    }
}
