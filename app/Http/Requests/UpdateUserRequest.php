<?php

namespace App\Http\Requests;

use App\Http\Requests\StoreUserRequest;

class UpdateUserRequest extends StoreUserRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules['email'] = 'required|unique:users,email,' . $this->route('user.id');
        $rules['phone'] = 'required|unique:users,phone,' . $this->route('user.id');
        $rules['password'] = 'nullable';
        return $rules;
    }
}
