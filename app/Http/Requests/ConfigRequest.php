<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ConfigRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'logo' => 'nullable|image',
            'logo1' => 'nullable|image',
            'logo2' => 'nullable|image',
            'logo3' => 'nullable|image',
            'logo_square' => 'nullable|image',
            'favicon' => 'nullable|image',
            'admin_logo' => 'nullable|string',
            'frontend_logo' => 'nullable|string',
            'email_logo' => 'nullable|string',
            'invoice_logo' => 'nullable|string',
            'light_logo' => 'nullable|string',
            'dark_logo' => 'nullable|string',
            
            'site_name' => 'sometimes|required|string',
            'site_email' => 'sometimes|required|email',
            'site_contact_email' => 'sometimes|required|email',

            'home_meta_title' => 'sometimes|required|string',
            'home_meta_description' => 'sometimes|required|string',
            'home_banner' => 'nullable|image',
            'home_banner_mobile' => 'nullable|image',
            'intro_right_image' => 'nullable|image',
            'intro_left_image' => 'nullable|image',
            'section_about_btn_link' => 'sometimes|nullable|string',
            'section_about_yt_link' => 'sometimes|nullable|string',
            'section_about_video_image' => 'nullable|image',
            'home_join_banner' => 'nullable|image',
            'home_count_1_value' => 'sometimes|nullable|string',
            'home_count_1_label' => 'sometimes|nullable|string',
            'home_count_2_value' => 'sometimes|nullable|string',
            'home_count_2_label' => 'sometimes|nullable|string',
            'home_count_3_value' => 'sometimes|nullable|string',
            'home_count_3_label' => 'sometimes|nullable|string',
            'home_count_4_value' => 'sometimes|nullable|string',
            'home_count_4_label' => 'sometimes|nullable|string',

            'featured_posts' => 'sometimes|nullable|array',
            'popular_posts' => 'sometimes|nullable|array',

            'auth_image' => 'nullable|image',
            'auth_image_employer' => 'nullable|image',

            'locale' => 'sometimes|nullable|string',

            'top_menu' => 'sometimes|nullable|numeric',
            'footer_menu' => 'sometimes|nullable|numeric',
            'top_menu_employer' => 'sometimes|nullable|numeric',

            'jf_companies' => 'sometimes|nullable|array',

            'footer_content' => 'sometimes|nullable|string',
            'footer_bg_image_enable' => 'sometimes|nullable|string|in:yes,no',
            'footer_bg_image' => 'nullable|image',

            'youtube_link' => 'sometimes|nullable|url',
            'instagram_link' => 'sometimes|nullable|url',
            'twitter_link' => 'sometimes|nullable|url',
            'facebook_link' => 'sometimes|nullable|url',

            'jap_rejection_messages' => 'sometimes|nullable|array',

            'english_test_q_count' => 'sometimes|nullable|numeric|min:1',
            'english_test_can_retest_after_days' => 'sometimes|nullable|numeric|min:0',
            'english_test_session_2_timer' => 'sometimes|nullable|numeric|min:2',
            'personal_test_can_retest_after_days' => 'sometimes|nullable|numeric|min:0',

            'javascript' => 'nullable|string',
        ];
    }
}
