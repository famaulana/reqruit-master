<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCompanyRequest extends StoreCompanyRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules['name'] = 'required|unique:companies,name,' . $this->route('company.id');
        $rules['logo'] = 'nullable|image';
        $rules['banner'] = 'nullable|image';
        $rules['phone'] = 'required|unique:companies,phone,' . $this->route('company.id');
        $rules['email'] = 'required|unique:companies,email,' . $this->route('company.id');
        return $rules;
    }
}
