<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'new_password' => 'required|string|min:8|confirmed',
            'new_password_confirmation' => 'required|string|min:8'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'new_password.required' => 'Password baru wajib diisi!',
            'new_password.min' => 'Password baru minimal 8 karakter!',
            'new_password_confirmation.required' => 'Konfirmasi password baru wajib diisi!',
            'new_password_confirmation.min' => 'Konfirmasi password baru minimal 8 karakter!',
        ];
    }
}
