<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|unique:companies',
            // 'logo' => 'required|image|mimes:jpg,png,jpeg|max:3000',
            'logo_file' => 'nullable|string',
            'banner' => 'nullable|image|mimes:jpg,png,jpeg|max:3000',
            'industry_ids' => 'required|array',
            'short_description' => 'required|string',
            'address' => 'required|string',
            'province_id' => 'required|numeric',
            'city_id' => 'required|numeric',
            'subdistrict_id' => 'required|numeric',
            'pos_code' => 'required|string',
            'phone' => 'required|string|unique:companies',
            'email' => 'required|email|unique:companies',
            'website_url' => 'nullable|url',
            'instagram' => 'nullable|url',
            'facebook' => 'nullable|url',
            'linkedin' => 'nullable|url',
            'twitter' => 'nullable|url',
            'description' => 'required|string',
            'image_file_names' => 'nullable|array|max:3',
            'image_file_names.*' => 'required|string',
            'image_descriptions' => 'nullable|array|max:3',
            'image_descriptions.*' => 'nullable|string',
        ];
    }
}
