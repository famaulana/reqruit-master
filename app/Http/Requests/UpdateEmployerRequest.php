<?php

namespace App\Http\Requests;

class UpdateEmployerRequest extends StoreEmployerRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules['email'] = 'required|unique:employers,email,' . $this->route('employer.id');
        $rules['phone'] = 'required|unique:employers,phone,' . $this->route('employer.id');
        $rules['password'] = 'nullable';
        return $rules;
    }
}
