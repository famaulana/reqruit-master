<?php

namespace App\Http\Requests;

use App\Models\Permission;
use Illuminate\Foundation\Http\FormRequest;

class StoreAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => 'required|email|unique:admins',
            'password' => 'required|string|min:8|confirmed',
            'primary' => 'nullable|string|in:true,false',
            'photo' => 'nullable|mimes:jpeg,png',
            'permissions.*' => 'nullable|in:'.implode(',', Permission::all()->pluck('id')->toArray())
        ];
    }
}
