<?php

namespace App\Http\Requests;

use App\Enums\WorkjobExperience;
use App\Enums\WorkjobPostingStatus;
use App\Enums\WorkjobSalaryDuration;
use App\Enums\WorkjobStatus;
use App\Enums\WorkjobType;
use Illuminate\Foundation\Http\FormRequest;

class StoreWorkjobRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $jobTypes = implode(',', WorkjobType::toArray());
        $jobPostingStatuses = implode(',', WorkjobPostingStatus::toArray());
        $experiences = implode(',', WorkjobExperience::toArray());
        $salariDurations = implode(',', WorkjobSalaryDuration::toArray());
        $statuses = implode(',', WorkjobStatus::toArray());

        return [
            'title' => 'required|string|max_words:28',
            'type' => 'required|string|in:'.$jobTypes,
            'province_id' => 'required|numeric',
            'city_id' => 'required|numeric',
            'number_of_vacancies' => 'required|numeric',
            'job_posting_status' => 'required|string|in:'.$jobPostingStatuses,
            'jobfunction_id' => 'required|numeric',
            'jobrole_ids' => 'required|array',
            'must_skill_ids' => 'required|array',
            'nice_skill_ids' => 'nullable|array',
            'work_experience' => 'required|string|in:'.$experiences,
            'salary_min' => 'nullable|numeric|min:0',
            'salary_max' => 'nullable|numeric|min:0',
            'salary_duration' => 'required|string|in:'.$salariDurations,
            'bonus_salary' => 'required|boolean',
            'bonus_salary_min' => 'nullable|numeric|min:0',
            'bonus_salary_max' => 'nullable|numeric|min:0',
            'bonus_salary_duration' => 'required|string|in:'.$salariDurations,
            'description' => 'required|string',
            'has_question' => 'required|boolean',
            'questions' => 'required_if:has_question,true|array',
            'status' => 'required|string|in:'.$statuses,
            'hide_company' => 'required|boolean',
            'remote' => 'required|boolean'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'Title wajib diisi!',
            'type.required' => 'Tipe pekerjaan wajib diisi!',
            'province_id.required' => 'Mohon pilih province!',
            'city_id.required' => 'Mohon pilih kota/kabupaten!',
            'number_of_vacancies.required' => 'Jumlah lowongan wajib diisi!',
            'job_posting_status.required' => 'Status postingan wajib dipilih!',
            'jobfunction_id.required' => 'Jabatan wajib dipilih!',
            'jobrole_ids.required' => 'Peran pekerjaan wajib dipilih!',
            'must_skill_ids.required' => 'Mohon pilih keahlian wajib!',
            'work_experience.required' => 'Pengalaman kerja wajib diisi!',
            'salary_min.required' => 'Gaji minimum wajib diisi!',
            'salary_max.required' => 'Gaji maksimum wajib diisi!',
            'salary_duration.required' => 'Durasi gaji wajib diisi!',
            'bonus_salary.required' => 'Apakah ada gaji bonus?',
            'bonus_salary_duration.required' => 'Durasi gaji bonus wajib diisi!',
            'description.required' => 'Deskripsi wajib diisi!',
            'has_question.required' => 'Apakah ada pre screening question?',
            'questions.required_if' => 'Question is required when Pre Screening Question is checked.',
            'status.required_if' => 'Status wajib dipilih!',
        ];
    }
}
