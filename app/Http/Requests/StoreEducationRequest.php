<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreEducationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'institution' => "required|string",
            'province_id' => "required|numeric",
            'city_id' => "required|numeric",
            'education_degree_id' => "required|numeric",
            'education_field_id' => "required|numeric",
            'gpa' => "required|numeric",
            'start_year' => "required|numeric|min:1945|max:".date('Y'),
            'end_year' => "nullable|required_if:still_working,false|numeric|min:1945|max:".date('Y'),
            'still_studying' => 'required|boolean',
            'more_info' => "required|string",
        ];
    }
}
