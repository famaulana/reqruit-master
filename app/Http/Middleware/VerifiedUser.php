<?php

namespace App\Http\Middleware;

use App\Repositories\AppConfigRepository;
use Closure;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;

class VerifiedUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('user')->check()) {
            $user = Auth::guard('user')->user();
            if (!$user->verified) {
                $userService = new UserService();

                $userService->sendVerification($user);
                Auth::guard('user')->logout();

                return redirect()->route('auth.user.login.form');
            }
            return $next($request);
        } else {
            return redirect()->route('auth.user.login.form');
        }
        return $next($request);
    }
}
