<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class VerifiedAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('admin')->check()) {
            $admin = Auth::guard('admin')->user();
            if (!$admin->verified) {
                $admin->sendVerification();
                Auth::guard('admin')->logout();

                return redirect()->route('auth.admin.login.form');
            }
            return $next($request);
        } else {
            return redirect()->route('auth.admin.login.form');
        }
        return $next($request);
    }
}
