<?php

namespace App\Http\Middleware;

use Closure;
use App\Services\EmployerService;
use Illuminate\Support\Facades\Auth;

class VerifiedEmployer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('employer')->check()) {
            $employer = Auth::guard('employer')->user();
            if (!$employer->verified) {
                $employerService = new EmployerService();
                $employerService->sendVerification($employer);
                Auth::guard('employer')->logout();

                return redirect()->route('auth.employer.login.form');
            }
            return $next($request);
        } else {
            return redirect()->route('auth.employer.login.form');
        }
        return $next($request);
    }
}
