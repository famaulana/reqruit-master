<?php

namespace App\Http\Controllers\Common;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Artesaos\SEOTools\Facades\SEOTools;

class ProfileController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  String  $identifier
     * @return \Illuminate\Http\Response
     */
    public function __invoke(String $identifier)
    {
        $user = User::with(['country', 'province', 'city', 'work_experiences', 'organization_experiences', 'educations', 'skills', 'job_interest'])
            ->where('identifier', $identifier)->first();
        
        if (is_null($user)) {
            abort(404);
        }

        SEOTools::setTitle('Profil '.$user->name);
        SEOTools::setDescription('Lihat profil '.$user->name);
        
        $user = new UserResource($user);

        return view($this->theme().'profile.index', compact('user'));
    }
}
