<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Http\Resources\Address\CityResource;
use App\Models\City;
use Illuminate\Http\Request;

class CityController extends Controller
{
    /**
      * Search city
      *
      * @param Request $request
      * @return Json response
      */
    public function search(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string'
        ]);

        $cities = City::where('name', 'like', '%'.$request->name.'%')->get();

        return $this->successResponse('Cities', CityResource::collection($cities));
    }
}
