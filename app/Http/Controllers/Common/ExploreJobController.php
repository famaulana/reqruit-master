<?php

namespace App\Http\Controllers\Common;

use App\Models\City;
use App\Models\Skill;
use App\Models\Company;
use App\Models\Workjob;
use App\Models\Jobfunction;
use Illuminate\Http\Request;
use App\Services\WorkjobService;
use App\Http\Controllers\Controller;
use App\Services\JobfunctionService;
use App\Http\Resources\SkillResource;
use App\Repositories\WorkjobRepository;
use Artesaos\SEOTools\Facades\SEOTools;
use App\Http\Resources\JobfunctionResource;
use App\Http\Resources\Address\CityResource;
use App\Http\Resources\CompanySimpleResource;
use App\Http\Resources\WorkjobSimpleResource;

class ExploreJobController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        SEOTools::setTitle('Pencarian: '.$request->keyword);
        SEOTools::setDescription('Hasil pencarian pekerjaan: '.$request->keyword);
        SEOTools::setCanonical(url('/explore-jobs'));

        $perPage = 30;
        if ($request->keyword == '') {
            $jobRepository = new WorkjobRepository();
            $jobRepository->adminListRequestValidation($request);

            $jobs = Workjob::with(['city', 'job_function', 'company']);

            $jobData = $jobRepository->search($jobs, $this->_filters($request))->paginate($perPage);
        } else {
            $jobData = WorkJob::latest()->paginate($perPage);
        }

        $jobData = WorkjobSimpleResource::collection($jobData);

        $total = empty($jobData) ? 0 : $jobData->total();
        $currentPage = empty($jobData) ? 0 : $jobData->currentPage();
        $totalPage = empty($jobData) ? 0 : $jobData->lastPage();
        $displayedStart = empty($jobData) ? 0 : (($jobData->currentPage() - 1) * $jobData->perPage()) + 1;
        $displayedEnd = empty($jobData) ? 0 : (($jobData->currentPage() == $jobData->lastPage()) ? $jobData->total() : $jobData->currentPage() * $jobData->perPage());
        $showingItemCount = empty($jobData) ? 0 : (($jobData->currentPage() == $jobData->lastPage()) ? $jobData->total() % $perPage : $perPage);

        $popularCats = Jobfunction::inRandomOrder()->limit(3)->get();
        $popularSkills = Skill::inRandomOrder()->limit(3)->get();
        $popularCities = City::with('province')->inRandomOrder()->limit(3)->get();
        $popularCompanies = Company::inRandomOrder()->limit(3)->get();

        $data = [
            'keyword' => $request->keyword,
            'job_type_select' => WorkjobService::jobTypeOptionSelect(),
            'job_function_select' => JobfunctionService::optionSelect(),
            'job_posting_status_select' => WorkjobService::jobPostingStatusOptionSelect(),
            'job_status_select' => WorkjobService::jobStatusOptionSelect(),
            'job_experience_select' => WorkjobService::jobExperienceOptionSelect(),
            'popular_cats' => JobfunctionResource::collection($popularCats),
            'popular_skills' => SkillResource::collection($popularSkills),
            'popular_cities' => CityResource::collection($popularCities),
            'popular_companies' => CompanySimpleResource::collection($popularCompanies),
            'jobs' => $jobData,
            'showing_items_count' => $showingItemCount,
            'total' => $total,
            'per_page' => empty($jobData) ? 0 : $jobData->perPage(),
            'current_page' => $currentPage,
            'total_page' => $totalPage,
            'displayed_start' => $displayedStart,
            'displayed_end' => $displayedEnd
        ];

        if ($request->ajax()) {
            return $this->successResponse('Job list', $data);
        }

        return view($this->theme().'explore-jobs.index', compact('data'));
    }

    private function _filters($request)
    {
        return [
            'keyword' => $request->keyword,

            'identifier' => $request->identifier,
            'title' => $request->title,

            'type' => $request->type,
            'job_types' => $request->job_types,

            'country_id' => $request->country_id,
            'city_id' => $request->city_id,
            'cities' => $request->cities,

            'jobfunction_id' => $request->jobfunction_id,
            'jobfunctions' => $request->jobfunctions,

            'company_id' => $request->company_id,
            'companies' => $request->companies,

            'skills' => $request->skills,

            'salary_min' => $request->salary_min,
            'salary_max' => $request->salary_max,
            'salary_duration' => $request->salary_duration,

            'work_experience' => $request->work_experience,

            'bonus_salary' => $request->bonus_salary,
            'bonus_salary_min' => $request->bonus_salary_min,
            'bonus_salary_max' => $request->bonus_salary_max,
            'bonus_salary_duration' => $request->bonus_salary_duration,

            'job_posting_status' => $request->job_posting_status,
            'status' => $request->status,

            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
        ];
    }
}
