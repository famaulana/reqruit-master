<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Http\Resources\SkillResource;
use App\Models\Skill;
use App\Repositories\SkillRepository;
use Illuminate\Http\Request;

class SkillController extends Controller
{
    /**
     * Search skill
     *
     * @param Request $request
     * @return Json response
     */
    public function search(Request $request)
    {
        $repository = new SkillRepository();
        $repository->adminListRequestValidation($request);
        $repository->setFilters($request);

        $skills = Skill::select('*');
        $skills = $repository->search($skills)->get();

        return $this->successResponse('Job functions', SkillResource::collection($skills));
    }
}
