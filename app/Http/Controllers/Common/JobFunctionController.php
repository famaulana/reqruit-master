<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Http\Resources\JobfunctionResource;
use App\Models\Jobfunction;
use App\Repositories\JobfunctionRepository;
use Illuminate\Http\Request;

class JobFunctionController extends Controller
{
    /**
     * Search jobfunction
     *
     * @param Request $request
     * @return Json response
     */
    public function search(Request $request)
    {
        $repository = new JobfunctionRepository();
        $repository->adminListRequestValidation($request);
        $repository->setFilters($request);

        $jobFunctions = Jobfunction::select('*');
        $jobFunctions = $repository->search($jobFunctions)->get();

        return $this->successResponse('Job functions', JobfunctionResource::collection($jobFunctions));
    }
}
