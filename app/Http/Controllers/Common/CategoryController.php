<?php

namespace App\Http\Controllers\Common;

use App\Models\Brand;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\BrandResource;
use App\Services\ProductSearchService;
use Artesaos\SEOTools\Facades\SEOTools;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\SearchableAttributeResource;

class CategoryController extends Controller
{
    /**
     * Category page
     *
     * @param Request $request
     * @param String $slug
     * @return void
     */
    public function __invoke(Request $request, String $slug)
    {
        $category = Category::with(['searchable_attributes.options'])->where('slug', $slug)->first();

        if ($category === null) {
            abort(404);
        }

        $additionalValidations = [];
        $searchableAttributes = $category->searchable_attributes;
        $additionalRepositoryFilters = [];
        $productSearchService = new ProductSearchService(
            $additionalValidations,
            $searchableAttributes,
            $additionalRepositoryFilters,
            $request->all()
        );

        $this->validate($request, $productSearchService->validations);

        SEOTools::setTitle($category->meta_title);
        SEOTools::setDescription($category->meta_description);
        SEOTools::setCanonical(url('/category/'.$category->slug));
        SEOTools::opengraph()->setUrl(url('/category/'.$category->slug));
        SEOTools::opengraph()->addProperty('type', 'product');
        SEOTools::twitter()->setTitle($category->meta_title);
        if ($category->featured_image_url != '') {
            SEOTools::jsonLd()->addImage($category->featured_image_url);
        }

        $products = $category->products()->relation()->active();
        $products = $productSearchService->products($products, $request->all());
        
        $paginate = ($request->has('paginate')) ? $request->paginate : 12;
        $products = $products->simplePaginate($paginate)->withQueryString();

        $filterData = $productSearchService->filterData($paginate);

        $data = [
            'category' => new CategoryResource($category),
            'products' => $request->ajax() ? view($this->theme().'share.product.product-list', ['products' => $products, 'col_lg' => 'col-lg-4'])->render() : $products,
            'prev_page_url' => $products->previousPageUrl(),
            'next_page_url' => $products->nextPageUrl(),
            'searchableAttributes' => SearchableAttributeResource::collection($searchableAttributes),
            'brands' => BrandResource::collection(Brand::active()->orderBy('name', 'ASC')->get()),
            'filters' => $filterData
        ];

        if ($request->ajax()) {
            return response()->json([
                'status' => 'success',
                'message' => 'Product list',
                'data' => $data
            ]);
        }
        
        return view($this->theme().'category.index', $data);
    }
}
