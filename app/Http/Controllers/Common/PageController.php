<?php

namespace App\Http\Controllers\Common;

use App\Models\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Artesaos\SEOTools\Facades\SEOTools;

class PageController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  String  $slug
     * @return Blade View
     */
    public function __invoke(String $slug)
    {
        $page = Page::where('slug', $slug)->first();

        if ($page === null) {
            abort(404);
        }
        
        SEOTools::setTitle($page->meta_title);
        SEOTools::setDescription($page->meta_description);
        SEOTools::setCanonical(url('/page/'.$page->slug));
        SEOTools::opengraph()->setUrl(url('/page/'.$page->slug));
        SEOTools::opengraph()->addProperty('type', 'article');
        SEOTools::twitter()->setTitle($page->meta_title);

        return view($this->theme().'pages.index', compact('page'));
    }
}
