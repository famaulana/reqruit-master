<?php

namespace App\Http\Controllers\Common;

use App\Models\Brand;
use App\Models\Product;
use App\Models\Attribute;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\BrandResource;
use App\Services\ProductSearchService;
use App\Http\Resources\ProductResource;
use Artesaos\SEOTools\Facades\SEOTools;
use App\Http\Resources\SearchableAttributeResource;

class SearchController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Blade view or json response
     */
    public function __invoke(Request $request)
    {
        $additionalValidations = ['name' => 'required|string'];
        $searchableAttributes = Attribute::searchable()->get();
        $additionalRepositoryFilters = ['name' => $request->name];
        $productSearchService = new ProductSearchService(
            $additionalValidations,
            $searchableAttributes,
            $additionalRepositoryFilters,
            $request->all()
        );

        $this->validate($request, $productSearchService->validations);

        SEOTools::setTitle('Pencarian: '.$request->name);
        SEOTools::setDescription('Hasil pencarian produk: '.$request->name);
        SEOTools::setCanonical(url('/search'));

        $products = Product::relation()->active();
        $products = $productSearchService->products($products, $request->all());
        
        $paginate = ($request->has('paginate')) ? $request->paginate : 12;
        $products = $products->simplePaginate($paginate)->withQueryString();

        $filterData = $productSearchService->filterData($paginate);

        $data = [
            'search_term' => $request->name,
            'products' => $request->ajax() ? view($this->theme().'share.product.product-list', ['products' => $products, 'col_lg' => 'col-lg-4'])->render() : $products,
            'prev_page_url' => $products->previousPageUrl(),
            'next_page_url' => $products->nextPageUrl(),
            'searchableAttributes' => SearchableAttributeResource::collection($searchableAttributes),
            'brands' => BrandResource::collection(Brand::active()->orderBy('name', 'ASC')->get()),
            'filters' => $filterData
        ];

        if ($request->ajax()) {
            return response()->json([
                'status' => 'success',
                'message' => 'Product list',
                'data' => $data
            ]);
        }
        
        return view($this->theme().'search.index', $data);
    }
}
