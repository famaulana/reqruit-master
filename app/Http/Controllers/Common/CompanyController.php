<?php

namespace App\Http\Controllers\Common;

use App\Models\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CompanyResource;
use App\Repositories\CompanyRepository;
use Artesaos\SEOTools\Facades\SEOTools;

class CompanyController extends Controller
{
    /**
      * Search company
      *
      * @param Request $request
      * @return Json response
      */
    public function search(Request $request)
    {
        $repository = new CompanyRepository();
        $repository->adminListRequestValidation($request);

        $companies = Company::select('*');
        $filters = [
            'keyword' => $request->keyword,
            'name' => $request->name,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
        ];
        $companies = $repository->search($companies, $filters)->get();

        return $this->successResponse('Job functions', CompanyResource::collection($companies));
    }

    /**
     * Company page
     *
     * @param String $slug
     * @return Blade view
     */
    public function index(String $slug)
    {
        $company = Company::where('slug', $slug)->first();

        SEOTools::setTitle($company->name);
        SEOTools::setDescription('Halaman detail perusahaan '.$company->name);
        SEOTools::setCanonical(url("/company/$company->slug"));

        $images = [];
        foreach ($company->company_images as $image) {
            $images[] = [
                'src' => $image['src'],
                'description' => $image['description']
            ];
        }

        return view($this->theme().'company.index', compact('company', 'images'));
    }
}
