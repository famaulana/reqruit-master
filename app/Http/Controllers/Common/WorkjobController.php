<?php

namespace App\Http\Controllers\Common;

use App\Models\Workjob;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\JobQuestionResource;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\WorkjobResource;
use App\Http\Resources\WorkjobSimpleResource;
use App\Services\UserService;
use Artesaos\SEOTools\Facades\SEOTools;
use Illuminate\Support\Facades\Session;

class WorkjobController extends Controller
{
    /**
     * Display job detail
     *
     * @param Request $request
     * @param String $slug
     * @param String $identifier
     * @return Blade view
     */
    public function index(Request $request, String $slug, String $identifier)
    {
        $job = Workjob::where('slug', $slug)->where('identifier', $identifier)->open()->published()->first();
        
        SEOTools::setTitle($job->title);
        SEOTools::setDescription('Lowongan kerja di '.$job->company->name.'. '.$job->title);
        SEOTools::setCanonical(url("/job/$job->slug/$job->identifier"));

        $jap = ['applied' => false, 'status' => ''];
        if (Auth::guard('user')->check()) {
            $user = $request->user();
            $jobApplication = $user->job_applications()->where('workjob_id', $job->id)->first();
            
            if (!is_null($jobApplication)) {
                $jap = ['applied' => true, 'status' => $jobApplication->status];
            }
        }

        $job = new WorkjobResource($job);
        
        return view($this->theme().'workjob.index', compact('job', 'jap'));
    }

    /**
     * Apply for the job
     *
     * @param Request $request
     * @param String $identifier
     * @return void
     */
    public function apply(Request $request, String $slug, String $identifier)
    {
        if (Auth::guest()) {
            Session::flash('flash_alert', [
                'level' => 'danger',
                'message' => "Anda harus login untuk melamar pekerjaan!",
            ]);
            return redirect()->back();
        }

        $job = Workjob::where('slug', $slug)->where('identifier', $identifier)->open()->published()->first();

        if (is_null($job)) {
            Session::flash('flash_alert', [
                'level' => 'danger',
                'message' => "Pekerjaan tidak ditemukan!",
            ]);
            return redirect()->back();
        }

        if ($job->has_question && $job->job_questions()->exists()) {
            return redirect()->route('workjob.questions', ['slug' => $job->slug, 'identifier' => $job->identifier]);
        }

        $user = $request->user();

        if ($user->job_applications()->where('workjob_id', $job->id)->exists()) {
            Session::flash('flash_alert', [
                'level' => 'danger',
                'message' => "Anda sudah melamar pekerjaan ini!",
            ]);
            return redirect()->back();
        }

        $service = new UserService();
        $service->applyJob($user, $job->id);

        Session::flash('flash_alert', [
            'level' => 'success',
            'message' => "Berhasil melamar pekerjaan!",
        ]);

        return redirect()->back();
    }

    /**
     * Displaying the job_questions to be answered
     *
     * @param Request $request
     * @param String $slug
     * @param String $identifier
     * @return Blade view
     */
    public function questions(Request $request, String $slug, String $identifier)
    {
        $job = Workjob::where('slug', $slug)->where('identifier', $identifier)->open()->published()->first();

        if (is_null($job)) {
            Session::flash('flash_alert', [
                'level' => 'danger',
                'message' => "Pekerjaan tidak ditemukan!",
            ]);
            return redirect()->route('explore-jobs');
        }

        if (Auth::guest()) {
            Session::flash('flash_alert', [
                'level' => 'danger',
                'message' => "Anda harus login untuk melamar pekerjaan!",
            ]);
            return redirect()->route('workjob.index', ['slug' => $job->slug, 'identifier' => $job->identifier]);
        }

        if (!$job->has_question || !$job->job_questions()->exists()) {
            return redirect()->route('workjob.index', ['slug' => $job->slug, 'identifier' => $job->identifier]);
        }

        SEOTools::setTitle('Pertanyaan: '.$job->title);
        SEOTools::setDescription('Pertanyaan untuk pekerjaan: '.$job->title);
        SEOTools::setCanonical(url("/job/$job->slug/$job->identifier/questions"));

        $questions = JobQuestionResource::collection($job->job_questions);
        $job = new WorkjobSimpleResource($job);

        return view($this->theme().'workjob.questions', compact('questions', 'job'));
    }

    /**
     * Answer the job questions
     *
     * @param Request $request
     * @param String $slug
     * @param String $identifier
     * @return Json response
     */
    public function answers(Request $request, String $slug, String $identifier)
    {
        $this->validate($request, [
            'answers' => 'required|Array'
        ]);

        $job = Workjob::where('slug', $slug)->where('identifier', $identifier)->open()->published()->first();

        if (is_null($job)) {
            return $this->errorResponse('Pekerjaan tidak ditemukan!', 404);
        }

        if (Auth::guest()) {
            return $this->errorResponse('Anda harus login untuk menjawab pertanyaan!', 403);
        }

        try {
            $service = new UserService();
            $service->applyJob($request->user(), $job->id, $request->answers);
        } catch (\Throwable $th) {
            return $this->throwException($th);
        }
        
        Session::flash('flash_alert', [
            'level' => 'success',
            'message' => "Berhasil melamar pekerjaan!",
        ]);

        return $this->successResponse('Berhasil melamar pekerjaan!', []);
    }

    /**
     * Cancel job application
     *
     * @param Request $request
     * @param String $slug
     * @param String $identifier
     * @return void
     */
    public function cancelApplication(Request $request, String $slug, String $identifier)
    {
        if (Auth::guest()) {
            Session::flash('flash_alert', [
                'level' => 'danger',
                'message' => "Anda harus login untuk membatalkan lamaran pekerjaan!",
            ]);
            return redirect()->back();
        }

        $job = Workjob::where('slug', $slug)->where('identifier', $identifier)->first();

        if (is_null($job)) {
            Session::flash('flash_alert', [
                'level' => 'danger',
                'message' => "Pekerjaan tidak ditemukan!",
            ]);
            return redirect()->back();
        }

        $user = $request->user();
        $jobApplication = $user->job_applications()->where('workjob_id', $job->id)->first();

        if (is_null($jobApplication)) {
            Session::flash('flash_alert', [
                'level' => 'danger',
                'message' => "Tidak ada lamaran untuk pekerjaan ini!",
            ]);
            return redirect()->back();
        }

        $jobApplication->delete();

        Session::flash('flash_alert', [
            'level' => 'success',
            'message' => "Berhasil membatalkan lamaran pekerjaan!",
        ]);

        return redirect()->back();
    }
}
