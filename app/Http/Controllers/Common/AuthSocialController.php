<?php

namespace App\Http\Controllers\Common;

use App\Models\User;
use Illuminate\Support\Str;
use App\Services\UserService;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;

class AuthSocialController extends Controller
{
    public function login($service)
    {
        // return Socialite::driver($service)->stateless()->redirect();
        return Socialite::driver($service)->redirect();
    }

    public function callback($service)
    {
        try {
            // $social = Socialite::driver($service)->stateless()->user();
            $social = Socialite::driver($service)->user();
        } catch (\Exception $e) {
            return redirect(config('frontend.frontend_url').'/auth/social/callback?error='.trans('auth.social.failed', ['service' => ucfirst($service)]));
        }

        $user = User::where('email', $social->email)->first();
        $origin = 'login';

        // Register new user
        if ($user === null) {
            $userService = new UserService();
            $user = $userService->createUser([
                'name' => $social->name,
                'email' => $social->email,
                'password' => Str::random(10)
            ]);

            $origin = 'register';
        }

        $token = $user->generateToken();
        
        return redirect(config('frontend.frontend_url')."/auth/social/callback?token=$token&origin=$origin");
    }
}
