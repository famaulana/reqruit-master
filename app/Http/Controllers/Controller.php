<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function jsonUnescaped($data)
    {
        $options = app('request')->header('accept-charset') == 'utf-8' ? JSON_UNESCAPED_UNICODE : null;

        return response()->json($data, 200, [], JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
    }

    /**
     * Get Admin permissions
     *
     * @param Admin $admin
     * @return Array
     */
    protected function getPermissions(Admin $admin)
    {
        return $admin->permissions->pluck('name')->toArray();
    }

    /**
     * Get active theme
     *
     * @return String
     */
    protected function theme()
    {
        return 'common.' . config('active_theme.domain') . '.';
    }

    /**
     * Success json response
     *
     * @param String $message
     * @param mix $data
     * @return Json response
     */
    protected function successResponse(String $message, $data)
    {
        return $this->jsonUnescaped([
            'status' => 'success',
            'message' => $message,
            'data' => $data
        ]);
    }

    /**
     * Error json response
     *
     * @param String $message
     * @param Integer $statusCode
     * @return Json response
     */
    protected function errorResponse(String $message, Int $statusCode)
    {
        return response()->json([
            'status' => 'error',
            'message' => $message,
        ], $statusCode);
    }

    /**
     * Throw exception
     *
     * @param \Throwable $th
     * @return void
     */
    protected function throwException(\Throwable $th)
    {
        if ($th->getCode() == 0 || $th->getCode() == 23000 || $th->getCode() == 22007) {
            return $this->errorResponse($th->getMessage(), 500);
        } else {
            return $this->errorResponse($th->getMessage(), $th->getCode());
        }
    }

    /**
     * Paginated data
     *
     * @return Array
     */
    protected function paginatedData($resource, $data)
    {
        return [
            'resource' => $resource,
            'total' => $data->total(),
            'per_page' => $data->perPage(),
            'current_page' => $data->currentPage(),
            'total_page' => $data->lastPage(),
            'displayed_start' => (($data->currentPage() - 1) * $data->perPage()) + 1,
            'displayed_end' => ($data->currentPage() == $data->lastPage()) ? $data->total() : $data->currentPage() * $data->perPage()
        ];
    }
}
