<?php

namespace App\Http\Controllers\Employer;

use App\Models\Country;
use App\Models\Workjob;
use App\Models\JobQuestion;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Services\WorkjobService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Http\Resources\WorkjobResource;
use Artesaos\SEOTools\Facades\SEOTools;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\StoreWorkjobRequest;
use App\Http\Requests\UpdateWorkjobRequest;
use App\Services\JobApplicationService;

class WorkjobController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $employer = $request->user();

            if (!$employer->self_recruitment) {
                return redirect()->route('account-empl.assist-recruitment');
            } else {
                return $next($request);
            }
        });
    }
    
    /**
     * Displaying jobs
     *
     * @param Request $request
     * @return Mix Json response or Blade template
     */
    public function index(Request $request)
    {
        $this->validate($request, [
            'keyword' => 'nullable|string',
            'sort' => 'nullable|string|in:created_at,updated_at,title',
        ]);

        SEOTools::setTitle('Kelola Pekerjaan');
        SEOTools::setDescription('Kelola Pekerjaan');

        $employer = $request->user();
        $company = $employer->company;

        if (is_null($company)) {
            Session::flash('flash_alert', [
                'level' => 'error',
                'message' => "Please create a company, before see the job listing!",
            ]);

            return redirect()->route('account-empl.company.index');
        }

        $jobs = $company->work_jobs();

        if ($request->has('keyword')) {
            $jobs = $jobs->where('title', 'LIKE', '%'.$request->keyword.'%');
        }
        
        if ($request->has('sort')) {
            $sortType = $request->sort == 'title' ? 'ASC' : 'DESC';
            $jobs = $jobs->orderBy($request->sort, $sortType);
        } else {
            $jobs = $jobs->orderBy('created_at', 'DESC');
        }

        $jobs = WorkjobResource::collection($jobs->simplePaginate(20));

        $data = [
            'jobs' => $jobs,
            'prev_url' => $jobs->previousPageUrl() ?? '',
            'next_url' => $jobs->nextPageUrl() ?? '',
        ];

        if ($request->ajax()) {
            return $this->successResponse('Job list', $data);
        }
        
        return view($this->theme().'employer.workjob.index', compact('data'));
    }

    /**
     * Get job option select
     *
     * @return Array
     */
    private function _getOptionSelect()
    {
        $service = new WorkjobService();

        return [
            'experience' => $service->jobExperienceOptionSelect(),
            'job_posting_status' => $service->jobPostingStatusOptionSelect(),
            'salary_duration' => $service->jobSalaryDurationOptionSelect(),
            'status' => $service->jobStatusOptionSelect(),
            'type' => $service->jobTypeOptionSelect(),
        ];
    }

    /**
     * Display the create new job form
     *
     * @return Blade view
     */
    public function create()
    {
        $this->authorize('create', Workjob::class);

        SEOTools::setTitle('Pekerjaan Baru');
        SEOTools::setDescription('Pekerjaan Baru');

        $optionSelect = $this->_getOptionSelect();

        return view($this->theme().'employer.workjob.create', compact('optionSelect'));
    }

    /**
     * Store new workjob
     *
     * @param StoreWorkjobRequest $request
     * @return Json response
     */
    public function store(StoreWorkjobRequest $request)
    {
        $this->authorize('create', Workjob::class);

        $employer = $request->user();
        $company = $employer->company;

        $additionalData = [
            'created_by' => $employer->id,
            'edited_by' => $employer->id,
            'identifier' => WorkjobService::generateIdentifier(),
            'slug' => Str::slug($request->title),
            'country_id' => Country::where('country_code', 'ID')->first()->id,
            'salary_currency' => 'IDR'
        ];

        $allData = array_merge($request->validated(), $additionalData);
        $workjob = $company->work_jobs()->create($allData);
        $workjob->job_roles()->attach($request->jobrole_ids);
        $workjob->must_skills()->attach($request->must_skill_ids);
        $workjob->nice_skills()->attach($request->nice_skill_ids);

        // Processing questions
        if ($request->has_question) {
            $questions = [];
            foreach ($request->questions as $q) {
                $questions[] = [
                    'question' => $q['question'],
                    'sort_order' => $q['sort_order'],
                ];
            }

            $workjob->job_questions()->createMany($questions);
        }

        Session::flash('flash_alert', [
            'level' => 'success',
            'message' => "Pekerjaan berhasil dibuat",
        ]);

        return $this->successResponse('Pekerjaan berhasil dibuat', []);
    }

    /**
     * Edit workjob
     *
     * @param Workjob $workjob
     * @return Blade view
     */
    public function edit(Workjob $workjob)
    {
        $this->authorize('update', $workjob);

        SEOTools::setTitle('Edit Pekerjaan');
        SEOTools::setDescription('Edit Pekerjaan');

        $workjob = new WorkjobResource($workjob);
        $optionSelect = $this->_getOptionSelect();

        return view($this->theme().'employer.workjob.edit', compact('workjob', 'optionSelect'));
    }

    /**
     * Update workjob
     *
     * @param UpdateWorkjobRequest $request
     * @param Workjob $workjob
     * @return Json response
     */
    public function update(UpdateWorkjobRequest $request, Workjob $workjob)
    {
        $this->authorize('update', $workjob);

        $employer = $request->user();

        $additionalData = [
            'edited_by' => $employer->id,
            'slug' => Str::slug($request->title),
        ];

        $allData = array_merge($request->validated(), $additionalData);
        $workjob->update($allData);
        $workjob->job_roles()->sync($request->jobrole_ids);
        $workjob->must_skills()->sync($request->must_skill_ids);
        $workjob->nice_skills()->sync($request->nice_skill_ids);

        // Processing job questions
        $qIdsExisting = $workjob->job_questions->pluck('id')->toArray();
        if ($request->has_question) {
            $qIdsKeep = [];
            foreach ($request->questions as $q) {
                if (is_null($q['id'])) {
                    // Create new question
                    $workjob->job_questions()->create([
                        'question' => $q['question'],
                        'sort_order' => $q['sort_order'],
                    ]);
                } else {
                    // Update existing question
                    $qIdsKeep[] = $q['id'];
                    $question = JobQuestion::find($q['id']);
                    if (!is_null($question)) {
                        if ($question->question != $q['question'] || $question->sort_order != $q['sort_order']) {
                            $question->update([
                                'question' => $q['question'],
                                'sort_order' => $q['sort_order'],
                            ]);
                        }
                    }
                }
            }

            // Removing deleted questions
            $qIdsRemove = [];
            foreach ($qIdsExisting as $exisingId) {
                if (!in_array($exisingId, $qIdsKeep)) {
                    $qIdsRemove[] = $exisingId;
                }
            }
            if (!empty($qIdsRemove)) {
                $qToBeDeleted = JobQuestion::findMany($qIdsRemove);
                foreach ($qToBeDeleted as $qDelete) {
                    $qDelete->delete();
                }
            }
        } else {
            // delete all questions
            if (!empty($qIdsExisting)) {
                $qToBeDeleted = JobQuestion::findMany($qIdsExisting);
                foreach ($qToBeDeleted as $qDelete) {
                    $qDelete->delete();
                }
            }
        }

        Session::flash('flash_alert', [
            'level' => 'success',
            'message' => "Pekerjaan berhasil diperbarui",
        ]);

        return $this->successResponse('Perkerjaan berhasil diperbarui', []);
    }

    /**
     * Force delete workjob
     *
     * @param Workjob $workjob
     * @return Json response
     */
    public function destroy(Workjob $workjob)
    {
        $this->authorize('delete', $workjob);

        $workjob->forceDelete();

        Session::flash('flash_alert', [
            'level' => 'success',
            'message' => 'Pekerjaan berhasil dihapus'
        ]);

        return $this->successResponse('Pekerjaan berhasil dihapus', []);
    }

    /**
     * Search workjob
     *
     * @param Request $request
     * @return Json response
     */
    public function search(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string'
        ]);
        
        $employer = $request->user();
        $company = $employer->company;

        $jobs = $company->work_jobs()->where('title', 'LIKE', '%'.$request->title.'%')->get();

        return $this->successResponse('Job search result', $jobs);
    }

    /**
     * Get workjob's job applications
     *
     * @param Workjob $workjob
     * @return Json response
     */
    public function jobApplications(Request $request, Workjob $workjob)
    {
        if (Gate::denies('view-job-applications', $workjob->company)) {
            return $this->errorResponse('Akses terlarang', 403);
        }
        
        $service = new JobApplicationService();
        $jobApplications = $service->getJobApplicationsAllStatus($workjob);

        return $this->successResponse('Job applications', $jobApplications);
    }
}
