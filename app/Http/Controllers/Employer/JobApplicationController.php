<?php

namespace App\Http\Controllers\Employer;

use App\Models\Workjob;
use Illuminate\Http\Request;
use App\Models\JobApplication;
use App\Enums\JobApplicationStatus;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Gate;
use App\Services\JobApplicationService;
use Artesaos\SEOTools\Facades\SEOTools;
use App\Http\Resources\WorkjobSimpleResource;
use App\Http\Resources\JobQuestionAnswerResource;

class JobApplicationController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $employer = $request->user();

            if (!$employer->self_recruitment) {
                return redirect()->route('account-empl.assist-recruitment');
            } else {
                return $next($request);
            }
        });
    }
    
    /**
     * Manage job applicants
     *
     * @return Blade view
     */
    public function index(Request $request)
    {
        SEOTools::setTitle('Kelola Pelamar Pekerjaan');
        SEOTools::setDescription('Kelola Pelamar Pekerjaan');

        $service = new JobApplicationService();

        $jobApplications = [];
        $workjob = [];
        if ($request->has('job_identifier')) {
            $workjob = Workjob::where('identifier', $request->job_identifier)->first();
            if (!is_null($workjob)) {
                $jobApplications = $service->getJobApplicationsAllStatus($workjob);
                $workjob = new WorkjobSimpleResource($workjob);
            }
        }

        $option_select = [
            'job_application_status' => $service->statusOptionSelect(),
        ];

        return view($this->theme().'employer.job-applications.index', compact('option_select', 'jobApplications', 'workjob'));
    }

    /**
     * View candidate
     *
     * @param JobApplication $jobApplication
     * @return Json response
     */
    public function profile(JobApplication $jobApplication)
    {
        if (Gate::denies('view-candidate', $jobApplication)) {
            return $this->errorResponse('Akses terlarang', 403);
        }

        $user = $jobApplication->user()->with(['country', 'province', 'city', 'work_experiences', 'educations', 'skills', 'job_interest'])->first();
        $user = new UserResource($user);
        $answers = JobQuestionAnswerResource::collection($jobApplication->job_question_answers);

        $data = [
            'user' => $user,
            'answers' => $answers,
            'cover_letter' => $jobApplication->cover_letter
        ];

        return $this->successResponse('User profile', $data);
    }

    /**
     * Update jobApplication status
     *
     * @param Request $request
     * @param JobApplication $jobApplication
     * @return Json response
     */
    public function statusUpdate(Request $request, JobApplication $jobApplication)
    {
        if (Gate::denies('view-candidate', $jobApplication)) {
            return $this->errorResponse('Akses terlarang', 403);
        }

        $statuses = implode(',', JobApplicationStatus::toArray());

        $this->validate($request, [
            'status' => 'required|string|in:'.$statuses
        ]);

        $service = new JobApplicationService();
        $service->setJobApplication($jobApplication);
        $jobApplication = $service->updateStatus($request->status);

        return $this->successResponse('Status lamaran berhasil diperbarui', []);
    }
}
