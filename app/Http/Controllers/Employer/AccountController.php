<?php

namespace App\Http\Controllers\Employer;

use App\Helpers\PathHelper;
use App\Models\StaticBlock;
use App\Helpers\ImageHelper;
use Illuminate\Http\Request;
use App\Models\JobApplication;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\PasswordRequest;
use Artesaos\SEOTools\Facades\SEOTools;
use Illuminate\Support\Facades\Session;
use App\Http\Resources\EmployerResource;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $employer = $request->user();

            $routeNames = [
                'account-empl.dashboard',
                'account-empl.recruitment-type',
                'account-empl.self-recruitment',
                'assist-recruitment.post',
            ];

            if (!is_null($employer->self_recruitment) && !$employer->self_recruitment && in_array($request->route()->getName(), $routeNames)) {
                return redirect()->route('account-empl.assist-recruitment');
            } else {
                return $next($request);
            }
        });
    }

    /**
     * Employer dashboard
     *
     * @param Request $request
     * @return Blade view
     */
    public function dashboard(Request $request)
    {
        $employer = $request->user();

        if (is_null($employer->self_recruitment)) {
            return redirect()->route('account-empl.recruitment-type');
        }

        SEOTools::setTitle('Dashboard Akun Perusahaan');
        SEOTools::setDescription('Dashboard akun perusahaan');

        $company = $employer->company;
        $jobIds = is_null($company) ? [] : $employer->company->work_jobs->pluck('id')->toArray();

        $data = [
            'employer' => $employer,
            'pending' => JobApplication::whereIn('workjob_id', $jobIds)->where('status', 'pending')->count(),
            'shortlisted' => JobApplication::whereIn('workjob_id', $jobIds)->where('status', 'shortlisted')->count(),
            'interview' => JobApplication::whereIn('workjob_id', $jobIds)->where('status', 'interview')->count(),
            'offered' => JobApplication::whereIn('workjob_id', $jobIds)->where('status', 'offered')->count(),
            'hired' => JobApplication::whereIn('workjob_id', $jobIds)->where('status', 'hired')->count(),
            'unsuitable' => JobApplication::whereIn('workjob_id', $jobIds)->where('status', 'unsuitable')->count(),
        ];

        return view($this->theme().'employer.account.dashboard', $data);
    }

    /**
     * Choosing the recruitment type page
     *
     * @return Blade view
     */
    public function recruitmentType()
    {
        SEOTools::setTitle('Recruitment Type');
        SEOTools::setDescription('Recruitment Type');

        $selfReqBlock = StaticBlock::where('name', 'Self Recruitment Details')->where('active', 1)->first();
        $assistReqBlock = StaticBlock::where('name', 'Assist Recruitment Details')->where('active', 1)->first();

        $data = [
            'selfReqBlock' => is_null($selfReqBlock) ? '' : $selfReqBlock,
            'assistReqBlock' => is_null($assistReqBlock) ? '' : $assistReqBlock
        ];

        return view($this->theme().'employer.account.recruitment-type', $data);
    }

    /**
     * Emloyer set as self recruitment
     *
     * @param Request $request
     * @return void
     */
    public function selfRecruitment(Request $request)
    {
        $employer = $request->user();

        if ($employer->role->value !== 'super-administrator') {
            Session::flash('flash_alert', [
                'level' => 'error',
                'message' => "You are not allowed to do this action",
            ]);

            return redirect()->route('account-empl.dashboard');
        }

        $employer->self_recruitment = true;
        $employer->save();

        return redirect()->route('account-empl.company.index');
    }

    /**
     * Assist recruitment type page
     *
     * @return Blade view
     */
    public function assistRecruitment(Request $request)
    {
        SEOTools::setTitle('Assist Recruitment Type');
        SEOTools::setDescription('Assist Recruitment Type');

        $employer = $request->user();

        return view($this->theme().'employer.account.assist-recruitment', compact('employer'));
    }

    /**
     * Send the details about assist recruitment
     *
     * @param Request $request
     * @return void
     */
    public function assistRecruitmentPost(Request $request)
    {
        $this->validate($request, [
            'position' => 'required|string',
            'more_info' => 'required|string',
        ]);

        $employer = $request->user();
        if ($employer->role->value !== 'super-administrator') {
            Session::flash('flash_alert', [
                'level' => 'error',
                'message' => "You are not allowed to do this action",
            ]);

            return redirect()->route('account-empl.dashboard');
        }

        $employer->self_recruitment = false;
        $employer->save();

        $employer->assits_recruitment()->create($request->all());

        return redirect()->route('account-empl.assist-recruitment');
    }

    /**
     * Edit employer account page
     *
     * @param Request $request
     * @return Blade view
     */
    public function editAccount(Request $request)
    {
        $employer = $request->user();

        SEOTools::setTitle('Edit Akun');
        SEOTools::setDescription('Edit Akun');

        $employer = new EmployerResource($employer);

        return view($this->theme().'employer.account.edit-account', compact('employer'));
    }

    /**
     * Update employer account
     *
     * @param Request $request
     * @return Json response
     */
    public function updateAccount(Request $request)
    {
        $employer = $request->user();

        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email|unique:employers,email,'.$employer->id,
            'phone' => 'required|string|unique:employers,phone,'.$employer->id,
            'gender' => 'required|string|in:male,female',
            'dob' => 'required|date_format:Y-m-d',
        ]);

        $employer->update($request->all());
        $employer = $employer->refresh();

        return $this->successResponse('Profil berhasil diperbarui', new EmployerResource($employer));
    }

    /**
     * Update password
     *
     * @param Request $request
     * @param PasswordRequest $request
     * @return void
     */
    public function updatePassword(PasswordRequest $request)
    {
        $employer = $request->user();

        $employer->password = Hash::make($request->new_password);
        $employer->save();

        return $this->successResponse('Password berhasil diperbarui', new EmployerResource($employer));
    }

    /**
     * Update employer avatar
     *
     * @param Request $request
     * @return Json response
     */
    public function updateAvatar(Request $request)
    {
        $this->validate($request, [
            'avatar' => 'required|image'
        ]);

        $employer = $request->user();
        $oldPhoto = $employer->photo;
        $path = PathHelper::employer_photo_path($employer->id);

        // Update photo
        $newPhoto = ImageHelper::processImg($request->avatar, $path, ['type' => 'fit', 'size' => 250], 'avatar');
        $employer->photo = $newPhoto;
        $employer->save();
        
        // Delete old resume
        PathHelper::deleteFile($path.$oldPhoto);

        return $this->successResponse('Avatar berhasil diperbarui', new EmployerResource($employer));
    }
}
