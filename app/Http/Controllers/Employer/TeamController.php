<?php

namespace App\Http\Controllers\Employer;

use App\Models\Employer;
use App\Enums\EmployerRole;
use Illuminate\Http\Request;
use App\Services\EmployerService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Artesaos\SEOTools\Facades\SEOTools;
use Illuminate\Support\Facades\Session;
use App\Http\Resources\EmployerResource;
use App\Http\Requests\InviteEmployerRequest;

class TeamController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $employer = $request->user();

            if (!$employer->self_recruitment) {
                return redirect()->route('account-empl.assist-recruitment');
            } else {
                return $next($request);
            }
        });
    }

    /**
     * Company team page index
     *
     * @param Request $request
     * @return Blade view
     */
    public function index(Request $request)
    {
        SEOTools::setTitle('Kelola Tim Perusahaan');
        SEOTools::setDescription('Kelola Tim Perusahaan');

        $employer = $request->user();
        $company = $employer->company;

        if (is_null($company)) {
            Session::flash('flash_alert', [
                'level' => 'error',
                'message' => "Please create a company, before invite team member!",
            ]);

            return redirect()->route('account-empl.company.index');
        }

        $team = $company->employers;
        $team = EmployerResource::collection($team);
        
        return view($this->theme().'employer.team.index', compact('team'));
    }

    /**
     * Invite employer to the team
     *
     * @param InviteEmployerRequest $request
     * @return Json response
     */
    public function invite(InviteEmployerRequest $request)
    {
        $employer = $request->user();

        try {
            $employerService = new EmployerService();
            $employerService->invite($employer, $request->name, $request->email, $request->role);
        } catch (\Throwable $th) {
            return $this->throwException($th);
        }

        $company = $employer->company;
        $team = $company->employers;
        $team = EmployerResource::collection($team);
        
        return $this->successResponse('Berhasil mengundang', $team);
    }

    /**
     * Update employer role
     *
     * @param Request $request
     * @param Employer $employer
     * @return Json response
     */
    public function roleUpdate(Request $request, Employer $employer)
    {
        if (Gate::denies('update-employer-role', $employer)) {
            return $this->errorResponse('Akses terlarang', 403);
        }

        $roles = implode(',', EmployerRole::toArray());

        $this->validate($request, [
            'role' => 'required|string|in:'.$roles
        ]);

        $employer->update([
            'role' => $request->role
        ]);

        $employer = new EmployerResource($employer);

        return $this->successResponse('Tim member role berhasil diperbarui', $employer);
    }
}
