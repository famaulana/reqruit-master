<?php

namespace App\Http\Controllers\Employer\Auth;

use App\Models\Employer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Services\EmployerService;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\VerifiesEmails;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = '/auth/employer/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }

    public function resend(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email'
        ]);

        $employer = Employer::where('email', $request->email)->first();
        if ($employer === null) {
            Session::flash('flash_alert', [
                'level' => 'warning',
                'message' => "Employer not found",
            ]);

            return redirect()->back();
        }

        $employerService = new EmployerService();
        $employerService->sendVerification($employer);

        return back()->with('resent', true);
    }

    public function verify(Request $request, $token = null)
    {
        $this->validate($request, [
            'email' => 'required|email'
        ]);

        if ($token === null) {
            abort(404);
        }

        $employer = Employer::where('email', $request->email)->first();

        if ($employer === null) {
            abort(404);
        }

        if ($employer->verify_token != '' && $employer->verify_token != $token) {
            abort(404);
        }

        if (!$employer->verified) {
            $employerService = new EmployerService();
            $employerService->verify($employer);
        }
        
        Session::flash('flash_alert', [
            'level' => 'success',
            'message' => "Email telah terverifikasi.",
        ]);

        return redirect()->route('auth.employer.login.form');
    }
}
