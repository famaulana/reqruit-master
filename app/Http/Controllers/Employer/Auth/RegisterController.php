<?php

namespace App\Http\Controllers\Employer\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Services\EmployerService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;
use Artesaos\SEOTools\Facades\SEOTools;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreEmployerRequest;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/employer/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:employer');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $storeEmployerValidator = new StoreEmployerRequest();
        return Validator::make($data, $storeEmployerValidator->rules());
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($employer = $this->create($request->all())));

        if ($response = $this->registered($request, $employer)) {
            return $response;
        }

        return $request->wantsJson()
                    ? new Response('', 201)
                    : redirect($this->redirectPath());
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $employerService = new EmployerService();
        $employer = $employerService->createEmployer($data);

        return $employer;
    }

    public function showRegistrationForm()
    {
        SEOTools::setTitle('Daftar');
        SEOTools::setDescription('Halaman pendaftaran');

        return view($this->theme().'auth.employer.register');
    }

    protected function guard()
    {
        return Auth::guard('employer');
    }
}
