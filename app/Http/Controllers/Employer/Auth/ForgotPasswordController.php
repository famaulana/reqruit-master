<?php

namespace App\Http\Controllers\Employer\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Artesaos\SEOTools\Facades\SEOTools;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Password;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    public function showLinkRequestForm()
    {
        SEOTools::setTitle('Lupa Password');
        SEOTools::setDescription('Halaman lupa password');

        return view($this->theme().'auth.employer.passwords.email');
    }

    public function broker()
    {
        return Password::broker('employers');
    }

    /**
     * Send a reset link to the given employer.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);

        // We will send the password reset link to this employer. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the employer. Finally, we'll send out a proper response.
        $response = $this->broker()->sendResetLink(
            $this->credentials($request)
        );

        if ($response == Password::RESET_LINK_SENT) {
            Session::flash('flash_alert', [
                'level' => 'success',
                'message' => "Reset password email terkirim. Silahkan cek email Kamu!",
            ]);

            return $this->sendResetLinkResponse($request, $response);
        } else {
            return $this->sendResetLinkFailedResponse($request, $response);
        }
    }
}
