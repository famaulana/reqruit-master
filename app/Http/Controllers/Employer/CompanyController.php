<?php

namespace App\Http\Controllers\Employer;

use Gate;
use App\Models\Company;
use App\Models\Country;
use App\Models\Industry;
use App\Helpers\PathHelper;
use App\Helpers\ImageHelper;
use Illuminate\Http\Request;
use App\Services\CompanyService;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Http\Resources\CompanyResource;
use Artesaos\SEOTools\Facades\SEOTools;
use App\Http\Resources\IndustryResource;
use App\Http\Requests\StoreCompanyRequest;
use App\Http\Requests\UpdateCompanyRequest;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{
    const BANNER_RESIZE = ['type' => 'resize', 'width' => 1400, 'height' => 525];
    const LOGO_RESIZE = ['type' => 'resize', 'width' => 300, 'height' => 300];

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $employer = $request->user();

            if (!$employer->self_recruitment) {
                return redirect()->route('account-empl.assist-recruitment');
            } else {
                return $next($request);
            }
        });
    }

    /**
     * Employer company page
     *
     * @param Request $request
     * @return Blade view
     */
    public function index(Request $request)
    {
        SEOTools::setTitle('Kelola Perusahaan');
        SEOTools::setDescription('Kelola Perusahaan');

        $company = $request->user()->company;
        $company = is_null($company) ? (object) array() : new CompanyResource($company);
        $industries = IndustryResource::collection(Industry::orderBy('name', 'ASC')->get());

        return view($this->theme().'employer.company.index', compact('company', 'industries'));
    }

    /**
     * Store new company
     *
     * @param StoreCompanyRequest $request
     * @return Json response
     */
    public function store(StoreCompanyRequest $request)
    {
        $employer = $request->user();

        if ($employer->companies()->exists()) {
            return $this->errorResponse('Anda sudah memiliki perusahaan', 403);
        }

        // Create slug
        $companyService = new CompanyService();
        $slug = $companyService->createSlug($request->name);

        // Generate images array
        $images = [];
        if ($request->has('image_file_name')) {
            foreach ($request->image_file_names as $key => $fileName) {
                $images[] = [
                    'file_name' => $fileName,
                    'description' => $request->image_descriptions[$key] == '' ? '' : $request->image_descriptions[$key]
                ];
            }
        }

        $company = $employer->companies()->create([
            'name' => $request->name,
            'slug' => $slug,
            'short_description' => $request->short_description,
            'country_id' => Country::where('country_code', 'ID')->first()->id,
            'address' => $request->address,
            'province_id' => $request->province_id,
            'city_id' => $request->city_id,
            'subdistrict_id' => $request->subdistrict_id,
            'pos_code' => $request->pos_code,
            'phone' => $request->phone,
            'email' => $request->email,
            'website_url' => $request->website_url,
            'instagram' => $request->instagram,
            'facebook' => $request->facebook,
            'linkedin' => $request->linkedin,
            'twitter' => $request->twitter,
            'description' => $request->description,
            'images' => $images,
        ]);

        $company->industries()->attach($request->industry_ids);

        $path = PathHelper::company_images_path($company->id);

        // Store banner and logo
        $banner = ImageHelper::processImg($request->banner, $path, self::BANNER_RESIZE, 'banner');
        $company->banner = $banner;
        $logo = ImageHelper::processImg($request->logo, $path, self::LOGO_RESIZE, 'logo');
        $company->logo = $logo;
        $company->save();

        // Move tmp company image file
        if ($request->has('image_file_name')) {
            $publicStoragePath = 'public/' . $path;
            $companyTmpPath = 'public/' . PathHelper::company_temp_path();
            foreach ($request->image_file_names as $fileName) {
                $oldPath = $companyTmpPath.$fileName;
                $newPath = $publicStoragePath.$fileName;
                Storage::move($oldPath, $newPath);
            }
        }

        return $this->successResponse('Perusahaan berhasil disimpan', new CompanyResource($company));
    }

    /**
     * Update company
     *
     * @param Request $request
     * @return void
     */
    public function update(UpdateCompanyRequest $request, Company $company)
    {
        if (Gate::denies('update-company', $company)) {
            return $this->errorResponse('Akses terlarang', 403);
        }

        // Generate slug
        $slug = $company->slug;
        if ($request->name != $company->name) {
            $companyService = new CompanyService();
            $slug = $companyService->createSlug($request->name);
        }

        /**
         * Generate new images array
         */
        $images = [];
        if (!is_null($request->image_file_names)) {
            foreach ($company->images as $image) {
                if (in_array($image['file_name'], $request->image_file_names)) {
                    $descKey = array_search($image['file_name'], $request->image_file_names);
                    $images[] = [
                        'file_name' => $image['file_name'],
                        'description' => $request->image_descriptions[$descKey] == '' ? '' : $request->image_descriptions[$descKey],
                    ];
                }
            }
        }

        $company->update([
            'name' => $request->name,
            'slug' => $slug,
            'short_description' => $request->short_description,
            'address' => $request->address,
            'province_id' => $request->province_id,
            'city_id' => $request->city_id,
            'subdistrict_id' => $request->subdistrict_id,
            'pos_code' => $request->pos_code,
            'phone' => $request->phone,
            'email' => $request->email,
            'website_url' => $request->website_url,
            'instagram' => $request->instagram,
            'facebook' => $request->facebook,
            'linkedin' => $request->linkedin,
            'twitter' => $request->twitter,
            'description' => $request->description,
            'images' => $images,
        ]);

        $company->industries()->sync($request->industry_ids);

        return $this->successResponse('Perusahaan berhasil diperbarui', new CompanyResource($company));
    }

    /**
     * Update company banner
     *
     * @param Request $request
     * @return Json response
     */
    public function updateBanner(Request $request, Company $company)
    {
        $this->validate($request, [
            'banner' => 'required|image|max:3000'
        ]);

        if (Gate::denies('update-company', $company)) {
            return $this->errorResponse('Akses terlarang', 403);
        }

        $oldBanner = $company->banner;
        $path = PathHelper::company_images_path($company->id);

        // Update banner
        $newBanner = ImageHelper::processImg($request->banner, $path, self::BANNER_RESIZE, 'banner');
        $company->banner = $newBanner;
        $company->save();
        
        // Delete old banner
        PathHelper::deleteFile($path.$oldBanner);

        return $this->successResponse('Banner perusahaan berhasil diperbarui', new CompanyResource($company));
    }

    /**
     * Update company logo
     *
     * @param Request $request
     * @return Json response
     */
    public function updateLogo(Request $request, Company $company)
    {
        $this->validate($request, [
            'logo' => 'required|image|max:3000'
        ]);

        if (Gate::denies('update-company', $company)) {
            return $this->errorResponse('Akses terlarang', 403);
        }

        $oldLogo = $company->logo;
        $path = PathHelper::company_images_path($company->id);

        // Update logo
        $newLogo = ImageHelper::processImg($request->logo, $path, self::LOGO_RESIZE, 'logo');
        $company->logo = $newLogo;
        $company->save();
        
        // Delete old logo
        PathHelper::deleteFile($path.$oldLogo);

        return $this->successResponse('Logo perusahaan berhasil diperbarui', new CompanyResource($company));
    }

    /**
     * Add new image for new company creation
     *
     * @param Request $request
     * @return Json response
     */
    public function addImageNewCompany(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|image|max:1500',
            'prev_file_name' => 'nullable|string',
        ]);

        // Upload new image
        $employer = $request->user();
        $path = PathHelper::company_temp_path();
        $newImage = ImageHelper::processImg($request->image, $path, [], "company-$employer->id-image");
    
        // Delete prev uploaded image
        if ($request->has('prev_file_name')) {
            PathHelper::deleteFile($path.$request->prev_file_name);
        }

        return $this->successResponse(
            'Foto berhasil diunggah',
            [
                'image_url' => ImageHelper::getImageUrl($path, $newImage),
                'file_name' => $newImage
            ]
        );
    }

    /**
     * Update company image
     *
     * @param Request $request
     * @return Json response
     */
    public function updateImage(Request $request, Company $company)
    {
        $this->validate($request, [
            'image' => 'required|image|max:1500',
            'index' => 'required|numeric'
        ]);

        if (Gate::denies('update-company', $company)) {
            return $this->errorResponse('Akses terlarang', 403);
        }

        $index = $request->index;
        $path = PathHelper::company_images_path($company->id);

        if (isset($company->images[$index])) {
            $oldImage = $company->images[$index]['file_name'];
        }
        
        // Update image
        $newImage = ImageHelper::processImg($request->image, $path, [], 'image');
        $images = [];
        if (is_null($company->images)) {
            $images[] = [
                'file_name' => $newImage,
                'description' => ''
            ];
        } else {
            if (isset($company->images[$index])) {
                foreach ($company->images as $key => $image) {
                    if ($key == $index) {
                        $images[] = [
                            'file_name' => $newImage,
                            'description' => $image['description']
                        ];
                    } else {
                        $images[] = [
                            'file_name' => $image['file_name'],
                            'description' => $image['description']
                        ];
                    }
                }
            } else {
                $existingImages = $company->images;
                $images[] = [
                    'file_name' => $newImage,
                    'description' => ''
                ];

                $images = array_merge($existingImages, $images);
            }
        }

        $company->images = $images;
        $company->save();
        
        // Delete old logo
        if (isset($oldImage)) {
            PathHelper::deleteFile($path.$oldImage);
        }
        
        return $this->successResponse(
            'Foto berhasil diperbarui',
            [
                'image_url' => $company->company_images[$index]['src'],
                'file_name' => $newImage
            ]
        );
    }

    /**
     * Remove company image
     *
     * @param Request $request
     * @param Company $company
     * @param Int $index
     * @return Json response
     */
    public function removeImage(Request $request, Company $company, Int $index)
    {
        $path = PathHelper::company_images_path($company->id);
        $fileName = $company->images[$index]['file_name'];
        PathHelper::deleteFile($path.$fileName);

        $images = [];
        foreach ($company->images as $key => $image) {
            if ($key != $index) {
                $images[] = [
                    'file_name' => $image['file_name'],
                    'description' => $image['description'],
                ];
            }
        }
        
        $company->images = $images;
        $company->save();

        return $this->successResponse('Foto berhasil dihapus', []);
    }
}
