<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Models\EnglishQuestion;
use App\Http\Resources\EnglishQuestionResource;
use App\Repositories\AppConfigRepository;
use App\Repositories\EnglishQuestionRepository;
use App\Services\EnglishAssessmentService;
use DB;

class EnglishAssessmentController extends AuthenticatedController
{
    /**
     * Get the english questions for assessment
     *
     * @return Json response
     */
    public function index()
    {
        $config = new AppConfigRepository();

        if ($this->user->english_assessment()->exists() && $config->getConfig('english_test_can_retest') == 'no') {
            return $this->errorResponse('You already completed the english assessment', 403);
        }

        $questions = EnglishQuestion::inRandomOrder()->skip(0)->limit($config->getConfig('english_test_q_count'))->get();
        $vocabQ = EnglishQuestionRepository::vocabularyQuestion();
        for ($i=1; $i < count($vocabQ['choices']) + 1; $i++) {
            $vocabQ = str_replace('[_'.$i.'_]', '<span class="blank-underline" id="answer-line-'.$i.'" data-number="'.$i.'" onclick="onClick(event);" ondragover="onDragOver(event);" ondrop="onDrop(event);"></span>', $vocabQ);
        }
        shuffle($vocabQ['choices']);

        $data = [
            'meta_title' => 'English Assessment',
            'meta_description' => 'English assessment page',
            'questions' => [
                'session_1' => EnglishQuestionResource::collection($questions),
                'session_2' => $vocabQ,
                'session_3' => EnglishQuestionRepository::listeningQuestions()
            ],
        ];

        return $this->successResponse('English assessment', $data);
    }

    /**
     * Post english assessment answer
     *
     * @param Request $request
     * @return Json response
     */
    public function postAnswer(Request $request)
    {
        $config = new AppConfigRepository();

        // if ($this->user->english_assessment()->exists() && $config->getConfig('english_test_can_retest') == 'no') {
        //     return $this->errorResponse('You already completed the english assessment', 403);
        // }

        $this->validate($request, [
            'answers' => 'required|array',
            'answers.session_1' => 'required|array',
            'answers.session_1.*.question_id' => 'required|numeric',
            'answers.session_1.*.question_text' => 'required|string',
            'answers.session_1.*.answer_id' => 'nullable|string',
            'answers.session_1.*.answer_text' => 'nullable|string',
            'answers.session_2' => 'required|array',
            'answers.session_2.q_group_no' => 'required|numeric',
            'answers.session_2.answers' => 'nullable|array',
            'answers.session_2.answers.*' => 'nullable|string',
            'answers.session_3' => 'required|array',
            'answers.session_3.*.q_group_no' => 'required|numeric',
            'answers.session_3.*.answers' => 'nullable|array',
            'answers.session_3.*.answers.*.q_no' => 'nullable|numeric',
            'answers.session_3.*.answers.*.answer' => 'nullable|string',
        ]);

        $answers = $request->answers;
        $process = EnglishAssessmentService::processAssessment($this->user, $answers);

        return $this->successResponse('English assessment complete', ['score' => $process]);
    }
}
