<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthenticatedController extends Controller
{
    
    /**
     * Current logged in user
     *
     * @var App\Models\User
     */
    public $user;

    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->user = Auth::guard('user')->user();
    }
}
