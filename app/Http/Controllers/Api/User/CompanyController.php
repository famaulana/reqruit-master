<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Resources\CompanyReviewResource;
use App\Models\Company;
use Illuminate\Http\Request;

class CompanyController extends AuthenticatedController
{
    /**
     * Post company review
     *
     * @param Request $request
     * @return Json response
     */
    public function postReview(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string',
            'rating' => 'required|numeric|min:0|max:5',
            'description' => 'required|string',
            'anonymous' => 'required|boolean',
        ]);

        $this->user->company_reviews()->create($request->all());

        return $this->successResponse('Review sent for moderation', []);
    }

    /**
     * Get my review for current company
     *
     * @param Company $company
     * @return Json response
     */
    public function myReview(Company $company)
    {
        $review = $company->company_reviews->where('user_id', $this->user->id)->first();
        if (!is_null($review)) {
            $review = new CompanyReviewResource($review);
        }

        return $this->successResponse('My review', $review);
    }
}
