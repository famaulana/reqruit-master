<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Repositories\PersonalAnalysRepository;
use App\Repositories\UserRepository;
use App\Services\PersonalAnalysService;

class PersonalityTestController extends AuthenticatedController
{
    /**
     * Get the personality test statements
     *
     * @return Json response
     */
    public function index()
    {
        if ($this->user->personality_test()->exists() && !PersonalAnalysService::canRedo($this->user)) {
            return $this->errorResponse('You have completed the personality test!', 403);
        }

        $data = [
            'meta_title' => 'Personality Test',
            'meta_description' => 'Personality test page.',
            // 'statements' => array_slice(PersonalAnalysRepository::statements(), 0, 4)
            'statements' => PersonalAnalysRepository::statements()
        ];

        return $this->successResponse('Personality Test', $data);
    }

    /**
     * Post personality test statements
     *
     * @param Request $request
     * @return Json response
     */
    public function postStatement(Request $request)
    {
        if ($this->user->personality_test()->exists() && !PersonalAnalysService::canRedo($this->user)) {
            return $this->errorResponse('You have completed the personality test!', 403);
        }

        $this->validate($request, [
            'statements' => 'required|array',
            'statements.*.most' => 'required|string',
            'statements.*.least' => 'required|string',
        ]);

        $result = PersonalAnalysService::processStatement($request->statements);

        $this->user->personality_test()->create([
            'statements' => $result['raw'],
            'result' => [
                'most' => $result['most'],
                'least' => $result['least'],
            ]
        ]);

        if (is_null($this->user->personality_test)) {
            $this->user->personality_test()->create([
                'statements' => $result['raw'],
                'result' => [
                    'most' => $result['most'],
                    'least' => $result['least'],
                ]
            ]);
        } else {
            $existingData = $this->user->personality_test;
            $existingData->statements = $result['raw'];
            $existingData->result = [
                'most' => $result['most'],
                'least' => $result['least'],
            ];
            $existingData->save();
        }

        $userRepository = new UserRepository();
        $userRepository->updateProfileCompletionCache($this->user);

        return $this->successResponse('Personality test complete', $result);
    }
}
