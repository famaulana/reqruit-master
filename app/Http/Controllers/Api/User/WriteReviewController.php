<?php

namespace App\Http\Controllers\Api\User;

use App\Enums\WorkjobType;
use App\Helpers\PathHelper;
use App\Helpers\ImageHelper;
use Illuminate\Http\Request;
use App\Models\CompanyReview;
use App\Enums\WorkjobExperience;
use App\Enums\WorkjobSalaryDuration;
use App\Repositories\CompanyReviewRepository;
use App\Http\Controllers\Api\User\AuthenticatedController;

class WriteReviewController extends AuthenticatedController
{
    /**
     * Post company review
     *
     * @param Request $request
     * @return Json response
     */
    public function postCompanyReview(Request $request)
    {
        $this->validate($request, [
            'type' => 'required|string|in:company_review',
            'job_status' => 'required|string|in:current,former',
            'company_id' => 'required|numeric',
            'rating' => 'required|numeric',
            'employment_status' => 'required|string|in:'.implode(',', WorkjobType::toArray()),
            'job_title' => 'required|string',
            'review_headline' => 'required|string',
            'pros' => 'required|string',
            'cons' => 'required|string',
            'advice' => 'required|string',
        ]);

        if (CompanyReview::where('type', 'company_review')->where('user_id', $this->user->id)->where('company_id', $request->company_id)->exists()) {
            return $this->errorResponse('You already have company review for the selected company!', 403);
        }

        $this->user->company_reviews()->create($request->all());

        return $this->successResponse('Review sent for moderation', []);
    }

    /**
     * Post salary review
     *
     * @param Request $request
     * @return Json response
     */
    public function postSalaryReview(Request $request)
    {
        $this->validate($request, [
            'type' => 'required|string|in:salary_review',
            'job_status' => 'required|string|in:current,former',
            'company_id' => 'required|numeric',
            'employment_status' => 'required|string|in:'.implode(',', WorkjobType::toArray()),
            'job_title' => 'required|string',
            'currency' => 'required|string',
            'base_salary' => 'required|numeric|min:0',
            'salary_duration' => 'required|string|in:'.implode(',', WorkjobSalaryDuration::toArray()),
            'get_bonus' => 'required|boolean',
            'work_experience' => 'required|string|in:'.implode(',', WorkjobExperience::toArray()),
            'country_id' => 'required|numeric',
            'location' => 'required|string',
            'gender' => 'required|string|in:male,female',
        ]);

        if (CompanyReview::where('type', 'salary_review')->where('user_id', $this->user->id)->where('company_id', $request->company_id)->exists()) {
            return $this->errorResponse('You already have salary review for the selected company!', 403);
        }

        $this->user->company_reviews()->create($request->all());

        return $this->successResponse('Review sent for moderation', []);
    }

    /**
     * Post interview review
     *
     * @param Request $request
     * @return Json response
     */
    public function postInterviewReview(Request $request)
    {
        $this->validate($request, [
            'type' => 'required|string|in:interview_review',
            'company_id' => 'required|numeric',
            'overall_exp' => 'required|string',
            'job_title' => 'required|string',
            'interview_process' => 'required|string',
            'interview_difficulty' => 'required|string',
            'interview_time' => 'required|date_format:Y-m-d',
            'get_offer' => 'required|string|in:no,yes-declined,yes-accepted',
            'qas' => 'required|array',
            'qas.*.question' => 'required|string',
            'qas.*.answer' => 'required|string',
            'how_to_get_interview' => 'required|string|in:online,university,referral,in-person,recruiter,staffing-agency,other',
            'how_long_number' => 'required|numeric',
            'how_long_unit' => 'required|string|in:days,weeks,months',
            'location' => 'required|string',
            'required_type' => 'required|array',
            'required_type.*' => 'required|string:in:phone-screen,1-on-1,group-panel',
            'testing' => 'required|array',
            'testing.*' => 'required|string|in:skills-review,personality-quiz,drug-test,iq-test',
            'other' => 'required|array',
            'other.*' => 'required|string|in:background-check,presentation,other',
            'how_helpful' => 'required|string|in:very-helpful,helpful,not-helpful,not-use',
        ]);

        if (CompanyReview::where('type', 'interview_review')->where('user_id', $this->user->id)->where('company_id', $request->company_id)->exists()) {
            return $this->errorResponse('You already have interview review for the selected company!', 403);
        }

        $companyReview = $this->user->company_reviews()->create([
            'type' => $request->type,
            'company_id' => $request->company_id,
            'overall_exp' => $request->overall_exp,
            'job_title' => $request->job_title,
            'interview_process' => $request->interview_process,
            'interview_difficulty' => $request->interview_difficulty,
            'get_offer' => $request->get_offer,
        ]);

        $companyReview->review_interview_questions()->createMany($request->qas);

        $companyReview->review_interview_how()->create([
            'how_to_get_interview' => $request->how_to_get_interview,
            'how_long_number' => $request->how_long_number,
            'how_long_unit' => $request->how_long_unit,
            'interview_time' => $request->interview_time,
            'location' => $request->location,
            'type' => $request->required_type,
            'testing' => $request->testing,
            'other' => $request->other,
            'how_helpful' => $request->how_helpful,
        ]);

        return $this->successResponse('Review sent for moderation', []);
    }

    /**
     * Post benefits review
     *
     * @param Request $request
     * @return Json response
     */
    public function postBenefitsReview(Request $request)
    {
        $validations1 = [
            'type' => 'required|string|in:benefits_review',
            'job_status' => 'required|string|in:current,former',
            'company_id' => 'required|numeric',
            'rating' => 'required|numeric',
            'employment_status' => 'required|string|in:'.implode(',', WorkjobType::toArray()),
            'job_title' => 'required|string',
            'country_id' => 'required|numeric',
            'location' => 'required|string',
            'benefits_review' => 'required|string',
        ];

        $validations2 = [];
        $benefitsReviewNames = CompanyReviewRepository::benefitsReviewNames();
        foreach ($benefitsReviewNames as $code => $name) {
            $validations2[] = [
                $code => 'nullable|string|in:yes,no,unsure'
            ];
        }

        $validations = array_merge($validations1, $validations2);

        $this->validate($request, $validations);

        if (CompanyReview::where('type', 'benefits_review')->where('user_id', $this->user->id)->where('company_id', $request->company_id)->exists()) {
            return $this->errorResponse('You already have benefits review for the selected company!', 403);
        }

        $companyReview = $this->user->company_reviews()->create($request->all());

        $companyReview->review_benefit()->create($request->all());

        return $this->successResponse('Review sent for moderation', []);
    }

    /**
     * Add new photo workplace photo
     *
     * @param Request $request
     * @return Json response
     */
    public function addPhoto(Request $request)
    {
        $this->validate($request, [
            'photo' => 'required|image|mimes:jpg,png,jpeg|max:3000',
            'prev_file_name' => 'nullable|string',
        ]);

        // Upload new photo
        $userId = $this->user->id;
        $path = PathHelper::user_workplace_photo_path($userId);
        $newImage = ImageHelper::processImg($request->photo, $path, ['type' => 'resize', 'width' => 1500, 'height' => 1500], "workplace-$userId-photo");
    
        // Delete prev uploaded photo
        if ($request->has('prev_file_name')) {
            PathHelper::deleteFile($path.$request->prev_file_name);
        }

        return $this->successResponse(
            'Photo uploaded',
            [
                'image_url' => ImageHelper::getImageUrl($path, $newImage),
                'file_name' => $newImage
            ]
        );
    }

    /**
     * Remove workplace photo
     *
     * @param Request $request
     * @param String $fileName
     * @return Json response
     */
    public function removePhoto(Request $request, String $fileName)
    {
        $userId = $this->user->id;
        $path = PathHelper::user_workplace_photo_path($userId);
        PathHelper::deleteFile($path.$fileName);

        return $this->successResponse('Photo removed', []);
    }

    /**
     * post workplace photos
     *
     * @param Request $request
     * @return Json response
     */
    public function postWorkplacePhotos(Request $request)
    {
        $this->validate($request, [
            'type' => 'required|string|in:workplace_photos',
            'company_id' => 'required|numeric',
            'photos' => 'required|array',
            'photos.*' => 'required|string',
        ]);

        if (CompanyReview::where('type', 'workplace_photos')->where('user_id', $this->user->id)->where('company_id', $request->company_id)->exists()) {
            return $this->errorResponse('You already submitted workplace photos for the selected company!', 403);
        }

        $this->user->company_reviews()->create($request->all());

        return $this->successResponse('Workplace photos sent for moderation', []);
    }
}
