<?php

namespace App\Http\Controllers\Api\User;

use App\Enums\InterviewStatusEnum;
use Illuminate\Http\Request;
use App\Models\JobApplication;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\JobApplicationResource;
use App\Notifications\InterviewUpdate;
use App\Notifications\UserDeclinedInterview;

class JobApplicationController extends AuthenticatedController
{
    /**
    * Job applications index controller
    *
    * @param Request $request
    * @return Json response
    */
    public function index(Request $request)
    {
        $user = $this->user;

        $data = [
            'meta_title' => 'My job applications',
            'meta_description' => 'My job applications page.',
            'pending' => $user->job_applications()->where('status', 'pending')->count(),
            'shortlisted' => $user->job_applications()->where('status', 'shortlisted')->count(),
            'interview' => $user->job_applications()->where('status', 'interview')->count(),
            'offered' => $user->job_applications()->where('status', 'offered')->count(),
            'hired' => $user->job_applications()->where('status', 'hired')->count(),
            'unsuitable' => $user->job_applications()->where('status', 'unsuitable')->count(),
        ];

        return $this->successResponse('Job applications', $data);
    }

    /**
     * Get job applications by status
     *
     * @param Request $request
     * @param String $status
     * @return Json response
     */
    public function jobApplicationsByStatus(Request $request, String $status)
    {
        $user = $this->user;
        $jobApplications = $user->job_applications()->where('status', $status)->simplePaginate(50);

        $data = [
            'job_applications' => JobApplicationResource::collection($jobApplications),
            'per_page' => $jobApplications->perPage(),
            'current_page' => $jobApplications->currentPage(),
            'next_page_url' => is_null($jobApplications->nextPageUrl()) ? '' : $jobApplications->nextPageUrl(),
            'prev_page_url' => is_null($jobApplications->previousPageUrl()) ? '' : $jobApplications->previousPageUrl()
        ];

        return $this->successResponse(ucfirst($status).' job applications', $data);
    }

    /**
     * Update interview action status
     *
     * @param Request $request
     * @return Json response
     */
    public function interviewAction(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|numeric',
            'action' => 'required|string|in:'.implode(',', InterviewStatusEnum::toArray())
        ]);
        
        $jobApplication = JobApplication::find($request->id);

        if (is_null($jobApplication)) {
            return $this->errorResponse('Job application not found', 404);
        }

        if ($jobApplication->user->id !== $this->user->id) {
            return $this->errorResponse('Forbidden action', 403);
        }

        if($request->action == 'time-not-suit'){
            $jobApplication->interview_status = $request->action;
            $jobApplication->response_msg = $request->response_msg;
        }else{
            $jobApplication->interview_status = $request->action;
        }

        $jobApplication->save();

        $company = $jobApplication->workjob->company;
        $company->notify(new InterviewUpdate($jobApplication, $request->action));

        return $this->successResponse('Interview status updated', new JobApplicationResource($jobApplication));
    }
}
