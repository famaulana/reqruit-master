<?php

namespace App\Http\Controllers\Api\User;

use App\Models\User;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Models\OauthAccessToken;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

class AuthController extends Controller
{
    public function broker()
    {
        return Password::broker('users');
    }

    /**
     * Get available social login
     *
     * @return Json response
     */
    public function availableSocialLogin()
    {
        $socialLogin = [
            'google' => config('services.google.client_id') == '' ? false : true,
            'facebook' => config('services.facebook.client_id') == '' ? false : true,
            'linkedin' => config('services.linkedin.client_id') == '' ? false : true,
        ];

        return $this->successResponse('Available social login', $socialLogin);
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:8|confirmed',
        ]);

        $userService = new UserService();
        $user = $userService->createUser($request->all());

        return response()->json([
            'status' => 'success',
            'message' => 'Registration success',
            'token' => $userService->generateToken($user),
            'data' => new UserResource($user)
        ]);
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        // Find existing user
        $user = User::where('email', $request->email)->first();
        if ($user === null) {
            return $this->errorResponse(trans('auth.account_not_found'), 400);
        }

        if (Hash::check($request->password, $user->password)) {
            $userService = new UserService();

            return response()->json([
                'status' => 'success',
                'message' => trans('auth.login_success'),
                'token' => $userService->generateToken($user),
            ]);
        } else {
            return $this->errorResponse(trans('auth.wrong_password'), 400);
        }
    }

    public function logout(Request $request)
    {
        $token = OauthAccessToken::where('user_id', Auth::guard('user')->user()->id)
            ->where('name', 'User')
            ->first();

        if ($token === null) {
            return $this->errorResponse('Unauthorized', 401);
        }

        $token->delete();
        
        return $this->successResponse('Logout', []);
    }

    public function userData(Request $request)
    {
        if (!Auth::guard('user')->check()) {
            return $this->errorResponse('Forbidden', 403);
        }

        $user = Auth::guard('user')->user();
        return $this->successResponse('User data', new UserResource($user));
    }

    public function sendResetPasswordLink(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email'
        ]);

        $user = User::where('email', $request->email)->first();
        if ($user === null) {
            return $this->errorResponse(trans('auth.account_not_found'), 400);
        }

        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );

        if ($response == Password::RESET_LINK_SENT) {
            return $this->successResponse(trans('auth.reset_password.link_send_success'), []);
        } else {
            return $this->errorResponse(trans('auth.reset_password.link_send_error'), 400);
        }
    }

    public function updatePassword(Request $request)
    {
        $this->validate($request, [
            'token' => 'required|string',
            'email' => 'required|email',
            'password' => 'required|string|min:8',
        ]);

        $resetTokenQuery = DB::table('password_resets_user')->where('email', $request->email);
        $resetToken = $resetTokenQuery->first();

        if ($resetToken === null) {
            return $this->errorResponse(trans('auth.reset_password.token_mismatch'), 400);
        }

        if (!Hash::check($request->token, $resetToken->token)) {
            return $this->errorResponse(trans('auth.reset_password.token_mismatch'), 400);
        }

        $user = User::where('email', $request->email)->first();
        if ($user === null) {
            return $this->errorResponse(trans('auth.account_not_found'), 400);
        }

        $user->password = Hash::make($request->password);
        $user->save();
        $resetTokenQuery->delete();

        return $this->successResponse(trans('auth.reset_password.update_success'), []);
    }

    public function verifyEmail(Request $request)
    {
        $this->validate($request, [
            'token' => 'required|string',
            'email' => 'required|email',
        ]);

        $user = User::where('verification_token', $request->token)
            ->where('email', $request->email)->first();

        if ($user === null) {
            return $this->errorResponse('Token and email mismatch', 404);
        }

        $userService = new UserService();
        $user = $userService->verifyEmail($user);

        return $this->jsonUnescaped([
            'status' => 'success',
            'message' => 'Email verification success',
            'user' => new UserResource($user)
        ]);
    }
}
