<?php

namespace App\Http\Controllers\Api\User;

use Auth;
use App\Models\Bookmark;
use Illuminate\Http\Request;
use App\Http\Resources\BookmarkResource;

class BookmarkController extends AuthenticatedController
{
    /**
     * Jobs per page
     *
     * @var Integer
     */
    public $jobsPerPage;

    public function __construct()
    {
        parent::__construct();
        $this->jobsPerPage = 3;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bookmarks = $this->user->bookmarks()->with(['workjob'])->simplePaginate($this->jobsPerPage);

        $data = [
            'meta_title' => 'Bookmark',
            'meta_description' => 'Bookmark page',
            'bookmarks' => BookmarkResource::collection($bookmarks),
            'current_page' => $bookmarks->currentPage(),
            'next_page_url' => $bookmarks->nextPageUrl(),
        ];

        return $this->successResponse('User\'s bookmarks', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'job_id' => 'required|numeric'
        ], [
            'job_id.required' => 'Job is required!'
        ]);

        $bookmark = $this->user->bookmarks()->where('workjob_id', $request->job_id)->first();

        if (is_null($bookmark)) {
            $bookmark = $this->user->bookmarks()->create([
                'workjob_id' => $request->job_id
            ]);
        }

        return $this->successResponse('Added to bookmark', ['id' => $bookmark->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bookmark = Bookmark::find($id);

        if (is_null($bookmark)) {
            return $this->errorResponse('Bookmark not found', 404);
        }

        $bookmark->delete();

        return $this->successResponse('Bookmark deleted', []);
    }
}
