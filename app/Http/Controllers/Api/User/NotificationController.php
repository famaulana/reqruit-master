<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Resources\NotificationResource;
use Illuminate\Http\Request;

class NotificationController extends AuthenticatedController
{
    /**
     * Get user notifications
     *
     * @return Json response
     */
    public function index()
    {
        $notifications = $this->user->notifications()->simplePaginate(5);

        $data = [
            'meta_title' => 'Notifications',
            'meta_description' => 'Notifications page',
            'notifications' => NotificationResource::collection($notifications),
            'has_unread' => $this->user->unreadNotifications()->exists(),
            'per_page' => $notifications->perPage(),
            'current_page' => $notifications->currentPage(),
            'next_page_url' => is_null($notifications->nextPageUrl()) ? '' : $notifications->nextPageUrl(),
            'prev_page_url' => is_null($notifications->previousPageUrl()) ? '' : $notifications->previousPageUrl()
        ];

        return $this->successResponse('User\'s notifications', $data);
    }

    /**
     * Mark notification as read
     *
     * @param String $id
     * @return Json response
     */
    public function read(String $id)
    {
        $notification = $this->user->notifications()->where('id', $id)->first();
        if (is_null($notification)) {
            return $this->errorResponse('Notification not found!', 404);
        }

        if ($notification->notifiable_id !== $this->user->id) {
            return $this->errorResponse('Forbidden action', 403);
        }

        $notification->markAsRead();

        return $this->successResponse('Notification has been marked as read', ['has_unread' => $this->user->unreadNotifications()->exists()]);
    }

    /**
     * Mark all unread notifications as read
     *
     * @return Json response
     */
    public function markAllRead()
    {
        $this->user->unreadNotifications->markAsRead();

        return $this->successResponse('All notifications marked as read', []);
    }
}
