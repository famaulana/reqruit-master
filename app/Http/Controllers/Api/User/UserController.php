<?php

namespace App\Http\Controllers\Api\User;

use Carbon\Carbon;
use App\Models\Award;
use App\Models\Education;
use App\Enums\WorkjobType;
use App\Helpers\FileHelper;
use App\Helpers\PathHelper;
use App\Models\JobInterest;
use App\Enums\UserJobStatus;
use App\Helpers\ImageHelper;
use Illuminate\Http\Request;
use App\Models\WorkExperience;
use App\Enums\ResidenceStatusEnum;
use App\Enums\UserRelationshipEnum;
use App\Enums\WhenCanStartWorkEnum;
use App\Repositories\CityRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;
use App\Repositories\SkillRepository;
use App\Http\Requests\PasswordRequest;
use App\Models\OrganizationExperience;
use App\Repositories\CountryRepository;
use App\Http\Requests\StoreAwardRequest;
use App\Http\Resources\UserSimpleResource;
use App\Repositories\JobfunctionRepository;
use App\Http\Requests\StoreEducationRequest;
use App\Http\Requests\StoreExperienceRequest;
use App\Repositories\EducationFieldRepository;
use App\Http\Resources\PersonalityTestResource;
use App\Repositories\EducationDegreeRepository;
use App\Http\Requests\StoreOrganizationExperienceRequest;

class UserController extends AuthenticatedController
{
    /**
    * Update user password
    *
    * @param PasswordRequest $request
    * @return Json response
    */
    public function updatePassword(PasswordRequest $request)
    {
        $user = $this->user;

        $user->password = Hash::make($request->new_password);
        $user->save();

        return $this->successResponse('Password updated', new UserSimpleResource($user));
    }

    /**
     * Account profile
     *
     * @return Json response
     */
    public function profile()
    {
        $user = $this->user;

        $data = [
            'meta_title' => 'Edit Profile',
            'meta_description' => 'Edit profile page.',
            'user' => UserRepository::UserResourceCache($user),
            'job_statuses' => UserJobStatus::toSelectArray(),
            'relationships' => UserRelationshipEnum::toSelectArray(),
            'when_can_start_options' => WhenCanStartWorkEnum::toSelectArray(),
            'job_types' => WorkjobType::toSelectArray(),
            'residence_status_options' => ResidenceStatusEnum::toVueSelectArray(),

            'job_functions' => JobfunctionRepository::formDataCache(),
            'education_degrees' => EducationDegreeRepository::formDataCache(),
            'education_fields' => EducationFieldRepository::formDataCache(),
            'skills' => SkillRepository::formDataCache(),
            'all_cities' => CityRepository::formDataCache(),
            'all_countries' => CountryRepository::formDataCache(),
            'personality_test' => $user->personality_test()->exists() ? new PersonalityTestResource($user->personality_test) : null,
        ];
        
        return $this->successResponse('User profile', $data);
    }

    /**
     * Account profile completion
     *
     * @return Json Response
     */
    public function profileCompletion()
    {
        $userRepository = new UserRepository();

        return $this->successResponse('User completion', $userRepository->profileCompletion($this->user));
    }

    /**
    * Update user avatar
    *
    * @param Request $request
    * @return Json response
    */
    public function updateAvatar(Request $request)
    {
        $this->validate($request, [
            'avatar' => 'required|image'
        ]);

        $user = $this->user;
        $oldPhoto = $user->photo;
        $path = PathHelper::user_photo_path($user->id);

        // Update photo
        $newPhoto = ImageHelper::processImg($request->avatar, $path, ['type' => 'fit', 'size' => 250], 'avatar');
        $user->photo = $newPhoto;
        $user->save();
        
        // Delete old resume
        PathHelper::deleteFile($path.$oldPhoto);

        UserRepository::deleteProfileCompletionCache($user);

        return $this->successResponse('User avatar updated', UserRepository::updateUserResourceCache($user));
    }

    /**
     * Update user basic info
     *
     * @param Request $request
     * @return Json response
     */
    public function updateBasicInfo(Request $request)
    {
        $user = $this->user;

        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'gender' => 'required|string|in:male,female',
            'phone' => 'required|string|unique:users,phone,'.$user->id,
            'dob' => 'required|date_format:Y-m-d',
            'address' => 'required|string',
            'country_id' => 'required|numeric',
            'province_id' => 'nullable|numeric',
            'province_name' => 'nullable|string',
            'city_id' => 'nullable|numeric',
            'city_name' => 'nullable|string',
            'subdistrict_id' => 'nullable|numeric',
            'pos_code' => 'required|string',
            'job_status' => 'required|string|in:'.implode(',', UserJobStatus::toArray()),
            'hobby' => 'required|string',
            'relationship' => 'required|string|in:'.implode(',', UserRelationshipEnum::toArray()),
            'when_can_start' => 'required|string|in:'.implode(',', WhenCanStartWorkEnum::toArray()),
            'can_start_date' => 'nullable|date_format:Y-m-d',
        ], [
            'gender.required' => 'Gender is required!',
            'dob.required' => 'Date of birth is required!',
            'pos_code.required' => 'Pos code is required!',
            'when_can_start.required' => 'When can start working is required!'
        ]);

        $user->update($request->all());
        UserRepository::deleteProfileCompletionCache($user);

        return $this->successResponse('User basic info updated', UserRepository::updateUserResourceCache($user));
    }

    /**
     * Update user about
     *
     * @param Request $request
     * @return Json response
     */
    public function updateAbout(Request $request)
    {
        $this->validate($request, [
            'about' => 'required|string',
        ], [
            'about.required' => 'About me is required!'
        ]);

        $user = $this->user;
        $user->update($request->all());
        UserRepository::deleteProfileCompletionCache($user);

        return $this->successResponse('About info updated', UserRepository::updateUserResourceCache($user));
    }

    /**
     * Add new user job experience
     *
     * @param StoreExperienceRequest $request
     * @return Json response
     */
    public function addExperience(StoreExperienceRequest $request)
    {
        $startTime = Carbon::parse($request->start_year.'-'.$request->start_month.'-1')->format('Y-m-d');
        if (is_null($request->end_year) || is_null($request->end_month)) {
            $endTime = null;
        } else {
            $endTime = Carbon::parse($request->end_year.'-'.$request->end_month.'-1')->format('Y-m-d');
        }

        $user = $this->user;
        $user->work_experiences()->create([
            'company' => $request->company,
            'position' => $request->position,
            'start_time' => $startTime,
            'end_time' => $endTime,
            'still_working' => $request->still_working,
            'more_info' => $request->more_info,
        ]);

        UserRepository::deleteProfileCompletionCache($user);
        
        return $this->successResponse('User experience added', UserRepository::updateUserResourceCache($user));
    }

    /**
     * Update user job experience
     *
     * @param StoreExperienceRequest $request
     * @param Int $id
     * @return Json response
     */
    public function updateExperience(StoreExperienceRequest $request, Int $id)
    {
        $experience = WorkExperience::find($id);
        $user = $this->user;

        if ($user->id != $experience->user_id) {
            return $this->errorResponse('Forbidden', 403);
        }

        $startTime = Carbon::parse($request->start_year.'-'.$request->start_month.'-1')->format('Y-m-d');
        if (is_null($request->end_year) || is_null($request->end_month)) {
            $endTime = null;
        } else {
            $endTime = Carbon::parse($request->end_year.'-'.$request->end_month.'-1')->format('Y-m-d');
        }

        $experience->update([
            'company' => $request->company,
            'position' => $request->position,
            'start_time' => $startTime,
            'end_time' => $endTime,
            'still_working' => $request->still_working,
            'more_info' => $request->more_info,
        ]);

        return $this->successResponse('User experience updated', UserRepository::updateUserResourceCache($user));
    }

    /**
     * Delete user job experience
     *
     * @param Request $request
     * @param Int $id
     * @return Json Response
     */
    public function deleteExperience(Int $id)
    {
        $experience = WorkExperience::find($id);
        $user = $this->user;

        if ($user->id != $experience->user_id) {
            return $this->errorResponse('Forbidden', 403);
        }

        $experience->delete();

        UserRepository::deleteProfileCompletionCache($user);

        return $this->successResponse('User experience deleted', UserRepository::updateUserResourceCache($user));
    }

    /**
     * Add new user organization experience
     *
     * @param StoreOrganizationExperienceRequest $request
     * @return Json response
     */
    public function addOrganizationExperience(StoreOrganizationExperienceRequest $request)
    {
        $startTime = Carbon::parse($request->start_year.'-'.$request->start_month.'-1')->format('Y-m-d');
        if (is_null($request->end_year) || is_null($request->end_month)) {
            $endTime = null;
        } else {
            $endTime = Carbon::parse($request->end_year.'-'.$request->end_month.'-1')->format('Y-m-d');
        }

        $user = $this->user;
        $user->organization_experiences()->create([
            'name' => $request->name,
            'position' => $request->position,
            'start_time' => $startTime,
            'end_time' => $endTime,
            'still_involved' => $request->still_involved,
            'more_info' => $request->more_info,
        ]);

        UserRepository::deleteProfileCompletionCache($user);
        
        return $this->successResponse('User organization experience added', UserRepository::updateUserResourceCache($user));
    }

    /**
     * Update user organization experience
     *
     * @param StoreOrganizationExperienceRequest $request
     * @param Int $id
     * @return Json response
     */
    public function updateOrganizationExperience(StoreOrganizationExperienceRequest $request, Int $id)
    {
        $organizationExperience = OrganizationExperience::find($id);
        $user = $this->user;

        if ($user->id != $organizationExperience->user_id) {
            return $this->errorResponse('Forbidden', 403);
        }

        $startTime = Carbon::parse($request->start_year.'-'.$request->start_month.'-1')->format('Y-m-d');
        if (is_null($request->end_year) || is_null($request->end_month)) {
            $endTime = null;
        } else {
            $endTime = Carbon::parse($request->end_year.'-'.$request->end_month.'-1')->format('Y-m-d');
        }

        $organizationExperience->update([
            'name' => $request->name,
            'position' => $request->position,
            'start_time' => $startTime,
            'end_time' => $endTime,
            'still_involved' => $request->still_involved,
            'more_info' => $request->more_info,
        ]);

        return $this->successResponse('User organization experience updated', UserRepository::updateUserResourceCache($user));
    }

    /**
     * Delete user organization experience
     *
     * @param Request $request
     * @param Int $id
     * @return Json Response
     */
    public function deleteOrganizationExperience(Int $id)
    {
        $organizationExperience = OrganizationExperience::find($id);
        $user = $this->user;

        if ($user->id != $organizationExperience->user_id) {
            return $this->errorResponse('Forbidden', 403);
        }

        $organizationExperience->delete();

        UserRepository::deleteProfileCompletionCache($user);

        return $this->successResponse('User organization experience deleted', UserRepository::updateUserResourceCache($user));
    }

    /**
    * Add new user education
    *
    * @param StoreEducationRequest $request
    * @return Json response
    */
    public function addEducation(StoreEducationRequest $request)
    {
        $startTime = Carbon::parse($request->start_year.'-1-1')->format('Y-m-d');
        if (is_null($request->end_year)) {
            $endTime = null;
        } else {
            $endTime = Carbon::parse($request->end_year.'-12-31')->format('Y-m-d');
        }

        $user = $this->user;
        $user->educations()->create([
            'institution' => $request->institution,
            'province_id' => $request->province_id,
            'city_id' => $request->city_id,
            'education_degree_id' => $request->education_degree_id,
            'education_field_id' => $request->education_field_id,
            'gpa' => $request->gpa,
            'start_time' => $startTime,
            'end_time' => $endTime,
            'still_studying' => $request->still_studying,
            'more_info' => $request->more_info,
        ]);

        UserRepository::deleteProfileCompletionCache($user);

        return $this->successResponse('User education added', UserRepository::updateUserResourceCache($user));
    }

    /**
     * Update user education
     *
     * @param StoreEducationRequest $request
     * @param Int $id
     * @return Json response
     */
    public function updateEducation(StoreEducationRequest $request, Int $id)
    {
        $education = Education::find($id);
        
        $user = $this->user;

        if ($user->id != $education->user_id) {
            return $this->errorResponse('Forbidden', 403);
        }

        $startTime = Carbon::parse($request->start_year.'-1-1')->format('Y-m-d');
        if (is_null($request->end_year)) {
            $endTime = null;
        } else {
            $endTime = Carbon::parse($request->end_year.'-12-31')->format('Y-m-d');
        }

        $education->update([
            'institution' => $request->institution,
            'province_id' => $request->province_id,
            'city_id' => $request->city_id,
            'education_degree_id' => $request->education_degree_id,
            'education_field_id' => $request->education_field_id,
            'gpa' => $request->gpa,
            'start_time' => $startTime,
            'end_time' => $endTime,
            'still_studying' => $request->still_studying,
            'more_info' => $request->more_info,
        ]);
        
        return $this->successResponse('User education updated', UserRepository::updateUserResourceCache($user));
    }

    /**
     * Delete user education
     *
     * @param Request $request
     * @param Int $id
     * @return Json Response
     */
    public function deleteEducation(Request $request, Int $id)
    {
        $education = Education::find($id);
        $user = $this->user;

        if ($user->id != $education->user_id) {
            return $this->errorResponse('Forbidden', 403);
        }

        $education->delete();

        UserRepository::deleteProfileCompletionCache($user);

        return $this->successResponse('User education deleted', UserRepository::updateUserResourceCache($user));
    }

    /**
    * Add new user award
    *
    * @param StoreAwardRequest $request
    * @return Json response
    */
    public function addAward(StoreAwardRequest $request)
    {
        $user = $this->user;
        $user->awards()->create([
            'title' => $request->title,
            'description' => $request->description,
            'awarded_at' => Carbon::parse($request->awarded_at.'-1-1')->format('Y-m-d'),
            'more_info' => $request->more_info,
        ]);

        UserRepository::deleteProfileCompletionCache($user);

        return $this->successResponse('User award added', UserRepository::updateUserResourceCache($user));
    }

    /**
     * Update user award
     *
     * @param StoreAwardRequest $request
     * @param Int $id
     * @return Json response
     */
    public function updateAward(StoreAwardRequest $request, Int $id)
    {
        $award = Award::find($id);
        
        $user = $this->user;

        if ($user->id !== $award->user_id) {
            return $this->errorResponse('Forbidden', 403);
        }

        $award->update([
            'title' => $request->title,
            'description' => $request->description,
            'awarded_at' => Carbon::parse($request->awarded_at.'-1-1')->format('Y-m-d'),
            'more_info' => $request->more_info,
        ]);
        
        return $this->successResponse('User award updated', UserRepository::updateUserResourceCache($user));
    }

    /**
     * Delete user award
     *
     * @param Request $request
     * @param Int $id
     * @return Json Response
     */
    public function deleteAward(Request $request, Int $id)
    {
        $award = Award::find($id);
        $user = $this->user;

        if ($user->id !== $award->user_id) {
            return $this->errorResponse('Forbidden', 403);
        }

        $award->delete();

        UserRepository::deleteProfileCompletionCache($user);

        return $this->successResponse('User award deleted', UserRepository::updateUserResourceCache($user));
    }

    /**
     * Update user skills
     *
     * @param Request $request
     * @return Json response
     */
    public function updateSkills(Request $request)
    {
        $this->validate($request, [
            'skill_ids' => 'required|array'
        ]);

        $user = $this->user;
        $user->skills()->sync($request->skill_ids);

        UserRepository::deleteProfileCompletionCache($user);

        return $this->successResponse('User skills updated', UserRepository::updateUserResourceCache($user));
    }

    /**
     * Update user job interest
     *
     * @param Request $request
     * @return Json response
     */
    public function updateInterest(Request $request)
    {
        $this->validate($request, [
            'jobfunction_ids' => 'required|array',
            'jobrole_ids' => 'required|array',
            'job_types' => 'required|array',
            'current_salary' => 'required|numeric',
            'expected_salary' => 'required|numeric',
            'cities' => 'required|array',
            'relocated' => 'required|boolean',
        ], [
            'jobfunction_ids.required' => 'Job field is required!',
            'jobrole_ids.required' => 'Job position is required!',
            'job_types.required' => 'Job type is required!',
            'current_salary.required' => 'Current salary is required!',
            'expected_salary.required' => 'Expected salary is required!',
            'cities.required' => 'City is required!',
        ]);

        $user = $this->user;

        JobInterest::updateOrCreate(
            ['user_id' => $user->id],
            $request->all()
        );

        UserRepository::deleteProfileCompletionCache($user);

        return $this->successResponse('User interest updated', UserRepository::updateUserResourceCache($user));
    }

    /**
     * Upload resume pdf file
     *
     * @param Request $request
     * @return Json response
     */
    public function uploadResume(Request $request)
    {
        $this->validate($request, [
            'resume' => 'required|mimetypes:application/pdf|max:1500'
        ], [
            'resume.mimetypes' => 'File harus pdf',
            'resume.max' => 'Max file size 1.5mb'
        ]);

        $user = $this->user;
        $oldResume = $user->resume;

        // Update resume
        $newResume = FileHelper::storeFile($request->resume, PathHelper::user_resume_path($user->id), 'resume');
        $user->resume = $newResume;
        $user->save();
        
        // Delete old resume
        PathHelper::deleteFile(PathHelper::user_resume_path($user->id).$oldResume);

        UserRepository::deleteProfileCompletionCache($user);

        return $this->successResponse('User resume updated', UserRepository::updateUserResourceCache($user));
    }

    /**
     * Update portofolio
     *
     * @param Request $request
     * @return Json response
     */
    public function uploadPortofolio(Request $request)
    {
        $this->validate($request, [
            'yt_video' => 'nullable|url',
            'website' => 'nullable|url',
            'facebook' => 'nullable|url',
            'twitter' => 'nullable|url',
            'instagram' => 'nullable|url',
            'linkedin' => 'nullable|url',
            'behance' => 'nullable|url',
            'github' => 'nullable|url',
            'codepen' => 'nullable|url',
            'vimeo' => 'nullable|url',
            'youtube' => 'nullable|url',
            'dribbble' => 'nullable|url',
        ], [
            'yt_video.url' => 'Youtube video must be URL',
            '*.url' => 'Input :attribute must be URL'
        ]);

        $user = $this->user;
        $user->update([
            'website' => $request->website,
            'yt_video' => $request->yt_video,
            'facebook' => $request->facebook,
            'twitter' => $request->twitter,
            'instagram' => $request->instagram,
            'linkedin' => $request->linkedin,
            'behance' => $request->behance,
            'github' => $request->github,
            'codepen' => $request->codepen,
            'vimeo' => $request->vimeo,
            'youtube' => $request->youtube,
            'dribbble' => $request->dribbble,
        ]);

        UserRepository::deleteProfileCompletionCache($user);

        return $this->successResponse('User portofolio updated', UserRepository::updateUserResourceCache($user));
    }

    /**
     * Upload video profile
     *
     * @param Request $request
     * @return Json response
     */
    public function uploadVideo(Request $request)
    {
        $this->validate($request, [
            'video' => 'required|file|max:10000|mimes:webm,mkv,x-ms-asf,x-flv,mp4,x-mpegURL,MP2T,3gpp,quicktime,x-msvideo,x-ms-wmv,avi'
        ], [
            'video.required' => 'Video is required!',
            'video.max' => 'File size too big! Max 10MB',
            'video.mimetypes' => 'Video file not supported!'
        ]);

        $user = $this->user;
        $oldVideo = $user->video;

        // Update video
        $newVideo = FileHelper::storeFile($request->video, PathHelper::user_video_path($user->id), 'video');
        $user->video = $newVideo;
        $user->save();
        
        // Delete old video
        PathHelper::deleteFile(PathHelper::user_video_path($user->id).$oldVideo);

        UserRepository::deleteProfileCompletionCache($user);

        return $this->successResponse('User video updated', UserRepository::updateUserResourceCache($user));
    }
}
