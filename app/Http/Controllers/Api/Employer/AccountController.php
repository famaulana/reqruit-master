<?php

namespace App\Http\Controllers\Api\Employer;

use Auth;
use employer;
use App\Helpers\PathHelper;
use App\Helpers\ImageHelper;
use Illuminate\Http\Request;
use App\Models\JobApplication;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\PasswordRequest;
use App\Http\Resources\EmployerResource;

class AccountController extends Controller
{
    /**
     * Current logged in employer
     *
     * @var Object
     */
    public $employer;

    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->employer = Auth::guard('employer')->user();
    }

    /**
     * Employer dashboard
     *
     * @param Request $request
     * @return Blade view
     */
    public function dashboard(Request $request)
    {
        $company = $this->employer->company;
        $jobIds = is_null($company) ? [] : $this->employer->company->work_jobs->pluck('id')->toArray();

        $data = [
            'meta_title' => 'Dashboard',
            'meta_description' => 'Company account dashboard',
            'employer' => $this->employer,
            'pending' => JobApplication::whereIn('workjob_id', $jobIds)->where('status', 'pending')->count(),
            'shortlisted' => JobApplication::whereIn('workjob_id', $jobIds)->where('status', 'shortlisted')->count(),
            'interview' => JobApplication::whereIn('workjob_id', $jobIds)->where('status', 'interview')->count(),
            'offered' => JobApplication::whereIn('workjob_id', $jobIds)->where('status', 'offered')->count(),
            'hired' => JobApplication::whereIn('workjob_id', $jobIds)->where('status', 'hired')->count(),
            'unsuitable' => JobApplication::whereIn('workjob_id', $jobIds)->where('status', 'unsuitable')->count(),
        ];

        return $this->successResponse('Employer dashboard', $data);
    }

    /**
     * Edit employer account page
     *
     * @param Request $request
     * @return Blade view
     */
    public function editAccount(Request $request)
    {
        $data = [
            'meta_title' => 'Edit Account',
            'meta_description' => 'Edit account page',
            'employer' => new EmployerResource($this->employer)
        ];

        return $this->successResponse('Edit account', $data);
    }

    /**
     * Update employer account
     *
     * @param Request $request
     * @return Json response
     */
    public function updateAccount(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email|unique:employers,email,'.$this->employer->id,
            'phone' => 'required|string|unique:employers,phone,'.$this->employer->id,
            'gender' => 'required|string|in:male,female',
            'dob' => 'required|date_format:Y-m-d',
        ], [
            'gender.required' => 'Gender is required!',
            'dob.required' => 'Date of birth is required!',
        ]);

        $this->employer->update($request->all());
        $employer = $this->employer->refresh();

        return $this->successResponse('Profile updated', new EmployerResource($employer));
    }

    /**
     * Update password
     *
     * @param Request $request
     * @param PasswordRequest $request
     * @return void
     */
    public function updatePassword(PasswordRequest $request)
    {
        $employer = $this->employer;
        $employer->password = Hash::make($request->new_password);
        $employer->save();

        return $this->successResponse('Password updated', new EmployerResource($employer));
    }

    /**
     * Update employer avatar
     *
     * @param Request $request
     * @return Json response
     */
    public function updateAvatar(Request $request)
    {
        $this->validate($request, [
            'avatar' => 'required|image|mimes:jpg,png,jpeg|max:3000'
        ], [
            'avatar.required' => 'Image is required!',
            'avatar.image' => 'File must be an image!',
            'avatar.mimes' => 'File type must be jpg, png or jpeg!',
            'avatar.max' => 'Max file size 3mb!'
        ]);

        $employer = $this->employer;
        $oldPhoto = $employer->photo;
        $path = PathHelper::employer_photo_path($employer->id);

        // Update photo
        $newPhoto = ImageHelper::processImg($request->avatar, $path, ['type' => 'fit', 'size' => 250], 'avatar');
        $employer->photo = $newPhoto;
        $employer->save();
        
        // Delete old resume
        PathHelper::deleteFile($path.$oldPhoto);

        return $this->successResponse('Avatar updated', new EmployerResource($employer));
    }
}
