<?php

namespace App\Http\Controllers\Api\Employer;

use App\Models\Country;
use App\Models\Workjob;
use App\Models\JobQuestion;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Services\WorkjobService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use App\Http\Resources\WorkjobResource;
use App\Services\JobApplicationService;
use App\Http\Requests\StoreWorkjobRequest;
use App\Http\Requests\UpdateWorkjobRequest;
use App\Http\Resources\WorkjobSimpleResource;

class WorkjobController extends Controller
{
    /**
     * Current logged in employer
     *
     * @var Object
     */
    public $employer;

    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->employer = Auth::guard('employer')->user();
    }
    
    /**
     * Displaying jobs
     *
     * @param Request $request
     * @return Json response
     */
    public function index(Request $request)
    {
        $this->validate($request, [
            'keyword' => 'nullable|string',
            'sort' => 'nullable|string|in:created_at,updated_at,title',
        ]);

        $company = $this->employer->company;

        if (is_null($company)) {
            return $this->errorResponse('Please create a company, before see the job listing!', 403);
        }

        $jobs = $company->work_jobs()->with([
            'company',
            'city',
            'job_function',
            'job_applications'
        ]);

        if ($request->has('keyword')) {
            $jobs = $jobs->where('title', 'LIKE', '%'.$request->keyword.'%');
        }
        
        if ($request->has('sort')) {
            $sortType = $request->sort == 'title' ? 'ASC' : 'DESC';
            $jobs = $jobs->orderBy($request->sort, $sortType);
        } else {
            $jobs = $jobs->orderBy('created_at', 'DESC');
        }

        $jobs = WorkjobSimpleResource::collection($jobs->simplePaginate(20));

        $data = [
            'meta_title' => 'Job',
            'meta_description' => 'Manage job',
            'jobs' => $jobs,
            'prev_url' => $jobs->previousPageUrl() ?? '',
            'next_url' => $jobs->nextPageUrl() ?? '',
        ];

        return $this->successResponse('Job list', $data);
    }

    /**
     * Get job option select
     *
     * @return Array
     */
    private function _getOptionSelect()
    {
        $service = new WorkjobService();

        return [
            'experience' => $service->jobExperienceOptionSelect(),
            'job_posting_status' => $service->jobPostingStatusOptionSelect(),
            'salary_duration' => $service->jobSalaryDurationOptionSelect(),
            'status' => $service->jobStatusOptionSelect(),
            'type' => $service->jobTypeOptionSelect(),
            'skills' => $service->skillsOptionSelect()
        ];
    }

    /**
     * Display the create new job form
     *
     * @return Json response
     */
    public function create()
    {
        $this->authorize('create', Workjob::class);

        $data = [
            'option_select' => $this->_getOptionSelect()
        ];

        return $this->successResponse('Create new job', $data);
    }

    /**
     * Store new workjob
     *
     * @param StoreWorkjobRequest $request
     * @return Json response
     */
    public function store(StoreWorkjobRequest $request)
    {
        $this->authorize('create', Workjob::class);

        $company = $this->employer->company;

        $additionalData = [
            'created_by' => $this->employer->id,
            'edited_by' => $this->employer->id,
            'identifier' => WorkjobService::generateIdentifier(),
            'slug' => Str::slug($request->title),
            'country_id' => Country::where('country_code', 'ID')->first()->id,
            'salary_currency' => 'IDR'
        ];

        $allData = array_merge($request->validated(), $additionalData);
        $workjob = $company->work_jobs()->create($allData);
        $workjob->job_roles()->attach($request->jobrole_ids);
        $workjob->must_skills()->attach($request->must_skill_ids);
        $workjob->nice_skills()->attach($request->nice_skill_ids);

        // Processing questions
        if ($request->has_question) {
            $questions = [];
            foreach ($request->questions as $q) {
                $questions[] = [
                    'question' => $q['question'],
                    'sort_order' => $q['sort_order'],
                ];
            }

            $workjob->job_questions()->createMany($questions);
        }

        return $this->successResponse('Job created', []);
    }

    /**
     * Edit workjob
     *
     * @param Workjob $workjob
     * @return Blade view
     */
    public function edit(Workjob $workjob)
    {
        $this->authorize('update', $workjob);

        $data = [
            'meta_title' => 'Edit Job',
            'meta_description' => 'Edit job',
            'job' => new WorkjobResource($workjob),
            'option_select' => $this->_getOptionSelect()
        ];

        return $this->successResponse('Edit job', $data);
    }

    /**
     * Update workjob
     *
     * @param UpdateWorkjobRequest $request
     * @param Workjob $workjob
     * @return Json response
     */
    public function update(UpdateWorkjobRequest $request, Workjob $workjob)
    {
        $this->authorize('update', $workjob);

        $additionalData = [
            'edited_by' => $this->employer->id,
            'slug' => Str::slug($request->title),
        ];

        $allData = array_merge($request->validated(), $additionalData);
        $workjob->update($allData);
        $workjob->job_roles()->sync($request->jobrole_ids);
        $workjob->must_skills()->sync($request->must_skill_ids);
        $workjob->nice_skills()->sync($request->nice_skill_ids);

        // Processing job questions
        $qIdsExisting = $workjob->job_questions->pluck('id')->toArray();
        if ($request->has_question) {
            $qIdsKeep = [];
            foreach ($request->questions as $q) {
                if (is_null($q['id'])) {
                    // Create new question
                    $workjob->job_questions()->create([
                        'question' => $q['question'],
                        'sort_order' => $q['sort_order'],
                    ]);
                } else {
                    // Update existing question
                    $qIdsKeep[] = $q['id'];
                    $question = JobQuestion::find($q['id']);
                    if (!is_null($question)) {
                        if ($question->question != $q['question'] || $question->sort_order != $q['sort_order']) {
                            $question->update([
                                'question' => $q['question'],
                                'sort_order' => $q['sort_order'],
                            ]);
                        }
                    }
                }
            }

            // Removing deleted questions
            $qIdsRemove = [];
            foreach ($qIdsExisting as $exisingId) {
                if (!in_array($exisingId, $qIdsKeep)) {
                    $qIdsRemove[] = $exisingId;
                }
            }
            if (!empty($qIdsRemove)) {
                $qToBeDeleted = JobQuestion::findMany($qIdsRemove);
                foreach ($qToBeDeleted as $qDelete) {
                    $qDelete->delete();
                }
            }
        } else {
            // delete all questions
            if (!empty($qIdsExisting)) {
                $qToBeDeleted = JobQuestion::findMany($qIdsExisting);
                foreach ($qToBeDeleted as $qDelete) {
                    $qDelete->delete();
                }
            }
        }

        return $this->successResponse('Job updated', []);
    }

    /**
     * Force delete workjob
     *
     * @param Workjob $workjob
     * @return Json response
     */
    public function destroy(Workjob $workjob)
    {
        $this->authorize('delete', $workjob);

        $workjob->forceDelete();

        return $this->successResponse('Job deleted', []);
    }

    /**
     * Search workjob
     *
     * @param Request $request
     * @return Json response
     */
    public function search(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string'
        ]);
        
        $company = $this->employer->company;

        $jobs = $company->work_jobs()->where('title', 'LIKE', '%'.$request->title.'%')->get();

        return $this->successResponse('Job search result', $jobs);
    }

    /**
     * Get workjob's job applications
     *
     * @param Workjob $workjob
     * @return Json response
     */
    public function jobApplications(Request $request, Workjob $workjob)
    {
        if (Gate::denies('view-job-applications', $workjob->company)) {
            return $this->errorResponse('Forbidden', 403);
        }
        
        $service = new JobApplicationService();
        $jobApplications = $service->getJobApplicationsAllStatus($workjob);

        return $this->successResponse('Job applications', $jobApplications);
    }
}
