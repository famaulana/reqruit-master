<?php

namespace App\Http\Controllers\Api\Employer;

use App\Models\StaticBlock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RecruitmentTypeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $selfReqBlock = StaticBlock::where('name', 'Self Recruitment Details')->where('active', 1)->first();
        $headHuntingBlock = StaticBlock::where('name', 'Head Hunting Details')->where('active', 1)->first();

        $data = [
            'self_detail' => is_null($selfReqBlock) ? '' : $selfReqBlock,
            'head_hunting_detail' => is_null($headHuntingBlock) ? '' : $headHuntingBlock
        ];

        return $this->successResponse('Reqruitment type details', $data);
    }
}
