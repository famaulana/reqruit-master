<?php

namespace App\Http\Controllers\Api\Employer;

use App\Models\Company;
use App\Models\Country;
use App\Models\Industry;
use App\Helpers\PathHelper;
use App\Helpers\ImageHelper;
use Illuminate\Http\Request;
use App\Services\CompanyService;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use App\Http\Resources\CompanyResource;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\IndustryResource;
use App\Http\Requests\StoreCompanyRequest;
use App\Http\Requests\UpdateCompanyRequest;

class CompanyController extends Controller
{
    const BANNER_RESIZE = ['type' => 'resize', 'width' => 1400, 'height' => 525];
    const LOGO_RESIZE = ['type' => 'resize', 'width' => 300, 'height' => 300];

    /**
     * Current logged in employer
     *
     * @var Object
     */
    public $employer;

    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->employer = Auth::guard('employer')->user();
    }

    /**
     * Employer company page
     *
     * @param Request $request
     * @return Blade view
     */
    public function index(Request $request)
    {
        $company = $this->employer->company;

        $data = [
            'meta_title' => 'Manage company',
            'meta_description' => 'Manage company',
            'company' => is_null($company) ? (object) array() : new CompanyResource($company),
            'industries' => IndustryResource::collection(Industry::orderBy('name', 'ASC')->get())
        ];

        return $this->successResponse('Manage company', $data);
    }

    /**
     * Store new company
     *
     * @param StoreCompanyRequest $request
     * @return Json response
     */
    public function store(StoreCompanyRequest $request)
    {
        if ($this->employer->companies()->exists()) {
            return $this->errorResponse('You already have company', 403);
        }

        // Create slug
        $companyService = new CompanyService();
        $slug = $companyService->createSlug($request->name);

        // Generate images array
        $images = [];
        if ($request->has('image_file_names')) {
            foreach ($request->image_file_names as $key => $fileName) {
                $images[] = [
                    'file_name' => $fileName,
                    'description' => $request->image_descriptions[$key] == '' ? '' : $request->image_descriptions[$key]
                ];
            }
        }

        $company = $this->employer->companies()->create([
            'name' => $request->name,
            'slug' => $slug,
            'logo' => $request->logo_file,
            'short_description' => $request->short_description,
            'country_id' => Country::where('country_code', 'ID')->first()->id,
            'address' => $request->address,
            'province_id' => $request->province_id,
            'city_id' => $request->city_id,
            'subdistrict_id' => $request->subdistrict_id,
            'pos_code' => $request->pos_code,
            'phone' => $request->phone,
            'email' => $request->email,
            'website_url' => $request->website_url,
            'instagram' => $request->instagram,
            'facebook' => $request->facebook,
            'linkedin' => $request->linkedin,
            'twitter' => $request->twitter,
            'description' => $request->description,
            'images' => $images,
        ]);

        $company->industries()->attach($request->industry_ids);

        $path = PathHelper::company_images_path($company->id);

        // Store banner and logo
        if ($request->has('banner') && $request->banner != '') {
            $banner = ImageHelper::processImg($request->banner, $path, self::BANNER_RESIZE, 'banner');
            $company->banner = $banner;
        }
        // $logo = ImageHelper::processImg($request->logo, $path, self::LOGO_RESIZE, 'logo');
        // $company->logo = $logo;
        $company->save();

        $companyTmpPath = 'public/' . PathHelper::company_temp_path();
        $publicStoragePath = 'public/' . $path;

        // Move tmp company logo file
        if ($request->has('logo_file')) {
            $oldPath = $companyTmpPath.$request->logo_file;
            $newPath = $publicStoragePath.$request->logo_file;
            Storage::move($oldPath, $newPath);
        }

        // Move tmp company image file
        if ($request->has('image_file_names')) {
            foreach ($request->image_file_names as $fileName) {
                $oldPath = $companyTmpPath.$fileName;
                $newPath = $publicStoragePath.$fileName;
                Storage::move($oldPath, $newPath);
            }
        }

        return $this->successResponse('Company saved', new CompanyResource($company));
    }

    /**
     * Update company
     *
     * @param Request $request
     * @return void
     */
    public function update(UpdateCompanyRequest $request, Company $company)
    {
        if (Gate::denies('update-company', $company)) {
            return $this->errorResponse('Forbidden', 403);
        }

        // Generate slug
        $slug = $company->slug;
        if ($request->name != $company->name) {
            $companyService = new CompanyService();
            $slug = $companyService->createSlug($request->name);
        }

        /**
         * Generate new images array
         */
        $images = [];
        if (!is_null($request->image_file_names)) {
            foreach ($company->images as $image) {
                if (in_array($image['file_name'], $request->image_file_names)) {
                    $descKey = array_search($image['file_name'], $request->image_file_names);
                    $images[] = [
                        'file_name' => $image['file_name'],
                        'description' => $request->image_descriptions[$descKey] == '' ? '' : $request->image_descriptions[$descKey],
                    ];
                }
            }
        }

        $company->update([
            'name' => $request->name,
            'slug' => $slug,
            'short_description' => $request->short_description,
            'address' => $request->address,
            'province_id' => $request->province_id,
            'city_id' => $request->city_id,
            'subdistrict_id' => $request->subdistrict_id,
            'pos_code' => $request->pos_code,
            'phone' => $request->phone,
            'email' => $request->email,
            'website_url' => $request->website_url,
            'instagram' => $request->instagram,
            'facebook' => $request->facebook,
            'linkedin' => $request->linkedin,
            'twitter' => $request->twitter,
            'description' => $request->description,
            'images' => $images,
        ]);

        $company->industries()->sync($request->industry_ids);

        return $this->successResponse('Company updated', new CompanyResource($company));
    }

    /**
     * Update company banner
     *
     * @param Request $request
     * @return Json response
     */
    public function updateBanner(Request $request, Company $company)
    {
        $this->validate($request, [
            'banner' => 'required|image|mimes:jpg,png,jpeg|max:3000'
        ]);

        if (Gate::denies('update-company', $company)) {
            return $this->errorResponse('Forbidden', 403);
        }

        $oldBanner = $company->banner;
        $path = PathHelper::company_images_path($company->id);

        // Update banner
        $newBanner = ImageHelper::processImg($request->banner, $path, self::BANNER_RESIZE, 'banner');
        $company->banner = $newBanner;
        $company->save();
        
        // Delete old banner
        PathHelper::deleteFile($path.$oldBanner);

        return $this->successResponse('Company banner updated', new CompanyResource($company));
    }

    /**
     * Add logo for new company creation
     *
     * @param Request $request
     * @return Json response
     */
    public function addLogo(Request $request)
    {
        $this->validate($request, [
            'logo' => 'required|image|mimes:jpg,png,jpeg|max:3000',
            'prev_file_name' => 'nullable|string',
        ]);

        // Upload new logo
        $employerId = $this->employer->id;
        $path = PathHelper::company_temp_path();
        $logo = ImageHelper::processImg($request->logo, $path, [], "company-$employerId-logo");
    
        // Delete prev uploaded image
        if ($request->has('prev_file_name')) {
            PathHelper::deleteFile($path.$request->prev_file_name);
        }

        return $this->successResponse(
            'Logo uploaded',
            [
                'logo_url' => ImageHelper::getImageUrl($path, $logo),
                'file_name' => $logo
            ]
        );
    }

    /**
     * Update company logo
     *
     * @param Request $request
     * @return Json response
     */
    public function updateLogo(Request $request, Company $company)
    {
        $this->validate($request, [
            'logo' => 'required|image|mimes:jpg,png,jpeg|max:3000'
        ]);

        if (Gate::denies('update-company', $company)) {
            return $this->errorResponse('Forbidden', 403);
        }

        $oldLogo = $company->logo;
        $path = PathHelper::company_images_path($company->id);

        // Update logo
        $newLogo = ImageHelper::processImg($request->logo, $path, self::LOGO_RESIZE, 'logo');
        $company->logo = $newLogo;
        $company->save();
        
        // Delete old logo
        PathHelper::deleteFile($path.$oldLogo);

        return $this->successResponse('Company logo updated', new CompanyResource($company));
    }

    /**
     * Add new image for new company creation
     *
     * @param Request $request
     * @return Json response
     */
    public function addImageNewCompany(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|image|mimes:jpg,png,jpeg|max:1500',
            'prev_file_name' => 'nullable|string',
        ]);

        // Upload new image
        $employerId = $this->employer->id;
        $path = PathHelper::company_temp_path();
        $newImage = ImageHelper::processImg($request->image, $path, [], "company-$employerId-image");
    
        // Delete prev uploaded image
        if ($request->has('prev_file_name')) {
            PathHelper::deleteFile($path.$request->prev_file_name);
        }

        return $this->successResponse(
            'Foto berhasil diunggah',
            [
                'image_url' => ImageHelper::getImageUrl($path, $newImage),
                'file_name' => $newImage
            ]
        );
    }

    /**
     * Update company image
     *
     * @param Request $request
     * @return Json response
     */
    public function updateImage(Request $request, Company $company)
    {
        $this->validate($request, [
            'image' => 'required|mimes:jpg,png,jpeg|image|max:1500',
            'index' => 'required|numeric'
        ]);

        if (Gate::denies('update-company', $company)) {
            return $this->errorResponse('Forbidden', 403);
        }

        $index = $request->index;
        $path = PathHelper::company_images_path($company->id);

        if (isset($company->images[$index])) {
            $oldImage = $company->images[$index]['file_name'];
        }
        
        // Update image
        $newImage = ImageHelper::processImg($request->image, $path, [], 'image');
        $images = [];
        if (is_null($company->images)) {
            $images[] = [
                'file_name' => $newImage,
                'description' => ''
            ];
        } else {
            if (isset($company->images[$index])) {
                foreach ($company->images as $key => $image) {
                    if ($key == $index) {
                        $images[] = [
                            'file_name' => $newImage,
                            'description' => $image['description']
                        ];
                    } else {
                        $images[] = [
                            'file_name' => $image['file_name'],
                            'description' => $image['description']
                        ];
                    }
                }
            } else {
                $existingImages = $company->images;
                $images[] = [
                    'file_name' => $newImage,
                    'description' => ''
                ];

                $images = array_merge($existingImages, $images);
            }
        }

        $company->images = $images;
        $company->save();
        
        // Delete old logo
        if (isset($oldImage)) {
            PathHelper::deleteFile($path.$oldImage);
        }
        
        return $this->successResponse(
            'Photo updated',
            [
                'image_url' => $company->company_images[$index]['src'],
                'file_name' => $newImage
            ]
        );
    }

    /**
     * Remove company image
     *
     * @param Request $request
     * @param Company $company
     * @param Int $index
     * @return Json response
     */
    public function removeImage(Request $request, Company $company, Int $index)
    {
        $path = PathHelper::company_images_path($company->id);
        $fileName = $company->images[$index]['file_name'];
        PathHelper::deleteFile($path.$fileName);

        $images = [];
        foreach ($company->images as $key => $image) {
            if ($key != $index) {
                $images[] = [
                    'file_name' => $image['file_name'],
                    'description' => $image['description'],
                ];
            }
        }
        
        $company->images = $images;
        $company->save();

        return $this->successResponse('Photo deleted', []);
    }
}
