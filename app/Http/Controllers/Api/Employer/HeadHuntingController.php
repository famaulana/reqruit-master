<?php

namespace App\Http\Controllers\Api\Employer;

use App\Models\HeadHunting;
use Illuminate\Http\Request;
use App\Mail\HeadHunterEmail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Repositories\AppConfigRepository;

class HeadHuntingController extends Controller
{
    /**
     * AppConfigRepository instance
     *
     * @var App\Repositories\AppConfigReposity
     */
    private $appConfig;

    public function __construct()
    {
        $this->appConfig = new AppConfigRepository();
    }
    
    /**
     * Head hunting post
     *
     * @param Request $request
     * @return Json response
     */
    public function post(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email',
            'phone' => 'required|string',
            'position' => 'required|string',
            'company' => 'required|string',
            'job_position' => 'required|string',
            'salary_min' => 'required|numeric|min:0',
            'salary_max' => 'required|numeric|min:0',
            'location' => 'required|string',
            'description' => 'required|string',
        ]);

        $emailData = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'position' => $request->position,
            'company' => $request->company,
            'job_position' => $request->job_position,
            'salary_min' => $request->salary_min,
            'salary_max' => $request->salary_max,
            'location' => $request->location,
            'description' => $request->description
        ];

        Mail::to($this->appConfig->getConfig('site_contact_email'))
            ->send(new HeadHunterEmail($emailData));

        HeadHunting::create($request->all());

        return $this->successResponse('Your message has been sent. Our team will contact you shortly.', []);
    }
}
