<?php

namespace App\Http\Controllers\Api\Employer;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthenticatedEmployerController extends Controller
{
    
    /**
     * Current logged in employer
     *
     * @var App\Models\Employer
     */
    public $employer;

    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->employer = Auth::guard('employer')->user();
    }
}
