<?php

namespace App\Http\Controllers\Api\Employer;

use Illuminate\Http\Request;
use App\Models\CompanyReview;
use App\Http\Resources\CompanyReviewResource;

class CompanyReviewController extends AuthenticatedEmployerController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->validate($request, [
            'status' => 'nullable|string|in:all,published,unpublished'
        ]);

        $company = $this->employer->company;

        if (is_null($company)) {
            return $this->errorResponse('Please create a company, before see company reviews!', 403);
        }

        $reviews = $company->company_reviews();
        if ($request->has('status') && $request->status != 'all') {
            $published = $request->status == 'published' ? 1 : 0;
            $reviews->where('published', $published);
        }
        $reviews->orderBy('created_at', 'DESC')->get();

        if ($reviews !== null) {
            $reviews = CompanyReviewResource::collection($reviews->simplePaginate(25));
        }

        $data = [
            'meta_title' => 'Company Reviews',
            'meta_description' => 'Company reviews',
            'reviews' => $reviews,
            'prev_url' => $reviews->previousPageUrl() ?? '',
            'next_url' => $reviews->nextPageUrl() ?? '',
        ];

        return $this->successResponse('Company reviews', $data);
    }

    /**
     * Company review status update
     *
     * @param Request $request
     * @param CompanyReview $companyReview
     * @return Json response
     */
    public function statusUpdate(Request $request, CompanyReview $companyReview)
    {
        $this->validate($request, [
            'published' => 'required|boolean'
        ]);

        $companyReview->published = $request->published;
        $companyReview->save();

        return $this->successResponse('Company review status updated', new CompanyReviewResource($companyReview));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  CompanyReview  $companyReview
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanyReview $companyReview)
    {
        $companyReview->forceDelete();

        return $this->successResponse('Company review deleted', []);
    }
}
