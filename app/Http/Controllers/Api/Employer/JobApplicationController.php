<?php

namespace App\Http\Controllers\Api\Employer;

use App\Models\Workjob;
use Illuminate\Http\Request;
use App\Models\JobApplication;
use App\Enums\JobApplicationStatus;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use App\Services\JobApplicationService;
use App\Repositories\AppConfigRepository;
use App\Http\Resources\WorkjobSimpleResource;
use App\Http\Resources\JobApplicationResource;
use App\Http\Resources\PersonalityTestResource;
use App\Http\Resources\EnglishAssessmentResource;
use App\Http\Resources\JobQuestionAnswerResource;

class JobApplicationController extends Controller
{
    /**
     * Current logged in employer
     *
     * @var Object
     */
    public $employer;

    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->employer = Auth::guard('employer')->user();
    }

    /**
     * Manage job applicants
     *
     * @return Blade view
     */
    public function index(Request $request)
    {
        $company = $this->employer->company;

        if (is_null($company)) {
            return $this->errorResponse('Please create a company, before see candidates!', 403);
        }

        $service = new JobApplicationService();
        $config = new AppConfigRepository();

        $jobApplications = [];
        $workjob = [];
        if ($request->has('job_identifier')) {
            $workjob = Workjob::where('identifier', $request->job_identifier)->first();
            if (!is_null($workjob)) {
                $jobApplications = $service->getJobApplicationsAllStatus($workjob);
                $workjob = new WorkjobSimpleResource($workjob);
            }
        }

        $jobsArray = $company->work_jobs()->select(['id', 'title'])->pluck('title', 'id')->toArray();
        $jobs = [];
        foreach ($jobsArray as $id => $title) {
            $jobs[] = [
                'id' => $id,
                'title' => $title
            ];
        }

        $data = [
            'meta_title' => 'Manage candidates',
            'meta_description' => 'Manage candidates page',
            'option_select' => [
                'job_application_status' => $service->statusOptionSelect(),
            ],
            'job_applications' => $jobApplications,
            'job' => $workjob,
            'jobs' => $jobs,
            'rejection_messages' => $config->getConfig('jap_rejection_messages')
        ];

        return $this->successResponse('Manage job applications', $data);
    }

    /**
     * View candidate
     *
     * @param JobApplication $jobApplication
     * @return Json response
     */
    public function profile(JobApplication $jobApplication)
    {
        if (Gate::denies('view-candidate', $jobApplication)) {
            return $this->errorResponse('Forbidden', 403);
        }

        $user = $jobApplication->user()->with([
            'country',
            'province',
            'city',
            'work_experiences',
            'educations',
            'skills',
            'job_interest',
            'english_assessment',
            'personality_test'
        ])->first();

        $user = new UserResource($user);
        $answers = JobQuestionAnswerResource::collection($jobApplication->job_question_answers);

        $data = [
            'job_application' => new JobApplicationResource($jobApplication),
            'user' => $user,
            'answers' => $answers,
            'cover_letter' => $jobApplication->cover_letter ?? '',
            'english_assessment' => $user->english_assessment()->exists() ? new EnglishAssessmentResource($user->english_assessment) : null,
            'personality_test' => $user->personality_test()->exists() ? new PersonalityTestResource($user->personality_test) : null,
            'data' => $jobApplication->data ?? [],
            'status' => $jobApplication->status,
            'interview_email_template' => JobApplicationService::interviewEmailTemplate()
        ];

        return $this->successResponse('User profile', $data);
    }

    /**
     * Update jobApplication status
     *
     * @param Request $request
     * @param JobApplication $jobApplication
     * @return Json response
     */
    public function statusUpdate(Request $request, JobApplication $jobApplication)
    {
        if (Gate::denies('view-candidate', $jobApplication)) {
            return $this->errorResponse('Forbidden', 403);
        }

        $statuses = implode(',', JobApplicationStatus::toArray());

        $this->validate($request, [
            'status' => 'required|string|in:'.$statuses,
            'rejection_message' => 'nullable|required_if:status,unsuitable|string',
            'interview_time' => 'nullable|date_format:Y-m-d H:i',
            'interview_location' => 'nullable|string',
            'interview_email' => 'nullable|string',
        ]);

        $service = new JobApplicationService();
        $service->setJobApplication($jobApplication);

        $dataArray = [];
        if ($request->has('rejection_message')) {
            $dataArray['rejection_message'] = $request->rejection_message;
        }
        if ($request->has('interview_time')) {
            $dataArray['interview_time'] = $request->interview_time;
        }
        if ($request->has('interview_location')) {
            $dataArray['interview_location'] = $request->interview_location;
        }

        $emailTemplate = $request->has('interview_email') ? $request->interview_email : '';

        $jobApplication = $service->updateStatus($request->status, $dataArray, $emailTemplate);

        return $this->successResponse('Job application updated', []);
    }
}
