<?php

namespace App\Http\Controllers\Api\Employer;

use App\Models\Employer;
use Illuminate\Http\Request;
use App\Models\OauthAccessToken;
use App\Services\EmployerService;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\EmployerResource;
use Illuminate\Support\Facades\Password;
use App\Http\Requests\StoreEmployerRequest;

class AuthController extends Controller
{
    public function broker()
    {
        return Password::broker('employers');
    }

    public function register(StoreEmployerRequest $request)
    {
        $employerService = new EmployerService();
        $employer = $employerService->createEmployer($request->all());

        return response()->json([
            'status' => 'success',
            'message' => 'Registration success',
            'token' => $employerService->generateToken($employer),
            'data' => new EmployerResource($employer)
        ]);
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        // Find existing user
        $employer = Employer::where('email', $request->email)->first();
        if ($employer === null) {
            return $this->errorResponse(trans('auth.account_not_found'), 404);
        }

        if (Hash::check($request->password, $employer->password)) {
            $employerService = new EmployerService();

            return response()->json([
                'status' => 'success',
                'message' => trans('auth.login_success'),
                'token' => $employerService->generateToken($employer),
            ]);
        } else {
            return $this->errorResponse(trans('auth.wrong_password'), 403);
        }
    }

    public function logout(Request $request)
    {
        $token = OauthAccessToken::where('user_id', Auth::guard('employer')->user()->id)
            ->where('name', 'Employer')
            ->first();

        if ($token === null) {
            return $this->errorResponse('Unauthorized', 401);
        }

        $token->delete();
        
        return $this->successResponse('Logout', []);
    }

    public function employerData(Request $request)
    {
        // if (!Auth::guard('employer')->check()) {
        //     return $this->errorResponse('Forbidden', 403);
        // }

        $employer = Auth::guard('employer')->user();
        return $this->successResponse('Employer data', new EmployerResource($employer));
    }

    public function sendResetPasswordLink(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email'
        ]);

        $employer = Employer::where('email', $request->email)->first();
        if ($employer === null) {
            return $this->errorResponse(trans('auth.account_not_found'), 404);
        }

        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );

        if ($response == Password::RESET_LINK_SENT) {
            return $this->successResponse(trans('auth.reset_password.link_send_success'), []);
        } else {
            return $this->errorResponse(trans('auth.reset_password.link_send_error'), 400);
        }
    }

    public function updatePassword(Request $request)
    {
        $this->validate($request, [
            'token' => 'required|string',
            'email' => 'required|email',
            'password' => 'required|string|min:8',
        ]);

        $resetTokenQuery = DB::table('password_resets_employer')->where('email', $request->email);
        $resetToken = $resetTokenQuery->first();

        if ($resetToken === null) {
            return $this->errorResponse(trans('auth.reset_password.token_mismatch'), 400);
        }

        if (!Hash::check($request->token, $resetToken->token)) {
            return $this->errorResponse(trans('auth.reset_password.token_mismatch'), 400);
        }

        $employer = Employer::where('email', $request->email)->first();
        if ($employer === null) {
            return $this->errorResponse(trans('auth.account_not_found'), 404);
        }

        $employer->password = Hash::make($request->password);
        $employer->save();
        $resetTokenQuery->delete();

        return $this->successResponse(trans('auth.reset_password.update_success'), []);
    }

    public function verifyEmail(Request $request)
    {
        $this->validate($request, [
            'token' => 'required|string',
            'email' => 'required|email',
        ]);

        $employer = Employer::where('verification_token', $request->token)
            ->where('email', $request->email)->first();

        if ($employer === null) {
            return $this->errorResponse('Token and email mismatch', 400);
        }

        $employerService = new EmployerService();
        $employer = $employerService->verifyEmail($employer);

        return $this->jsonUnescaped([
            'status' => 'success',
            'message' => 'Email verification success',
            'employer' => new EmployerResource($employer)
        ]);
    }
}
