<?php

namespace App\Http\Controllers\Api\Employer;

use App\Models\Employer;
use App\Enums\EmployerRole;
use Illuminate\Http\Request;
use App\Services\EmployerService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use App\Http\Resources\EmployerResource;
use App\Http\Requests\InviteEmployerRequest;

class TeamController extends Controller
{
    /**
     * Current logged in employer
     *
     * @var Object
     */
    public $employer;

    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->employer = Auth::guard('employer')->user();
    }

    /**
     * Company team page index
     *
     * @param Request $request
     * @return Json response
     */
    public function index(Request $request)
    {
        $company = $this->employer->company;

        if (is_null($company)) {
            return $this->errorResponse('Please create a company, before invite team member!', 403);
        }

        $data = [
            'meta_title' => 'Company team',
            'meta_description' => 'Manage company team',
            'team' => EmployerResource::collection($company->employers)
        ];

        return $this->successResponse('Company team', $data);
    }

    /**
     * Invite employer to the team
     *
     * @param InviteEmployerRequest $request
     * @return Json response
     */
    public function invite(InviteEmployerRequest $request)
    {
        try {
            $employerService = new EmployerService();
            $employerService->invite($this->employer, $request->name, $request->email, $request->role);
        } catch (\Throwable $th) {
            return $this->throwException($th);
        }

        $company = $this->employer->company;
        $team = $company->employers;
        $team = EmployerResource::collection($team);

        return $this->successResponse('Success invited', $team);
    }

    /**
     * Update employer role
     *
     * @param Request $request
     * @param Employer $employer
     * @return Json response
     */
    public function roleUpdate(Request $request, Employer $employer)
    {
        if (Gate::denies('update-employer-role', $employer)) {
            return $this->errorResponse('Forbidden', 403);
        }

        $roles = implode(',', EmployerRole::toArray());

        $this->validate($request, [
            'role' => 'required|string|in:'.$roles
        ]);

        $employer->update([
            'role' => $request->role
        ]);

        $employer = new EmployerResource($employer);

        return $this->successResponse('Member role updated', $employer);
    }
}
