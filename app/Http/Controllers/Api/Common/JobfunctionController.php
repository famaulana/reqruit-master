<?php

namespace App\Http\Controllers\Api\Common;

use App\Models\Jobfunction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\JobfunctionResource;
use App\Repositories\JobfunctionRepository;

class JobfunctionController extends Controller
{
    /**
     * Get the job functions list
     *
     * @return Json response
     */
    public function index()
    {
        $jobfunctions = Jobfunction::all();
        
        return $this->successResponse('Job functions', JobfunctionResource::collection($jobfunctions));
    }

    /**
     * Search jobfunction
     *
     * @param Request $request
     * @return Json response
     */
    public function search(Request $request)
    {
        $repository = new JobfunctionRepository();
        $repository->adminListRequestValidation($request);
        $filters = [
            'keyword' => $request->keyword,
            'name' => $request->name,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
        ];

        $jobFunctions = Jobfunction::select('*');
        $jobFunctions = $repository->search($jobFunctions, $filters)->get();

        return $this->successResponse('Job functions', JobfunctionResource::collection($jobFunctions));
    }
}
