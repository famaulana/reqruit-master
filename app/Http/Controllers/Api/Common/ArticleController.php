<?php

namespace App\Http\Controllers\Api\Common;

use App\Http\Controllers\Controller;
use App\Http\Resources\ArticleResource;
use App\Models\Article;
use App\Repositories\AppConfigRepository;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * App configs
     *
     * @var Object
     */
    public $configs;

    /**
     * Articles per page
     *
     * @var Integer
     */
    public $articlesPerPage;

    public function __construct()
    {
        $this->configs = new AppConfigRepository();
        $this->articlesPerPage = 6;
    }

    /**
     * Get article list
     *
     * @return Json response
     */
    public function index()
    {
        $articles = ArticleResource::collection(Article::select('*')->latest()->simplePaginate($this->articlesPerPage));
        // $ftPostIds = $this->configs->getConfig('featured_posts');
        $popPostIds = $this->configs->getConfig('popular_posts');
        // $ftPostIdsOrdered = implode(',', $ftPostIds);
        $popPostIdsOrdered = implode(',', $popPostIds);
        // $ftPosts = ArticleResource::collection(Article::whereIn('id', $ftPostIds)->orderByRaw("FIELD(id, $ftPostIdsOrdered)")->get());
        $popPosts = ArticleResource::collection(Article::whereIn('id', $popPostIds)->orderByRaw("FIELD(id, $popPostIdsOrdered)")->get());

        $data = [
            'meta_title' => 'Article List',
            'meta_description' => 'Article list',
            'articles' => [
                'articles' => $articles,
                'next_page_url' => $articles->nextPageUrl(),
                'current_page' => $articles->currentPage(),
            ] ,
            // 'featured_posts' => $ftPosts,
            'popular_posts' => $popPosts,
        ];

        return $this->successResponse('Article list', $data);
    }

    /**
     * Load articles
     *
     * @return Json response
     */
    public function load()
    {
        $articles = Article::select('*')->latest()->simplePaginate($this->articlesPerPage);
        
        $data = [
            'articles' => ArticleResource::collection($articles),
            'next_page_url' => $articles->nextPageUrl(),
            'current_page' => $articles->currentPage()
        ];

        return $this->successResponse('Articles loaded', $data);
    }

    /**
     * Get article data
     *
     * @param String $slug
     * @return Json Response
     */
    public function article(Request $request, String $slug)
    {
        $article = Article::where('slug', $slug)->first();

        if ($article === null) {
            return $this->errorResponse('Page not found', 404);
        }

        $data = [
            'article' => new ArticleResource($article),
            'related_articles' => ArticleResource::collection(Article::take(3)->inRandomOrder()->get())
        ];
        
        return $this->successResponse('Article details', $data);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return Json Response
     */
    public function search(Request $request)
    {
        $this->validate($request, [
            'keyword' => 'string|required'
        ]);

        $articles = Article::where('title', 'LIKE', '%'.$request->keyword.'%')
            ->latest()->simplePaginate(6);
        
        $data = [
            'articles' => ArticleResource::collection($articles),
            'next_page_url' => $articles->nextPageUrl(),
            'current_page' => $articles->currentPage(),
        ];

        return $this->successResponse('Article search result', $data);
    }
}
