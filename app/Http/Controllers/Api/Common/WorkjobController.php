<?php

namespace App\Http\Controllers\Api\Common;

use App\Models\Workjob;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\WorkjobResource;
use App\Repositories\WorkjobRepository;
use App\Http\Resources\JobQuestionResource;
use App\Http\Resources\WorkjobSimpleResource;
use App\Repositories\UserRepository;

class WorkjobController extends Controller
{
    /**
     * Search jobs
     *
     * @param Request $request
     * @return Json response
     */
    public function search(Request $request)
    {
        $jobRepository = new WorkjobRepository();
        $jobRepository->adminListRequestValidation($request);

        $jobs = Workjob::with(['province', 'city', 'job_function', 'company']);
        $perPage = $request->has('per_page') ? $request->per_page : 30;
        $filters = $jobRepository->filters($request);
        $orderBy = $request->has('order_by') ? $request->order_by : 'id';
        $sortOrder = $request->has('sort_order') ? $request->sort_order : 'DESC';
        $paginate = $request->has('paginate') ? ($request->paginate == 'true' ? true : false) : true;

        $jobData = $jobRepository->search($jobs, $filters, $orderBy, $sortOrder);
        $jobData = $paginate ? $jobData->simplePaginate($perPage) : $jobData->get();
        $jobData = WorkjobSimpleResource::collection($jobData);

        if ($paginate) {
            $data = [
                'jobs' => $jobData,
                'next_page_url' => $jobData->nextPageUrl(),
                'current_page' => $jobData->currentPage()
            ];

            return $this->successResponse('Job search results', $data);
        } else {
            return $this->successResponse('Job search result', $jobData);
        }
    }

    /**
     * Display job detail
     *
     * @param Request $request
     * @param String $slug
     * @param String $identifier
     * @return Blade view
     */
    public function index(Request $request, String $slug, String $identifier)
    {
        $job = Workjob::with(['job_roles', 'must_skills', 'nice_skills', 'creator', 'editor', 'company', 'country', 'province', 'city', 'job_function', 'job_questions'])
            ->where('slug', $slug)
            ->where('identifier', $identifier)
            ->open()
            ->published()
            ->first();

        $jap = UserService::jobAppliedStatus($job->id);

        $data = [
            'meta_title' => $job->title,
            'meta_description' => 'Job opportunity at '.$job->company->name.'. '.$job->title,
            'job' => new WorkjobResource($job),
            'job_application' => $jap
        ];

        return $this->successResponse('Job details', $data);
    }

    /**
     * Apply for the job
     *
     * @param Request $request
     * @param String $identifier
     * @return void
     */
    public function apply(Request $request, String $slug, String $identifier)
    {
        $this->validate($request, [
            // 'have_cover_letter' => 'required|boolean',
            // 'cover_letter' => 'nullable|required_if:have_cover_letter,true|string',
            'have_cover_letter' => 'boolean',
            'cover_letter' => 'nullable|string',
            'has_question' => 'required|boolean',
            'answers' => 'nullable|required_if:has_question,true|array',
            'answers.*.question' => 'nullable|required_if:has_question,true|string',
            'answers.*.answer' => 'nullable|required_if:has_question,true|string',
        ], [
            // 'have_cover_letter.required' => 'Do you have an application letter?',
            // 'cover_letter.required_if' => 'Short description is required!',
            'has_question.required' => 'Please answer all questions!',
            'answers.required_if' => 'Please answer all questions!',
            'answers.*.question.required_if' => 'Please answer the question!',
            'answers.*.answer.required_if' => 'Please answer the question!',
        ]);

        if (!Auth::guard('user')->check()) {
            return $this->successResponse('You have to login to apply job!', ['login' => true]);
        }

        $userRepository = new UserRepository();
        if (!$userRepository->isBasicInfoComplete(Auth::guard('user')->user())) {
            return $this->successResponse('Please complete your basic info profile!', [
                'redirect_route' => "/account/edit-profile"
            ]);
        }

        $job = Workjob::with(['job_questions'])->where('slug', $slug)->where('identifier', $identifier)->open()->published()->first();

        if (is_null($job)) {
            return $this->errorResponse('Job not found!', 404);
        }

        $user = Auth::guard('user')->user();

        if ($user->job_applications()->where('workjob_id', $job->id)->exists()) {
            return $this->errorResponse('You have applied to this job!', 403);
        }

        try {
            $service = new UserService();
            $jobApplication = $service->applyJob($user, $job->id, $request->answers, $request->cover_letter);
        } catch (\Throwable $th) {
            // return $this->throwException($th);
            return;
        }

        return $this->successResponse('Success applied to this job!', ['applied' => true, 'status' => $jobApplication->status]);
    }

    /**
     * Displaying the job_questions to be answered
     *
     * @param Request $request
     * @param String $slug
     * @param String $identifier
     * @return Blade view
     */
    public function questions(Request $request, String $slug, String $identifier)
    {
        $job = Workjob::with(['company', 'city', 'job_function'])
            ->where('slug', $slug)
            ->where('identifier', $identifier)
            ->open()
            ->published()
            ->first();

        if (is_null($job)) {
            return $this->errorResponse('Job not found!', 404);
        }

        if (!Auth::guard('user')->check()) {
            return $this->errorResponse('You have to login to apply job!', 403);
        }

        $data = [
            'questions' => JobQuestionResource::collection($job->job_questions),
        ];

        return $this->successResponse('Job questions', $data);
    }

    /**
     * Answer the job questions
     *
     * @param Request $request
     * @param String $slug
     * @param String $identifier
     * @return Json response
     */
    public function answers(Request $request, String $slug, String $identifier)
    {
        $this->validate($request, [
            'answers' => 'required|array',
            'cover_letter' => 'nullable|string'
        ]);

        $job = Workjob::where('slug', $slug)->where('identifier', $identifier)->open()->published()->first();

        if (is_null($job)) {
            return $this->errorResponse('Job not found!', 404);
        }

        if (!Auth::guard('user')->check()) {
            return $this->successResponse('You have to login to answer the question!', ['login' => true]);
        }

        try {
            $service = new UserService();
            $user = Auth::guard('user')->user();
            $jobApplication = $service->applyJob($user, $job->id, $request->answers, $request->cover_letter);
        } catch (\Throwable $th) {
            return $this->throwException($th);
        }
        
        return $this->successResponse('Success applied to this job!', ['applied' => true, 'status' => $jobApplication->status]);
    }

    /**
     * Cancel job application
     *
     * @param Request $request
     * @param String $slug
     * @param String $identifier
     * @return void
     */
    public function cancelApplication(Request $request, String $slug, String $identifier)
    {
        if (!Auth::guard('user')->check()) {
            return $this->successResponse('You have to login to cancel job application!', ['login' => true]);
        }

        $job = Workjob::where('slug', $slug)->where('identifier', $identifier)->first();

        if (is_null($job)) {
            return $this->errorResponse('Job not found!', 404);
        }

        $user = Auth::guard('user')->user();
        $jobApplication = $user->job_applications()->where('workjob_id', $job->id)->first();

        if (is_null($jobApplication)) {
            return $this->errorResponse('You were not applied to this job!', 403);
        }

        $jobApplication->delete();

        return $this->successResponse('Job application cancelled!', ['applied' => false, 'status' => '']);
    }

    /**
     * Get job applied status for current user
     *
     * @param Request $request
     * @param String $slug
     * @param String $identifier
     * @return Json response
     */
    public function appliedStatus(Request $request, String $slug, String $identifier)
    {
        $job = Workjob::where('slug', $slug)->where('identifier', $identifier)->first();

        if (is_null($job)) {
            return $this->errorResponse('Job not found!', 404);
        }

        return $this->successResponse('Job applied status', UserService::jobAppliedStatus($job->id));
    }
}
