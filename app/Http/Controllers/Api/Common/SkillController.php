<?php

namespace App\Http\Controllers\Api\Common;

use App\Models\Skill;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\SkillResource;
use App\Repositories\SkillRepository;

class SkillController extends Controller
{
    /**
     * Search skills
     *
     * @param Request $request
     * @return Json response
     */
    public function search(Request $request)
    {
        $this->validate($request, [
            'keyword' => 'nullable|string'
        ]);

        $repository = new SkillRepository();
        $filters = [
            'keyword' => $request->keyword,
        ];

        $skills = Skill::select('*');
        $skills = $repository->search($skills, $filters)->get();

        return $this->successResponse('Search skills', SkillResource::collection($skills));
    }
}
