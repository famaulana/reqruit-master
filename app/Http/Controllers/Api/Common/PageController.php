<?php

namespace App\Http\Controllers\Api\Common;

use App\Models\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PageResource;

class PageController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param String $slug
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, String $slug)
    {
        $page = Page::where('slug', $slug)->first();

        if ($page === null) {
            return $this->errorResponse('Page not found', 404);
        }

        $data = [
            'page' => new PageResource($page)
        ];
        
        return $this->successResponse('Page details', $data);
    }
}
