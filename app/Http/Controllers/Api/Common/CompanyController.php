<?php

namespace App\Http\Controllers\Api\Common;

use Auth;
use App\Models\Company;
use App\Enums\WorkjobType;
use App\Enums\JobStatusEnum;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CompanyResource;
use App\Http\Resources\WorkjobResource;
use App\Repositories\CompanyRepository;
use App\Http\Resources\CompanyReviewResource;
use App\Http\Resources\WorkjobSimpleResource;

class CompanyController extends Controller
{
    /**
     * Reviews per page
     *
     * @var Integer
     */
    protected $reviewsPerPage;

    /**
     * Jobs per page
     *
     * @var Integer
     */
    protected $jobsPerPage;

    public function __construct()
    {
        $this->reviewsPerPage = 5;
        $this->jobsPerPage = 3;
    }

    /**
      * Search company
      *
      * @param Request $request
      * @return Json response
      */
    public function search(Request $request)
    {
        $repository = new CompanyRepository();
        $repository->adminListRequestValidation($request);
  
        $companies = Company::select('*');
        $filters = [
              'keyword' => $request->keyword,
              'name' => $request->name,
              'start_date' => $request->start_date,
              'end_date' => $request->end_date,
          ];
        $companies = $repository->search($companies, $filters)->get();
  
        return $this->successResponse('Company search result', CompanyResource::collection($companies));
    }
  
    /**
     * Company page
     *
     * @param String $slug
     * @return Blade view
     */
    public function index(String $slug)
    {
        $company = Company::with(['industries', 'country', 'province', 'city', 'subdistrict', 'work_jobs', 'company_reviews'])->where('slug', $slug)->first();
  
        $images = [];
        foreach ($company->company_images as $image) {
            $images[] = [
                'src' => $image['src'],
                'description' => $image['description']
            ];
        }

        $reviews = $company->company_reviews()->published()->latest()->paginate($this->reviewsPerPage);
        $jobStatusSelect = [];
        foreach (JobStatusEnum::toSelectArray() as $value => $text) {
            $jobStatusSelect[] = [
                'value' => $value,
                'text' => $text
            ];
        }
        $workjobTypeSelect = [];
        foreach (WorkjobType::toSelectArray() as $value => $text) {
            $workjobTypeSelect[] = [
                'value' => $value,
                'text' => $text
            ];
        }

        $jobData = $company->work_jobs()->with(['province', 'city', 'job_function', 'company'])->latest()->simplePaginate($this->jobsPerPage);
        $jobData = WorkjobSimpleResource::collection($jobData);

        $data = [
            'meta_title' => $company->name,
            'meta_description' => 'Company detail page: '.$company->name,
            'job_data' => [
                'jobs' => $jobData,
                'next_page_url' => $jobData->nextPageUrl(),
                'current_page' => $jobData->currentPage()
            ],
            'company' => new CompanyResource($company),
            'images' => $images,
            'review_form' => [
                'job_status' => $jobStatusSelect,
                'employment_status' => $workjobTypeSelect
            ],
            'reviews' => [
                'current_page' => $reviews->currentPage(),
                'last_page' => $reviews->lastPage(),
                'data' => CompanyReviewResource::collection($reviews)
            ]
        ];

        return $this->successResponse('Company details', $data);
    }

    /**
     * Company jobs
     *
     * @param String $slug
     * @return Json response
     */
    public function jobs(String $slug)
    {
        $company = Company::with(['work_jobs'])->where('slug', $slug)->first();
        $jobData = $company->work_jobs()->with(['province', 'city', 'job_function', 'company'])->latest()->simplePaginate($this->jobsPerPage);
        $jobData = WorkjobSimpleResource::collection($jobData);
        
        $data = [
            'jobs' => $jobData,
            'next_page_url' => $jobData->nextPageUrl(),
            'current_page' => $jobData->currentPage()
        ];

        return $this->successResponse('Company jobs', $data);
    }

    /**
     * Company reviews
     *
     * @param String $slug
     * @return Json response
     */
    public function reviews(String $slug)
    {
        $company = Company::with(['company_reviews'])->where('slug', $slug)->first();
        $reviews = $company->company_reviews()->published()->orderBy('created_at', 'DESC')->paginate($this->reviewsPerPage);
        
        $data = [
            'current_page' => $reviews->currentPage(),
            'last_page' => $reviews->lastPage(),
            'data' => CompanyReviewResource::collection($reviews)
        ];

        return $this->successResponse('Company reviews', $data);
    }

    /**
     * Submit review
     *
     * @param Request $request
     * @param String $slug
     * @return Json response
     */
    public function submitReview(Request $request, String $slug)
    {
        $this->validate($request, [
            'job_status' => 'required|string|in:'.implode(',', JobStatusEnum::getValues()),
            'employment_status' => 'required|string|in:'.implode(',', WorkjobType::getValues()),
            'job_title' => 'required|string',
            'rating' => 'required|numeric|min:1|max:5',
            'review' => 'required|string'
        ]);

        $user = Auth::guard('user')->user();
        $company = Company::with(['company_reviews'])->where('slug', $slug)->first();
        if ($company->company_reviews()->where('user_id', $user->id)->exists()) {
            return $this->errorResponse('You already reviewed this company', 403);
        }

        $company->company_reviews()->create([
            'user_id' => $user->id,
            'anonymous' => false,
            'job_status' => $request->job_status,
            'employment_status' => $request->employment_status,
            'job_title' => $request->job_title,
            'rating' => $request->rating,
            'review' => $request->review
        ]);

        return $this->successResponse('Review submitted, pending for moderation', []);
    }
}
