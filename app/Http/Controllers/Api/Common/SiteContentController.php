<?php

namespace App\Http\Controllers\Api\Common;

use Illuminate\Http\Request;
use App\Services\WorkjobService;
use App\Http\Controllers\Controller;
use App\Repositories\MenuRepository;
use App\Repositories\AppConfigRepository;

class SiteContentController extends Controller
{
    public function index()
    {
        $menuRepository = new MenuRepository();
        $appConfig = new AppConfigRepository();
        $workjobService = new WorkjobService();

        $topMenuId = $appConfig->getConfig('top_menu');
        $footerMenuId = $appConfig->getConfig('footer_menu');
        $topMenuEmployerId = $appConfig->getConfig('top_menu_employer');
    
        $topMenu = $topMenuId != '' ? json_decode($menuRepository->getMenu($topMenuId, true), true) : '';
        $footerMenu = $footerMenuId != '' ? json_decode($menuRepository->getMenu($footerMenuId, true), true) : '';
        $topMenuEmployer = $topMenuEmployerId != '' ? json_decode($menuRepository->getMenu($topMenuEmployerId, true), true) : '';
            
        $configfileUrls = $appConfig->configFileUrls();

        return $this->successResponse('Menu', [
            'site_name' => $appConfig->getConfig('site_name'),
            'logo' => [
                'frontend' => $configfileUrls['frontend_logo'],
                'light' => $configfileUrls['light_logo'],
                'dark' => $configfileUrls['dark_logo'],
                'favicon' => $configfileUrls['favicon'],
            ],
            'locale' => $appConfig->getConfig('locale'),
            'currency' => [
                'base_currency' => 'IDR',
                'currency_symbol' => 'IDR',
            ],
            'menus' => [
                'top_menu' => $topMenu,
                'footer_menu' => $footerMenu,
                'top_menu_employer' => $topMenuEmployer,
            ],
            'footer' => [
                'content' => $appConfig->getConfig('footer_content'),
                'enable_bg_image' => $appConfig->getConfig('footer_bg_image_enable'),
                'bg_image' => $configfileUrls['footer_bg_image'],
            ],
            'social_media' => [
                'youtube' => $appConfig->getConfig('youtube_link'),
                'instagram' => $appConfig->getConfig('instagram_link'),
                'twitter' => $appConfig->getConfig('twitter_link'),
                'facebook' => $appConfig->getConfig('facebook_link'),
            ],
            'option_select' => [
                'job_experience' => $workjobService->jobExperienceOptionSelect(),
                'job_posting_status' => $workjobService->jobPostingStatusOptionSelect(),
                'job_salary_duration' => $workjobService->jobSalaryDurationOptionSelect(),
                'job_status' => $workjobService->jobStatusOptionSelect(),
                'job_type' => $workjobService->jobTypeOptionSelect(),
            ]
        ]);
    }
}
