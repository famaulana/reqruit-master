<?php

namespace App\Http\Controllers\Api\Common;

use App\Mail\ContactEmail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Repositories\AppConfigRepository;

class ContactController extends Controller
{
    /**
     * AppConfigRepository instance
     *
     * @var App\Repositories\AppConfigReposity
     */
    private $appConfig;

    public function __construct()
    {
        $this->appConfig = new AppConfigRepository();
    }
    
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|string',
            'phone' => 'required|string',
            'message' => 'required|string',
        ]);


        Mail::to($this->appConfig->getConfig('site_contact_email'))
            ->send(new ContactEmail($request->name, $request->email, $request->phone, $request->message));
        
        return $this->successResponse('Message sent', []);
    }
}
