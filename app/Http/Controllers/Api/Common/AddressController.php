<?php

namespace App\Http\Controllers\Api\Common;

use App\Models\City;
use App\Models\Province;
use App\Models\Subdistrict;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Address\CityResource;
use App\Http\Resources\Address\ProvinceResource;
use App\Http\Resources\Address\SubdistrictResource;
use App\Http\Resources\CountryResource;
use App\Models\Country;

class AddressController extends Controller
{
    /**
     * Get all countries
     *
     * @return Json response
     */
    public function countries()
    {
        $countries = Country::orderBy('name', 'ASC')->get();

        // return $countries;
        return response()->json([
            'status' => 'success',
            'data' => CountryResource::collection($countries),
        ]);
    }

    /**
     * Get all provinces
     *
     * @return Json response
     */
    public function provinces()
    {
        $provinces = Province::orderBy('name', 'ASC')->get();

        // return $provinces;
        return response()->json([
            'status' => 'success',
            'data' => ProvinceResource::collection($provinces),
        ]);
    }

    /**
     * Get cities by province id
     *
     * @param Int $province_id
     * @return Json response
     */
    public function cities(Int $province_id)
    {
        $cities = City::with('province')->where('province_id', $province_id)->orderBy('name', 'ASC')->get();

        return response()->json([
            'status' => 'success',
            'data' => CityResource::collection($cities)
        ]);
    }

    /**
     * Search cities
     *
     * @param Request $request
     * @return Json response
     */
    public function searchCities(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string'
        ]);

        $cities = City::where('name', 'LIKE', '%'.$request->name.'%')->get();
        $cities = CityResource::collection($cities);

        return $this->successResponse('Search cities', $cities);
    }

    /**
     * Get subdistricts by city id
     *
     * @param Int $city_id
     * @return Json response
     */
    public function subdistricts(Int $city_id)
    {
        $subdistricts = Subdistrict::with('city')->where('city_id', $city_id)->orderBy('name', 'ASC')->get();

        return response()->json([
            'status' => 'success',
            'data' => SubdistrictResource::collection($subdistricts)
        ]);
    }
}
