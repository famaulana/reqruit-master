<?php

namespace App\Http\Controllers\Api\Common;

use App\Http\Controllers\Controller;
use App\Http\Resources\JobroleResource;
use App\Models\Jobrole;

class JobroleController extends Controller
{
    /**
     * Get job roles by jobfunction_id
     *
     * @param Int $jobfunction_id
     * @return void
     */
    public function jobroles(Int $jobfunction_id)
    {
        $jobroles = Jobrole::where('jobfunction_id', $jobfunction_id)->get();
        $jobroles = JobroleResource::collection($jobroles);

        return $this->successResponse('Jobroles', $jobroles);
    }
}
