<?php

namespace App\Http\Controllers\Api\Common;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\AppConfigRepository;

class AuthDataController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $config = new AppConfigRepository();

        $data = [
            'auth_image' => $config->getFileUrl('auth_image'),
            'auth_image_employer' => $config->getFileUrl('auth_image_employer'),
        ];

        return $this->successResponse('Auth data content', $data);
    }
}
