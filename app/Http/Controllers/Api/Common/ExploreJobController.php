<?php

namespace App\Http\Controllers\Api\Common;

use App\Models\City;
use App\Models\Skill;
use App\Models\Company;
use App\Models\Province;
use App\Models\Jobfunction;
use Illuminate\Http\Request;
use App\Services\WorkjobService;
use App\Http\Controllers\Controller;
use App\Http\Resources\Address\CityResource;
use App\Services\JobfunctionService;
use App\Http\Resources\SkillResource;
use App\Repositories\AppConfigRepository;
use App\Http\Resources\JobfunctionResource;
use App\Http\Resources\CompanySimpleResource;
use App\Http\Resources\Address\ProvinceResource;

class ExploreJobController extends Controller
{
    /**
     * AppConfigRepository instance
     *
     * @var App\Repositories\AppConfigReposity
     */
    private $appConfig;

    public function __construct()
    {
        $this->appConfig = new AppConfigRepository();
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $lastUpdateSelect = [
            ['value' => '1', 'text' => 'Last Day'],
            ['value' => '7', 'text' => 'Last Week'],
            ['value' => '30', 'text' => 'Last Month'],
            ['value' => 'anytime', 'text' => 'Any Time']
        ];
        $popularCats = Jobfunction::inRandomOrder()->limit(3)->get();
        $popularSkills = Skill::inRandomOrder()->limit(3)->get();
        $companyIds = $this->appConfig->getConfig('jf_companies');
        $idsOrdered = implode(',', $companyIds);
        $allCities = City::all();

        $data = [
            'last_update_select' => $lastUpdateSelect,
            'job_type_select' => WorkjobService::jobTypeOptionSelect(),
            'job_function_select' => JobfunctionService::optionSelect(),
            'job_posting_status_select' => WorkjobService::jobPostingStatusOptionSelect(),
            'job_status_select' => WorkjobService::jobStatusOptionSelect(),
            'job_experience_select' => WorkjobService::jobExperienceOptionSelect(),
            'city_select' => CityResource::collection($allCities),
            'popular_cats' => JobfunctionResource::collection($popularCats),
            'popular_skills' => SkillResource::collection($popularSkills),
            'provinces' => ProvinceResource::collection(Province::all()),
            'ft_companies' => $idsOrdered == '' ? [] : CompanySimpleResource::collection(Company::whereIn('id', $companyIds)->orderByRaw("FIELD(id, $idsOrdered)")->get()),
        ];

        return $this->successResponse('Explore jobs', $data);
    }
}
