<?php

namespace App\Http\Controllers\Api\Common;

use App\Http\Controllers\Controller;
use App\Repositories\MenuRepository;
use App\Repositories\AppConfigRepository;

class MenuController extends Controller
{
    public function index()
    {
        $menuRepository = new MenuRepository();
        $appConfig = new AppConfigRepository();

        $topMenuId = $appConfig->getThemeConfig('top_menu');
        $footerMenuId = $appConfig->getThemeConfig('footer_menu');

        $topMenu = $topMenuId != '' ? json_decode($menuRepository->getMenu($topMenuId, true), true) : '';
        $footerMenu = $footerMenuId != '' ? json_decode($menuRepository->getMenu($footerMenuId, true), true) : '';
        
        return $this->successResponse('Menu', [
            'top_menu' => $topMenu,
            'footer_menu' => $footerMenu
        ]);
    }
}
