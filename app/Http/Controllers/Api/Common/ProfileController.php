<?php

namespace App\Http\Controllers\Api\Common;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Http\Resources\PersonalityTestResource;

class ProfileController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param String $identifier
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, String $identifier)
    {
        $user = User::with(['country', 'province', 'city', 'work_experiences', 'organization_experiences', 'educations', 'skills', 'job_interest'])
            ->where('identifier', $identifier)->first();
    
        if (is_null($user)) {
            abort(404);
        }

        $data = [
            'meta_title' => 'Profil '.$user->name,
            'meta_description' => 'Lihat profil '.$user->name,
            'user' => UserRepository::UserResourceCache($user),
            'personality_test' => $user->personality_test()->exists() ? new PersonalityTestResource($user->personality_test) : null,
        ];

        return $this->successResponse('User profile', $data);
    }
}
