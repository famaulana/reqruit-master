<?php

namespace App\Http\Controllers\Api\Common;

use App\Models\Article;
use App\Models\Company;
use App\Models\Workjob;
use App\Helpers\AppHelper;
use App\Models\StaticBlock;
use App\Models\Testimonial;
use Illuminate\Http\Request;
use App\Models\JobApplication;
use App\Http\Controllers\Controller;
use App\Http\Resources\ArticleResource;
use App\Repositories\AppConfigRepository;
use App\Http\Resources\TestimonialResource;
use App\Http\Resources\WorkjobSimpleResource;

class HomeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $config = new AppConfigRepository();

        $about = StaticBlock::where('name', 'About')->where('active', 1)->first();
        $about = is_null($about) ? '' : $about->content;

        $data = [
            'meta_title' => $config->getConfig('home_meta_title'),
            'meta_description' => $config->getConfig('home_meta_description'),
            'home_banner' => $config->getFileUrl('home_banner'),
            'home_banner_mobile' => $config->getFileUrl('home_banner_mobile'),
            'intro_right_image' => $config->getFileUrl('intro_right_image'),
            'intro_left_image' => $config->getFileUrl('intro_left_image'),
            'about_content' => $about,
            'about_btn_link' => $config->getConfig('section_about_btn_link'),
            'about_yt_embed' => $config->getConfig('section_about_yt_link') != '' ? AppHelper::convertYoutubeEmbed($config->getConfig('section_about_yt_link')) : '',
            'about_video_image_url' => $config->getFileUrl('section_about_video_image'),
            'home_join_banner' => $config->getFileUrl('home_join_banner'),
            'home_count_1_value' => $config->getConfig('home_count_1_value'),
            'home_count_1_label' => $config->getConfig('home_count_1_label'),
            'home_count_2_value' => $config->getConfig('home_count_2_value'),
            'home_count_2_label' => $config->getConfig('home_count_2_label'),
            'home_count_3_value' => $config->getConfig('home_count_3_value'),
            'home_count_3_label' => $config->getConfig('home_count_3_label'),
            'home_count_4_value' => $config->getConfig('home_count_4_value'),
            'home_count_4_label' => $config->getConfig('home_count_4_label'),
            'job_applications_count' => JobApplication::all()->count(),
            'companies_count' => Company::all()->count(),
            'jobs_count' => Workjob::all()->count(),
            'testimonials' => TestimonialResource::collection(Testimonial::take(5)->inRandomOrder()->get()),
            'articles' => ArticleResource::collection(Article::take(4)->inRandomOrder()->get())
        ];

        return $this->successResponse('Homepage content', $data);
    }
}
