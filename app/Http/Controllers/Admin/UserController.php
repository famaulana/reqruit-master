<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Address;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\StoreUserRequest;
use App\Http\Resources\AddressResource;
use App\Repositories\AddressRepository;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\UserSimpleResource;
use App\Http\Resources\PersonalityTestResource;

class UserController extends Controller
{
    protected $perPage;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $admin = $request->user();
            if ($admin->primary) {
                return $next($request);
            }
            
            if (in_array('manage-user', $this->getPermissions($admin))) {
                return $next($request);
            } else {
                return redirect()->route('admin.home');
            }
        });

        $this->perPage = 50;
    }

    /**
     * request filters
     *
     * @param Request $request
     * @return Array
     */
    private function _filters(Request $request)
    {
        return [
            'keyword' => $request->keyword,
            'identifier' => $request->identifier,
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'gender' => $request->gender,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $repository = new UserRepository();
        $repository->adminListRequestValidation($request);

        $users = User::select('*');
        $userData = $repository->search($users, $this->_filters($request))->paginate(50);

        $data = [
            'users' => UserSimpleResource::collection($userData),
            'total' => $userData->total(),
            'per_page' => $userData->perPage(),
            'current_page' => $userData->currentPage(),
            'total_page' => $userData->lastPage(),
            'displayed_start' => (($userData->currentPage() - 1) * $userData->perPage()) + 1,
            'displayed_end' => ($userData->currentPage() == $userData->lastPage()) ? $userData->total() : $userData->currentPage() * $userData->perPage()
        ];

        if ($request->ajax()) {
            return $this->jsonUnescaped([
                'status' => 'success',
                'data' => $data
            ]);
        }

        return view('admin.users.index', compact('data'));
    }

    public function trashed(Request $request)
    {
        $repository = new UserRepository();
        $repository->adminListRequestValidation($request);

        $users = User::onlyTrashed()->select('*');
        $userData = $repository->search($users, $this->_filters($request))->paginate(50);

        $data = [
            'users' => UserSimpleResource::collection($userData),
            'total' => $userData->total(),
            'per_page' => $userData->perPage(),
            'current_page' => $userData->currentPage(),
            'total_page' => $userData->lastPage(),
            'displayed_start' => (($userData->currentPage() - 1) * $userData->perPage()) + 1,
            'displayed_end' => ($userData->currentPage() == $userData->lastPage()) ? $userData->total() : $userData->currentPage() * $userData->perPage()
        ];

        if ($request->ajax()) {
            return $this->jsonUnescaped([
                'status' => 'success',
                'data' => $data
            ]);
        }

        return view('admin.users.trashed', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        $userservice = new UserService();
        $user = $userservice->createUser($request->validated());

        session()->flash('flash_alert', [
            'level' => 'success',
            'message' => trans('common.saved'),
        ]);

        return response()->json([
            'status' => 'success',
            'message' => trans('common.saved'),
            'data' => new UserResource($user)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $user = new UserResource($user);

        $personalityTest = $user->personality_test()->exists() ? new PersonalityTestResource($user->personality_test) : null;

        return view('admin.users.informations.show', compact('user', 'personalityTest'));
    }

    /**
     * Display user addresses
     *
     * @param User $user
     * @return Blade view
     */
    // public function addresses(Request $request, User $user)
    // {
    //     $this->validate($request, [
    //         'keyword' => 'nullable|string',
    //         'name' => 'nullable|string',
    //         'street' => 'nullable|string',
    //         'province_name' => 'nullable|string',
    //         'city_name' => 'nullable|string',
    //         'subdistrict_name' => 'nullable|string',
    //         'post_code' => 'nullable|string',
    //         'phone' => 'nullable|string',
    //         'start_date' => 'required_with:end_date|string|date_format:Y-m-d',
    //         'end_date' => 'required_with:start_date|string|date_format:Y-m-d'
    //     ]);

    //     $address = Address::relation()->where('user_id', $user->id);
    //     $addressRepository = new AddressRepository();
    //     $filters = [
    //         'keyword' => $request->keyword,
    //         'name' => $request->name,
    //         'street' => $request->street,
    //         'province_name' => $request->province_name,
    //         'city_name' => $request->city_name,
    //         'subdistrict_name' => $request->subdistrict_name,
    //         'post_code' => $request->post_code,
    //         'phone' => $request->phone,
    //         'start_date' => $request->start_date,
    //         'end_date' => $request->end_date,
    //     ];
    //     $address = $addressRepository->search($address, $filters)->paginate(50);

    //     $data = [
    //         'user' => new UserResource($user),
    //         'addresses' => AddressResource::collection($address),
    //         'total' => $address->total(),
    //         'per_page' => $address->perPage(),
    //         'current_page' => $address->currentPage(),
    //         'total_page' => $address->lastPage(),
    //         'displayed_start' => (($address->currentPage() - 1) * $address->perPage()) + 1,
    //         'displayed_end' => ($address->currentPage() == $address->lastPage()) ? $address->total() : $address->currentPage() * $address->perPage()
    //     ];

    //     if ($request->ajax()) {
    //         return $this->jsonUnescaped([
    //             'status' => 'success',
    //             'data' => $data
    //         ]);
    //     }

    //     return view('admin.users.informations.addresses', compact('data', 'user'));
    // }

    /**
     * Delete user address
     *
     * @param User $user
     * @param Address $address
     * @return Json response
     */
    // public function addressesDelete(User $user, Address $address)
    // {
    //     if ($user->id != $address->user_id) {
    //         return response()->json([
    //             'status' => 'error',
    //             'message' => 'Mismatch user id',
    //         ]);
    //     }

    //     $address->delete();

    //     return response()->json([
    //         'status' => 'success',
    //         'message' => trans('common.permanent_deleted'),
    //     ]);
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('admin.users.informations.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $user->update($request->all());
        
        if ($request->photo_profile != '') {
            $userservice = new UserService();
            $userservice->processPhoto($user, $request->all(), false);
        }

        session()->flash('flash_alert', [
            'level' => 'success',
            'message' => trans('common.updated'),
        ]);

        return response()->json([
            'status' => 'success',
            'message' => trans('common.updated'),
            'data' => new UserResource($user)
        ]);
    }

    public function updatePassword(Request $request, $id)
    {
        $this->validate($request, [
            'password' => 'required|string|min:8|confirmed',
            'password_confirmation' => 'required|string|min:8'
        ]);

        $user = User::find($id);

        $user->password = Hash::make($request->password);
        $user->save();

        return response()->json([
            'status' => 'success',
            'message' => trans('common.updated'),
            'data' => new UserResource($user)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return response()->json([
            'status' => 'success',
            'message' => trans('common.trashed'),
        ]);
    }

    public function destroyPermanent($id)
    {
        $user = User::onlyTrashed()->findOrFail($id);
        $user->forceDelete();

        return response()->json([
            'status' => 'success',
            'message' => trans('common.permanent_deleted'),
        ]);
    }

    public function bulkDestroy(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $users = User::findMany($request->ids);

        foreach ($users as $user) {
            $user->delete();
        }

        return response()->json([
            'status' => 'success',
            'message' => trans('common.trashed'),
        ]);
    }

    public function bulkDestroyPermanent(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $users = User::onlyTrashed()->findMany($request->ids);

        foreach ($users as $user) {
            $user->forceDelete();
        }

        return response()->json([
            'status' => 'success',
            'message' => trans('common.permanent_deleted'),
        ]);
    }

    public function restore($id)
    {
        $admin = User::onlyTrashed()->findOrFail($id);
        $admin->restore();

        return response()->json([
            'status' => 'success',
            'message' => trans('common.restored'),
        ]);
    }
}
