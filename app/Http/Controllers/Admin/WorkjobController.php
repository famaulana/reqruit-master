<?php

namespace App\Http\Controllers\Admin;

use App\Models\Workjob;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\JobfunctionService;
use App\Http\Resources\WorkjobSimpleResource;
use App\Repositories\WorkjobRepository;
use App\Services\CityService;
use App\Services\WorkjobService;

class WorkjobController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $admin = $request->user();
            if ($admin->primary) {
                return $next($request);
            }
            
            if (in_array('manage-job', $this->getPermissions($admin))) {
                return $next($request);
            } else {
                return redirect()->route('admin.home');
            }
        });
    }

    private function _filters($request)
    {
        return [
            'keyword' => $request->keyword,

            'identifier' => $request->identifier,
            'title' => $request->title,

            'type' => $request->type,
            'job_types' => $request->job_types,

            'country_id' => $request->country_id,
            'city_id' => $request->city_id,
            'cities' => $request->cities,

            'jobfunction_id' => $request->jobfunction_id,
            'jobfunctions' => $request->jobfunctions,

            'company_id' => $request->company_id,
            'companies' => $request->companies,

            'skills' => $request->skills,

            'salary_min' => $request->salary_min,
            'salary_max' => $request->salary_max,
            'salary_duration' => $request->salary_duration,

            'work_experience' => $request->work_experience,

            'bonus_salary' => $request->bonus_salary,
            'bonus_salary_min' => $request->bonus_salary_min,
            'bonus_salary_max' => $request->bonus_salary_max,
            'bonus_salary_duration' => $request->bonus_salary_duration,

            'job_posting_status' => $request->job_posting_status,
            'status' => $request->status,

            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $jobRepository = new WorkjobRepository();
        $jobRepository->adminListRequestValidation($request);

        $jobs = Workjob::with(['city', 'job_function']);

        $jobData = $jobRepository->search($jobs, $this->_filters($request))->paginate(50);

        $data = [
            'city_select' => CityService::optionSelect(),
            'job_type_select' => WorkjobService::jobTypeOptionSelect(),
            'job_function_select' => JobfunctionService::optionSelect(),
            'job_posting_status_select' => WorkjobService::jobPostingStatusOptionSelect(),
            'job_status_select' => WorkjobService::jobStatusOptionSelect(),
            'jobs' => WorkjobSimpleResource::collection($jobData),
            'total' => $jobData->total(),
            'per_page' => $jobData->perPage(),
            'current_page' => $jobData->currentPage(),
            'total_page' => $jobData->lastPage(),
            'displayed_start' => (($jobData->currentPage() - 1) * $jobData->perPage()) + 1,
            'displayed_end' => ($jobData->currentPage() == $jobData->lastPage()) ? $jobData->total() : $jobData->currentPage() * $jobData->perPage()
        ];

        if ($request->ajax()) {
            return $this->successResponse('Job list', $data);
        }

        return view('admin.work-jobs.index', compact('data'));
    }

    /**
     * Trashed work jobs
     *
     * @param Request $request
     * @return Mixed Json or blade view
     */
    public function trashed(Request $request)
    {
        $jobRepository = new WorkjobRepository();
        $jobRepository->adminListRequestValidation($request);

        $jobs = Workjob::with(['city', 'job_function'])->onlyTrashed();

        $jobData = $jobRepository->search($jobs, $this->_filters($request))->paginate(50);

        $data = [
            'city_select' => CityService::optionSelect(),
            'job_type_select' => WorkjobService::jobTypeOptionSelect(),
            'job_function_select' => JobfunctionService::optionSelect(),
            'job_posting_status_select' => WorkjobService::jobPostingStatusOptionSelect(),
            'job_status_select' => WorkjobService::jobStatusOptionSelect(),
            'jobs' => WorkjobSimpleResource::collection($jobData),
            'total' => $jobData->total(),
            'per_page' => $jobData->perPage(),
            'current_page' => $jobData->currentPage(),
            'total_page' => $jobData->lastPage(),
            'displayed_start' => (($jobData->currentPage() - 1) * $jobData->perPage()) + 1,
            'displayed_end' => ($jobData->currentPage() == $jobData->lastPage()) ? $jobData->total() : $jobData->currentPage() * $jobData->perPage()
        ];

        if ($request->ajax()) {
            return $this->successResponse('Trashed jobs', $data);
        }

        return view('admin.work-jobs.trashed', compact('data'));
    }


    /**
     * Display the specified resource.
     *
     * @param  Workjob  $job
     * @return \Illuminate\Http\Response
     */
    public function show(Workjob $job)
    {
        return view('admin.work-jobs.show', compact('job'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Workjob $job
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Workjob $job)
    {
        $job->delete();

        if ($request->ajax()) {
            return $this->successResponse(trans('common.trashed'), []);
        }

        session()->flash('flash_alert', [
            'level' => 'success',
            'message' => trans('common.trashed'),
        ]);

        return redirect()->route('admin.jobs.index');
    }

    /**
     * Destroy permanent jobs
     *
     * @param Int $id
     * @return Json response
     */
    public function destroyPermanent(Int $id)
    {
        $job = Workjob::onlyTrashed()->findOrFail($id);
        $job->forceDelete();

        return $this->successResponse(trans('common.permanent_deleted'), []);
    }

    /**
     * Bulk destroy jobs
     *
     * @param Request $request
     * @return Json response
     */
    public function bulkDestroy(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $jobs = Workjob::findMany($request->ids);

        foreach ($jobs as $job) {
            $job->delete();
        }

        return $this->successResponse(trans('common.trashed'), []);
    }

    /**
     * Bulk destroy permanent jobs
     *
     * @param Request $request
     * @return Json response
     */
    public function bulkDestroyPermanent(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $jobs = Workjob::onlyTrashed()->findMany($request->ids);

        foreach ($jobs as $job) {
            $job->forceDelete();
        }

        return $this->successResponse(trans('common.permanent_deleted'), []);
    }

    /**
    * Restore job from trashed
    *
    * @param Int $id
    * @return Json response
    */
    public function restore(Int $id)
    {
        $job = Workjob::onlyTrashed()->findOrFail($id);
        $job->restore();

        return $this->successResponse(trans('common.restored'), []);
    }
}
