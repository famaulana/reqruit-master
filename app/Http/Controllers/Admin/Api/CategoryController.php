<?php

namespace App\Http\Controllers\Admin\Api;

use App\Models\Category;
use App\Helpers\PathHelper;
use App\Helpers\ImageHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\CategoryResource;

class CategoryController extends Controller
{
    protected $sizing;

    public function __construct()
    {
        $this->sizing = ['type' => 'fit', 'size' => 100];
    }

    public function index(Request $request)
    {
        $categoryIds = [];
        // if ($request->has('brand_id')) {
        //     $brand = Brand::findOrFail($request->brand_id);
        //     foreach ($brand->categories as $category) {
        //         $categoryIds[] = $category->id;
        //     }
        // }

        return $this->jsonUnescaped([
            'status' => 'success',
            'categories' => $this->_getTree($categoryIds)
        ]);
    }

    public function trashed(Request $request)
    {
        $this->validate($request, [
            'keyword' => 'nullable|string',
            'name' => 'nullable|string',
            'title' => 'nullable|string',
            'start_date' => 'nullable|string|date_format:Y-m-d',
            'end_date' => 'nullable|string|date_format:Y-m-d'
        ]);

        $categories = Category::onlyTrashed()->select('*');

        if ($request->has('keyword')) {
            $categories = $categories->where('name', 'like', '%' . $request->keyword . '%')
                ->orWhere('title', 'like', '%' . $request->keyword . '%');
        }

        if ($request->has('start_date') && $request->has('end_date')) {
            $categories = $categories->where('created_at', '>=', $request->start_date . ' 00 : 00:00')
                ->where('created_at', '<=', $request->end_date . ' 23 : 59:59');
        }

        if ($request->has('name')) {
            $categories = $categories->where('name', 'like', '%' . $request->name . '%');
        }

        if ($request->has('title')) {
            $categories = $categories->where('title', 'like', '%' . $request->title . '%');
        }

        $categories = $categories->orderBy('created_at', 'DESC')->paginate(50);

        $data = [
            'categories' => CategoryResource::collection($categories),
            'total' => $categories->total(),
            'per_page' => $categories->perPage(),
            'current_page' => $categories->currentPage(),
            'total_page' => $categories->lastPage(),
            'displayed_start' => (($categories->currentPage() - 1) * $categories->perPage()) + 1,
            'displayed_end' => ($categories->currentPage() == $categories->lastPage()) ? $categories->total() : $categories->currentPage() * $categories->perPage()
        ];

        return $this->jsonUnescaped([
            'status' => 'success',
            'data' => $data
        ]);
    }

    private function _getTree($categoryIds = [])
    {
        $categories = Category::with(['childs'])->where('parent_id', 0)->get();
        
        return $this->_buildTree($categories, $categoryIds);
    }

    private function _buildTree($categories, $categoryIds)
    {
        $tree = [];
        foreach ($categories as $category) {
            $tree[] = $this->_buildTreeArray($category, $categoryIds);
        }

        return $tree;
    }

    private function _buildTreeArray($category, $categoryIds)
    {
        $array = [
            'id' => $category->id,
            'text' => $category->name,
            'opened' => true,
            'selected' => (in_array($category->id, $categoryIds)) ? true : false,
            'data' => new CategoryResource($category),
        ];

        if ($category->childs !== null) {
            $array['children'] = $this->_buildTree($category->childs, $categoryIds);
        }

        return $array;
    }

    public function all()
    {
        $categories = Category::orderBy('name', 'ASC')->get();

        return $this->jsonUnescaped([
            'status' => 'success',
            'data' => CategoryResource::collection($categories)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'parent_id' => 'nullable|numeric',
            'name' => 'required|string|unique:categories',
            'slug' => 'required|string|unique:categories',
            'page_title' => 'required|string|unique:categories',
            'icon' => 'nullable|image|max:100',
            'content' => 'required|string',
            'featured_image' => 'nullable|image|max:500',
            'meta_title' => 'required|string|unique:categories',
            'meta_description' => 'required|string|unique:categories',
        ]);

        $category = Category::create([
            'name' => $request->name,
            'slug' => $request->slug,
            'page_title' => $request->page_title,
            'content' => $request->content,
            'meta_title' => $request->meta_title,
            'meta_description' => $request->meta_description,
        ]);

        if ($request->has('parent_id')) {
            $category->parent_id = $request->parent_id;
            $category->save();
        }

        if ($request->icon != '') {
            $path = PathHelper::category_icon_path($category->id);

            $fileName = ImageHelper::processImg($request->icon, $path, $this->sizing);
            $category->icon = $fileName;
            $category->save();
        }

        if ($request->featured_image != '') {
            $path = PathHelper::category_featured_image_path($category->id);

            $fileName = ImageHelper::processImg($request->featured_image, $path);
            $category->featured_image = $fileName;
            $category->save();
        }

        return $this->jsonUnescaped([
            'status' => 'success',
            'message' => trans('common.saved'),
            'data' => $this->_getTree()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        $allCategories = Category::where('id', '!=', $id)->get();
        
        return $this->jsonUnescaped([
            'status' => 'success',
            'message' => trans('common.saved'),
            'data' => new CategoryResource($category),
            'all_categories' => $allCategories
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:categories,name,'.$id,
            'slug' => 'required|string|unique:categories,slug,'.$id,
            'page_title' => 'required|string|unique:categories,page_title,'.$id,
            'parent_id' => 'required',
            'icon' => 'image|max:100',
            'content' => 'required|string',
            'featured_image' => 'image|max:500',
            'meta_title' => 'required|string|unique:categories,meta_title,'.$id,
            'meta_description' => 'required|string|unique:categories,meta_description,'.$id,
        ]);

        $category = Category::findOrFail($id);

        $category->update([
            'name' => $request->name,
            'slug' => $request->slug,
            'page_title' => $request->page_title,
            'parent_id' => $request->parent_id,
            'content' => $request->content,
            'meta_title' => $request->meta_title,
            'meta_description' => $request->meta_description,
        ]);

        if ($request->icon != '') {
            $path = PathHelper::category_icon_path($category->id);

            PathHelper::deleteFile($path . $category->icon);

            $fileName = ImageHelper::processImg($request->icon, $path, $this->sizing);
            $category->icon = $fileName;
            $category->save();
        }

        if ($request->featured_image != '') {
            $path = PathHelper::category_featured_image_path($category->id);

            PathHelper::deleteFile($path . $category->featured_image);

            $fileName = ImageHelper::processImg($request->featured_image, $path);
            $category->featured_image = $fileName;
            $category->save();
        }

        return $this->jsonUnescaped([
            'status' => 'success',
            'message' => trans('common.updated'),
            'data' => $this->_getTree()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);

        $parentId = $category->parent_id;
        $childs = $category->childs;

        // add childs category into its parent;
        foreach ($childs as $child) {
            if ($parentId === null) {
                $child->parent_id = null;
            } else {
                $child->parent_id = $parentId;
            }
            $child->save();
        }

        $category->delete();

        return $this->jsonUnescaped([
            'status' => 'success',
            'message' => trans('common.trashed'),
            'data' => $this->_getTree()
        ]);
    }

    public function destroyPermanent($id)
    {
        $categories = Category::onlyTrashed()->findOrFail($id);
        $categories->forceDelete();

        return response()->json([
            'status' => 'success',
            'message' => trans('common.permanent_deleted'),
        ]);
    }

    public function bulkDestroyPermanent(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $categories = Category::onlyTrashed()->findMany($request->ids);

        foreach ($categories as $category) {
            $category->forceDelete();
        }

        return response()->json([
            'status' => 'success',
            'message' => trans('common.permanent_deleted'),
        ]);
    }

    public function restore($id)
    {
        $category = Category::onlyTrashed()->findOrFail($id);

        $category->restore();

        return response()->json([
            'status' => 'success',
            'message' => trans('common.restored'),
        ]);
    }

    public function storeContentImage(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:700',
        ]);

        $path = PathHelper::category_content_image_path();
        $fileName = ImageHelper::processImg($request->image, $path);

        return $this->jsonUnescaped([
            'status' => 'success',
            'url' => ImageHelper::getImageUrl($path, $fileName)
        ]);
    }

    public function search(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string'
        ]);

        $categories = Category::where('name', 'like', '%' . $request->name . '%')->get();

        return $this->jsonUnescaped([
            'status' => 'success',
            'message' => 'Category search result',
            'data' => CategoryResource::collection($categories)
        ]);
    }
}
