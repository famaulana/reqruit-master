<?php

namespace App\Http\Controllers\Admin\Api;

use App\Models\Config;
use App\Models\Permission;
use App\Helpers\PathHelper;
use App\Helpers\ImageHelper;
use App\Helpers\ConfigHelper;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\ConfigRequest;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

class ConfigController extends Controller
{
    public function update(ConfigRequest $request)
    {
        return response()->json([
            'status' => 'success',
            'message' => 'Configuration updated',
        ]);
    }

    public function dataConfigs()
    {
        return response()->json([
            'status' => 'success',
            'data' => [
                'admin_permissions' => Permission::select('id', 'name', 'display_name')->get()->toArray(),
            ]
        ]);
    }
}
