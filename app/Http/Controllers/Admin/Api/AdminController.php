<?php

namespace App\Http\Controllers\Admin\Api;

use App\Models\Admin;
use App\Models\Permission;
use App\Helpers\PathHelper;
use App\Helpers\ImageHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\AdminResource;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\StoreAdminRequest;
use App\Http\Requests\UpdateAdminRequest;

class AdminController extends Controller
{
    protected $perPage;
    protected $sizing;

    public function __construct()
    {
        $this->perPage = 50;
        $this->sizing = ['type' => 'fit', 'size' => 100];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->validate($request, [
            'keyword' => 'nullable|string',
            'name' => 'nullable|string',
            'email' => 'nullable|email',
            'primary' => 'nullable|integer|in:0,1',
            'start_date' => 'nullable|string|date_format:Y-m-d',
            'end_date' => 'nullable|string|date_format:Y-m-d'
        ]);

        $admins = Admin::select('*');

        $searchQuery = $this->_searchQuery($request, $admins);
        $adminData = $searchQuery['search_data'];

        $data = [
            'admins' => AdminResource::collection($adminData),
            'total' => $adminData->total(),
            'per_page' => $adminData->perPage(),
            'current_page' => $adminData->currentPage(),
            'total_page' => $adminData->lastPage(),
            'displayed_start' => (($adminData->currentPage() - 1) * $adminData->perPage()) + 1,
            'displayed_end' => ($adminData->currentPage() == $adminData->lastPage()) ? $adminData->total() : $adminData->currentPage() * $adminData->perPage()
        ];

        return $this->jsonUnescaped([
            'status' => 'success',
            'data' => $data
        ]);
    }

    public function trashed(Request $request)
    {
        $this->validate($request, [
            'keyword' => 'nullable|string',
            'name' => 'nullable|string',
            'email' => 'nullable|email',
            'primary' => 'nullable|integer|in:0,1',
            'start_date' => 'nullable|string|date_format:Y-m-d',
            'end_date' => 'nullable|string|date_format:Y-m-d'
        ]);

        $admins = Admin::onlyTrashed()->select('*');

        $searchQuery = $this->_searchQuery($request, $admins);
        $adminData = $searchQuery['search_data'];

        $data = [
            'admins' => AdminResource::collection($adminData),
            'total' => $adminData->total(),
            'per_page' => $adminData->perPage(),
            'current_page' => $adminData->currentPage(),
            'total_page' => $adminData->lastPage(),
            'displayed_start' => (($adminData->currentPage() - 1) * $adminData->perPage()) + 1,
            'displayed_end' => ($adminData->currentPage() == $adminData->lastPage()) ? $adminData->total() : $adminData->currentPage() * $adminData->perPage()
        ];

        return $this->jsonUnescaped([
            'status' => 'success',
            'data' => $data
        ]);
    }

    private function _searchQuery(Request $request, Object $admins)
    {
        if ($request->has('keyword')) {
            $admins = $admins->where('name', 'like', '%' . $request->keyword . '%')
                ->orWhere('email', 'like', '%' . $request->keyword . '%');
        }

        if ($request->has('start_date') && $request->has('end_date')) {
            $admins = $admins->where('created_at', '>=', $request->start_date . ' 00 : 00:00')
                ->where('created_at', '<=', $request->end_date . ' 23 : 59:59');
        }

        if ($request->has('name')) {
            $admins = $admins->where('name', 'like', '%' . $request->name . '%');
        }

        if ($request->has('email')) {
            $admins = $admins->where('email', 'like', '%' . $request->email_address . '%');
        }

        if ($request->has('primary')) {
            $admins = $admins->where('primary', $request->primary);
        }

        return [
            'search_data' => $admins->orderBy('created_at', 'DESC')->paginate($this->perPage)
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAdminRequest $request)
    {
        $admin = Admin::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'primary' => ($request->primary == 'true') ? 1 : 0
        ]);

        if ($request->photo != '') {
            $path = PathHelper::admin_photo_path($admin->id);

            $fileName = ImageHelper::processImg($request->photo, $path, $this->sizing, '', 'local');
            $admin->photo = $fileName;
            $admin->save();
        }

        if ($request->permissions != '') {
            $admin->permissions()->sync(explode(',', $request->permissions));
        }

        return $this->jsonUnescaped([
            'status' => 'success',
            'message' => trans('common.saved'),
            'data' => new AdminResource($admin)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        $admindata = new AdminResource($admin);

        return $this->jsonUnescaped([
            'status' => 'success',
            'admin' => $admindata,
            'admin_permissions' => Permission::select('id', 'name', 'display_name')->get()->toArray()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAdminRequest $request, Admin $admin)
    {
        $request['primary'] = ($request->primary == 'true') ? 1 : 0;
        $admin->update($request->all());
        if ($request->permissions != '') {
            $admin->permissions()->sync(explode(',', $request->permissions));
        }
        
        if ($request->photo_profile != '') {
            $path = PathHelper::admin_photo_path($admin->id);
            PathHelper::deleteFile($path . $admin->photo, 'local');

            $fileName = ImageHelper::processImg($request->photo_profile, $path, $this->sizing, '', 'local');
            $admin->photo = $fileName;
            $admin->save();
        }

        return $this->jsonUnescaped([
            'status' => 'success',
            'message' => trans('common.updated'),
            'data' => new AdminResource($admin)
        ]);
    }

    public function updatePassword(Request $request, $id)
    {
        $this->validate($request, [
            'new_password' => 'required|string|min:8|confirmed',
            'new_password_confirmation' => 'required|string|min:8'
        ]);

        $admin = Admin::findOrFail($id);
        $admin->password = Hash::make($request->new_password);
        $admin->save();

        return $this->jsonUnescaped([
            'status' => 'success',
            'message' => trans('common.updated'),
            'data' => new AdminResource($admin)
        ]);
    }

    public function destroy(Admin $admin)
    {
        $admin->delete();

        return response()->json([
            'status' => 'success',
            'message' => trans('common.trashed'),
        ]);
    }

    public function destroyPermanent($id)
    {
        $admin = Admin::onlyTrashed()->findOrFail($id);
        $admin->forceDelete();

        return response()->json([
            'status' => 'success',
            'message' => trans('common.permanent_deleted'),
        ]);
    }

    public function bulkDestroy(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $admins = Admin::findMany($request->ids);

        foreach ($admins as $admin) {
            $admin->delete();
        }

        return response()->json([
            'status' => 'success',
            'message' => trans('common.trashed'),
        ]);
    }

    public function bulkDestroyPermanent(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $admins = Admin::onlyTrashed()->findMany($request->ids);

        foreach ($admins as $admin) {
            $admin->forceDelete();
        }

        return response()->json([
            'status' => 'success',
            'message' => trans('common.permanent_deleted'),
        ]);
    }

    public function restore($id)
    {
        $admin = Admin::onlyTrashed()->findOrFail($id);
        $admin->restore();

        return response()->json([
            'status' => 'success',
            'message' => trans('common.restored'),
        ]);
    }

    public function avatar($id)
    {
        $admin = Admin::findOrFail($id);

        if ($admin->photo != '') {
            $path = PathHelper::admin_photo_path($id) . $admin->photo;
            return ImageHelper::showImage($path, 'storage');
        }

        return ImageHelper::showImage('images/default/default-avatar.png', 'public');
    }
}
