<?php

namespace App\Http\Controllers\Admin\Api;

use App\Models\Admin;
use Illuminate\Http\Request;
use App\Models\OauthAccessToken;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\AdminResource;
use Illuminate\Support\Facades\Password;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class AuthController extends Controller
{
    use SendsPasswordResetEmails;

    public function broker()
    {
        return Password::broker('admins');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        // Find existing token
        $admin = Admin::where('email', $request->email)->first();
        if ($admin === null) {
            $response = [
                'status' => 'error',
                'message' => trans('auth.account_not_found'),
            ];
            return response()->json($response);
        }

        $tokens = OauthAccessToken::where('user_id', $admin->id)->where('name', 'Admin')->get();
        if ($tokens !== null) {
            foreach ($tokens as $token) {
                $token->delete();
            }
        }

        if (Hash::check($request->password, $admin->password)) {
            $accessToken = $admin->createToken('Admin')->accessToken;
            $response = [
                'status' => 'success',
                'message' => trans('auth.login_success'),
                'token' => $accessToken,
            ];

            return response()->json($response);
        } else {
            $response = [
                'status' => 'error',
                'message' => trans('auth.wrong_password'),
            ];
            return response()->json($response);
        }
    }

    public function logout(Request $request)
    {
        $token = OauthAccessToken::where('user_id', $request->user()->id)
            ->where('name', 'Admin')
            ->first();

        if ($token !== null) {
            $token->delete();
            $response = ['status' => 'success'];
        } else {
            $response = [
                'status' => 'error',
                'message' => 'Unauthorized'
            ];
        }

        return response()->json($response);
    }

    public function adminData(Request $request)
    {
        return response()->json([
            'status' => 'success',
            'admin' => new AdminResource($request->user())
        ]);
    }

    public function sendResetPasswordLink(Request $request)
    {
        $this->validateEmail($request);

        $admin = Admin::where('email', $request->email)->first();
        if ($admin === null) {
            return response()->json([
                'status' => 'error',
                'message' => trans('auth.account_not_found')
            ]);
        }

        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );

        if ($response == Password::RESET_LINK_SENT) {
            return response()->json([
                'status' => 'success',
                'message' => trans('auth.reset_password.link_send_success')
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => trans('auth.reset_password.link_send_error')
            ]);
        }
    }

    public function updatePassword(Request $request)
    {
        $this->validate($request, [
            'token' => 'required|string',
            'email' => 'required|email',
            'password' => 'required|string|min:8',
        ]);

        $resetTokenQuery = DB::table('password_resets_admin')->where('email', $request->email);
        $resetToken = $resetTokenQuery->first();
        
        if ($resetToken === null) {
            return response()->json([
                'status' => 'error',
                'message' => trans('auth.reset_password.token_mismatch')
            ]);
        }

        if (!Hash::check($request->token, $resetToken->token)) {
            return response()->json([
                'status' => 'error',
                'message' => trans('auth.reset_password.token_mismatch')
            ]);
        }


        $admin = Admin::where('email', $request->email)->first();
        if ($admin === null) {
            return response()->json([
                'status' => 'error',
                'message' => trans('auth.account_not_found')
            ]);
        }

        $admin->password = Hash::make($request->password);
        $admin->save();
        $resetTokenQuery->delete();

        return response()->json([
            'status' => 'success',
            'message' => trans('auth.reset_password.update_success')
        ]);
    }
}
