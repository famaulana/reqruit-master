<?php

namespace App\Http\Controllers\Admin;

use stdClass;
use App\Models\City;
use App\Models\Menu;
use App\Models\Article;
use App\Models\Company;
use App\Models\Province;
use App\Models\Permission;
use App\Helpers\PathHelper;
use App\Models\Subdistrict;
use App\Helpers\ImageHelper;
use Illuminate\Http\Request;
use App\Services\LocaleService;
use App\Services\CurrencyService;
use App\Http\Controllers\Controller;
use App\Http\Requests\ConfigRequest;
use App\Http\Resources\MenuResource;
use Illuminate\Support\Facades\File;
use App\Theming\Helpers\ThemingHelper;
use App\Repositories\AppConfigRepository;

class ConfigController extends Controller
{
    /**
     * AppConfigRepository instance
     *
     * @var App\Repositories\AppConfigReposity
     */
    private $appConfig;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (in_array('manage-config', $this->getPermissions($request->user()))) {
                return $next($request);
            } else {
                return redirect()->route('admin.home');
            }
        });

        $this->appConfig = new AppConfigRepository();
    }

    /**
     * General Configuration controller
     *
     * @return Blade view
     */
    public function general()
    {
        $data = [
            'site_name' => $this->appConfig->getConfig('site_name'),
            'site_email' => $this->appConfig->getConfig('site_email'),
            'site_contact_email' => $this->appConfig->getConfig('site_contact_email'),
            // 'site_phone' => $this->appConfig->getConfig('site_phone'),
            // 'site_wa' => $this->appConfig->getConfig('site_wa'),
            // 'store_street' => $this->appConfig->getConfig('store_street'),
            // 'store_province_id' => $this->appConfig->getConfig('store_province_id'),
            // 'store_city_id' => $this->appConfig->getConfig('store_city_id'),
            // 'store_subdistrict_id' => $this->appConfig->getConfig('store_subdistrict_id'),
        ];

        return view('admin.configs.general', $data);
    }

    /**
     * Logo Configuration controller
     *
     * @return Blade view
     */
    public function logo()
    {
        $fileUrls = $this->appConfig->configFileUrls();

        $data = [
            'logo1' => $this->appConfig->getConfig('logo1'),
            'logo2' => $this->appConfig->getConfig('logo2'),
            'logo3' => $this->appConfig->getConfig('logo3'),
            'logo_square' => $this->appConfig->getConfig('logo_square'),
            'favicon' => $this->appConfig->getConfig('favicon'),
            'admin_logo' => $this->appConfig->getConfig('admin_logo'),
            'frontend_logo' => $this->appConfig->getConfig('frontend_logo'),
            'email_logo' => $this->appConfig->getConfig('email_logo'),
            'invoice_logo' => $this->appConfig->getConfig('invoice_logo'),
            'light_logo' => $this->appConfig->getConfig('light_logo'),
            'dark_logo' => $this->appConfig->getConfig('dark_logo'),
            'logo1_url' => $fileUrls['logo1'],
            'logo2_url' => $fileUrls['logo2'],
            'logo3_url' => $fileUrls['logo3'],
            'logo_square_url' => $fileUrls['logo_square'],
            'favicon_url' => $fileUrls['favicon'],
        ];

        return view('admin.configs.logo', $data);
    }

    /**
     * Homepage controller
     *
     * @return Blade view
     */
    public function homepage()
    {
        $data = [
            'home_meta_title' => $this->appConfig->getConfig('home_meta_title'),
            'home_meta_description' => $this->appConfig->getConfig('home_meta_description'),
            'home_banner' => $this->appConfig->getConfig('home_banner'),
            'home_banner_url' => $this->appConfig->getFileUrl('home_banner'),
            'home_banner_mobile' => $this->appConfig->getConfig('home_banner_mobile'),
            'home_banner_mobile_url' => $this->appConfig->getFileUrl('home_banner_mobile'),
            'intro_right_image' => $this->appConfig->getConfig('intro_right_image'),
            'intro_right_image_url' => $this->appConfig->getFileUrl('intro_right_image'),
            'intro_left_image' => $this->appConfig->getConfig('intro_left_image'),
            'intro_left_image_url' => $this->appConfig->getFileUrl('intro_left_image'),
            'section_about_btn_link' => $this->appConfig->getConfig('section_about_btn_link'),
            'section_about_yt_link' => $this->appConfig->getConfig('section_about_yt_link'),
            'section_about_video_image' => $this->appConfig->getConfig('section_about_video_image'),
            'section_about_video_image_url' => $this->appConfig->getFileUrl('section_about_video_image'),
            'home_join_banner' => $this->appConfig->getConfig('home_join_banner'),
            'home_join_banner_url' => $this->appConfig->getFileUrl('home_join_banner'),
            'home_count_1_value' => $this->appConfig->getConfig('home_count_1_value'),
            'home_count_1_label' => $this->appConfig->getConfig('home_count_1_label'),
            'home_count_2_value' => $this->appConfig->getConfig('home_count_2_value'),
            'home_count_2_label' => $this->appConfig->getConfig('home_count_2_label'),
            'home_count_3_value' => $this->appConfig->getConfig('home_count_3_value'),
            'home_count_3_label' => $this->appConfig->getConfig('home_count_3_label'),
            'home_count_4_value' => $this->appConfig->getConfig('home_count_4_value'),
            'home_count_4_label' => $this->appConfig->getConfig('home_count_4_label'),
        ];

        return view('admin.configs.homepage', $data);
    }

    /**
    * Blog configuration controller
    *
    * @return Blade view
    */
    public function blog()
    {
        $ftPostIds = $this->appConfig->getConfig('featured_posts');
        $popPostIds = $this->appConfig->getConfig('popular_posts');
        $ftPostIdsOrdered = implode(',', $ftPostIds);
        $popPostIdsOrdered = implode(',', $popPostIds);

        $data = [
            'ft_posts_data' => $ftPostIdsOrdered == '' ? '[]' : Article::whereIn('id', $ftPostIds)->orderByRaw("FIELD(id, $ftPostIdsOrdered)")->get(),
            'featured_post_ids' => $ftPostIds,
            'pop_posts_data' => $popPostIdsOrdered == '' ? '[]' : Article::whereIn('id', $popPostIds)->orderByRaw("FIELD(id, $popPostIdsOrdered)")->get(),
            'pop_post_ids' => $popPostIds,
        ];

        return view('admin.configs.blog', $data);
    }

    /**
    * auth controller
    *
    * @return Blade view
    */
    public function auth()
    {
        $data = [
            'auth_image' => $this->appConfig->getConfig('auth_image'),
            'auth_image_url' => $this->appConfig->getFileUrl('auth_image'),
            'auth_image_employer' => $this->appConfig->getConfig('auth_image_employer'),
            'auth_image_employer_url' => $this->appConfig->getFileUrl('auth_image_employer'),
        ];

        return view('admin.configs.auth', $data);
    }

    /**
     * menu Configuration controller
     *
     * @return Blade view
     */
    public function menu()
    {
        $menus = Menu::all();
        
        $data = [
            'menus' => MenuResource::collection($menus),
            'top_menu' => $this->appConfig->getConfig('top_menu'),
            'footer_menu' => $this->appConfig->getConfig('footer_menu'),
        ];

        return view('admin.configs.menu', $data);
    }

    /**
     * Assessment Configuration controller
     *
     * @return Blade view
     */
    public function assessment()
    {
        $data = [
            'english_test_q_count' => $this->appConfig->getConfig('english_test_q_count'),
            'english_test_can_retest_after_days' => $this->appConfig->getConfig('english_test_can_retest_after_days'),
            'english_test_session_2_timer' => $this->appConfig->getConfig('english_test_session_2_timer'),
            'personal_test_can_retest_after_days' => $this->appConfig->getConfig('personal_test_can_retest_after_days'),
        ];

        return view('admin.configs.assessment', $data);
    }

    /**
    * Job Filters Configuration controller
    *
    * @return Blade view
    */
    public function jobFilters()
    {
        $companyIds = $this->appConfig->getConfig('jf_companies');
        $idsOrdered = implode(',', $companyIds);
        $data = [
            'companies' => $idsOrdered == '' ? '[]' : Company::whereIn('id', $companyIds)->orderByRaw("FIELD(id, $idsOrdered)")->get(),
            'jf_companies' => $companyIds,
        ];

        return view('admin.configs.job-filters', $data);
    }

    /**
     * Footer Configuration controller
     *
     * @return Blade view
     */
    public function footer()
    {
        $data = [
            'footer_content' => [
                'the_content' => $this->appConfig->getConfig('footer_content')
            ],
            'footer_bg_image_enable' => $this->appConfig->getConfig('footer_bg_image_enable'),
            'footer_bg_image' => $this->appConfig->getConfig('footer_bg_image'),
            'footer_bg_image_url' => $this->appConfig->getFileUrl('footer_bg_image'),
        ];

        return view('admin.configs.footer', $data);
    }

    /**
     * Social Configuration controller
     *
     * @return Blade view
     */
    public function social()
    {
        $data = [
            'youtube_link' => $this->appConfig->getConfig('youtube_link'),
            'instagram_link' => $this->appConfig->getConfig('instagram_link'),
            'twitter_link' => $this->appConfig->getConfig('twitter_link'),
            'facebook_link' => $this->appConfig->getConfig('facebook_link'),
        ];

        return view('admin.configs.social', $data);
    }

    /**
     * Formatting text Configuration controller
     *
     * @return Blade view
     */
    public function locale()
    {
        $baseCurrency = $this->appConfig->getConfig('base_currency');

        $configs = [
            'locale' => $this->appConfig->getConfig('locale'),
        ];
        $locales = LocaleService::locales();

        $data = [
            'configs' => $configs,
            'locales' => $locales,
        ];

        return view('admin.configs.locale', $data);
    }

    /**
     * Additional Javascript code Configuration controller
     *
     * @return Blade view
     */
    public function javascript()
    {
        $configs = [
            'javascript' => $this->appConfig->getConfig('javascript')
        ];

        return view('admin.configs.javascript', compact('configs'));
    }

    /**
     * Update config controller
     *
     * @param ConfigRequest $request
     * @return Json response
     */
    public function update(ConfigRequest $request)
    {
        $optionNames = array_keys(config('app_configs_default'));
        $updated = false;

        foreach ($request->validated() as $option => $value) {
            if (in_array($option, $optionNames)) {
                // Set config value at runtime
                config(['app_configs.' . $option => $value]);
                $updated = true;
            }

            if ($option == 'theme') {
                $themeInfo = ThemingHelper::themeInfo($value);
                $themeName = $themeInfo['name'];
                $themeDomain = $themeInfo['domain'];
                $themeNamespace = $themeInfo['namespace'];
                File::put(
                    base_path('config/active_theme.php'),
                    "<?php 
return [
    'name' => '$themeName',
    'domain' => '$themeDomain',
    'namespace' => '$themeNamespace',
];"
                );
            }
        }

        // Update store address cache
        if (array_key_exists('store_street', $request->validated())) {
            $province = Province::find(config('app_configs.store_province_id'));
            config(['app_configs.store_province_name' => ($province !== null) ? $province->name : '']);

            $city = City::find(config('app_configs.store_city_id'));
            config(['app_configs.store_city_name' => ($city !== null) ? $city->name : '']);

            $subdistrict = Subdistrict::find(config('app_configs.store_subdistrict_id'));
            config(['app_configs.store_subdistrict_name' => ($subdistrict !== null) ? $subdistrict->name : '']);
        }

        if ($updated) {
            $text = '<?php return ' . var_export(config('app_configs'), true) . ';';
            file_put_contents(config_path('app_configs.php'), $text);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Configuration updated'
        ]);
    }

    /**
     * Update configuration that has file option
     *
     * @param ConfigRequest $request
     * @return Json response
     */
    public function updateFile(ConfigRequest $request)
    {
        $updated = false;

        foreach ($request->validated() as $option => $value) {
            if ($request->file($option) != '') {
                $oldImage = $this->appConfig->getConfig($option);

                $newImage = $request->file($option);
                $imagePath = PathHelper::config_image_path();
                $filename = ImageHelper::processImg($newImage, $imagePath);

                // Delete old image
                if ($oldImage !== '') {
                    PathHelper::deleteFile($imagePath . $oldImage);
                }

                // Set config value at runtime
                config(['app_configs.' . $option => $filename]);
                $updated = true;
            }
        }

        $fileUrl = '';
        if ($updated) {
            $appConfigRepository = new AppConfigRepository();
            $fileUrl = $appConfigRepository->getFileUrl($option);

            $text = '<?php return ' . var_export(config('app_configs'), true) . ';';
            file_put_contents(config_path('app_configs.php'), $text);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Configuration updated',
            'file_url' => $fileUrl
        ]);
    }

    /**
     * Upload image configuration
     *
     * @param Request $request
     * @return Json response
     */
    public function uploadImage(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|image'
        ]);

        if ($request->file('image') != '') {
            $image = $request->file('image');
            $imagePath = PathHelper::config_image_path();
            $filename = ImageHelper::processImg($image, $imagePath);
            $appConfigRepository = new AppConfigRepository();
            $imageUrl = $appConfigRepository->getUrl($filename);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Image uploaded',
            'file_name' => $filename,
            'image_url' => $imageUrl
        ]);
    }

    /**
     * Get data configs
     *
     * @return Json response
     */
    public function dataConfigs()
    {
        $configs = config('app_configs');
        $others = [
            'admin_permissions' => Permission::select('id', 'name', 'display_name')->get()->toArray(),
        ];

        return response()->json([
            'status' => 'success',
            'data' => array_merge($configs, $others)
        ]);
    }

    /**
     * Update currency rates config
     *
     * @param Request $request
     * @return Json response
     */
    public function updateCurrency(Request $request)
    {
        $this->validate($request, [
            'base' => 'required|string',
            'other_currencies' => 'required|string'
        ]);

        $currencyService = new CurrencyService();
        $rates = $currencyService->getRatesAndUpdate($request->base, $request->other_currencies);

        return $this->successResponse('Success update currency', $rates);
    }
}
