<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\PathHelper;
use App\Models\StaticBlock;
use App\Helpers\ImageHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\StaticBlockResource;
use App\Repositories\StaticBlockRepository;

class StaticBlockController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (in_array('manage-static-block', $this->getPermissions($request->user()))) {
                return $next($request);
            } else {
                return redirect()->route('admin.home');
            }
        });
    }

    /**
     * Filter request
     *
     * @param Request $request
     * @return Array
     */
    private function _filters(Request $request)
    {
        return [
            'keyword' => $request->keyword,
            'name' => $request->name,
            'active' => $request->active,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $repository = new StaticBlockRepository();
        $repository->adminListRequestValidation($request);

        $blocks = StaticBlock::select('*');
        $blockData = $repository->search($blocks, $this->_filters($request))->paginate(50);

        $data = [
            'blocks' => StaticBlockResource::collection($blockData),
            'total' => $blockData->total(),
            'per_page' => $blockData->perPage(),
            'current_page' => $blockData->currentPage(),
            'total_page' => $blockData->lastPage(),
            'displayed_start' => (($blockData->currentPage() - 1) * $blockData->perPage()) + 1,
            'displayed_end' => ($blockData->currentPage() == $blockData->lastPage()) ? $blockData->total() : $blockData->currentPage() * $blockData->perPage()
        ];

        if ($request->ajax()) {
            return $this->jsonUnescaped([
                'status' => 'success',
                'data' => $data
            ]);
        }

        return view('admin.static-blocks.index', compact('data'));
    }

    public function trashed(Request $request)
    {
        $repository = new StaticBlockRepository();
        $repository->adminListRequestValidation($request);

        $blocks = StaticBlock::onlyTrashed()->select('*');
        $blockData = $repository->search($blocks, $this->_filters($request))->paginate(50);

        $data = [
            'blocks' => StaticBlockResource::collection($blockData),
            'total' => $blockData->total(),
            'per_page' => $blockData->perPage(),
            'current_page' => $blockData->currentPage(),
            'total_page' => $blockData->lastPage(),
            'displayed_start' => (($blockData->currentPage() - 1) * $blockData->perPage()) + 1,
            'displayed_end' => ($blockData->currentPage() == $blockData->lastPage()) ? $blockData->total() : $blockData->currentPage() * $blockData->perPage()
        ];

        if ($request->ajax()) {
            return $this->jsonUnescaped([
                'status' => 'success',
                'data' => $data
            ]);
        }
        
        return view('admin.static-blocks.trashed', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.static-blocks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:static_blocks',
            'content' => 'required|string',
            'active' => 'required|numeric|in:0,1',
        ]);

        $block = StaticBlock::create([
            'name' => $request->name,
            'content' => $request->content,
            'active' => $request->active,
        ]);

        session()->flash('flash_alert', [
            'level' => 'success',
            'message' => trans('common.saved'),
        ]);

        return $this->jsonUnescaped([
            'status' => 'success',
            'message' => trans('common.saved'),
            'merchant' => new StaticBlockResource($block)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\StaticBlock  $staticBlock
     * @return \Illuminate\Http\Response
     */
    public function edit(StaticBlock $staticBlock)
    {
        return view('admin.static-blocks.edit', compact('staticBlock'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\StaticBlock  $staticBlock
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StaticBlock $staticBlock)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:static_blocks,name,'.$staticBlock->id,
            'content' => 'required|string',
            'active' => 'required|numeric|in:0,1',
        ]);

        $staticBlock->update([
            'name' => $request->name,
            'content' => $request->content,
            'active' => $request->active,
        ]);

        session()->flash('flash_alert', [
            'level' => 'success',
            'message' => trans('common.updated'),
        ]);

        return $this->jsonUnescaped([
            'status' => 'success',
            'message' => trans('common.updated'),
            'block' => new StaticBlockResource($staticBlock)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\StaticBlock  $staticBlock
     * @return \Illuminate\Http\Response
     */
    public function destroy(StaticBlock $staticBlock)
    {
        $staticBlock->delete();

        return response()->json([
            'status' => 'success',
            'message' => trans('common.trashed'),
        ]);
    }

    public function destroyPermanent($id)
    {
        $block = StaticBlock::onlyTrashed()->findOrFail($id);
        $block->forceDelete();

        return response()->json([
            'status' => 'success',
            'message' => trans('common.permanent_deleted'),
        ]);
    }

    public function bulkDestroy(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $blocks = StaticBlock::findMany($request->ids);

        foreach ($blocks as $block) {
            $block->delete();
        }

        return response()->json([
            'status' => 'success',
            'message' => trans('common.trashed'),
        ]);
    }

    public function bulkDestroyPermanent(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $blocks = StaticBlock::onlyTrashed()->findMany($request->ids);

        foreach ($blocks as $block) {
            $block->forceDelete();
        }

        return response()->json([
            'status' => 'success',
            'message' => trans('common.permanent_deleted'),
        ]);
    }

    public function restore($id)
    {
        $block = StaticBlock::onlyTrashed()->findOrFail($id);
        $block->restore();

        return response()->json([
            'status' => 'success',
            'message' => trans('common.restored'),
        ]);
    }

    public function storeContentImage(Request $request)
    {
        $this->validate($request, [
            'upload' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:700',
        ]);

        $path = PathHelper::static_block_content_image_path();
        $fileName = ImageHelper::processImg($request->upload, $path);

        return $this->jsonUnescaped([
            'status' => 'success',
            'url' => ImageHelper::getImageUrl($path, $fileName)
        ]);

        // return $this->successResponse('dgsdg', []);
    }
}
