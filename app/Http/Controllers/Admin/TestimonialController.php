<?php

namespace App\Http\Controllers\Admin;

use App\Models\Testimonial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTestimonialRequest;
use App\Http\Requests\UpdateTestimonialRequest;
use App\Http\Resources\TestimonialResource;
use App\Repositories\TestimonialRepository;
use App\Services\TestimonialService;

class TestimonialController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $admin = $request->user();
            if ($admin->primary) {
                return $next($request);
            }
            
            if (in_array('manage-testimonial', $this->getPermissions($admin))) {
                return $next($request);
            } else {
                return redirect()->route('admin.home');
            }
        });
    }

    /**
     * Request filters
     *
     * @param Request $request
     * @return Array
     */
    private function _filters(Request $request)
    {
        return [
            'keyword' => $request->keyword,
            'name' => $request->name,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $repository = new TestimonialRepository();
        $repository->adminListRequestValidation($request);

        $testimonials = Testimonial::select('*');
        $testimonials = $repository->search($testimonials, $this->_filters($request))->paginate(50);

        $data = [
            'testimonials' => TestimonialResource::collection($testimonials),
            'total' => $testimonials->total(),
            'per_page' => $testimonials->perPage(),
            'current_page' => $testimonials->currentPage(),
            'total_page' => $testimonials->lastPage(),
            'displayed_start' => (($testimonials->currentPage() - 1) * $testimonials->perPage()) + 1,
            'displayed_end' => ($testimonials->currentPage() == $testimonials->lastPage()) ? $testimonials->total() : $testimonials->currentPage() * $testimonials->perPage()
        ];

        if ($request->ajax()) {
            return $this->successResponse('Testimonials', $data);
        }

        return view('admin.testimonials.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.testimonials.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\StoreTestimonialRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTestimonialRequest $request)
    {
        $service = new TestimonialService();
        $testimonial = $service->createTestimonial($request->validated());

        session()->flash('flash_alert', [
            'level' => 'success',
            'message' => trans('common.saved'),
        ]);

        return response()->json([
            'status' => 'success',
            'message' => trans('common.saved'),
            'data' => new TestimonialResource($testimonial)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function edit(Testimonial $testimonial)
    {
        return view('admin.testimonials.edit', compact('testimonial'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\UpdateTestimonialRequest  $request
     * @param  \App\Models\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTestimonialRequest $request, Testimonial $testimonial)
    {
        $testimonial->update($request->validated());
        
        if ($request->avatar_image != '') {
            $service = new TestimonialService();
            $service->processAvatar($testimonial, $request->all(), false);
        }

        session()->flash('flash_alert', [
            'level' => 'success',
            'message' => trans('common.updated'),
        ]);

        return response()->json([
            'status' => 'success',
            'message' => trans('common.updated'),
            'data' => new TestimonialResource($testimonial)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function destroy(Testimonial $testimonial)
    {
        $testimonial->delete();

        return response()->json([
            'status' => 'success',
            'message' => trans('common.permanent_deleted'),
        ]);
    }
}
