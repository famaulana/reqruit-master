<?php

namespace App\Http\Controllers\Admin;

use App\Models\Jobrole;
use App\Models\Jobfunction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\JobroleResource;
use App\Repositories\JobroleRepository;
use App\Services\JobfunctionService;

class JobroleController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $admin = $request->user();
            if ($admin->primary) {
                return $next($request);
            }
            
            if (in_array('manage-job-role', $this->getPermissions($admin))) {
                return $next($request);
            } else {
                return redirect()->route('admin.home');
            }
        });
    }

    /**
     * Request filters
     *
     * @param Request $request
     * @return Array
     */
    private function _filters(Request $request)
    {
        return [
            'keyword' => $request->keyword,
            'jobfunction_id' => $request->jobfunction_id,
            'name' => $request->name,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $jobroleRepository = new JobroleRepository();
        $jobroleRepository->adminListRequestValidation($request);

        $jobRoles = Jobrole::select('*');
        $jobRoleData = $jobroleRepository->search($jobRoles, $this->_filters($request))->paginate(50);

        $data = [
            'job_function_select' => JobfunctionService::optionSelect(),
            'job_roles' => JobroleResource::collection($jobRoleData),
            'total' => $jobRoleData->total(),
            'per_page' => $jobRoleData->perPage(),
            'current_page' => $jobRoleData->currentPage(),
            'total_page' => $jobRoleData->lastPage(),
            'displayed_start' => (($jobRoleData->currentPage() - 1) * $jobRoleData->perPage()) + 1,
            'displayed_end' => ($jobRoleData->currentPage() == $jobRoleData->lastPage()) ? $jobRoleData->total() : $jobRoleData->currentPage() * $jobRoleData->perPage()
        ];

        if ($request->ajax()) {
            return $this->successResponse('Job roles', $data);
        }

        return view('admin.job-roles.index', compact('data'));
    }

    /**
     * Trashed job roles
     *
     * @param Request $request
     * @return Mixed Json or blade view
     */
    public function trashed(Request $request)
    {
        $jobroleRepository = new JobroleRepository();
        $jobroleRepository->adminListRequestValidation($request);

        $jobRoles = Jobrole::onlyTrashed();
        $jobRoleData = $jobroleRepository->search($jobRoles, $this->_filters($request))->paginate(50);

        $data = [
            'job_function_select' => JobfunctionService::optionSelect(),
            'job_roles' => JobroleResource::collection($jobRoleData),
            'total' => $jobRoleData->total(),
            'per_page' => $jobRoleData->perPage(),
            'current_page' => $jobRoleData->currentPage(),
            'total_page' => $jobRoleData->lastPage(),
            'displayed_start' => (($jobRoleData->currentPage() - 1) * $jobRoleData->perPage()) + 1,
            'displayed_end' => ($jobRoleData->currentPage() == $jobRoleData->lastPage()) ? $jobRoleData->total() : $jobRoleData->currentPage() * $jobRoleData->perPage()
        ];

        if ($request->ajax()) {
            return $this->successResponse('Trashed job roles', $data);
        }

        return view('admin.job-roles.trashed', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jobFunctionSelect = JobfunctionService::optionSelect();

        return view('admin.job-roles.create', compact('jobFunctionSelect'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'jobfunction_id' => 'required|numeric',
            'name' => 'required|string|unique:jobroles'
        ]);

        Jobrole::create($request->all());

        return $this->successResponse(trans('common.saved'), []);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Jobrole  $jobrole
     * @return \Illuminate\Http\Response
     */
    public function edit(Jobrole $jobRole)
    {
        $jobFunctionSelect = JobfunctionService::optionSelect();

        return view('admin.job-roles.edit', compact('jobRole', 'jobFunctionSelect'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Jobrole  $jobRole
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Jobrole $jobRole)
    {
        $this->validate($request, [
            'jobfunction_id' => 'required|numeric',
            'name' => 'required|string|unique:jobroles,name,'.$jobRole->id
        ]);

        $jobRole->update($request->all());

        return $this->successResponse(trans('common.updated'), []);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Jobrole  $jobRole
     * @return \Illuminate\Http\Response
     */
    public function destroy(Jobrole $jobRole)
    {
        $jobRole->delete();

        return $this->successResponse(trans('common.trashed'), []);
    }

    /**
     * Destroy permanent job role
     *
     * @param Int $id
     * @return Json response
     */
    public function destroyPermanent(Int $id)
    {
        $jobRole = Jobrole::onlyTrashed()->findOrFail($id);
        $jobRole->forceDelete();

        return $this->successResponse(trans('common.permanent_deleted'), []);
    }

    /**
     * Bulk destroy job roles
     *
     * @param Request $request
     * @return Json response
     */
    public function bulkDestroy(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $jobRoles = Jobrole::findMany($request->ids);

        foreach ($jobRoles as $jobRole) {
            $jobRole->delete();
        }

        return $this->successResponse(trans('common.trashed'), []);
    }

    /**
     * Bulk destroy permanent job roles
     *
     * @param Request $request
     * @return Json response
     */
    public function bulkDestroyPermanent(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $jobRoles = Jobrole::onlyTrashed()->findMany($request->ids);

        foreach ($jobRoles as $jobRole) {
            $jobRole->forceDelete();
        }

        return $this->successResponse(trans('common.permanent_deleted'), []);
    }

    /**
    * Restore job role from trashed
    *
    * @param Int $id
    * @return Json response
    */
    public function restore(Int $id)
    {
        $jobRole = Jobrole::onlyTrashed()->findOrFail($id);
        $jobRole->restore();

        return $this->successResponse(trans('common.restored'), []);
    }
}
