<?php

namespace App\Http\Controllers\Admin;

use App\Models\MainBanner;
use App\Helpers\PathHelper;
use App\Helpers\ImageHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\MainBannerResource;
use App\Http\Requests\StoreMainBannerRequest;
use App\Http\Requests\UpdateMainBannerRequest;
use App\Repositories\MainBannerRepository;

class MainBannerController extends Controller
{
    protected $mainBannerPath;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (in_array('manage-banner', $this->getPermissions($request->user()))) {
                return $next($request);
            } else {
                return redirect()->route('admin.home');
            }
        });

        $this->mainBannerPath = PathHelper::main_banner_path();
    }

    /**
     * Request filters
     *
     * @param Request $request
     * @return Array
     */
    private function _filters(Request $request)
    {
        return [
            'keyword' => $request->keyword
        ];
    }

    /**
     * Display banner list
     *
     * @param Request $request
     * @return Mix Blade view or Json response
     */
    public function index(Request $request)
    {
        $repository = new MainBannerRepository();
        $repository->adminListRequestValidation($request);

        $banners = MainBanner::select('*');
        $banners = $repository->search($banners, $this->_filters($request))->paginate(50);

        $data = [
            'banners' => MainBannerResource::collection($banners),
            'total' => $banners->total(),
            'per_page' => $banners->perPage(),
            'current_page' => $banners->currentPage(),
            'total_page' => $banners->lastPage(),
            'displayed_start' => (($banners->currentPage() - 1) * $banners->perPage()) + 1,
            'displayed_end' => ($banners->currentPage() == $banners->lastPage()) ? $banners->total() : $banners->currentPage() * $banners->perPage()
        ];

        if ($request->ajax()) {
            return $this->jsonUnescaped([
                'status' => 'success',
                'data' => $data
            ]);
        }

        return view('admin.main-banners.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.main-banners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMainBannerRequest $request)
    {
        $banner = MainBanner::create([
            'alt_image' => $request->alt_image,
            'link' => $request->link,
            'open_new_tab' => $request->open_new_tab == 'true' ? 1 : 0,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'enable' => $request->enable == 'true' ? 1 : 0,
            'sort_order' => $request->sort_order
        ]);

        if ($request->image != '') {
            $fileName = ImageHelper::processImg($request->image, $this->mainBannerPath);
            $banner->image = $fileName;
            $banner->save();
        }

        session()->flash('flash_alert', [
            'level' => 'success',
            'message' => trans('common.saved'),
        ]);

        return $this->jsonUnescaped([
            'status' => 'success',
            'message' => trans('common.saved'),
            'banner' => new MainBannerResource($banner)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\Mainbanner  $mainBanner
     * @return \Illuminate\Http\Response
     */
    public function edit(MainBanner $mainBanner)
    {
        return view('admin.main-banners.edit', compact('mainBanner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\Mainbanner  $mainBanner
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMainBannerRequest $request, Mainbanner $mainBanner)
    {
        $mainBanner->update([
            'alt_image' => $request->alt_image,
            'link' => $request->link,
            'open_new_tab' => $request->open_new_tab == 'true' ? 1 : 0,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'enable' => $request->enable == 'true' ? 1 : 0,
            'sort_order' => $request->sort_order
        ]);

        if ($request->image != '') {
            PathHelper::deleteFile($this->mainBannerPath . $mainBanner->image);

            $fileName = ImageHelper::processImg($request->image, $this->mainBannerPath);
            $mainBanner->image = $fileName;
            $mainBanner->save();
        }

        session()->flash('flash_alert', [
            'level' => 'success',
            'message' => trans('common.updated'),
        ]);

        return $this->jsonUnescaped([
            'status' => 'success',
            'message' => trans('common.updated'),
            'banner' => new MainBannerResource($mainBanner)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\Mainbanner  $mainBanner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mainbanner $mainBanner)
    {
        $mainBanner->delete();

        return response()->json([
            'status' => 'success',
            'message' => trans('common.removed'),
        ]);
    }

    public function bulkDestroy(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $banners = MainBanner::findMany($request->ids);

        foreach ($banners as $banner) {
            $banner->delete();
        }

        return response()->json([
            'status' => 'success',
            'message' => trans('common.removed'),
        ]);
    }
}
