<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin;
use App\Helpers\PathHelper;
use App\Helpers\ImageHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminAvatarController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, Int $id)
    {
        $admin = Admin::findOrFail($id);

        if ($admin->photo != '') {
            $path = PathHelper::admin_photo_path($id) . $admin->photo;
            return ImageHelper::showImage($path, 'storage');
        }

        return ImageHelper::showImage('images/default/default-avatar.png', 'public');
    }
}
