<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin;
use App\Models\Permission;
use App\Helpers\PathHelper;
use App\Helpers\ImageHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\AdminResource;
use App\Http\Requests\StoreAdminRequest;
use App\Http\Requests\UpdateAdminRequest;
use App\Repositories\AdminRepository;

class AdminController extends Controller
{
    protected $perPage;
    protected $sizing;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $user = $request->user();
            if ($user->primary) {
                return $next($request);
            }
            
            if (in_array('manage-admin', $this->getPermissions($user))) {
                return $next($request);
            } else {
                return redirect()->route('admin.home');
            }
        });

        $this->perPage = 50;
        $this->sizing = ['type' => 'fit', 'size' => 100];
    }

    /**
     * Request filters
     *
     * @param Request $request
     * @return Array
     */
    private function _filters(Request $request)
    {
        return [
            'keyword' => $request->keyword,
            'name' => $request->name,
            'email' => $request->email,
            'primary' => $request->primary,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
       ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $repository = new AdminRepository();
        $repository->adminListRequestValidation($request);

        $admins = Admin::select('*');
        $adminData = $repository->search($admins, $this->_filters($request))->paginate(50);

        $data = [
            'admins' => AdminResource::collection($adminData),
            'total' => $adminData->total(),
            'per_page' => $adminData->perPage(),
            'current_page' => $adminData->currentPage(),
            'total_page' => $adminData->lastPage(),
            'displayed_start' => (($adminData->currentPage() - 1) * $adminData->perPage()) + 1,
            'displayed_end' => ($adminData->currentPage() == $adminData->lastPage()) ? $adminData->total() : $adminData->currentPage() * $adminData->perPage()
        ];

        if ($request->ajax()) {
            return $this->jsonUnescaped([
                'status' => 'success',
                'data' => $data
            ]);
        }

        return view('admin.admins.index', compact('data'));
    }

    public function trashed(Request $request)
    {
        $repository = new AdminRepository();
        $repository->adminListRequestValidation($request);

        $admins = Admin::onlyTrashed()->select('*');
        $adminData = $repository->search($admins, $this->_filters($request))->paginate(50);

        $data = [
            'admins' => AdminResource::collection($adminData),
            'total' => $adminData->total(),
            'per_page' => $adminData->perPage(),
            'current_page' => $adminData->currentPage(),
            'total_page' => $adminData->lastPage(),
            'displayed_start' => (($adminData->currentPage() - 1) * $adminData->perPage()) + 1,
            'displayed_end' => ($adminData->currentPage() == $adminData->lastPage()) ? $adminData->total() : $adminData->currentPage() * $adminData->perPage()
        ];

        if ($request->ajax()) {
            return $this->jsonUnescaped([
                'status' => 'success',
                'data' => $data
            ]);
        }

        return view('admin.admins.trashed', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::all();
        return view('admin.admins.create', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAdminRequest $request)
    {
        $admin = Admin::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'primary' => ($request->primary) ? 1 : 0
        ]);

        if ($request->photo != '') {
            $path = PathHelper::admin_photo_path($admin->id);

            $fileName = ImageHelper::processImg($request->photo, $path, $this->sizing, '', 'local');
            $admin->photo = $fileName;
            $admin->save();
        }

        if ($request->permissions != '') {
            $admin->permissions()->sync($request->permissions);
        }

        session()->flash('flash_alert', [
            'level' => 'success',
            'message' => trans('common.saved'),
        ]);

        return redirect()->route('admin.admins.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        $permissions = Permission::all();
        $currentPms = [];
        foreach ($admin->permissions as $currentPm) {
            $currentPms[] = $currentPm->id;
        }

        return view('admin.admins.edit', compact('admin', 'permissions', 'currentPms'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAdminRequest $request, Admin $admin)
    {
        if ($admin->primary && !isset($request->primary)) {
            $totalPrimaryAdmins = Admin::where('primary', 1)->count();
            if ($totalPrimaryAdmins == 1) {
                session()->flash('flash_alert', [
                    'level' => 'danger',
                    'message' => 'Cannot make this admin as not primary, because there is only 1 primary admin',
                ]);
        
                return redirect()->back();
            }
        }

        $request['primary'] = ($request->primary) ? 1 : 0;
        $admin->update($request->all());
        if (count($request->permissions) > 0) {
            $admin->permissions()->sync($request->permissions);
        }
        
        if ($request->photo != '') {
            $path = PathHelper::admin_photo_path($admin->id);
            PathHelper::deleteFile($path . $admin->photo, 'local');

            $fileName = ImageHelper::processImg($request->photo, $path, $this->sizing, '', 'local');
            $admin->photo = $fileName;
            $admin->save();
        }

        session()->flash('flash_alert', [
            'level' => 'success',
            'message' => trans('common.updated'),
        ]);

        return redirect()->back();
    }

    public function updatePassword(Request $request, $id)
    {
        $this->validate($request, [
            'new_password' => 'required|string|min:8|confirmed',
            'new_password_confirmation' => 'required|string|min:8'
        ]);

        $admin = Admin::findOrFail($id);
        $admin->password = Hash::make($request->new_password);
        $admin->save();

        session()->flash('flash_alert', [
            'level' => 'success',
            'message' => trans('common.updated'),
        ]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Admin $admin)
    {
        $currentAdmin = Auth::guard('admin')->user();
        if ($admin->id == $currentAdmin->id) {
            return response()->json([
                'status' => 'error',
                'message' => 'Cannot delete current logged in admin',
            ], 422);
        }

        if ($admin->primary) {
            return response()->json([
                'status' => 'error',
                'message' => 'Cannot delete primary admin',
            ], 422);
        }

        $admin->delete();

        return response()->json([
            'status' => 'success',
            'message' => trans('common.trashed'),
        ]);
    }

    public function destroyPermanent($id)
    {
        $admin = Admin::onlyTrashed()->findOrFail($id);
        $admin->forceDelete();

        return response()->json([
            'status' => 'success',
            'message' => trans('common.permanent_deleted'),
        ]);
    }

    public function bulkDestroy(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $currentAdmin = Auth::guard('admin')->user();
        $admins = Admin::findMany($request->ids);

        foreach ($admins as $admin) {
            if ($admin->id != $currentAdmin->id && !$admin->primary) {
                $admin->delete();
            }
        }

        return response()->json([
            'status' => 'success',
            'message' => trans('common.trashed'),
        ]);
    }

    public function bulkDestroyPermanent(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $admins = Admin::onlyTrashed()->findMany($request->ids);

        foreach ($admins as $admin) {
            $admin->forceDelete();
        }

        return response()->json([
            'status' => 'success',
            'message' => trans('common.permanent_deleted'),
        ]);
    }

    public function restore($id)
    {
        $admin = Admin::onlyTrashed()->findOrFail($id);
        $admin->restore();

        return response()->json([
            'status' => 'success',
            'message' => trans('common.restored'),
        ]);
    }
}
