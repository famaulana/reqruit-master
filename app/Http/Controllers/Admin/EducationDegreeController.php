<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\EducationDegree;
use App\Http\Controllers\Controller;
use App\Http\Resources\EducationDegreeResource;
use App\Models\Education;

class EducationDegreeController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $admin = $request->user();
            if ($admin->primary) {
                return $next($request);
            }
            
            if (in_array('manage-education-degree', $this->getPermissions($admin))) {
                return $next($request);
            } else {
                return redirect()->route('admin.home');
            }
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->validate($request, [
            'keyword' => 'nullable|string'
        ]);

        $eduDegrees = EducationDegree::where('name', 'LIKE', '%'.$request->keyword.'%')->orderBy('created_at', 'DESC')->paginate(50);

        $data = [
            'education_degrees' => EducationDegreeResource::collection($eduDegrees),
            'total' => $eduDegrees->total(),
            'per_page' => $eduDegrees->perPage(),
            'current_page' => $eduDegrees->currentPage(),
            'total_page' => $eduDegrees->lastPage(),
            'displayed_start' => (($eduDegrees->currentPage() - 1) * $eduDegrees->perPage()) + 1,
            'displayed_end' => ($eduDegrees->currentPage() == $eduDegrees->lastPage()) ? $eduDegrees->total() : $eduDegrees->currentPage() * $eduDegrees->perPage()
        ];

        if ($request->ajax()) {
            return $this->jsonUnescaped([
                'status' => 'success',
                'data' => $data
            ]);
        }

        return view('admin.education-degrees.index', compact('data'));
    }

    public function trashed(Request $request)
    {
        $this->validate($request, [
            'keyword' => 'nullable|string'
        ]);

        $eduDegrees = EducationDegree::onlyTrashed()->where('name', 'LIKE', '%'.$request->keyword.'%')->orderBy('created_at', 'DESC')->paginate(50);

        $data = [
            'education_degrees' => EducationDegreeResource::collection($eduDegrees),
            'total' => $eduDegrees->total(),
            'per_page' => $eduDegrees->perPage(),
            'current_page' => $eduDegrees->currentPage(),
            'total_page' => $eduDegrees->lastPage(),
            'displayed_start' => (($eduDegrees->currentPage() - 1) * $eduDegrees->perPage()) + 1,
            'displayed_end' => ($eduDegrees->currentPage() == $eduDegrees->lastPage()) ? $eduDegrees->total() : $eduDegrees->currentPage() * $eduDegrees->perPage()
        ];

        if ($request->ajax()) {
            return $this->jsonUnescaped([
                'status' => 'success',
                'data' => $data
            ]);
        }
        
        return view('admin.education-degrees.trashed', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.education-degrees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required|string|unique:education_degrees']);

        EducationDegree::create(['name' => $request->name]);

        session()->flash('flash_alert', [
            'level' => 'success',
            'message' => trans('common.saved'),
        ]);

        return redirect()->route('admin.education-degrees.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EducationDegree  $educationDegree
     * @return \Illuminate\Http\Response
     */
    public function edit(EducationDegree $educationDegree)
    {
        return view('admin.education-degrees.edit', compact('educationDegree'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EducationDegree  $educationDegree
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EducationDegree $educationDegree)
    {
        $this->validate($request, ['name' => 'required|string|unique:education_degrees,name,' . $educationDegree->id]);

        $educationDegree->update(['name' => $request->name]);

        session()->flash('flash_alert', [
            'level' => 'success',
            'message' => trans('common.updated'),
        ]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EducationDegree  $educationDegree
     * @return \Illuminate\Http\Response
     */
    public function destroy(EducationDegree $educationDegree)
    {
        $educationDegree->delete();

        return $this->successResponse(trans('common.trashed'), []);
    }

    /**
     * Destroy permanent education degree
     *
     * @param Int $id
     * @return Json response
     */
    public function destroyPermanent(Int $id)
    {
        $educationDegree = EducationDegree::onlyTrashed()->findOrFail($id);
        $educationDegree->forceDelete();

        return $this->successResponse(trans('common.permanent_deleted'), []);
    }

    /**
     * Bulk destroy education degrees
     *
     * @param Request $request
     * @return Json response
     */
    public function bulkDestroy(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $educationDegrees = EducationDegree::findMany($request->ids);

        foreach ($educationDegrees as $degree) {
            $degree->delete();
        }

        return $this->successResponse(trans('common.trashed'), []);
    }

    public function bulkDestroyPermanent(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $educationDegrees = EducationDegree::onlyTrashed()->findMany($request->ids);

        foreach ($educationDegrees as $educationDegree) {
            $educationDegree->forceDelete();
        }

        return $this->successResponse(trans('common.permanent_deleted'), []);
    }

    public function restore(Int $id)
    {
        $educationDegree = EducationDegree::onlyTrashed()->findOrFail($id);
        $educationDegree->restore();

        return $this->successResponse(trans('common.restored'), []);
    }
}
