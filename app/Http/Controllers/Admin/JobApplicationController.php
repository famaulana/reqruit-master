<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\JobApplicationAdminListResource;
use App\Models\JobApplication;
use App\Repositories\JobApplicationRepository;
use Illuminate\Http\Request;

class JobApplicationController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $admin = $request->user();
            if ($admin->primary) {
                return $next($request);
            }
            
            if (in_array('manage-job-application', $this->getPermissions($admin))) {
                return $next($request);
            } else {
                return redirect()->route('admin.home');
            }
        });
    }

    /**
     * Filter request
     *
     * @param Request $request
     * @return Array
     */
    private function _filters(Request $request)
    {
        return [
            'keyword' => $request->keyword,
            'user_name' => $request->user_name,
            'user_email' => $request->user_email,
            'job_title' => $request->job_title,
            'company_name' => $request->company_name,
            'status' => $request->status,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $repository = new JobApplicationRepository();
        $repository->adminListRequestValidation($request);

        $jobApplications = JobApplication::with(['user', 'workjob', 'workjob.company']);
        $jobApplications = $repository->search($jobApplications, $this->_filters($request))->paginate(50);

        $data = [
            'job_applications' => JobApplicationAdminListResource::collection($jobApplications),
            'total' => $jobApplications->total(),
            'per_page' => $jobApplications->perPage(),
            'current_page' => $jobApplications->currentPage(),
            'total_page' => $jobApplications->lastPage(),
            'displayed_start' => (($jobApplications->currentPage() - 1) * $jobApplications->perPage()) + 1,
            'displayed_end' => ($jobApplications->currentPage() == $jobApplications->lastPage()) ? $jobApplications->total() : $jobApplications->currentPage() * $jobApplications->perPage()
        ];

        if ($request->ajax()) {
            return $this->jsonUnescaped([
                'status' => 'success',
                'data' => $data
            ]);
        }

        return view('admin.job-applications.index', compact('data'));
    }

    /**
     * Show job application
     *
     * @param JobApplication $jobApplication
     * @return Blade view
     */
    public function show(JobApplication $jobApplication)
    {
        $job = $jobApplication->workjob;
        $user = $jobApplication->user;
        $company = $jobApplication->workjob->company;

        return view('admin.job-applications.show', compact('jobApplication', 'job', 'user', 'company'));
    }
}
