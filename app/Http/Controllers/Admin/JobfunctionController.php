<?php

namespace App\Http\Controllers\Admin;

use App\Models\Jobfunction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\JobfunctionResource;
use App\Repositories\JobfunctionRepository;

class JobfunctionController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $admin = $request->user();
            if ($admin->primary) {
                return $next($request);
            }
            
            if (in_array('manage-job-function', $this->getPermissions($admin))) {
                return $next($request);
            } else {
                return redirect()->route('admin.home');
            }
        });
    }

    /**
     * Request filters
     *
     * @param Request $request
     * @return Array
     */
    private function _filters(Request $request)
    {
        return [
            'keyword' => $request->keyword,
            'name' => $request->name,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $jobFunctionRepository = new JobfunctionRepository();
        $jobFunctionRepository->adminListRequestValidation($request);

        $jobFunctions = Jobfunction::select('*');
        $jobFunctionData = $jobFunctionRepository->search($jobFunctions, $this->_filters($request))->paginate(50);

        $data = [
            'job_functions' => JobfunctionResource::collection($jobFunctionData),
            'total' => $jobFunctionData->total(),
            'per_page' => $jobFunctionData->perPage(),
            'current_page' => $jobFunctionData->currentPage(),
            'total_page' => $jobFunctionData->lastPage(),
            'displayed_start' => (($jobFunctionData->currentPage() - 1) * $jobFunctionData->perPage()) + 1,
            'displayed_end' => ($jobFunctionData->currentPage() == $jobFunctionData->lastPage()) ? $jobFunctionData->total() : $jobFunctionData->currentPage() * $jobFunctionData->perPage()
        ];

        if ($request->ajax()) {
            return $this->successResponse('Job functions', $data);
        }

        return view('admin.job-functions.index', compact('data'));
    }

    /**
     * Trashed job functions
     *
     * @param Request $request
     * @return Mixed Json or blade view
     */
    public function trashed(Request $request)
    {
        $jobFunctionRepository = new JobfunctionRepository();
        $jobFunctionRepository->adminListRequestValidation($request);

        $jobFunctions = Jobfunction::onlyTrashed();
        $jobFunctionData = $jobFunctionRepository->search($jobFunctions, $this->_filters($request))->paginate(50);

        $data = [
            'job_functions' => JobfunctionResource::collection($jobFunctionData),
            'total' => $jobFunctionData->total(),
            'per_page' => $jobFunctionData->perPage(),
            'current_page' => $jobFunctionData->currentPage(),
            'total_page' => $jobFunctionData->lastPage(),
            'displayed_start' => (($jobFunctionData->currentPage() - 1) * $jobFunctionData->perPage()) + 1,
            'displayed_end' => ($jobFunctionData->currentPage() == $jobFunctionData->lastPage()) ? $jobFunctionData->total() : $jobFunctionData->currentPage() * $jobFunctionData->perPage()
        ];

        if ($request->ajax()) {
            return $this->successResponse('Trashed job functions', $data);
        }

        return view('admin.job-functions.trashed', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.job-functions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:jobfunctions'
        ]);

        Jobfunction::create($request->all());

        return $this->successResponse(trans('common.saved'), []);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Jobfunction  $jobFunction
     * @return \Illuminate\Http\Response
     */
    public function edit(Jobfunction $jobFunction)
    {
        return view('admin.job-functions.edit', compact('jobFunction'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Jobfunction  $jobFunction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Jobfunction $jobFunction)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:jobfunctions,name,'.$jobFunction->id
        ]);

        $jobFunction->update($request->all());

        return $this->successResponse(trans('common.updated'), []);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Jobfunction  $jobFunction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Jobfunction $jobFunction)
    {
        $jobFunction->delete();

        return $this->successResponse(trans('common.trashed'), []);
    }

    /**
     * Destroy permanent job function
     *
     * @param Int $id
     * @return Json response
     */
    public function destroyPermanent(Int $id)
    {
        $jobFunction = Jobfunction::onlyTrashed()->findOrFail($id);
        $jobFunction->forceDelete();

        return $this->successResponse(trans('common.permanent_deleted'), []);
    }

    /**
     * Bulk destroy job functions
     *
     * @param Request $request
     * @return Json response
     */
    public function bulkDestroy(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $jobFunctions = Jobfunction::findMany($request->ids);

        foreach ($jobFunctions as $jobFunction) {
            $jobFunction->delete();
        }

        return $this->successResponse(trans('common.trashed'), []);
    }

    /**
     * Bulk destroy permanent job functions
     *
     * @param Request $request
     * @return Json response
     */
    public function bulkDestroyPermanent(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $jobFunctions = Jobfunction::onlyTrashed()->findMany($request->ids);

        foreach ($jobFunctions as $Jobfunction) {
            $Jobfunction->forceDelete();
        }

        return $this->successResponse(trans('common.permanent_deleted'), []);
    }

    /**
    * Restore job function from trashed
    *
    * @param Int $id
    * @return Json response
    */
    public function restore(Int $id)
    {
        $jobFunction = Jobfunction::onlyTrashed()->findOrFail($id);
        $jobFunction->restore();

        return $this->successResponse(trans('common.restored'), []);
    }
}
