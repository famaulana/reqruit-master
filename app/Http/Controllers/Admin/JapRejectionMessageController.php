<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\AppConfigRepository;
use Illuminate\Http\Request;

class JapRejectionMessageController extends Controller
{
    /**
     * Manage Job application rejection messages
     *
     * @return Blade view
     */
    public function index()
    {
        $config = new AppConfigRepository();
        $messages = $config->getConfig('jap_rejection_messages');

        return view('admin.jap-rejection-messages.index', compact('messages'));
    }
}
