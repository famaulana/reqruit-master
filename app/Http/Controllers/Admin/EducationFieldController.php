<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\EducationField;
use App\Http\Controllers\Controller;
use App\Http\Resources\EducationFieldResource;

class EducationFieldController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $admin = $request->user();
            if ($admin->primary) {
                return $next($request);
            }
            
            if (in_array('manage-education-field', $this->getPermissions($admin))) {
                return $next($request);
            } else {
                return redirect()->route('admin.home');
            }
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->validate($request, [
            'keyword' => 'nullable|string'
        ]);

        $eduFields = EducationField::where('name', 'LIKE', '%'.$request->keyword.'%')->orderBy('created_at', 'DESC')->paginate(50);

        $data = [
            'education_fields' => EducationFieldResource::collection($eduFields),
            'total' => $eduFields->total(),
            'per_page' => $eduFields->perPage(),
            'current_page' => $eduFields->currentPage(),
            'total_page' => $eduFields->lastPage(),
            'displayed_start' => (($eduFields->currentPage() - 1) * $eduFields->perPage()) + 1,
            'displayed_end' => ($eduFields->currentPage() == $eduFields->lastPage()) ? $eduFields->total() : $eduFields->currentPage() * $eduFields->perPage()
        ];

        if ($request->ajax()) {
            return $this->jsonUnescaped([
                'status' => 'success',
                'data' => $data
            ]);
        }

        return view('admin.education-fields.index', compact('data'));
    }

    public function trashed(Request $request)
    {
        $this->validate($request, [
            'keyword' => 'nullable|string'
        ]);

        $eduFields = EducationField::onlyTrashed()->where('name', 'LIKE', '%'.$request->keyword.'%')->orderBy('created_at', 'DESC')->paginate(50);

        $data = [
            'education_fields' => EducationFieldResource::collection($eduFields),
            'total' => $eduFields->total(),
            'per_page' => $eduFields->perPage(),
            'current_page' => $eduFields->currentPage(),
            'total_page' => $eduFields->lastPage(),
            'displayed_start' => (($eduFields->currentPage() - 1) * $eduFields->perPage()) + 1,
            'displayed_end' => ($eduFields->currentPage() == $eduFields->lastPage()) ? $eduFields->total() : $eduFields->currentPage() * $eduFields->perPage()
        ];

        if ($request->ajax()) {
            return $this->jsonUnescaped([
                'status' => 'success',
                'data' => $data
            ]);
        }
        
        return view('admin.education-fields.trashed', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.education-fields.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required|string|unique:education_fields']);

        EducationField::create(['name' => $request->name]);

        session()->flash('flash_alert', [
            'level' => 'success',
            'message' => trans('common.saved'),
        ]);

        return redirect()->route('admin.education-fields.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EducationField  $educationField
     * @return \Illuminate\Http\Response
     */
    public function edit(EducationField $educationField)
    {
        return view('admin.education-fields.edit', compact('educationField'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EducationField  $educationField
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EducationField $educationField)
    {
        $this->validate($request, ['name' => 'required|string|unique:education_fields,name,' . $educationField->id]);

        $educationField->update(['name' => $request->name]);

        session()->flash('flash_alert', [
            'level' => 'success',
            'message' => trans('common.updated'),
        ]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EducationField  $educationField
     * @return \Illuminate\Http\Response
     */
    public function destroy(EducationField $educationField)
    {
        $educationField->delete();

        return $this->successResponse(trans('common.trashed'), []);
    }

    /**
     * Destroy permanent education degree
     *
     * @param Int $id
     * @return Json response
     */
    public function destroyPermanent(Int $id)
    {
        $educationField = EducationField::onlyTrashed()->findOrFail($id);
        $educationField->forceDelete();

        return $this->successResponse(trans('common.permanent_deleted'), []);
    }

    /**
     * Bulk destroy education degrees
     *
     * @param Request $request
     * @return Json response
     */
    public function bulkDestroy(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $educationField = EducationField::findMany($request->ids);

        foreach ($educationField as $field) {
            $field->delete();
        }

        return $this->successResponse(trans('common.trashed'), []);
    }

    public function bulkDestroyPermanent(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $educationField = EducationField::onlyTrashed()->findMany($request->ids);

        foreach ($educationField as $field) {
            $field->forceDelete();
        }

        return $this->successResponse(trans('common.permanent_deleted'), []);
    }

    public function restore(Int $id)
    {
        $educationField = EducationField::onlyTrashed()->findOrFail($id);
        $educationField->restore();

        return $this->successResponse(trans('common.restored'), []);
    }
}
