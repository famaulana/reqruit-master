<?php

namespace App\Http\Controllers\Admin;

use App\Models\Company;
use App\Models\Workjob;
use App\Http\Controllers\Controller;
use App\Models\JobApplication;
use App\Models\User;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{
    public function index()
    {
        $stats = Cache::remember('dashboard_stats', 22*60, function () {
            $data = [
                'total_companies' => Company::all()->count(),
                'total_jobs' => Workjob::all()->count(),
                'total_users' => User::all()->count(),
                'total_job_applications' => JobApplication::all()->count(),
            ];

            return $data;
        });

        $companies =  Cache::remember('dashboard_companies', 22*60, function () {
            return Company::select('id', 'name')->take(10)->orderBy('created_at', 'DESC')->get();
        });

        $jobs =  Cache::remember('dashboard_jobs', 22*60, function () {
            return Workjob::select('id', 'title')->take(10)->orderBy('created_at', 'DESC')->get();
        });

        $users =  Cache::remember('dashboard_users', 22*60, function () {
            return User::select('id', 'name')->take(10)->orderBy('created_at', 'DESC')->get();
        });

        $job_applications =  Cache::remember('dashboard_job_applications', 22*60, function () {
            return JobApplication::select('id', 'workjob_id', 'status')->with(['workjob'])->take(10)->orderBy('created_at', 'DESC')->get();
        });

        $data = [
            'stats' => $stats,
            'companies' => $companies,
            'jobs' => $jobs,
            'users' => $users,
            'job_applications' => $job_applications,
        ];

        return view('admin.home', compact('data'));
    }
}
