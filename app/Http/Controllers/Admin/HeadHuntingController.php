<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\HeadHunting;
use App\Http\Controllers\Controller;
use App\Http\Resources\HeadHuntingResource;
use App\Repositories\HeadHuntingRepository;

class HeadHuntingController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $admin = $request->user();
            if ($admin->primary) {
                return $next($request);
            }
            
            if (in_array('manage-head-hunting', $this->getPermissions($admin))) {
                return $next($request);
            } else {
                return redirect()->route('admin.home');
            }
        });
    }

    /**
     * Filter request
     *
     * @param Request $request
     * @return Array
     */
    private function _filters(Request $request)
    {
        return [
            'keyword' => $request->keyword,
            'position' => $request->position,
            'employer_name' => $request->employer_name,
            'employer_email' => $request->employer_email,
            'contacted' => $request->contacted,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $repository = new HeadHuntingRepository();
        $repository->adminListRequestValidation($request);

        $headHuntings = HeadHunting::select('*');
        $headHuntings = $repository->search($headHuntings, $this->_filters($request))->paginate(50);

        $data = [
            'head_huntings' => HeadHuntingResource::collection($headHuntings),
            'total' => $headHuntings->total(),
            'per_page' => $headHuntings->perPage(),
            'current_page' => $headHuntings->currentPage(),
            'total_page' => $headHuntings->lastPage(),
            'displayed_start' => (($headHuntings->currentPage() - 1) * $headHuntings->perPage()) + 1,
            'displayed_end' => ($headHuntings->currentPage() == $headHuntings->lastPage()) ? $headHuntings->total() : $headHuntings->currentPage() * $headHuntings->perPage()
        ];

        if ($request->ajax()) {
            return $this->jsonUnescaped([
                'status' => 'success',
                'data' => $data
            ]);
        }

        return view('admin.head-huntings.index', compact('data'));
    }

    /**
     * Show head hunting
     *
     * @param HeadHunting $headHunting
     * @return Blade view
     */
    public function show(HeadHunting $headHunting)
    {
        return view('admin.head-huntings.show', compact('headHunting'));
    }

    /**
     * Update head hunting
     *
     * @param Request $request
     * @param HeadHunting $headHunting
     * @return void
     */
    public function update(Request $request, HeadHunting $headHunting)
    {
        $this->validate($request, [
            'contacted' => 'required|numeric|in:0,1'
        ]);

        $headHunting->contacted = $request->contacted;
        $headHunting->save();

        session()->flash('flash_alert', [
            'level' => 'success',
            'message' => trans('common.updated'),
        ]);

        return redirect()->route('admin.head-huntings.show', $headHunting->id);
    }
}
