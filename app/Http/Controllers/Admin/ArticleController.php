<?php

namespace App\Http\Controllers\Admin;

use App\Models\Article;
use App\Helpers\PathHelper;
use App\Helpers\ImageHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ArticleResource;

class ArticleController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (in_array('manage-article', $this->getPermissions($request->user()))) {
                return $next($request);
            } else {
                return redirect()->route('admin.home');
            }
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->validate($request, [
            'keyword' => 'nullable|string',
        ]);

        $articles = Article::select('*');

        $searchQuery = $this->_searchQuery($request, $articles);
        $articleData = $searchQuery['search_data'];

        $data = [
            'articles' => ArticleResource::collection($articleData),
            'total' => $articleData->total(),
            'per_page' => $articleData->perPage(),
            'current_page' => $articleData->currentPage(),
            'total_page' => $articleData->lastPage(),
            'displayed_start' => (($articleData->currentPage() - 1) * $articleData->perPage()) + 1,
            'displayed_end' => ($articleData->currentPage() == $articleData->lastPage()) ? $articleData->total() : $articleData->currentPage() * $articleData->perPage()
        ];

        if ($request->ajax()) {
            return $this->jsonUnescaped([
                'status' => 'success',
                'data' => $data
            ]);
        }

        return view('admin.articles.index', compact('data'));
    }

    /**
     * Trashed articles
     *
     * @param Request $request
     * @return Blade vie
     */
    public function trashed(Request $request)
    {
        $this->validate($request, [
            'keyword' => 'nullable|string',
        ]);

        $articles = Article::onlyTrashed()->select('*');

        $searchQuery = $this->_searchQuery($request, $articles);
        $articleData = $searchQuery['search_data'];

        $data = [
            'articles' => ArticleResource::collection($articleData),
            'total' => $articleData->total(),
            'per_page' => $articleData->perPage(),
            'current_page' => $articleData->currentPage(),
            'total_page' => $articleData->lastPage(),
            'displayed_start' => (($articleData->currentPage() - 1) * $articleData->perPage()) + 1,
            'displayed_end' => ($articleData->currentPage() == $articleData->lastPage()) ? $articleData->total() : $articleData->currentPage() * $articleData->perPage()
        ];

        if ($request->ajax()) {
            return $this->jsonUnescaped([
                'status' => 'success',
                'data' => $data
            ]);
        }

        return view('admin.articles.trashed', compact('data'));
    }

    /**
     * Search query
     *
     * @param Request $request
     * @param Eloquent collection $articles
     * @return Array
     */
    private function _searchQuery(Request $request, $articles)
    {
        if ($request->has('keyword')) {
            $articles = $articles->where('title', 'like', '%' . $request->keyword . '%');
        }

        return [
           'search_data' => $articles->orderBy('created_at', 'DESC')->paginate(50)
       ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.articles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'             => 'required|string|unique:articles',
            'slug'             => 'required|string|unique:articles',
            'content'           => 'required|string|unique:articles',
            'featured_image' => 'nullable|image|max:3000',
            'excerpt'        => 'nullable|string|unique:articles',
            'meta_title'        => 'required|string|unique:articles',
            'meta_description'  => 'required|string|unique:articles',
        ]);

        $article = Article::create($request->all());

        if ($request->featured_image != '') {
            $fileName = ImageHelper::processImg($request->featured_image, PathHelper::article_featured_image_path($article->id));
            $article->featured_image = $fileName;
            $article->save();
        }

        session()->flash('flash_alert', [
            'level' => 'success',
            'message' => trans('common.saved'),
        ]);

        return $this->jsonUnescaped([
            'status' => 'success',
            'message' => trans('common.saved'),
            'data' => new ArticleResource($article)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        $article = new ArticleResource($article);
        return view('admin.articles.edit', compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Int $id)
    {
        $this->validate($request, [
            'title'             => 'required|string|unique:articles,title,'.$id,
            'slug'             => 'required|string|unique:articles,slug,'.$id,
            'content'           => 'required|string|unique:articles,content,'.$id,
            'excerpt'        => 'nullable|string|unique:articles,excerpt,'.$id,
            'featured_image' => 'nullable|image|max:2000',
            'meta_title'        => 'required|string|unique:articles,meta_title,'.$id,
            'meta_description'  => 'required|string|unique:articles,meta_description,'.$id,
        ]);

        $article = Article::findOrFail($id);
        $oldFeaturedImage = $article->featured_image;

        $article->update($request->all());

        if ($request->featured_image != '') {
            $featuredImagePath = PathHelper::article_featured_image_path($article->id);
            PathHelper::deleteFile($featuredImagePath . $oldFeaturedImage);

            $fileName = ImageHelper::processImg($request->featured_image, $featuredImagePath);
            $article->featured_image = $fileName;
            $article->save();
        }

        session()->flash('flash_alert', [
            'level' => 'success',
            'message' => trans('common.updated'),
        ]);

        return $this->jsonUnescaped([
            'status' => 'success',
            'message' => trans('common.updated'),
            'data' => new ArticleResource($article)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->delete();
        
        return $this->jsonUnescaped([
            'status' => 'success',
            'message' => trans('common.trashed'),
        ]);
    }

    /**
     * Destroy permanent article
     *
     * @param Int $id
     * @return void
     */
    public function destroyPermanent(Int $id)
    {
        $article = Article::onlyTrashed()->findOrFail($id);
        $article->forceDelete();

        return response()->json([
            'status' => 'success',
            'message' => trans('common.permanent_deleted'),
        ]);
    }

    /**
     * Bulk destroy articles
     *
     * @param Request $request
     * @return Json response
     */
    public function bulkDestroy(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $articles = Article::findMany($request->ids);

        foreach ($articles as $article) {
            $article->delete();
        }

        return response()->json([
            'status' => 'success',
            'message' => trans('common.trashed'),
        ]);
    }

    /**
     * Bulk destroy permanent articles
     *
     * @param Request $request
     * @return Json response
     */
    public function bulkDestroyPermanent(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $articles = Article::onlyTrashed()->findMany($request->ids);

        foreach ($articles as $article) {
            $article->forceDelete();
        }

        return response()->json([
            'status' => 'success',
            'message' => trans('common.permanent_deleted'),
        ]);
    }

    /**
     * restore trashed article
     *
     * @param Int $id
     * @return Json response
     */
    public function restore(Int $id)
    {
        $article = Article::onlyTrashed()->findOrFail($id);
        $article->restore();

        return response()->json([
            'status' => 'success',
            'message' => trans('common.restored'),
        ]);
    }

    /**
     * Store article content image
     *
     * @param Request $request
     * @return Json response
     */
    public function storeContentImage(Request $request)
    {
        $this->validate($request, [
            'upload' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:3000',
        ]);

        $path = PathHelper::article_content_image_path();
        $fileName = ImageHelper::processImg($request->upload, $path);

        return $this->jsonUnescaped([
            'status' => 'success',
            'url' => ImageHelper::getImageUrl($path, $fileName)
        ]);
    }
}
