<?php

namespace App\Http\Controllers\Admin;

use App\Models\Menu;
use App\Models\Page;
use App\Models\Category;
use App\Helpers\PathHelper;
use App\Helpers\ImageHelper;
use Illuminate\Http\Request;
use App\Services\MenuService;
use App\Http\Controllers\Controller;
use App\Http\Resources\MenuResource;
use App\Repositories\MenuRepository;

class MenuController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (in_array('manage-menu', $this->getPermissions($request->user()))) {
                return $next($request);
            } else {
                return redirect()->route('admin.home');
            }
        });
    }

    private function _filters(Request $request)
    {
        return [
            'keyword' => $request->keyword
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $repository = new MenuRepository();
        $repository->adminListRequestValidation($request);

        $menus = Menu::select('*');
        $menus = $repository->search($menus, $this->_filters($request))->paginate(50);

        $data = [
            'menus' => MenuResource::collection($menus),
            'total' => $menus->total(),
            'per_page' => $menus->perPage(),
            'current_page' => $menus->currentPage(),
            'total_page' => $menus->lastPage(),
            'displayed_start' => (($menus->currentPage() - 1) * $menus->perPage()) + 1,
            'displayed_end' => ($menus->currentPage() == $menus->lastPage()) ? $menus->total() : $menus->currentPage() * $menus->perPage()
        ];

        if ($request->ajax()) {
            return $this->jsonUnescaped([
                'status' => 'success',
                'data' => $data
            ]);
        }

        return view('admin.menus.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pages = Page::orderBy('title', 'ASC')->get();

        return view('admin.menus.create', compact('pages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'links' => 'required'
        ]);
        $menu = Menu::create($request->all());
        
        $menuRepository = new MenuRepository();
        $menuRepository->cacheMenu($menu);

        session()->flash('flash_alert', [
            'level' => 'success',
            'message' => trans('common.saved'),
        ]);
          
        return $this->jsonUnescaped([
            'status' => 'success',
            'message' => trans('common.saved'),
            'data' => new MenuResource($menu)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
        $pages = Page::orderBy('title', 'ASC')->get();
        $menu = new MenuResource($menu);

        return view('admin.menus.edit', compact('pages', 'menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Menu $menu)
    {
        $this->validate($request, [
            'name' => 'required',
            'links' => 'required'
        ]);

        $menu->update($request->all());

        $menu = $menu->refresh();

        $menuRepository = new MenuRepository();
        $menuRepository->cacheMenu($menu);
       
        session()->flash('flash_alert', [
            'level' => 'success',
            'message' => trans('common.updated'),
        ]);

        return $this->jsonUnescaped([
            'status' => 'success',
            'message' => trans('common.updated'),
            'banner' => new MenuResource($menu)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        $menu->delete();

        session()->flash('flash_alert', [
            'level' => 'success',
            'message' => trans('common.removed'),
        ]);
        
        return response()->json([
            'status' => 'success',
            'message' => trans('common.removed'),
        ]);
    }

    public function bulkDestroy(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $menus = Menu::findMany($request->ids);

        foreach ($menus as $menu) {
            $menu->delete();
        }

        session()->flash('flash_alert', [
            'level' => 'success',
            'message' => trans('common.removed'),
        ]);

        return response()->json([
            'status' => 'success',
            'message' => trans('common.removed'),
        ]);
    }

    /**
     * Upload menu image
     *
     * @param Request $request
     * @return Json response
     */
    public function uploadImage(Request $request)
    {
        $this->validate($request, [
            'type' => 'required|string|in:mega_menu_image,icon_image',
            'image' => 'required|image'
        ]);

        if ($request->file('image') != '') {
            $image = $request->file('image');
            $imagePath = $request->type == 'mega_menu_image' ? PathHelper::mega_menu_image_path() :PathHelper::icon_image_path();
            $filename = ImageHelper::processImg($image, $imagePath);
        }

        $menuService = new MenuService();
        return response()->json([
            'status' => 'success',
            'message' => 'Configuration updated',
            'file_url' => $menuService->getImageFileUrl($request->type, $filename),
            'file_name' => $filename
        ]);
    }
}
