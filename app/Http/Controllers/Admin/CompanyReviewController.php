<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\CompanyReviewResource;
use App\Models\CompanyReview;
use App\Repositories\CompanyReviewRepository;
use Illuminate\Http\Request;

class CompanyReviewController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $admin = $request->user();
            if ($admin->primary) {
                return $next($request);
            }
            
            if (in_array('manage-company-review', $this->getPermissions($admin))) {
                return $next($request);
            } else {
                return redirect()->route('admin.home');
            }
        });
    }

    private function _filters(Request $request)
    {
        return [
            'user_name' => $request->user_name,
            'user_email' => $request->user_email,
            'company_name' => $request->company_name,
            'rating' => $request->rating,
            'published' => $request->published,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $repository = new CompanyReviewRepository();
        $repository->adminListRequestValidation($request);

        $review = CompanyReview::select('*');
        $reviewData = $repository->search($review, $this->_filters($request))->paginate(50);

        $data = [
            'reviews' => CompanyReviewResource::collection($reviewData),
            'total' => $reviewData->total(),
            'per_page' => $reviewData->perPage(),
            'current_page' => $reviewData->currentPage(),
            'total_page' => $reviewData->lastPage(),
            'displayed_start' => (($reviewData->currentPage() - 1) * $reviewData->perPage()) + 1,
            'displayed_end' => ($reviewData->currentPage() == $reviewData->lastPage()) ? $reviewData->total() : $reviewData->currentPage() * $reviewData->perPage()
        ];

        if ($request->ajax()) {
            return $this->jsonUnescaped([
                'status' => 'success',
                'data' => $data
            ]);
        }

        return view('admin.company-reviews.index', compact('data'));
    }

    /**
     * Trashed company reviews
     *
     * @param Request $request
     * @return Mixed Json or blade view
     */
    public function trashed(Request $request)
    {
        $repository = new CompanyReviewRepository();
        $repository->adminListRequestValidation($request);

        $companies = CompanyReview::onlyTrashed();
        $data = $repository->search($companies, $this->_filters($request))->paginate(50);

        $data = [
            'reviews' => CompanyReviewResource::collection($data),
            'total' => $data->total(),
            'per_page' => $data->perPage(),
            'current_page' => $data->currentPage(),
            'total_page' => $data->lastPage(),
            'displayed_start' => (($data->currentPage() - 1) * $data->perPage()) + 1,
            'displayed_end' => ($data->currentPage() == $data->lastPage()) ? $data->total() : $data->currentPage() * $data->perPage()
        ];

        if ($request->ajax()) {
            return $this->jsonUnescaped([
                'status' => 'success',
                'data' => $data
            ]);
        }

        return view('admin.company-reviews.trashed', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  CompanyReview $companyReview
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyReview $companyReview)
    {
        $review = new CompanyReviewResource($companyReview);
        return view('admin.company-reviews.edit', compact('review'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  CompanyReview  $companyReview
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompanyReview $companyReview)
    {
        $this->validate($request, [
            'published' => 'required|boolean'
        ]);

        $companyReview->update([
            'published' => $request->published
        ]);

        return $this->successResponse(trans('common.updated'), []);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  CompanyReview  $companyReview
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanyReview $companyReview)
    {
        $companyReview->delete();

        return response()->json([
            'status' => 'success',
            'message' => trans('common.trashed'),
        ]);
    }

    /**
     * Destroy permanent company reviews
     *
     * @param Int $id
     * @return Json response
     */
    public function destroyPermanent(Int $id)
    {
        $companyReview = CompanyReview::onlyTrashed()->findOrFail($id);
        $companyReview->forceDelete();

        return response()->json([
            'status' => 'success',
            'message' => trans('common.permanent_deleted'),
        ]);
    }

    /**
     * Bulk destroy company reviews
     *
     * @param Request $request
     * @return Json response
     */
    public function bulkDestroy(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $companyReviews = CompanyReview::findMany($request->ids);

        foreach ($companyReviews as $cr) {
            $cr->delete();
        }

        return response()->json([
            'status' => 'success',
            'message' => trans('common.trashed'),
        ]);
    }

    /**
     * Bulk destroy permanent company reviews
     *
     * @param Request $request
     * @return Json response
     */
    public function bulkDestroyPermanent(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $companyReviews = CompanyReview::onlyTrashed()->findMany($request->ids);

        foreach ($companyReviews as $cr) {
            $cr->forceDelete();
        }

        return response()->json([
            'status' => 'success',
            'message' => trans('common.permanent_deleted'),
        ]);
    }

    /**
     * Restore company review from trashed
     *
     * @param Int $id
     * @return Json response
     */
    public function restore(Int $id)
    {
        $companyReview = CompanyReview::onlyTrashed()->findOrFail($id);
        $companyReview->restore();

        return response()->json([
            'status' => 'success',
            'message' => trans('common.restored'),
        ]);
    }
}
