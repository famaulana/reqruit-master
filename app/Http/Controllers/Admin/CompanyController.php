<?php

namespace App\Http\Controllers\Admin;

use App\Models\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CompanyResource;
use App\Repositories\CompanyRepository;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $admin = $request->user();
            if ($admin->primary) {
                return $next($request);
            }
            
            if (in_array('manage-company', $this->getPermissions($admin))) {
                return $next($request);
            } else {
                return redirect()->route('admin.home');
            }
        });
    }

    private function _filters(Request $request)
    {
        return [
            'keyword' => $request->keyword,
            'name' => $request->name,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $companyRepository = new CompanyRepository();
        $companyRepository->adminListRequestValidation($request);

        $company = Company::select('*');
        $companyData = $companyRepository->search($company, $this->_filters($request))->paginate(50);

        $data = [
            'companies' => CompanyResource::collection($companyData),
            'total' => $companyData->total(),
            'per_page' => $companyData->perPage(),
            'current_page' => $companyData->currentPage(),
            'total_page' => $companyData->lastPage(),
            'displayed_start' => (($companyData->currentPage() - 1) * $companyData->perPage()) + 1,
            'displayed_end' => ($companyData->currentPage() == $companyData->lastPage()) ? $companyData->total() : $companyData->currentPage() * $companyData->perPage()
        ];

        if ($request->ajax()) {
            return $this->jsonUnescaped([
                'status' => 'success',
                'data' => $data
            ]);
        }

        return view('admin.companies.index', compact('data'));
    }

    /**
     * Trashed companies
     *
     * @param Request $request
     * @return Mixed Json or blade view
     */
    public function trashed(Request $request)
    {
        $companyRepository = new CompanyRepository();
        $companyRepository->adminListRequestValidation($request);

        $companies = Company::onlyTrashed();
        $companyData = $companyRepository->search($companies, $this->_filters($request))->paginate(50);

        $data = [
            'companies' => CompanyResource::collection($companyData),
            'total' => $companyData->total(),
            'per_page' => $companyData->perPage(),
            'current_page' => $companyData->currentPage(),
            'total_page' => $companyData->lastPage(),
            'displayed_start' => (($companyData->currentPage() - 1) * $companyData->perPage()) + 1,
            'displayed_end' => ($companyData->currentPage() == $companyData->lastPage()) ? $companyData->total() : $companyData->currentPage() * $companyData->perPage()
        ];

        if ($request->ajax()) {
            return $this->jsonUnescaped([
                'status' => 'success',
                'data' => $data
            ]);
        }

        return view('admin.companies.trashed', compact('data'));
    }

    /**
     * Show company
     *
     * @param Company $company
     * @return Blade view
     */
    public function show(Company $company)
    {
        return view('admin.companies.show', compact('company'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        $company->delete();

        return response()->json([
            'status' => 'success',
            'message' => trans('common.trashed'),
        ]);
    }

    /**
     * Destroy permanent company
     *
     * @param Int $id
     * @return Json response
     */
    public function destroyPermanent(Int $id)
    {
        $company = Company::onlyTrashed()->findOrFail($id);
        $company->forceDelete();

        return response()->json([
            'status' => 'success',
            'message' => trans('common.permanent_deleted'),
        ]);
    }

    /**
     * Bulk destroy companies
     *
     * @param Request $request
     * @return Json response
     */
    public function bulkDestroy(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $companies = Company::findMany($request->ids);

        foreach ($companies as $company) {
            $company->delete();
        }

        return response()->json([
            'status' => 'success',
            'message' => trans('common.trashed'),
        ]);
    }

    /**
     * Bulk destroy permanent companies
     *
     * @param Request $request
     * @return Json response
     */
    public function bulkDestroyPermanent(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $companies = Company::onlyTrashed()->findMany($request->ids);

        foreach ($companies as $company) {
            $company->forceDelete();
        }

        return response()->json([
            'status' => 'success',
            'message' => trans('common.permanent_deleted'),
        ]);
    }

    /**
     * Restore company from trashed
     *
     * @param Int $id
     * @return Json response
     */
    public function restore(Int $id)
    {
        $company = Company::onlyTrashed()->findOrFail($id);
        $company->restore();

        return response()->json([
            'status' => 'success',
            'message' => trans('common.restored'),
        ]);
    }
}
