<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\EnglishQuestion;
use App\Http\Controllers\Controller;
use App\Http\Resources\EnglishQuestionResource;

class EnglishQuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $admin = $request->user();
            if ($admin->primary) {
                return $next($request);
            }
            
            if (in_array('manage-question', $this->getPermissions($admin))) {
                return $next($request);
            } else {
                return redirect()->route('admin.home');
            }
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->validate($request, [
            'keyword' => 'nullable|string'
        ]);

        $questions = EnglishQuestion::where('question', 'LIKE', '%'.$request->keyword.'%')->orderBy('created_at', 'DESC')->paginate(50);

        $data = [
            'questions' => EnglishQuestionResource::collection($questions),
            'total' => $questions->total(),
            'per_page' => $questions->perPage(),
            'current_page' => $questions->currentPage(),
            'total_page' => $questions->lastPage(),
            'displayed_start' => (($questions->currentPage() - 1) * $questions->perPage()) + 1,
            'displayed_end' => ($questions->currentPage() == $questions->lastPage()) ? $questions->total() : $questions->currentPage() * $questions->perPage()
        ];

        if ($request->ajax()) {
            return $this->jsonUnescaped([
                'status' => 'success',
                'data' => $data
            ]);
        }

        return view('admin.english-questions.index', compact('data'));
    }

    public function trashed(Request $request)
    {
        $this->validate($request, [
            'keyword' => 'nullable|string'
        ]);

        $questions = EnglishQuestion::onlyTrashed()->where('question', 'LIKE', '%'.$request->keyword.'%')->orderBy('created_at', 'DESC')->paginate(50);

        $data = [
            'questions' => EnglishQuestionResource::collection($questions),
            'total' => $questions->total(),
            'per_page' => $questions->perPage(),
            'current_page' => $questions->currentPage(),
            'total_page' => $questions->lastPage(),
            'displayed_start' => (($questions->currentPage() - 1) * $questions->perPage()) + 1,
            'displayed_end' => ($questions->currentPage() == $questions->lastPage()) ? $questions->total() : $questions->currentPage() * $questions->perPage()
        ];

        if ($request->ajax()) {
            return $this->jsonUnescaped([
                'status' => 'success',
                'data' => $data
            ]);
        }
        
        return view('admin.english-questions.trashed', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $answers = [];

        while (count($answers) < 4) {
            $randomString = Str::random(10);
            if (!in_array($randomString, $answers)) {
                $answers[] = [
                    'id' => $randomString,
                    'answer' => ''
                ];
            }
        }

        return view('admin.english-questions.create', compact('answers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'question' => 'required|string|unique:english_questions',
            'timer' => 'required|numeric|min:1',
            'answers' => 'required|array|min:4',
            'answers.*' => 'required|string',
            'answer_ids' => 'required|array|min:4',
            'answer_ids.*' => 'required|string',
            'correct_answer' => 'required|string',
        ], [
            'answers.*.required' => 'Please input the answer selection!'
        ]);

        $answerIds = $request->answer_ids;
        $correctAnswer = $request->correct_answer;

        if (!in_array($correctAnswer, $answerIds)) {
            session()->flash('flash_alert', [
                'level' => 'danger',
                'message' => 'Correct Answer ID is not present in answer IDs!'
            ]);

            return redirect()->back()->withInput();
        }

        $answers = [];
        foreach ($request->answers as $key => $answer) {
            $answers[] = [
                'id' => $answerIds[$key],
                'answer' => $answer
            ];
        }

        EnglishQuestion::create([
            'question' => $request->question,
            'timer' => $request->timer,
            'answers' => $answers,
            'correct_answer' => $correctAnswer,
        ]);

        session()->flash('flash_alert', [
            'level' => 'success',
            'message' => trans('common.saved'),
        ]);

        return redirect()->route('admin.english-questions.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EnglishQuestion  $englishQuestion
     * @return \Illuminate\Http\Response
     */
    public function edit(EnglishQuestion $englishQuestion)
    {
        $answers = $englishQuestion->answers;
        return view('admin.english-questions.edit', compact('englishQuestion', 'answers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EnglishQuestion  $englishQuestion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EnglishQuestion $englishQuestion)
    {
        $this->validate($request, [
            'question' => 'required|string|unique:english_questions,question,' . $englishQuestion->id
        ]);

        $this->validate($request, [
            'question' => 'required|string|unique:english_questions,question,' . $englishQuestion->id,
            'timer' => 'required|numeric|min:1',
            'answers' => 'required|array|min:4',
            'answers.*' => 'required|string',
            'answer_ids' => 'required|array|min:4',
            'answer_ids.*' => 'required|string',
            'correct_answer' => 'required|string',
        ], [
            'answers.*.required' => 'Please input the answer selection!'
        ]);

        $answerIds = $request->answer_ids;
        $correctAnswer = $request->correct_answer;

        if (!in_array($correctAnswer, $answerIds)) {
            session()->flash('flash_alert', [
                'level' => 'danger',
                'message' => 'Correct Answer ID is not present in answer IDs!'
            ]);

            return redirect()->back()->withInput();
        }

        $answers = [];
        foreach ($request->answers as $key => $answer) {
            $answers[] = [
                'id' => $answerIds[$key],
                'answer' => $answer
            ];
        }

        $englishQuestion->update([
            'question' => $request->question,
            'timer' => $request->timer,
            'answers' => $answers,
            'correct_answer' => $correctAnswer,
        ]);

        session()->flash('flash_alert', [
            'level' => 'success',
            'message' => trans('common.updated'),
        ]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EnglishQuestion  $englishQuestion
     * @return \Illuminate\Http\Response
     */
    public function destroy(EnglishQuestion $englishQuestion)
    {
        $englishQuestion->delete();

        return $this->successResponse(trans('common.trashed'), []);
    }

    /**
     * Destroy permanent education degree
     *
     * @param Int $id
     * @return Json response
     */
    public function destroyPermanent(Int $id)
    {
        $question = EnglishQuestion::onlyTrashed()->findOrFail($id);
        $question->forceDelete();

        return $this->successResponse(trans('common.permanent_deleted'), []);
    }

    /**
     * Bulk destroy education degrees
     *
     * @param Request $request
     * @return Json response
     */
    public function bulkDestroy(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $questions = EnglishQuestion::findMany($request->ids);

        foreach ($questions as $question) {
            $question->delete();
        }

        return $this->successResponse(trans('common.trashed'), []);
    }

    public function bulkDestroyPermanent(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $questions = EnglishQuestion::onlyTrashed()->findMany($request->ids);

        foreach ($questions as $question) {
            $question->forceDelete();
        }

        return $this->successResponse(trans('common.permanent_deleted'), []);
    }

    public function restore(Int $id)
    {
        $question = EnglishQuestion::onlyTrashed()->findOrFail($id);
        $question->restore();

        return $this->successResponse(trans('common.restored'), []);
    }
}
