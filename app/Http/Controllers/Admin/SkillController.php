<?php

namespace App\Http\Controllers\Admin;

use App\Models\Skill;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\SkillResource;
use App\Repositories\SkillRepository;

class SkillController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $admin = $request->user();
            if ($admin->primary) {
                return $next($request);
            }
            
            if (in_array('manage-skill', $this->getPermissions($admin))) {
                return $next($request);
            } else {
                return redirect()->route('admin.home');
            }
        });
    }

    /**
     * Filter request
     *
     * @param Request $request
     * @return Array
     */
    private function _filters(Request $request)
    {
        return [
            'keyword' => $request->keyword,
            'name' => $request->name,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $skillRepository = new SkillRepository();
        $skillRepository->adminListRequestValidation($request);

        $skills = Skill::select('*');
        $skillData = $skillRepository->search($skills, $this->_filters($request))->paginate(50);

        $data = [
            'skills' => SkillResource::collection($skillData),
            'total' => $skillData->total(),
            'per_page' => $skillData->perPage(),
            'current_page' => $skillData->currentPage(),
            'total_page' => $skillData->lastPage(),
            'displayed_start' => (($skillData->currentPage() - 1) * $skillData->perPage()) + 1,
            'displayed_end' => ($skillData->currentPage() == $skillData->lastPage()) ? $skillData->total() : $skillData->currentPage() * $skillData->perPage()
        ];

        if ($request->ajax()) {
            return $this->jsonUnescaped([
                'status' => 'success',
                'data' => $data
            ]);
        }

        return view('admin.skills.index', compact('data'));
    }

    /**
     * Trashed skills
     *
     * @param Request $request
     * @return Mixed Json or blade view
     */
    public function trashed(Request $request)
    {
        $skillRepository = new SkillRepository();
        $skillRepository->adminListRequestValidation($request);

        $skills = Skill::onlyTrashed();
        $skillData = $skillRepository->search($skills, $this->_filters($request))->paginate(50);

        $data = [
            'skills' => SkillResource::collection($skillData),
            'total' => $skillData->total(),
            'per_page' => $skillData->perPage(),
            'current_page' => $skillData->currentPage(),
            'total_page' => $skillData->lastPage(),
            'displayed_start' => (($skillData->currentPage() - 1) * $skillData->perPage()) + 1,
            'displayed_end' => ($skillData->currentPage() == $skillData->lastPage()) ? $skillData->total() : $skillData->currentPage() * $skillData->perPage()
        ];

        if ($request->ajax()) {
            return $this->jsonUnescaped([
                'status' => 'success',
                'data' => $data
            ]);
        }

        return view('admin.skills.trashed', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.skills.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:skills'
        ]);

        Skill::create($request->all());

        return $this->successResponse(trans('common.saved'), []);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Skill  $skill
     * @return \Illuminate\Http\Response
     */
    public function edit(Skill $skill)
    {
        return view('admin.skills.edit', compact('skill'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Skill  $skill
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Skill $skill)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:skills,name,'.$skill->id
        ]);

        $skill->update($request->all());

        return $this->successResponse(trans('common.updated'), []);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Skill  $skill
     * @return \Illuminate\Http\Response
     */
    public function destroy(Skill $skill)
    {
        $skill->delete();

        return $this->successResponse(trans('common.trashed'), []);
    }

    /**
     * Destroy permanent skill
     *
     * @param Int $id
     * @return Json response
     */
    public function destroyPermanent(Int $id)
    {
        $skill = Skill::onlyTrashed()->findOrFail($id);
        $skill->forceDelete();

        return $this->successResponse(trans('common.permanent_deleted'), []);
    }

    /**
     * Bulk destroy skills
     *
     * @param Request $request
     * @return Json response
     */
    public function bulkDestroy(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $skills = Skill::findMany($request->ids);

        foreach ($skills as $skill) {
            $skill->delete();
        }

        return $this->successResponse(trans('common.trashed'), []);
    }

    /**
     * Bulk destroy permanent skills
     *
     * @param Request $request
     * @return Json response
     */
    public function bulkDestroyPermanent(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $skills = Skill::onlyTrashed()->findMany($request->ids);

        foreach ($skills as $skill) {
            $skill->forceDelete();
        }

        return $this->successResponse(trans('common.permanent_deleted'), []);
    }

    /**
     * Restore skill from trashed
     *
     * @param Int $id
     * @return Json response
     */
    public function restore(Int $id)
    {
        $skill = Skill::onlyTrashed()->findOrFail($id);
        $skill->restore();

        return $this->successResponse(trans('common.restored'), []);
    }
}
