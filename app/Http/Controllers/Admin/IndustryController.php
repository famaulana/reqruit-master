<?php

namespace App\Http\Controllers\Admin;

use App\Models\Industry;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\IndustryResource;

class IndustryController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $admin = $request->user();
            if ($admin->primary) {
                return $next($request);
            }
            
            if (in_array('manage-industry', $this->getPermissions($admin))) {
                return $next($request);
            } else {
                return redirect()->route('admin.home');
            }
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->validate($request, [
            'keyword' => 'nullable|string'
        ]);

        $industries = Industry::where('name', 'LIKE', '%'.$request->keyword.'%')->orderBy('created_at', 'DESC')->paginate(50);

        $data = [
            'industries' => IndustryResource::collection($industries),
            'total' => $industries->total(),
            'per_page' => $industries->perPage(),
            'current_page' => $industries->currentPage(),
            'total_page' => $industries->lastPage(),
            'displayed_start' => (($industries->currentPage() - 1) * $industries->perPage()) + 1,
            'displayed_end' => ($industries->currentPage() == $industries->lastPage()) ? $industries->total() : $industries->currentPage() * $industries->perPage()
        ];

        if ($request->ajax()) {
            return $this->jsonUnescaped([
                'status' => 'success',
                'data' => $data
            ]);
        }

        return view('admin.industries.index', compact('data'));
    }

    /**
     * Trashed industries
     *
     * @param Request $request
     * @return Mixed Json or blade view
     */
    public function trashed(Request $request)
    {
        $this->validate($request, [
            'keyword' => 'nullable|string'
        ]);

        $industries = Industry::onlyTrashed()->where('name', 'LIKE', '%'.$request->keyword.'%')->orderBy('created_at', 'DESC')->paginate(50);

        $data = [
            'industries' => IndustryResource::collection($industries),
            'total' => $industries->total(),
            'per_page' => $industries->perPage(),
            'current_page' => $industries->currentPage(),
            'total_page' => $industries->lastPage(),
            'displayed_start' => (($industries->currentPage() - 1) * $industries->perPage()) + 1,
            'displayed_end' => ($industries->currentPage() == $industries->lastPage()) ? $industries->total() : $industries->currentPage() * $industries->perPage()
        ];

        if ($request->ajax()) {
            return $this->jsonUnescaped([
                'status' => 'success',
                'data' => $data
            ]);
        }

        return view('admin.industries.trashed', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.industries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required|string|unique:industries']);

        Industry::create(['name' => $request->name]);

        session()->flash('flash_alert', [
            'level' => 'success',
            'message' => trans('common.saved'),
        ]);

        return redirect()->route('admin.industries.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Industry  $industry
     * @return \Illuminate\Http\Response
     */
    public function edit(Industry $industry)
    {
        return view('admin.industries.edit', compact('industry'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Industry  $industry
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Industry $industry)
    {
        $this->validate($request, ['name' => 'required|string|unique:industries,name,' . $industry->id]);

        $industry->update(['name' => $request->name]);

        session()->flash('flash_alert', [
            'level' => 'success',
            'message' => trans('common.updated'),
        ]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Industry  $industry
     * @return \Illuminate\Http\Response
     */
    public function destroy(Industry $industry)
    {
        $industry->delete();

        return $this->successResponse(trans('common.permanent_deleted'), []);
    }

    /**
     * Destroy permanent industry
     *
     * @param Int $id
     * @return Json response
     */
    public function destroyPermanent(Int $id)
    {
        $industry = Industry::onlyTrashed()->findOrFail($id);
        $industry->forceDelete();

        return $this->successResponse(trans('common.permanent_deleted'), []);
    }

    /**
     * Bulk destroy education degrees
     *
     * @param Request $request
     * @return Json response
     */
    public function bulkDestroy(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $industries = Industry::findMany($request->ids);

        foreach ($industries as $industry) {
            $industry->delete();
        }

        return $this->successResponse(trans('common.permanent_deleted'), []);
    }

    /**
     * Bulk destroy permanent industries
     *
     * @param Request $request
     * @return Json response
     */
    public function bulkDestroyPermanent(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $industries = Industry::onlyTrashed()->findMany($request->ids);

        foreach ($industries as $industry) {
            $industry->forceDelete();
        }

        return $this->successResponse(trans('common.permanent_deleted'), []);
    }

    /**
     * Restore industry from trashed
     *
     * @param Int $id
     * @return Json response
     */
    public function restore(Int $id)
    {
        $industry = Industry::onlyTrashed()->findOrFail($id);
        $industry->restore();

        return $this->successResponse(trans('common.restored'), []);
    }
}
