<?php

namespace App\Http\Controllers\Admin;

use App\Models\Page;
use App\Helpers\PathHelper;
use App\Helpers\ImageHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PageResource;

class PageController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (in_array('manage-page', $this->getPermissions($request->user()))) {
                return $next($request);
            } else {
                return redirect()->route('admin.home');
            }
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->validate($request, [
            'keyword' => 'nullable|string',
        ]);

        $pages = Page::select('*');

        $searchQuery = $this->_searchQuery($request, $pages);
        $pageData = $searchQuery['search_data'];

        $data = [
            'pages' => PageResource::collection($pageData),
            'total' => $pageData->total(),
            'per_page' => $pageData->perPage(),
            'current_page' => $pageData->currentPage(),
            'total_page' => $pageData->lastPage(),
            'displayed_start' => (($pageData->currentPage() - 1) * $pageData->perPage()) + 1,
            'displayed_end' => ($pageData->currentPage() == $pageData->lastPage()) ? $pageData->total() : $pageData->currentPage() * $pageData->perPage()
        ];

        if ($request->ajax()) {
            return $this->jsonUnescaped([
                'status' => 'success',
                'data' => $data
            ]);
        }

        return view('admin.pages.index', compact('data'));
    }

    public function trashed(Request $request)
    {
        $this->validate($request, [
            'keyword' => 'nullable|string',
        ]);

        $pages = Page::onlyTrashed()->select('*');

        $searchQuery = $this->_searchQuery($request, $pages);
        $pageData = $searchQuery['search_data'];

        $data = [
            'pages' => PageResource::collection($pageData),
            'total' => $pageData->total(),
            'per_page' => $pageData->perPage(),
            'current_page' => $pageData->currentPage(),
            'total_page' => $pageData->lastPage(),
            'displayed_start' => (($pageData->currentPage() - 1) * $pageData->perPage()) + 1,
            'displayed_end' => ($pageData->currentPage() == $pageData->lastPage()) ? $pageData->total() : $pageData->currentPage() * $pageData->perPage()
        ];

        if ($request->ajax()) {
            return $this->jsonUnescaped([
                'status' => 'success',
                'data' => $data
            ]);
        }

        return view('admin.pages.trashed', compact('data'));
    }

    private function _searchQuery(Request $request, $pages)
    {
        if ($request->has('keyword')) {
            $pages = $pages->where('title', 'like', '%' . $request->keyword . '%');
        }

        return [
           'search_data' => $pages->orderBy('created_at', 'DESC')->paginate(50)
       ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'             => 'required|unique:pages',
            'slug'             => 'required|unique:pages',
            'content'           => 'required|unique:pages',
            'meta_title'        => 'required|unique:pages',
            'meta_description'  => 'required|unique:pages',
        ]);

        $page = Page::create([
            'title' => $request->title,
            'slug' => $request->slug,
            'content' => $request->content,
            'meta_title' => $request->meta_title,
            'meta_description' => $request->meta_description,
        ]);

        session()->flash('flash_alert', [
            'level' => 'success',
            'message' => trans('common.saved'),
        ]);

        return $this->jsonUnescaped([
            'status' => 'success',
            'message' => trans('common.saved'),
            'data' => new PageResource($page)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        return view('admin.pages.edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'             => 'required|unique:pages,title,'.$id,
            'slug'             => 'required|unique:pages,slug,'.$id,
            'content'           => 'required|unique:pages,content,'.$id,
            'meta_title'        => 'required|unique:pages,meta_title,'.$id,
            'meta_description'  => 'required|unique:pages,meta_description,'.$id,
        ]);

        $page = Page::findOrFail($id);

        $page->update([
            'title' => $request->title,
            'slug' => $request->slug,
            'content' => $request->content,
            'meta_title' => $request->meta_title,
            'meta_description' => $request->meta_description,
        ]);

        session()->flash('flash_alert', [
            'level' => 'success',
            'message' => trans('common.updated'),
        ]);

        return $this->jsonUnescaped([
            'status' => 'success',
            'message' => trans('common.updated'),
            'data' => new PageResource($page)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        $page->delete();
        
        return $this->jsonUnescaped([
            'status' => 'success',
            'message' => trans('common.trashed'),
        ]);
    }

    public function destroyPermanent($id)
    {
        $page = Page::onlyTrashed()->findOrFail($id);
        $page->forceDelete();

        return response()->json([
            'status' => 'success',
            'message' => trans('common.permanent_deleted'),
        ]);
    }

    public function bulkDestroy(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $pages = Page::findMany($request->ids);

        foreach ($pages as $page) {
            $page->delete();
        }

        return response()->json([
            'status' => 'success',
            'message' => trans('common.trashed'),
        ]);
    }

    public function bulkDestroyPermanent(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $pages = Page::onlyTrashed()->findMany($request->ids);

        foreach ($pages as $page) {
            $page->forceDelete();
        }

        return response()->json([
            'status' => 'success',
            'message' => trans('common.permanent_deleted'),
        ]);
    }

    public function restore($id)
    {
        $page = Page::onlyTrashed()->findOrFail($id);
        $page->restore();

        return response()->json([
            'status' => 'success',
            'message' => trans('common.restored'),
        ]);
    }

    public function storeContentImage(Request $request)
    {
        $this->validate($request, [
            'upload' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:700',
        ]);

        $path = PathHelper::page_content_image_path();
        $fileName = ImageHelper::processImg($request->upload, $path);

        return $this->jsonUnescaped([
            'status' => 'success',
            'url' => ImageHelper::getImageUrl($path, $fileName)
        ]);
    }
}
