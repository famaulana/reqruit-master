<?php

namespace App\Http\Controllers\Admin;

use App\Models\Employer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CompanyResource;
use App\Repositories\CompanyRepository;
use App\Http\Resources\EmployerResource;
use App\Repositories\EmployerRepository;

class EmployerController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $admin = $request->user();
            if ($admin->primary) {
                return $next($request);
            }
            
            if (in_array('manage-employer', $this->getPermissions($admin))) {
                return $next($request);
            } else {
                return redirect()->route('admin.home');
            }
        });
    }

    /**
     * Filter request
     *
     * @param Request $request
     * @return Array
     */
    private function _filters(Request $request)
    {
        return [
            'keyword' => $request->keyword,
            'identifier' => $request->identifier,
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'gender' => $request->gender,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $employerRepository = new EmployerRepository();
        $employerRepository->adminListRequestValidation($request);

        $employers = Employer::select('*');
        $employerData = $employerRepository->search($employers, $this->_filters($request))->paginate(50);

        $data = [
            'employers' => EmployerResource::collection($employerData),
            'total' => $employerData->total(),
            'per_page' => $employerData->perPage(),
            'current_page' => $employerData->currentPage(),
            'total_page' => $employerData->lastPage(),
            'displayed_start' => (($employerData->currentPage() - 1) * $employerData->perPage()) + 1,
            'displayed_end' => ($employerData->currentPage() == $employerData->lastPage()) ? $employerData->total() : $employerData->currentPage() * $employerData->perPage()
        ];

        if ($request->ajax()) {
            return $this->jsonUnescaped([
                'status' => 'success',
                'data' => $data
            ]);
        }

        return view('admin.employers.index', compact('data'));
    }

    /**
     * Trashed employers
     *
     * @param Request $request
     * @return Mixed Json or blade view
     */
    public function trashed(Request $request)
    {
        $employerRepository = new EmployerRepository();
        $employerRepository->adminListRequestValidation($request);

        $employers = Employer::onlyTrashed();
        $employerData = $employerRepository->search($employers, $this->_filters($request))->paginate(50);

        $data = [
            'employers' => EmployerResource::collection($employerData),
            'total' => $employerData->total(),
            'per_page' => $employerData->perPage(),
            'current_page' => $employerData->currentPage(),
            'total_page' => $employerData->lastPage(),
            'displayed_start' => (($employerData->currentPage() - 1) * $employerData->perPage()) + 1,
            'displayed_end' => ($employerData->currentPage() == $employerData->lastPage()) ? $employerData->total() : $employerData->currentPage() * $employerData->perPage()
        ];

        if ($request->ajax()) {
            return $this->jsonUnescaped([
                'status' => 'success',
                'data' => $data
            ]);
        }

        return view('admin.employers.trashed', compact('data'));
    }

    /**
     * Display the specified resource.
     *
     * @param  Employer  $employer
     * @return \Illuminate\Http\Response
     */
    public function show(Employer $employer)
    {
        $company = $employer->company;
        return view('admin.employers.informations.show', compact('employer', 'company'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Employer  $employer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employer $employer)
    {
        $employer->delete();

        return response()->json([
            'status' => 'success',
            'message' => trans('common.trashed'),
        ]);
    }

    /**
     * Destroy permanent employer
     *
     * @param Int $id
     * @return Json response
     */
    public function destroyPermanent(Int $id)
    {
        $employer = Employer::onlyTrashed()->findOrFail($id);
        $employer->forceDelete();

        return response()->json([
            'status' => 'success',
            'message' => trans('common.permanent_deleted'),
        ]);
    }

    /**
     * Bulk destroy employers
     *
     * @param Request $request
     * @return Json response
     */
    public function bulkDestroy(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $employers = Employer::findMany($request->ids);

        foreach ($employers as $employer) {
            $employer->delete();
        }

        return response()->json([
            'status' => 'success',
            'message' => trans('common.trashed'),
        ]);
    }

    /**
     * Bulk destroy permanent employers
     *
     * @param Request $request
     * @return Json response
     */
    public function bulkDestroyPermanent(Request $request)
    {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);

        $employers = Employer::onlyTrashed()->findMany($request->ids);

        foreach ($employers as $employer) {
            $employer->forceDelete();
        }

        return response()->json([
            'status' => 'success',
            'message' => trans('common.permanent_deleted'),
        ]);
    }

    /**
     * Restore employer from trashed
     *
     * @param Int $id
     * @return Json response
     */
    public function restore(Int $id)
    {
        $employer = Employer::onlyTrashed()->findOrFail($id);
        $employer->restore();

        return response()->json([
            'status' => 'success',
            'message' => trans('common.restored'),
        ]);
    }

    /**
     * List of employer companies
     *
     * @param Request $request
     * @param Employer $employer
     * @return Blade view
    */
    public function companies(Request $request, Employer $employer)
    {
        $this->validate($request, [
            'keyword' => 'nullable|string',
            'name' => 'nullable|string',
            'start_date' => 'required_with:end_date|string|date_format:Y-m-d',
            'end_date' => 'required_with:start_date|string|date_format:Y-m-d'
        ]);

        $companies = $employer->companies();
        $companyRepository = new CompanyRepository();
        $filters = [
            'keyword' => $request->keyword,
            'name' => $request->name,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
        ];
        $companies = $companyRepository->search($companies, $filters)->paginate(50);

        $data = [
            'employer' => new EmployerResource($employer),
            'companies' => CompanyResource::collection($companies),
            'total' => $companies->total(),
            'per_page' => $companies->perPage(),
            'current_page' => $companies->currentPage(),
            'total_page' => $companies->lastPage(),
            'displayed_start' => (($companies->currentPage() - 1) * $companies->perPage()) + 1,
            'displayed_end' => ($companies->currentPage() == $companies->lastPage()) ? $companies->total() : $companies->currentPage() * $companies->perPage()
        ];

        if ($request->ajax()) {
            return $this->jsonUnescaped([
                'status' => 'success',
                'data' => $data
            ]);
        }

        return view('admin.employers.informations.companies', compact('data', 'employer'));
    }
}
