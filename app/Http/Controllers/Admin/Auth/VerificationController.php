<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\VerifiesEmails;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = '/auth/panel-login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }

    public function resend(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email'
        ]);

        $admin = Admin::where('email', $request->email)->first();
        if ($admin === null) {
            Session::flash('flash_alert', [
                'level' => 'warning',
                'message' => "User not found",
            ]);

            return redirect()->back();
        }

        $admin->sendVerification();

        return back()->with('resent', true);
    }

    public function verify(Request $request, $token = null)
    {
        if ($token === null) {
            Session::flash('flash_alert', [
                'level' => 'warning',
                'message' => "Missing token",
            ]);

            return view('admin.auth.verify');
        }

        $admin = Admin::where('verify_token', $token)->first();
        if ($admin === null) {
            Session::flash('flash_alert', [
                'level' => 'warning',
                'message' => "Admin not found",
            ]);

            return redirect()->route('auth.admin.login.form');
        }

        $admin->verify();

        Session::flash('flash_alert', [
            'level' => 'success',
            'message' => "Email telah terverifikasi.",
        ]);

        return redirect()->route('auth.admin.login.form');
    }
}
