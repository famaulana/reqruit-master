<?php

namespace App\Http\View\Composers;

use App\Repositories\AppConfigRepository;
use Illuminate\View\View;

class LayoutComposer
{
    /**
     * The configuration file urls.
     *
     * @var Array
     */
    protected $configFileUrls;

    /**
     * Class constructor.
     *
     * @param  AppConfigRepository  $appConfigRepository
     * @return void
     */
    public function __construct(AppConfigRepository $appConfigRepository)
    {
        $this->configFileUrls = $appConfigRepository->configFileUrls();
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('adminLogoUrl', $this->configFileUrls['admin_logo']);
        $view->with('frontendLogoUrl', $this->configFileUrls['frontend_logo']);
        $view->with('emailLogoUrl', $this->configFileUrls['email_logo']);
        $view->with('invoiceLogoUrl', $this->configFileUrls['invoice_logo']);
        $view->with('lightLogoUrl', $this->configFileUrls['light_logo']);
        $view->with('darkLogoUrl', $this->configFileUrls['dark_logo']);
        $view->with('faviconUrl', $this->configFileUrls['favicon']);
    }
}
