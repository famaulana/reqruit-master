<?php

namespace App\Http\View\Composers;

use App\Repositories\AppConfigRepository;
use Illuminate\View\View;

class FormattingComposer
{
    /**
     * locale.
     *
     * @var String
     */
    protected $locale;

    /**
     * Class constructor.
     *
     * @param  AppConfigRepository  $appConfigRepository
     * @return void
     */
    public function __construct(AppConfigRepository $appConfigRepository)
    {
        $this->locale = $appConfigRepository->getConfig('locale');
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('locale', $this->locale);
        $view->with('baseCurrency', 'IDR');
        $view->with('currencySymbol', 'IDR');
    }
}
