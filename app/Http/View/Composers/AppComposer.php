<?php

namespace App\Http\View\Composers;

use App\Repositories\AppConfigRepository;
use Illuminate\View\View;

class AppComposer
{
    /**
     * Website name
     *
     * @var String
     */
    protected $siteName;

    /**
     * Javascript
     *
     * @var String
     */
    protected $javascript;

    /**
     * Class constructor.
     *
     * @param  AppConfigRepository  $appConfigRepository
     * @return void
     */
    public function __construct(AppConfigRepository $appConfigRepository)
    {
        $this->siteName = $appConfigRepository->getConfig('site_name');
        $this->javascript = $appConfigRepository->getConfig('javascript');
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('siteName', $this->siteName);
        $view->with('javascript', $this->javascript);
    }
}
