<?php

namespace App\Theming\Helpers;

class ThemingHelper
{
    /**
     * Get available themes
     *
     * @return Array
     */
    public static function availableThemes()
    {
        $themesDirs = glob(base_path('app/Themes') . '/*', GLOB_ONLYDIR);

        $themes = [];
        foreach ($themesDirs as $themeDir) {
            $themeInfoFile = "$themeDir/etc/theme_info.php";

            if (file_exists($themeInfoFile)) {
                $themeInfo = require $themeInfoFile;
                $themes[] = [
                    'name' => $themeInfo['name'],
                    'domain' => $themeInfo['domain'],
                    'namespace' => $themeInfo['namespace'],
                ];
            }
        }

        return $themes;
    }

    /**
     * Get theme info
     *
     * @return Array
     */
    public static function themeInfo($theme)
    {
        $themeInfoFile = app_path('Themes/'.$theme.'/etc/theme_info.php');

        if (!file_exists($themeInfoFile)) {
            return [];
        }

        return require $themeInfoFile;
    }

    /**
     * Get Active theme info
     *
     * @return Array
     */
    public static function activeThemeInfo()
    {
        return self::themeInfo(config('active_theme.domain'));
    }
}
