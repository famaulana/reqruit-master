<?php

namespace App\Theming\Interfaces\Admin;

interface ThemeControllerInterface
{
    /**
     * Root admin view directory
     *
     * @return String
     */
    public function rootAdminView();

    /**
     * Root common view directory
     *
     * @return String
     */
    public function rootCommonView();

    /**
     * Root customer view directory
     *
     * @return String
     */
    public function rootCustomerView();
}
