<?php

namespace App\Theming\Interfaces\Admin;

interface AdminMenuInterface
{
    /**
     * Declare admin menus
     *
     * @return Array of path => menu name
     */
    public function declareMenus();
}
