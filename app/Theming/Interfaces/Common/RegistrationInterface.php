<?php

namespace App\Theming\Interfaces\Common;

interface RegistrationInterface
{
    /**
     * Theme name
     *
     * @return String
     */
    public function themeName();

    /**
     * Theme domain name
     *
     * @return String
     */
    public function themeDomain();

    /**
     * Theme root namespace
     *
     * @return String
     */
    public function themeNamespace();
}
