<?php

namespace App\Theming\Admin;

use App\Theming\Helpers\ThemingHelper;

class AdminMenu
{
    public function getAdminMenus()
    {
        $menus = '\App\Themes\\'.config('active_theme.namespace').'\\Admin\Menus';
        $menus = new $menus;
        return $menus->declareMenus();
    }
}
