<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Open()
 * @method static static Closed()
 */
final class WorkjobPostingStatus extends Enum
{
    const Open = 'open';
    const Closed = 'closed';
}
