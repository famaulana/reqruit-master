<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static SuperAdministrator()
 * @method static static Administrator()
 * @method static static Reqruiter()
 */
final class EmployerRole extends Enum
{
    const SuperAdministrator = 'super-administrator';
    const Administrator = 'administrator';
    const Reqruiter = 'reqruiter';
}
