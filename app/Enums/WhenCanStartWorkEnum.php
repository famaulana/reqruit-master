<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static AnyTime()
 * @method static static NotDecide()
 * @method static static ChooseDate()
 */
final class WhenCanStartWorkEnum extends Enum
{
    const AnyTime = 'anytime';
    const NotDecide = 'not-decide';
    const ChooseDate = 'choose-date';

    public static function getDescription($value): string
    {
        if ($value === self::AnyTime) {
            return 'Kapan saja';
        }
        if ($value === self::NotDecide) {
            return 'Belum tahu';
        }
        if ($value === self::ChooseDate) {
            return 'Pilih tanggal';
        }

        return parent::getDescription($value);
    }
}
