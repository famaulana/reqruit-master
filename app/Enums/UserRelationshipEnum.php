<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Married()
 * @method static static Single()
 */
final class UserRelationshipEnum extends Enum
{
    const Married = 'married';
    const Single = 'single';
}
