<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Pending()
 * @method static static Shortlisted()
 * @method static static Interview()
 * @method static static Offered()
 * @method static static Hired()
 * @method static static Unsuitable()
 */
final class JobApplicationStatus extends Enum
{
    const Pending = 'pending';
    const Shortlisted = 'shortlisted';
    const Interview = 'interview';
    const Offered = 'offered';
    const Hired = 'hired';
    const Unsuitable = 'unsuitable';
}
