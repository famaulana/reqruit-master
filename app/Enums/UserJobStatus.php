<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OpportunityClosed()
 * @method static static OpportunityOpened()
 * @method static static ActivelyJobSeeker()
 */
final class UserJobStatus extends Enum
{
    const OpportunityClosed = 'opportunity-closed';
    const OpportunityOpened = 'opportunity-opened';
    const ActivelyJobSeeker = 'actively-job-seeker';

    public static function getDescription($value): string
    {
        if ($value === self::OpportunityClosed) {
            return 'Tidak menerima lowongan';
        }
        if ($value === self::OpportunityOpened) {
            return 'Terbuka untuk lowongan';
        }
        if ($value === self::ActivelyJobSeeker) {
            return 'Aktif mencari kerja';
        }

        return parent::getDescription($value);
    }
}
