<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Accept()
 * @method static static Decline()
 * @method static static TimeNotSuit()
 */
final class InterviewStatusEnum extends Enum
{
    const Accept = 'accept';
    const Decline = 'decline';
    const TimeNotSuit = 'time-not-suit';

    public static function getDescription($value): string
    {
        if ($value === self::Accept) {
            return 'Terima';
        }
        if ($value === self::Decline) {
            return 'Tolak';
        }
        if ($value === self::TimeNotSuit) {
            return 'Waktu tidak sesuai';
        }

        return parent::getDescription($value);
    }
}
