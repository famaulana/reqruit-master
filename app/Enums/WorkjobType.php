<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Contract()
 * @method static static ContractEmployee()
 * @method static static Freelance()
 * @method static static FullTime()
 * @method static static Internship()
 */
final class WorkjobType extends Enum
{
    const Contract = 'contract';
    const ContractEmployee = 'contract-employee';
    const Freelance = 'freelance';
    const FullTime = 'full-time';
    const Internship = 'internship';
}
