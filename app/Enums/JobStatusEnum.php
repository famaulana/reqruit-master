<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Current()
 * @method static static Former()
 */
final class JobStatusEnum extends Enum
{
    const Current = 'current';
    const Former = 'former';

    public static function getDescription($value): string
    {
        if ($value === self::Current) {
            return 'Current employee';
        }

        if ($value === self::Former) {
            return 'Former employee';
        }

        return parent::getDescription($value);
    }
}
