<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Citizen()
 * @method static static PermanentResident()
 * @method static static WorkPermit()
 * @method static static EmploymentPass()
 * @method static static StudentPass()
 */
final class ResidenceStatusEnum extends Enum
{
    const Citizen = 'citizen';
    const PermanentResident = 'permanent-resident';
    const WorkPermit = 'work-permit';
    const EmploymentPass = 'employment-pass';
    const StudentPass = 'student-pass';

    public static function toVueSelectArray()
    {
        $array = self::toSelectArray();
        $result = [];
        foreach ($array as $value => $description) {
            $result[] = [
                'id' => $value,
                'name' => $description
            ];
        }

        return $result;
    }
}
