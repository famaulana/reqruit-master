<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Monthly()
 * @method static static Yearly()
 * @method static static PerProject()
 */
final class WorkjobSalaryDuration extends Enum
{
    const Monthly = 'monthly';
    const Yearly = 'yearly';
    const PerProject = 'per-project';
}
