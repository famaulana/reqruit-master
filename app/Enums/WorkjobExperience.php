<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static LessThenOneYear()
 * @method static static OneToThreeYears()
 * @method static static ThreeToFiveYears()
 * @method static static FiveToTenYears()
 * @method static static TenYearsMore()
 * @method static static NoPreferences()
 */
final class WorkjobExperience extends Enum
{
    const LessThenOneYear = 'less-then-1-year';
    const OneToThreeYears = '1-3-years';
    const ThreeToFiveYears = '3-5-years';
    const FiveToTenYears = '5-10-years';
    const TenYearsMore = '10-years-more';
    const NoPreferences = 'no-preferences';

    public static function getDescription($value): string
    {
        if ($value === self::LessThenOneYear) {
            return 'Less then a year';
        }
        if ($value === self::OneToThreeYears) {
            return '1 - 3 years';
        }
        if ($value === self::ThreeToFiveYears) {
            return '3 - 5 years';
        }
        if ($value === self::FiveToTenYears) {
            return '5 - 10 years';
        }
        if ($value === self::TenYearsMore) {
            return 'More than 10 years';
        }
        if ($value === self::NoPreferences) {
            return 'No preferences';
        }

        return parent::getDescription($value);
    }
}
