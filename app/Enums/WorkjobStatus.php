<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Draft()
 * @method static static Published()
 */
final class WorkjobStatus extends Enum
{
    const Draft = 'draft';
    const Published = 'published';
}
