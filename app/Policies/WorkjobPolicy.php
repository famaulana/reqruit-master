<?php

namespace App\Policies;

use App\Models\Employer;
use App\Models\Workjob;
use Illuminate\Auth\Access\HandlesAuthorization;

class WorkjobPolicy
{
    use HandlesAuthorization;

    private $_allowedRoles;

    public function __construct()
    {
        $this->_allowedRoles = ['super-administrator', 'administrator'];
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\Employer  $employer
     * @return mixed
     */
    public function create(Employer $employer)
    {
        return in_array($employer->role->value, $this->_allowedRoles);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\Employer  $employer
     * @param  \App\Models\Workjob  $workjob
     * @return mixed
     */
    public function update(Employer $employer, Workjob $workjob)
    {
        $role = $employer->role->value;
        $company = $workjob->company;
        $team = $company->employers->pluck('id')->toArray();

        return in_array($role, $this->_allowedRoles) && in_array($employer->id, $team);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\Employer  $employer
     * @param  \App\Models\Workjob  $workjob
     * @return mixed
     */
    public function delete(Employer $employer, Workjob $workjob)
    {
        $role = $employer->role->value;
        $company = $workjob->company;
        $team = $company->employers->pluck('id')->toArray();

        return in_array($role, $this->_allowedRoles) && in_array($employer->id, $team);
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\Employer  $employer
     * @param  \App\Models\Workjob  $workjob
     * @return mixed
     */
    public function restore(Employer $employer, Workjob $workjob)
    {
        $role = $employer->role->value;
        $company = $workjob->company;
        $team = $company->employers->pluck('id')->toArray();

        return in_array($role, $this->_allowedRoles) && in_array($employer->id, $team);
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\Employer  $employer
     * @param  \App\Models\Workjob  $workjob
     * @return mixed
     */
    public function forceDelete(Employer $employer, Workjob $workjob)
    {
        $role = $employer->role->value;
        $company = $workjob->company;
        $team = $company->employers->pluck('id')->toArray();

        return in_array($role, $this->_allowedRoles) && in_array($employer->id, $team);
    }
}
