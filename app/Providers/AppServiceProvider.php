<?php

namespace App\Providers;

use App\Rules\MaxWordsRule;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(IdeHelperServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        
        View::composer(
            [
                'common.*.layouts.app',
                'common.*.layouts.purchase',
                'common.*.layouts.auth',
                'common.*.layouts.blank',
                'admin.auth.layout',
                'admin.layouts.app'
            ],
            'App\Http\View\Composers\LayoutComposer'
        );

        View::composer('*', 'App\Http\View\Composers\AppComposer');
        View::composer('*', 'App\Http\View\Composers\FormattingComposer');

        Validator::extend(MaxWordsRule::handle(), MaxWordsRule::class);
    }
}
