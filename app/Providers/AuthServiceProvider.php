<?php

namespace App\Providers;

use App\Models\Workjob;
use App\Models\Address;
use Illuminate\Support\Arr;
use App\Policies\WorkjobPolicy;
use App\Policies\AddressPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Workjob::class => WorkjobPolicy::class,
        Address::class => AddressPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // Ability to add guard to route group
        $this->app['router']->matched(function (\Illuminate\Routing\Events\RouteMatched $e) {
            $route = $e->route;
            if (!Arr::has($route->getAction(), 'guard')) {
                return;
            }
            $routeGuard = Arr::get($route->getAction(), 'guard');
            $this->app['auth']->resolveUsersUsing(function ($guard = null) use ($routeGuard) {
                return $this->app['auth']->guard($routeGuard)->user();
            });
            $this->app['auth']->setDefaultDriver($routeGuard);
        });

        // update company
        Gate::define('update-company', function ($employer, $company) {
            return $employer->id == $company->employers()->first()->id;
        });

        // View job applications
        Gate::define('view-job-applications', function ($employer, $company) {
            $team = $company->employers->pluck('id')->toArray();
            return in_array($employer->id, $team);
        });

        // View candidate
        Gate::define('view-candidate', function ($employer, $jobApplication) {
            $company = $jobApplication->workjob->company;
            $team = $company->employers->pluck('id')->toArray();
            return in_array($employer->id, $team);
        });

        // Update employer role
        Gate::define('update-employer-role', function ($employer, $targetEmployer) {
            if ($targetEmployer->role->value == 'super-administrator') {
                return false;
            }
            
            $company = $employer->company;
            $team = $company->employers->pluck('id')->toArray();
            
            return $employer->role->value == 'super-administrator' && in_array($targetEmployer->id, $team);
        });
    }
}
