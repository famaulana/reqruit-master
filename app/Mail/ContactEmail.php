<?php

namespace App\Mail;

use App\Repositories\AppConfigRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Sender name
     *
     * @var String
     */
    public $senderName;

    /**
     * Sender email
     *
     * @var String
     */
    public $senderEmail;

    /**
     * Sender phone
     *
     * @var String
     */
    public $senderPhone;

    /**
     * Message
     *
     * @var String
     */
    public $message;

    /**
     * App configs
     *
     * @var Object
     */
    public $configs;

    /**
     * Logo URL
     *
     * @var String
     */
    public $logoUrl;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(String $senderName, String $senderEmail, String $senderPhone, String $message)
    {
        $this->senderName = $senderName;
        $this->senderEmail = $senderEmail;
        $this->senderPhone = $senderPhone;
        $this->message = $message;
        $this->configs = new AppConfigRepository();
        $this->logoUrl = $this->configs->configFileUrls()['email_logo'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->configs->getConfig('site_email'), $this->configs->getConfig('site_name'))
            ->subject('Contact Us Email: '.$this->senderName)
            ->replyTo($this->senderEmail, $this->senderName)
            ->markdown('emails.contact.contact');
    }
}
