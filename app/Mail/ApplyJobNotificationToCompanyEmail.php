<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Repositories\AppConfigRepository;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApplyJobNotificationToCompanyEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Job application
     *
     * @var Object
     */
    public $jobApplication;

    /**
     * Job title
     *
     * @var String
     */
    public $jobTitle;

    /**
     * User name
     *
     * @var String
     */
    public $userName;

    /**
     * App configs
     *
     * @var Object
     */
    public $configs;

    /**
     * Logo URL
     *
     * @var String
     */
    public $logoUrl;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Object $jobApplication)
    {
        $this->jobApplication = $jobApplication;
        $this->jobTitle = $jobApplication->workjob->title;
        $this->userName = $jobApplication->user->name;
        $this->configs = new AppConfigRepository();
        $this->logoUrl = $this->configs->configFileUrls()['email_logo'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->configs->getConfig('site_email'), $this->configs->getConfig('site_name'))
            ->subject($this->userName.' Applied to Job: '.$this->jobTitle)
            ->markdown('emails.job-application.apply-job-notification-to-company');
    }
}
