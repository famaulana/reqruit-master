<?php

namespace App\Mail;

use App\Models\Employer;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Repositories\AppConfigRepository;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmployerInviteEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $employer;
    public $company;
    public $newEmployer;
    public $password;
    public $logoUrl;
    public $configs;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Employer $employer, $newEmployer, String $password)
    {
        $this->employer = $employer;
        $this->company = $employer->company;
        $this->newEmployer = $newEmployer;
        $this->password = $password;
        $this->configs = new AppConfigRepository();
        $this->logoUrl = $this->configs->configFileUrls()['email_logo'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->configs->getConfig('site_email'), $this->configs->getConfig('site_name'))
            ->subject('Anda Diundang Sebagai Tim '.$this->company->name)
            ->markdown('emails.company.invite');
    }
}
