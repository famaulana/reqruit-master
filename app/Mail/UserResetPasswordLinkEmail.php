<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Repositories\AppConfigRepository;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserResetPasswordLinkEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $user;
    public $logoUrl;
    public $link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, String $token)
    {
        $this->user = $user;

        $configRepositories = new AppConfigRepository();
        $this->logoUrl = $configRepositories->configFileUrls()['email_logo'];
        $this->link = config('frontend.frontend_url').'/auth/reset-password/'.$token.'?email='.$user->email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(config('app_configs.site_email'), config('app_configs.site_name'))
            ->subject('Reset Password - ' . config('app_configs.site_name'))
            ->markdown('emails.user.reset-link');
    }
}
