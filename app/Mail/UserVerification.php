<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Repositories\AppConfigRepository;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserVerification extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $configs;
    public $user;
    public $siteName;
    public $logoUrl;
    public $link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, String $token)
    {
        $this->configs = new AppConfigRepository();
        $this->user = $user;

        $this->siteName = $this->configs->getConfig('site_name');
        $this->logoUrl = $this->configs->configFileUrls()['email_logo'];
        $this->link = config('frontend.frontend_url').'/auth/verify?token='.$token.'&email='.$user->email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->configs->getConfig('site_email'), $this->siteName)
            ->subject('Verifikasi akun - ' . $this->siteName)
            ->markdown('emails.user.verification');
    }
}
