<?php

namespace App\Mail;

use App\Models\Employer;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Repositories\AppConfigRepository;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmployerResetPasswordLinkEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $employer;
    public $logoUrl;
    public $link;
    public $siteEmail;
    public $siteName;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Employer $employer, String $token)
    {
        $this->employer = $employer;

        $configs = new AppConfigRepository();
        $this->logoUrl = $configs->configFileUrls()['email_logo'];
        $this->link = config('frontend.employer_url').'/auth/reset-password/'.$token.'?email='.$employer->email;
        $this->siteEmail = $configs->getConfig('site_email');
        $this->siteName = $configs->getConfig('site_name');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->siteEmail, $this->siteName)
            ->subject('Reset Password - ' . $this->siteName)
            ->markdown('emails.employer.reset-link');
    }
}
