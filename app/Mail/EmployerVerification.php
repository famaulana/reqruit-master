<?php

namespace App\Mail;

use App\Models\Employer;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Repositories\AppConfigRepository;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmployerVerification extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $configs;
    public $employer;
    public $siteName;
    public $logoUrl;
    public $link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Employer $employer, String $token)
    {
        $this->configs = new AppConfigRepository();
        $this->employer = $employer;

        $this->siteName = $this->configs->getConfig('site_name');
        $this->logoUrl = $this->configs->configFileUrls()['email_logo'];
        $this->link = config('frontend.employer_url').'/auth/verify?token='.$token.'&email='.$employer->email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->configs->getConfig('site_email'), $this->siteName)
            ->subject('Verifikasi akun - ' . $this->siteName)
            ->markdown('emails.employer.verification');
    }
}
