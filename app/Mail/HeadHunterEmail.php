<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Repositories\AppConfigRepository;
use Illuminate\Contracts\Queue\ShouldQueue;

class HeadHunterEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Email data
     *
     * @var Array
     */
    public $emailData;

    /**
     * App configs
     *
     * @var Object
     */
    public $configs;

    /**
     * Logo URL
     *
     * @var String
     */
    public $logoUrl;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Array $emailData)
    {
        $this->emailData = $emailData;
        $this->configs = new AppConfigRepository();
        $this->logoUrl = $this->configs->configFileUrls()['email_logo'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->configs->getConfig('site_email'), $this->configs->getConfig('site_name'))
            ->subject('Head Hunter Email: '.$this->emailData['name'])
            ->replyTo($this->emailData['email'], $this->emailData['name'])
            ->markdown('emails.head-hunter.head-hunter');
    }
}
