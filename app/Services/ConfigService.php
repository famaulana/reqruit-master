<?php

namespace App\Services;

class ConfigService
{
    /**
     * Configuration option name that contain image
     *
     * @return Array
     */
    public static function configImages()
    {
        return [
            'logo1',
            'logo2',
            'logo3',
            'logo_square',
            'favicon',
            'footer_bg_image'
        ];
    }

    /**
     * Get Whatsapp link
     *
     * @param String $phone
     * @return String
     */
    public static function getWaLink(String $phone)
    {
        $firstChar = substr($phone, 0, 1);
        if ($firstChar == "0") {
            $newString = substr($phone, 1);
            $phoneWa = "62" . $newString;
        } else {
            $phoneWa = $phone;
        }

        return "https://wa.me/" . $phoneWa;
    }

    /**
     * Store Theme configs
     *
     * @param Array $optionNames
     * @param Array $request
     * @return Boolean
     */
    public function storeThemeConfigs(array $optionNames, array $request)
    {
        $defaultOptions = array_keys(config('theme_configs'));
        $activeTheme = config('active_theme.domain');
        $updated = false;
        foreach ($optionNames as $option) {
            if (in_array($option, $defaultOptions)) {
                // Set config value at runtime
                config(["theme.$activeTheme.$option" => $request[$option]]);
                $updated = true;
            }
        }

        if ($updated) {
            $text = '<?php return ' . var_export(config("theme.$activeTheme"), true) . ';';
            file_put_contents(config_path("theme/$activeTheme.php"), $text);
        }

        return true;
    }
}
