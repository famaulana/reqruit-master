<?php

namespace App\Services;

use Carbon\Carbon;
use App\Models\User;
use App\Models\EnglishQuestion;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Repositories\UserRepository;
use App\Repositories\AppConfigRepository;
use App\Repositories\EnglishQuestionRepository;

class EnglishAssessmentService
{
    public static function processAssessment(User $user, array $answers)
    {
        $session1 = $answers['session_1'];
        $session2 = $answers['session_2'];
        $session3 = $answers['session_3'];

        // Process session 1
        $totalQuestionS1 = count($session1);
        $qIds = [];
        foreach ($session1 as $answer) {
            $qIds[] = $answer['question_id'];
        }

        $orderBy = implode(',', $qIds);

        $questions = EnglishQuestion::whereIn('id', $qIds)->orderByRaw(DB::raw("FIELD(id, $orderBy)"))->get();

        $resultS1 = [];
        $correctCountS1 = 0;
        foreach ($questions as $key => $q) {
            $isCorrect = $session1[$key]['answer_id'] === $q->correct_answer ? true : false;

            $resultS1[] = [
                'question_id' => $session1[$key]['question_id'],
                'question_text' => $session1[$key]['question_text'],
                'answer_id' => $session1[$key]['answer_id'],
                'answer_text' => $session1[$key]['answer_text'],
                'is_correct' => $isCorrect,
            ];

            if ($isCorrect) {
                $correctCountS1++;
            }
        }

        // Process session 2
        $totalQuestionS2 = count($session2['answers']);
        $vocabAnsChoices = EnglishQuestionRepository::vocabularyQuestion()['choices'];
        $correctCountS2 = 0;
        $processedResult2 = [];
        foreach ($session2['answers'] as $key => $answer) {
            $ansNum = $key + 1;
            $choiceKey = array_search($ansNum, array_column($vocabAnsChoices, 'ans_no'));
            $correctAnswer = $vocabAnsChoices[$choiceKey]['answer'];
            $isCorrect = $answer == $correctAnswer;

            $processedResult2[] = [
                'ans_no' => $ansNum,
                'answer' => $answer,
                'is_correct' => $isCorrect,
            ];

            if ($isCorrect) {
                $correctCountS2++;
            }
        }

        $result2 = [
            'q_group_no' => $session2['q_group_no'],
            'answers' => $processedResult2
        ];

        // Process session 3
        $allListeningQuestions = EnglishQuestionRepository::listeningQuestions();
        $totalQuestionS3 = 0;
        $correctCountS3 = 0;
        $result3 = [];
        foreach ($session3 as $session3Ans) {
            $qGroupKey = array_search($session3Ans['q_group_no'], array_column($allListeningQuestions, 'q_group_no'));
            $listeningQuestions = $allListeningQuestions[$qGroupKey]['questions'];

            $processedResult3 = [];
            $answers = $session3Ans['answers'];
            $totalQuestionS3 += count($answers);
            foreach ($answers as $answer) {
                $qKey = array_search($answer['q_no'], array_column($listeningQuestions, 'q_no'));
                $correctAnswer = $listeningQuestions[$qKey]['correct'];
                $isCorrect = $answer['answer'] == $correctAnswer;
    
                $processedResult3[] = [
                    'q_no' => $answer['q_no'],
                    'answer' => $answer,
                    'is_correct' => $isCorrect,
                ];
    
                if ($isCorrect) {
                    $correctCountS3++;
                }
            }

            $result3[] = [
                'q_group_no' => $session3Ans['q_group_no'],
                'answers' => $processedResult3
            ];
        }

        // Process total
        $totalCorrectCount = $correctCountS1 + $correctCountS2 + $correctCountS3;
        $totalQuestion = $totalQuestionS1 + $totalQuestionS2 + $totalQuestionS3;
        $assessmentResult = [
            'session_1' => $resultS1,
            'session_2' => $result2,
            'session_3' => $result3
        ];

        $percentCorrect = ($totalCorrectCount / $totalQuestion) * 100;
        $score = $percentCorrect / 10;

        if (is_null($user->english_assessment)) {
            $user->english_assessment()->create([
                'assessment' => $assessmentResult,
                'score' => $score
            ]);
        } else {
            $existingAssessment = $user->english_assessment;
            $existingAssessment->assessment = $assessmentResult;
            $existingAssessment->score = $score;
            $existingAssessment->save();
        }

        $userRepository = new UserRepository();
        return $userRepository->updateProfileCompletionCache($user);

        return $score;
    }

    /**
     * Is user can re do english test
     *
     * @param User $user
     * @return boolean
     */
    public static function canRedo(User $user)
    {
        $config = new AppConfigRepository();
        // Can re do personal test
        $canRedo = false;
        if (is_null($user->english_assessment)) {
            $canRedo = false;
        } else {
            $canRedoEnglishTestAfterDays = intval($config->getConfig('english_test_can_retest_after_days'));
            $hours = $user->english_assessment->updated_at->diffInHours(Carbon::now());
            $days = ceil($hours/24);
            if ($days > $canRedoEnglishTestAfterDays) {
                $canRedo = true;
            } else {
                $canRedo = false;
            }
        }

        return $canRedo;
    }
}
