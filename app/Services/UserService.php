<?php

namespace App\Services;

use App\Models\User;
use App\Helpers\AppHelper;
use App\Helpers\PathHelper;
use Illuminate\Support\Str;
use App\Helpers\ImageHelper;
use App\Mail\ApplyJobNotificationToCompanyEmail;
use App\Mail\UserVerification;
use App\Models\OauthAccessToken;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use App\Mail\UserResetPasswordLinkEmail;

class UserService
{

    /**
     * Create new user
     *
     * @param array $data
     * @param Bool $verified
     * @return App\Models\User
     */
    public function createUser(array $data, Bool $verified = false): User
    {
        $user = User::create([
            'identifier' => $this->generateUserIdentifier(),
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'remember_token' => Str::random(10),
        ]);

        if (isset($data['photo_profile']) && $data['photo_profile'] != '') {
            $this->processPhoto($user, $data, true);
        }

        if ($verified) {
            $user->verified = 1;
            $user->email_verified_at = now();
            $user->save();
        } else {
            $this->sendVerification($user);
        }

        return $user;
    }

    /**
     * Genreate user identifier
     *
     * @return String
     */
    public function generateUserIdentifier()
    {
        return AppHelper::uniqueRandomString('users', 'identifier', 16, 'user', false);
    }

    /**
     * Verify user's email
     *
     * @param User $user
     * @return User
     */
    public function verifyEmail(User $user)
    {
        $user->verified = 1;
        $user->email_verified_at = now();
        $user->verification_token = null;
        $user->save();

        return $user->refresh();
    }

    /**
     * Generate auth token
     *
     * @param User $user
     * @return String
     */
    public function generateToken(User $user)
    {
        $tokens = OauthAccessToken::where('user_id', $user->id)->where('name', 'User')->get();
        if ($tokens !== null) {
            foreach ($tokens as $token) {
                $token->delete();
            }
        }

        return $user->createToken('User')->accessToken;
    }

    /**
     * Process user photo
     *
     * @param user $user
     * @param array $data
     * @param Bool $isNew
     * @return App\Models\user
     */
    public function processPhoto(user $user, array $data, Bool $isNew)
    {
        $path = PathHelper::user_photo_path($user->id);

        if (!$isNew) {
            PathHelper::deleteFile($path . $user->photo, 'local');
        }
        
        $fileName = ImageHelper::processImg($data['photo_profile'], $path, ['type' => 'fit', 'size' => 100], '', 'local');
        $user->photo = $fileName;
        $user->save();

        return $user;
    }

    /**
     * Create new user address
     *
     * @param User $user
     * @param array $data
     * @return App\Models\Address
     */
    public function createAddress(User $user, array $data)
    {
        $address = $user->addresses()->create([
            'name' => $data['name'],
            'street' => $data['street'],
            'province_id' => $data['province_id'],
            'city_id' => $data['city_id'],
            'subdistrict_id' => $data['subdistrict_id'],
            'post_code' => $data['post_code'],
            'phone' => $data['phone'],
        ]);

        return $address;
    }

    /**
     * Send email verification to user
     *
     * @param User $user
     * @return String
     */
    public function sendVerification(User $user)
    {
        $token = $this->generateVerificationToken($user);
       
        Mail::to($user->email)
            ->send(new UserVerification($user, $token));
        
        return $token;
    }

    /**
     * Send user reset password link
     *
     * @param User $user
     * @param String $token
     * @return App\Models\User
     */
    public function sendResetPasswordLink(User $user, String $token)
    {
        Mail::to($user->email)
            ->send(new UserResetPasswordLinkEmail($user, $token));

        return $user;
    }

    /**
     * Generate user verification token
     *
     * @param User $user
     * @return String
     */
    public function generateVerificationToken(User $user)
    {
        $token = Str::random(40);
        $user->verification_token = $token;
        $user->save();
        
        return $token;
    }

    /**
     * Verify user email
     *
     * @param User $user
     * @return App\Models\User
     */
    public function verify(User $user)
    {
        $user->verified = 1;
        $user->verify_token = null;
        $user->email_verified_at = now();
        $user->save();

        return $user;
    }

    /**
     * Apply to job
     *
     * @param User $user
     * @param Int $workjobId
     * @param Array $answers
     * @param String $coverLetter
     * @return Object
     */
    public function applyJob(User $user, Int $workjobId, array $answers = [], ?String $coverLetter = '')
    {
        $jobApplication = $user->job_applications()->create([
            'workjob_id' => $workjobId,
            'cover_letter' => $coverLetter,
            'status' => 'pending'
        ]);

        if (!empty($answers)) {
            $answerData = [];
            foreach ($answers as $ans) {
                if ($ans['answer'] == '') {
                    throw new \Exception("Mohon jawab semua pertanyaan", 403);
                    return;
                }

                $answerData[] = [
                    'user_id' => $user->id,
                    'workjob_id' => $workjobId,
                    'question' => $ans['question'],
                    'answer' => $ans['answer'],
                ];
            }
            $jobApplication->job_question_answers()->createMany($answerData);
        }

        Mail::to($jobApplication->workjob->company->email)
            ->send(new ApplyJobNotificationToCompanyEmail($jobApplication));

        return $jobApplication;
    }

    /**
     * Is applied to job
     *
     * @param Int $workjobId
     * @return Array
     */
    public static function jobAppliedStatus(Int $workjobId)
    {
        $jap = ['applied' => false, 'status' => ''];
        if (Auth::guard('user')->check()) {
            $user = Auth::guard('user')->user();
            $jobApplication = $user->job_applications->where('workjob_id', $workjobId)->first();
            
            if (!is_null($jobApplication)) {
                $jap = ['applied' => true, 'status' => $jobApplication->status];
            }
        }

        return $jap;
    }
}
