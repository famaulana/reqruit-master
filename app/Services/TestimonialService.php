<?php

namespace App\Services;

use App\Helpers\PathHelper;
use App\Models\Testimonial;
use App\Helpers\ImageHelper;

class TestimonialService
{
    /**
     * Create new testimonial
     *
     * @param array $data
     * @param Bool $verified
     * @return App\Models\Testimonial
     */
    public function createTestimonial(array $data, Bool $verified = false)
    {
        $testimonial = Testimonial::create([
            'name' => $data['name'],
            'profession' => $data['profession'],
            'content' => $data['content'],
            'rating' => $data['rating'],
        ]);

        $this->processAvatar($testimonial, $data, true);

        return $testimonial;
    }

    /**
     * Process avatar
     *
     * @param App\Models\Testimonial $testimonial
     * @param array $data
     * @param Bool $isNew
     * @return App\Models\Testimonial
     */
    public function processAvatar(Testimonial $testimonial, array $data, Bool $isNew)
    {
        $path = PathHelper::testimonial_avatar_path($testimonial->id);

        if (!$isNew) {
            PathHelper::deleteFile($path . $testimonial->avatar, 'local');
        }
        
        $fileName = ImageHelper::processImg($data['avatar_image'], $path, ['type' => 'fit', 'size' => 200], '', 'local');
        $testimonial->avatar = $fileName;
        $testimonial->save();

        return $testimonial;
    }
}
