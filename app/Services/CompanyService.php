<?php

namespace App\Services;

use App\Models\Company;
use Illuminate\Support\Str;

class CompanyService
{
    /**
     * Create company slug
     *
     * @param String $name
     * @param Int $id
     * @return String
     */
    public function createSlug(String $name, Int $id = 0)
    {
        // Normalize the name
        $slug = Str::slug($name);

        // Get any that could possibly be related.
        // This cuts the queries down by doing it once.
        $allSlugs = $this->getRelatedSlugs($slug, $id);

        // If we haven't used it before then we are all good.
        if (!$allSlugs->contains('slug', $slug)) {
            return $slug;
        }

        // Just append numbers like a savage until we find not used.
        for ($i = 1; $i <= 10; $i++) {
            $newSlug = $slug . '-' . $i;
            if (!$allSlugs->contains('slug', $newSlug)) {
                return $newSlug;
            }
        }

        throw new \Exception('Can not create a unique slug');
    }

    /**
     * Get related slug from existing companies
     *
     * @param String $slug
     * @param Int $id
     * @return App\Models\Company
     */
    protected function getRelatedSlugs(String $slug, Int $id = 0)
    {
        return Company::select('slug')->where('slug', 'like', $slug . '%')
            ->where('id', '<>', $id)
            ->get();
    }
}
