<?php

namespace App\Services;

use App\Models\Employer;
use App\Helpers\AppHelper;
use App\Helpers\PathHelper;
use Illuminate\Support\Str;
use App\Helpers\ImageHelper;
use App\Models\OauthAccessToken;
use App\Mail\EmployerInviteEmail;
use App\Mail\EmployerVerification;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use App\Mail\EmployerResetPasswordLinkEmail;

class EmployerService
{
    /**
     * Create new employer
     *
     * @param array $data
     * @param Bool $verified
     * @return App\Models\Employer
     */
    public function createEmployer(array $data, Bool $verified = false): Employer
    {
        $employer = Employer::create([
            'identifier' => $this->generateEmployerIdentifier(),
            'name' => $data['name'],
            'email' => $data['email'],
            'phone' => isset($data['phone']) ? $data['phone'] : null,
            'job_position' => isset($data['job_position']) ? $data['job_position'] : null,
            'password' => Hash::make($data['password']),
            'role' => isset($data['role']) ? $data['role'] : 'super-administrator',
            'remember_token' => Str::random(10),
        ]);

        if (isset($data['photo_profile']) && $data['photo_profile'] != '') {
            $this->processPhoto($employer, $data, true);
        }

        if ($verified) {
            $employer->verified = 1;
            $employer->email_verified_at = now();
            $employer->save();
        } else {
            $this->sendVerification($employer);
        }
        return $employer;
    }

    /**
     * Genreate employer identifier
     *
     * @return String
     */
    public function generateEmployerIdentifier()
    {
        return AppHelper::uniqueRandomString('employers', 'identifier', 16, 'empl', false);
    }

    /**
     * Verify employer's email
     *
     * @param Employer $employer
     * @return Employer
     */
    public function verifyEmail(Employer $employer)
    {
        $employer->verified = 1;
        $employer->email_verified_at = now();
        $employer->verification_token = null;
        $employer->save();

        return $employer->refresh();
    }

    /**
     * Generate auth token
     *
     * @param User $user
     * @return String
     */
    public function generateToken(Employer $employer)
    {
        $tokens = OauthAccessToken::where('user_id', $employer->id)->where('name', 'Employer')->get();
        if ($tokens !== null) {
            foreach ($tokens as $token) {
                $token->delete();
            }
        }

        return $employer->createToken('Employer')->accessToken;
    }

    /**
     * Process user photo
     *
     * @param Employer $employer
     * @param array $data
     * @param Bool $isNew
     * @return App\Models\Employer
     */
    public function processPhoto(Employer $employer, array $data, Bool $isNew)
    {
        $path = PathHelper::employer_photo_path($employer->id);

        if (!$isNew) {
            PathHelper::deleteFile($path . $employer->photo, 'local');
        }
        
        $fileName = ImageHelper::processImg($data['photo_profile'], $path, ['type' => 'fit', 'size' => 100], '', 'local');
        $employer->photo = $fileName;
        $employer->save();

        return $employer;
    }

    /**
     * Send email verification to employer
     *
     * @param Employer $employer
     * @return String
     */
    public function sendVerification(Employer $employer)
    {
        $token = $this->generateVerificationToken($employer);
       
        Mail::to($employer->email)
            ->send(new EmployerVerification($employer, $token));
        
        return $token;
    }

    /**
     * Send employer reset password link
     *
     * @param Employer $employer
     * @param String $token
     * @return App\Models\Employer
     */
    public function sendResetPasswordLink(Employer $employer, String $token)
    {
        Mail::to($employer->email)
            ->send(new EmployerResetPasswordLinkEmail($employer, $token));

        return $employer;
    }

    /**
     * Generate employer verification token
     *
     * @param Employer $employer
     * @return String
     */
    public function generateVerificationToken(Employer $employer)
    {
        $token = Str::random(40);
        $employer->verification_token = $token;
        $employer->save();

        return $token;
    }

    /**
     * Verify employer email
     *
     * @param Employer $employer
     * @return App\Models\Employer
     */
    public function verify(Employer $employer)
    {
        $employer->verified = 1;
        $employer->verify_token = null;
        $employer->email_verified_at = now();
        $employer->save();

        return $employer;
    }

    /**
     * Invite new employer and attach it to the company
     *
     * @param Employer $employer
     * @param String $name
     * @param String $email
     * @param String $role
     * @return Employer
     */
    public function invite(Employer $employer, String $name, String $email, String $role)
    {
        if ($employer->role->value != 'super-administrator') {
            throw new \Exception("Forbidden action", 403);
        }

        if (!$employer->companies()->exists()) {
            throw new \Exception("Anda belum memiliki perusahaan", 403);
        }

        $company = $employer->company;
        $password = Str::random(10);
        $data = [
            'name' => $name,
            'email' => $email,
            'role' => $role,
            'password' => $password,
        ];

        $newEmployer = $this->createEmployer($data, true);
        $newEmployer->companies()->attach($company->id);

        // Sending email to new invited employer
        Mail::to($newEmployer->email)->send(new EmployerInviteEmail($employer, $newEmployer, $password));

        return $newEmployer;
    }
}
