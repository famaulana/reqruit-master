<?php

namespace App\Services;

use App\Models\Skill;
use App\Enums\WorkjobType;
use App\Helpers\AppHelper;
use App\Enums\WorkjobStatus;
use App\Enums\WorkjobExperience;
use App\Enums\WorkjobPostingStatus;
use App\Enums\WorkjobSalaryDuration;

class WorkjobService
{
    /**
     * Generate workjob identifier
     *
     * @return String
     */
    public static function generateIdentifier()
    {
        return AppHelper::uniqueRandomString('workjobs', 'identifier', 16, 'job');
    }

    /**
     * Get job type option select
     *
     * @return Array
     */
    public static function jobTypeOptionSelect()
    {
        $options = [];
        foreach (WorkjobType::toSelectArray() as $value => $text) {
            $options[] = [
                'value' => $value,
                'text' => $text
            ];
        }

        return $options;
    }

    /**
     * Get job posting status option select
     *
     * @return Array
     */
    public static function jobPostingStatusOptionSelect()
    {
        $options = [];
        foreach (WorkjobPostingStatus::toSelectArray() as $value => $text) {
            $options[] = [
                'value' => $value,
                'text' => $text
            ];
        }

        return $options;
    }

    /**
     * Get job status option select
     *
     * @return Array
     */
    public static function jobStatusOptionSelect()
    {
        $options = [];
        foreach (WorkjobStatus::toSelectArray() as $value => $text) {
            $options[] = [
                'value' => $value,
                'text' => $text
            ];
        }

        return $options;
    }

    /**
     * Get job experience option select
     *
     * @return Array
     */
    public static function jobExperienceOptionSelect()
    {
        $options = [];
        foreach (WorkjobExperience::toSelectArray() as $value => $text) {
            $options[] = [
                'value' => $value,
                'text' => $text
            ];
        }

        return $options;
    }

    /**
     * Get job salary duration option select
     *
     * @return Array
     */
    public static function jobSalaryDurationOptionSelect()
    {
        $options = [];
        foreach (WorkjobSalaryDuration::toSelectArray() as $value => $text) {
            $options[] = [
                'value' => $value,
                'text' => $text
            ];
        }

        return $options;
    }

    /**
     * Get skills option select
     *
     * @return Array
     */
    public static function skillsOptionSelect()
    {
        $skills = Skill::skip(0)->take(10)->orderBy('name')->get();
        $options = [];
        foreach ($skills as $skill) {
            $options[] = [
                'id' => $skill->id,
                'name' => $skill->name
            ];
        }

        return $options;
    }
}
