<?php

namespace App\Services;

/**
 * Locale class
 */
class LocaleService
{
    /**
     * Get available locales
     *
     * @return void
     */
    public static function locales()
    {
        return [
            'id-ID',
            'en-US',
            'en-AU'
        ];
    }
}
