<?php

namespace App\Services;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Cache;
use App\Repositories\AppConfigRepository;
use Stevebauman\Location\Facades\Location;

/**
 * Currency class
 */
class CurrencyService
{
    /**
     * App config repository instance
     *
     * @var Object class
     */
    protected $configRepository;

    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->configRepository = new AppConfigRepository();
    }

    /**
     * Get available currencies
     *
     * @return Array country code => currency
     */
    public static function currencies()
    {
        return [
            'AU' => 'AUD',
            'ID' => 'IDR',
            'SG' => 'SGD',
            'US' => 'USD',
        ];
    }

    /**
     * Get default currency
     *
     * @return String
     */
    public function defaultCurrency()
    {
        return $this->configRepository->getConfig('default_currency');
    }

    /**
     * Get base currency
     *
     * @return String
     */
    public function baseCurrency()
    {
        return $this->configRepository->getConfig('base_currency');
    }

    /**
     * Is using auto currency
     *
     * @return boolean
     */
    public function isAutoCurrency()
    {
        return $this->configRepository->getConfig('auto_currency') == 1 ? true : false;
    }

    /**
     * Get locale
     *
     * @param String $currency
     * @return String
     */
    public function getLocale(String $currency = '')
    {
        if ($currency == '') {
            $currency = $this->getStoreCurrency();
        }

        if ($currency == 'IDR') {
            return 'id-ID';
        }

        return 'en-US';
    }

    /**
     * Get Currency by country code
     *
     * @param String $countryCode
     * @return String
     */
    public function getCurrency(String $countryCode)
    {
        $currencies = $this->currencies();
        if (!isset($currencies[$countryCode])) {
            return $this->defaultCurrency();
        }

        $currency = $currencies[$countryCode];

        if (!array_key_exists($currency, $this->allowedCurrencies())) {
            return $this->defaultCurrency();
        }

        return $currency;
    }

    /**
     * Get allowed currencies
     *
     * @return Array currency => symbol
     */
    public function allowedCurrencies()
    {
        $rawAllowedCurrencies = json_decode($this->configRepository->getConfig('allowed_currencies'), true);

        $currencyAndSymbols = [];
        foreach ($rawAllowedCurrencies as $currency) {
            $currencyAndSymbols[$currency['currency']] = $currency['symbol'];
        }

        // Add base currency into allowed currencies
        $baseCurrency = $this->configRepository->getConfig('base_currency');
        $baseCurrencySymbol = $this->configRepository->getConfig('currency_symbol');
        $currencyAndSymbols[$baseCurrency] = $baseCurrencySymbol;

        return $currencyAndSymbols;
    }

    /**
     * Get currency rates curl
     *
     * @param String $baseCurrency
     * @param String $otherCurrencies
     * @return Array
     */
    public function getRatesCurl(String $baseCurrency, String $otherCurrencies)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.exchangeratesapi.io/latest?base=$baseCurrency&symbols=$otherCurrencies",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json",
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $errMsg = "Get currency rates curl error #: $err";
            Log::error();
            throw new \Exception($errMsg);
        } else {
            $result = json_decode($response, true);
            return $result;
        }
    }

    /**
     * Get currency rates
     *
     * @param String $baseCurrency
     * @param String $otherCurrencies
     * @return Array
     */
    public function getRates(String $baseCurrency = '', String $otherCurrencies = '')
    {
        if ($baseCurrency == '') {
            $baseCurrency = $this->configRepository->getConfig('base_currency');
        }

        if ($otherCurrencies == '') {
            $allowedCurrencies = json_decode($this->configRepository->getConfig('allowed_currencies'), true);

            $otherCurrencies = '';
            foreach ($allowedCurrencies as $currency) {
                $otherCurrencies .= $currency['currency'] . ',';
            }
            $otherCurrencies = rtrim($otherCurrencies, ',');
        }

        $rawRates = $this->getRatesCurl($baseCurrency, $otherCurrencies);

        $rates = [];
        foreach ($rawRates['rates'] as $currency => $rate) {
            $rates[$currency] = sprintf('%.10f', floatval($rate));
        }

        $result = [
            'base' => $rawRates['base'],
            'date' => $rawRates['date'],
            'updated_at' => now()->format('Y-m-d H:i:s'),
            'rates' => $rates
        ];

        return $result;
    }

    /**
     * Get currency rates and update
     *
     * @param String $baseCurrency
     * @param String $otherCurrencies
     * @return Array
     */
    public function getRatesAndUpdate(String $baseCurrency, String $otherCurrencies)
    {
        $rates = $this->getRates($baseCurrency, $otherCurrencies);
        $text = '<?php return ' . var_export($rates, true) . ';';
        $this->updateConfigFile($text);

        return $rates;
    }

    /**
     * Update currency rates based on current config
     *
     * @return Array
     */
    public function updateRates()
    {
        $rates = $this->getRates();
        $text = '<?php return ' . var_export($rates, true) . ';';
        $this->updateConfigFile($text);

        return $rates;
    }

    /**
     * Update currency rates config file
     *
     * @param String $text
     * @return void
     */
    public function updateConfigFile(String $text)
    {
        File::put(
            base_path('config/currency_rates.php'),
            $text
        );
    }

    /**
     * Get store currency cymbol
     *
     * @params String $currency
     * @return String
     */
    public function getStoreCurrencySymbol(String $currency = '')
    {
        $allowedCurrencies = $this->allowedCurrencies();

        if ($currency != '') {
            if (!isset($allowedCurrencies[$currency])) {
                return $this->configRepository->getConfig('currency_symbol');
            } else {
                return $allowedCurrencies[$currency];
            }
        }

        $storeCurrency = $this->getStoreCurrency();
        if (
            !$this->isAutoCurrency() ||
            $storeCurrency == '' ||
            $storeCurrency == $this->baseCurrency()
        ) {
            return $this->configRepository->getConfig('currency_symbol');
        }

        return $allowedCurrencies[$storeCurrency];
    }

    /**
     * Get current active store currency
     *
     * @return String
     */
    public function getStoreCurrency()
    {
        return \Request::session()->get('store_currency');
    }

    /**
     * Set store currency for customer
     *
     * @param Bool $auto
     * @return String
     */
    public function setStoreCurrency(Bool $auto = true)
    {
        if (!$this->isAutoCurrency()) {
            return $this->baseCurrency();
        }

        $currency = $this->getStoreCurrency();

        if ($currency == '' || !$auto) {
            $position = Location::get();
            $currency = $this->getCurrency($position->countryCode);

            \Request::session()->put('store_currency', $currency);
        }

        return $currency;
    }

    /**
     * Calculate number based on currency
     *
     * @param Float $number
     * @return Float
     */
    public function calculateCurrency(Float $number, String $currency = '')
    {
        $rates = config('currency_rates.rates');

        if ($currency != '') {
            if (!isset($rates[$currency])) {
                return $number;
            }

            return $number * $rates[$currency];
        }

        $storeCurrency = $this->getStoreCurrency();

        if (
            !$this->isAutoCurrency() ||
            $storeCurrency == '' ||
            $storeCurrency == $this->baseCurrency()
        ) {
            return $number;
        }


        return $number * $rates[$storeCurrency];
    }
}
