<?php

namespace App\Services;

use App\Models\Jobfunction;

class JobfunctionService
{
    /**
     * Get options select
     *
     * @return Array
     */
    public static function optionSelect()
    {
        $jobFunctions = Jobfunction::select('id', 'name')->orderBy('name', 'ASC')->pluck('name', 'id')->toArray();

        $options = [];
        foreach ($jobFunctions as $id => $name) {
            $options[] = [
                'value' => $id,
                'text' => $name,
            ];
        }

        return $options;
    }
}
