<?php

namespace App\Services;

use App\Models\City;

class CityService
{
    /**
     * Get cities option select
     *
     * @param Int $province
     * @return Array
     */
    public static function optionSelect(Int $province = null)
    {
        $cities = City::select('id', 'name')->orderBy('name', 'ASC')->pluck('name', 'id')->toArray();

        $options = [];
        foreach ($cities as $value => $text) {
            $options[] = [
                'value' => $value,
                'text' => $text
            ];
        }

        return $options;
    }
}
