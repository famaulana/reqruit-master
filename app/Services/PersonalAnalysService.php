<?php

namespace App\Services;

use Carbon\Carbon;
use App\Models\User;
use Illuminate\Support\Facades\Log;
use App\Repositories\AppConfigRepository;
use App\Repositories\PersonalAnalysRepository;

class PersonalAnalysService
{
    /**
     * Process posted statements
     *
     * @param Array $statements
     * @return Array
     */
    public static function processStatement(array $statements)
    {
        $statementData = PersonalAnalysRepository::statements();
        $conversionData = PersonalAnalysRepository::conversions();
        $behaveCodes = PersonalAnalysRepository::behavioralCodes();

        $raw = [];
        $totalMostD = 0;
        $totalMostI = 0;
        $totalMostS = 0;
        $totalMostC = 0;
        $totalMostX = 0;
        $totalLeastD = 0;
        $totalLeastI = 0;
        $totalLeastS = 0;
        $totalLeastC = 0;
        $totalLeastX = 0;
        foreach ($statements as $key => $statement) {
            $charMost = $statementData[$key]['char_most'][$statement['most']];
            $charLeast = $statementData[$key]['char_least'][$statement['least']];

            $raw[] = [
                'q_no' => $key + 1,
                'most' => [
                    'statement' => $statement['most'],
                    'char' => $statementData[$key]['char_most'][$statement['most']]
                ],
                'least' => [
                    'statement' => $statement['least'],
                    'char' => $statementData[$key]['char_least'][$statement['least']]
                ],
            ];

            if ($charMost == 'D') {
                $totalMostD++;
            }

            if ($charMost == 'I') {
                $totalMostI++;
            }

            if ($charMost == 'S') {
                $totalMostS++;
            }

            if ($charMost == 'C') {
                $totalMostC++;
            }

            if ($charMost == 'X') {
                $totalMostX++;
            }

            if ($charLeast == 'D') {
                $totalLeastD++;
            }

            if ($charLeast == 'I') {
                $totalLeastI++;
            }

            if ($charLeast == 'S') {
                $totalLeastS++;
            }

            if ($charLeast == 'C') {
                $totalLeastC++;
            }

            if ($charLeast == 'X') {
                $totalLeastX++;
            }
        }

        $conversionMostD = $conversionData['most']['D'][$totalMostD];
        $conversionMostI = $conversionData['most']['I'][$totalMostI];
        $conversionMostS = $conversionData['most']['S'][$totalMostS];
        $conversionMostC = $conversionData['most']['C'][$totalMostC];

        $conversionLeastD = $conversionData['least']['D'][$totalLeastD];
        $conversionLeastI = $conversionData['least']['I'][$totalLeastI];
        $conversionLeastS = $conversionData['least']['S'][$totalLeastS];
        $conversionLeastC = $conversionData['least']['C'][$totalLeastC];

        $totalMost = [
            [
                'char' => 'D',
                'total' => $totalMostD,
                'conversion' => $conversionMostD,
                'code' => $conversionMostD > 50 ? 'HD' : 'LD'
            ],
            [
                'char' => 'I',
                'total' => $totalMostI,
                'conversion' => $conversionMostI,
                'code' => $conversionMostI > 50 ? 'HI' : 'LI'
            ],
            [
                'char' => 'S',
                'total' => $totalMostS,
                'conversion' => $conversionMostS,
                'code' => $conversionMostS > 50 ? 'HS' : 'LS'
            ],
            [
                'char' => 'C',
                'total' => $totalMostC,
                'conversion' => $conversionMostC,
                'code' => $conversionMostC > 50 ? 'HC' : 'LC'
            ],
            [
                'char' => 'X',
                'total' => $totalMostX,
                'conversion' => 0,
                'code' => ''
            ],
        ];

        $totalLeast = [
            [
                'char' => 'D',
                'total' => $totalLeastD,
                'conversion' => $conversionLeastD,
                'code' => $conversionLeastD > 50 ? 'HD' : 'LD'
            ],
            [
                'char' => 'I',
                'total' => $totalLeastI,
                'conversion' => $conversionLeastI,
                'code' => $conversionLeastI > 50 ? 'HI' : 'LI'
            ],
            [
                'char' => 'S',
                'total' => $totalLeastS,
                'conversion' => $conversionLeastS,
                'code' => $conversionLeastS > 50 ? 'HS' : 'LS'
            ],
            [
                'char' => 'C',
                'total' => $totalLeastC,
                'conversion' => $conversionLeastC,
                'code' => $conversionLeastC > 50 ? 'HC' : 'LC'
            ],
            [
                'char' => 'X',
                'total' => $totalLeastX,
                'conversion' => 0,
                'code' => ''
            ],
        ];

        usort($totalMost, function ($a, $b) {
            return $b['conversion'] <=> $a['conversion'];
        });

        usort($totalLeast, function ($a, $b) {
            return $b['conversion'] <=> $a['conversion'];
        });

        $totalMostCode = '';
        foreach ($totalMost as $total) {
            $totalMostCode .= $total['code'];
        }

        $totalLeastCode = '';
        foreach ($totalLeast as $total) {
            $totalLeastCode .= $total['code'];
        }

        $result = [
            'raw' => $raw,
            'most' => [
                'total' => $totalMost,
                'behave' => [
                    'code' => $totalMostCode,
                    'name' => isset($behaveCodes[$totalMostCode]) ? $behaveCodes[$totalMostCode]['name'] : 'N/A',
                    'type' => isset($behaveCodes[$totalMostCode]) ? $behaveCodes[$totalMostCode] ['type'] : 'N/A'
                ]
            ],
            'least' => [
                'total' => $totalLeast,
                'behave' => [
                    'code' => $totalLeastCode,
                    'name' => isset($behaveCodes[$totalLeastCode]) ? $behaveCodes[$totalLeastCode]['name'] : 'N/A',
                    'type' => isset($behaveCodes[$totalLeastCode]) ? $behaveCodes[$totalLeastCode]['type'] : 'N/A'
                ]
            ]
        ];

        return $result;
    }

    /**
     * Is user can re do personal test
     *
     * @param User $user
     * @return boolean
     */
    public static function canRedo(User $user)
    {
        $config = new AppConfigRepository();
        // Can re do personal test
        $canRedo = false;
        if (is_null($user->personality_test)) {
            $canRedo = false;
        } else {
            $canRedoPersonalTestAfterDays = intval($config->getConfig('personal_test_can_retest_after_days'));
            $hours = $user->personality_test->updated_at->diffInHours(Carbon::now());
            $days = ceil($hours/24);
            if ($days > $canRedoPersonalTestAfterDays) {
                $canRedo = true;
            } else {
                $canRedo = false;
            }
        }

        return $canRedo;
    }
}
