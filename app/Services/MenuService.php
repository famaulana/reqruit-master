<?php

namespace App\Services;

use App\Helpers\PathHelper;

class MenuService
{
    /**
     * Get menu url
     *
     * @param Array $menu
     * @return void
     */
    public static function getUrl(array $menu)
    {
        if ($menu['type'] == "custom") {
            return $menu['url'];
        }

        return route($menu['type'], $menu['url']);
    }

    /**
     * Get Menu image url
     *
     * @param String $type
     * @param String $file
     * @return String
     */
    public function getImageFileUrl(String $type, String $file)
    {
        if ($type == 'mega_menu_image') {
            $imgPath = PathHelper::mega_menu_image_path();
        } else {
            $imgPath = PathHelper::icon_image_path();
        }
        return url('storage/' . $imgPath . $file);
    }
}
