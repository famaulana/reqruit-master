<?php

namespace App\Services;

use App\Enums\JobApplicationStatus;
use App\Http\Resources\JobApplicationSimpleResource;
use App\Models\JobApplication;
use App\Models\Workjob;
use App\Notifications\JobApplicationUpdate;

class JobApplicationService
{
    public $jobApplication;

    /**
     * Set job Application
     *
     * @param JobApplication $jobApplication
     * @return void
     */
    public function setJobApplication(JobApplication $jobApplication)
    {
        $this->jobApplication = $jobApplication;
    }

    /**
    * Get job application status option select
    *
    * @return Array
    */
    public static function statusOptionSelect()
    {
        $options = [];
        foreach (JobApplicationStatus::toSelectArray() as $value => $text) {
            $options[] = [
                'value' => $value,
                'text' => $text
            ];
        }

        return $options;
    }

    /**
     * Update job application status
     *
     * @param String $status
     * @param Array $data
     * @param String $emailTemplate
     * @return JobApplication
     */
    public function updateStatus(String $status, array $data = [], String $emailTemplate = '')
    {
        $dataArray = $this->jobApplication->data;
        if (isset($data['rejection_message']) && $status === 'unsuitable') {
            $dataArray['rejection_message'] = $data['rejection_message'];
        }

        $updateData = [
            'status' => $status,
            'data' => $dataArray
        ];

        if ($status === 'interview') {
            if (isset($data['interview_time'])) {
                $updateData['interview_time'] = $data['interview_time'];
            }

            if (isset($data['interview_location'])) {
                $updateData['interview_location'] = $data['interview_location'];
            }

            $updateData['interview_status'] = null;
        }

        $this->jobApplication->update($updateData);

        $user = $this->jobApplication->user;
        $user->notify(new JobApplicationUpdate($this->jobApplication, $emailTemplate));

        return $this->jobApplication;
    }

    /**
     * Get job applications by workjob for all status
     *
     * @param Workjob $workjob
     * @return Array
     */
    public function getJobApplicationsAllStatus(Workjob $workjob)
    {
        $pendingJaps = $workjob->job_applications()->with('user.province', 'user.city')->where('status', 'pending')->paginate(50);
        $shortlistedJaps = $workjob->job_applications()->with('user.province', 'user.city')->where('status', 'shortlisted')->paginate(50);
        $interviewJaps = $workjob->job_applications()->with('user.province', 'user.city')->where('status', 'interview')->paginate(50);
        $offeredJaps = $workjob->job_applications()->with('user.province', 'user.city')->where('status', 'offered')->paginate(50);
        $hiredJaps = $workjob->job_applications()->with('user.province', 'user.city')->where('status', 'hired')->paginate(50);
        $unsuitableJaps = $workjob->job_applications()->with('user.province', 'user.city')->where('status', 'unsuitable')->paginate(50);

        return [
            'pending' => [
                'job_applications' => JobApplicationSimpleResource::collection($pendingJaps),
                'prev_url' => $pendingJaps->previousPageUrl() ?? '',
                'next_url' => $pendingJaps->nextPageUrl() ?? '',
                'total' => $pendingJaps->total() ?? '',
            ],
            'shortlisted' => [
                'job_applications' => JobApplicationSimpleResource::collection($shortlistedJaps),
                'prev_url' => $shortlistedJaps->previousPageUrl() ?? '',
                'next_url' => $shortlistedJaps->nextPageUrl() ?? '',
                'total' => $shortlistedJaps->total() ?? '',
            ],
            'interview' => [
                'job_applications' => JobApplicationSimpleResource::collection($interviewJaps),
                'prev_url' => $interviewJaps->previousPageUrl() ?? '',
                'next_url' => $interviewJaps->nextPageUrl() ?? '',
                'total' => $interviewJaps->total() ?? '',
            ],
            'offered' => [
                'job_applications' => JobApplicationSimpleResource::collection($offeredJaps),
                'prev_url' => $offeredJaps->previousPageUrl() ?? '',
                'next_url' => $offeredJaps->nextPageUrl() ?? '',
                'total' => $offeredJaps->total() ?? '',
            ],
            'hired' => [
                'job_applications' => JobApplicationSimpleResource::collection($hiredJaps),
                'prev_url' => $hiredJaps->previousPageUrl() ?? '',
                'next_url' => $hiredJaps->nextPageUrl() ?? '',
                'total' => $hiredJaps->total() ?? '',
            ],
            'unsuitable' => [
                'job_applications' => JobApplicationSimpleResource::collection($unsuitableJaps),
                'prev_url' => $unsuitableJaps->previousPageUrl() ?? '',
                'next_url' => $unsuitableJaps->nextPageUrl() ?? '',
                'total' => $unsuitableJaps->total() ?? '',
            ],
        ];
    }

    /**
     * Get interview email template
     *
     * @return String
     */
    public static function interviewEmailTemplate()
    {
        $emailTemplate = "Hello %name%,

Status lamaran kerja Anda untuk %job_title% pada perusahaan %company% telah berubah ke interview.

Berikut ini adalah waktu dan tempat untuk interview:
        ";

        return $emailTemplate;
    }
}
