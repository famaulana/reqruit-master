## How to Setup

-   Create .env file, copy the content from .env.example
-   Setup database credentials in .env file
-   Run composer install
-   Run php artisan migrate
-   Run php artisan db:seed
-   Run php artisan passport:install
-   Run php artisan storage:link
-   Run npm install
-   To start dev server, run php artisan serve
-   To start js development, run npm run watch
