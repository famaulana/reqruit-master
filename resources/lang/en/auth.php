<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'account_not_found' => 'Account not found.',
    'wrong_password' => 'Wrong Password',
    'login_success' => 'Login success',
    'reset_password' => [
        'link_send_success' => 'Reset password link sent to your email',
        'link_send_error' => 'Error sending reset password link',
        'token_mismatch' => 'Token mismatch',
        'update_success' => 'Update password success'
    ],
    'social' => [
        'failed' => 'Unable to login using :service.'
    ]
];
