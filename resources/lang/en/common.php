<?php

return [
    'updated' => 'Update success',
    'saved' => 'Save success',
    'stored' => 'Stored success',
    'removed' => 'Removed success',
    'trashed' => 'Move to trashed success',
    'permanent_deleted' => 'Permanently deleted',
    'restored' => 'Restore success',
];
