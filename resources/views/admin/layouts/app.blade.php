<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="icon" type="image/png" href="{{ $faviconUrl }}">

    <script type="text/javascript">
        var DASHBOARD_BASE_URL = {!! json_encode(url('/admin')) !!};
        var APP_BASE_URL = {!! json_encode(url('/')) !!};
        var FRONTEND_BASE_URL = '{{ config('frontend.frontend_url') }}';
        var LOCALE = '{{ $locale }}';
        var BASE_CURRENCY = '{{ $baseCurrency }}';
        var BASE_CURRENCY_SYMBOL = '{{ $currencySymbol }}';
    </script>

    <!-- Scripts -->
        <script src="{{ asset('admin/js/admin.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('admin/css/admin.css') }}" rel="stylesheet">

</head>
<body>
    <div id="app" class="dashboard">
        @include('admin.layouts._sidebar', [
            'logoUrl' => $adminLogoUrl,
        ])

        <main class="main-panel">
            <top-nav :admin="{{ json_encode(Auth::guard('admin')->user()) }}"></top-nav>
            <form id="logout-form" action="{{ route('auth.admin.logout') }}" method="POST" style="display: none;">
                @csrf
            </form>

            <div id="content">
                <div class="container">
                    @include('share.flash-alert')
                </div>
                @yield('content')
            </div>
            <vue-snotify></vue-snotify>
            <page-overlay></page-overlay>
        </main>
    </div>
</body>
</html>
