@php
    $activeMenu = '';
    $activeParentMenu = '';
    if(Request::is('*/home')) {
        $activeMenu = 'home';
    }
    // if(Request::is('*/categories*')) {
    //     $activeMenu = 'categories';
    // }
    if(Request::is('*/pages*')) {
        $activeMenu = 'pages';
    }
    if(Request::is('*/employers*')) {
        $activeMenu = 'employers';
    }
    if(Request::is('*/head-huntings*')) {
        $activeMenu = 'head-huntings';
    }
    if(Request::is('*/companies*')) {
        $activeMenu = 'companies';
    }
    if(Request::is('*/company-reviews*')) {
        $activeMenu = 'company-reviews';
    }
    if(Request::is('*/industries*')) {
        $activeMenu = 'industries';
    }
    if(Request::is('*/job*')) {
        $activeParentMenu = 'job';
        if(Request::is('*/job/jobs*')) {
            $activeMenu = 'jobs';
        }
        if(Request::is('*/job/job-applications*')) {
            $activeMenu = 'job-applications';
        }
        if(Request::is('*/job/skills*')) {
            $activeMenu = 'skills';
        }
        if(Request::is('*/job/job-functions*')) {
            $activeMenu = 'job_functions';
        }
        if(Request::is('*/job/job-roles*')) {
            $activeMenu = 'job_roles';
        }
        if(Request::is('*/job/jap-rejection-messages*')) {
            $activeMenu = 'jap_rejection_messages';
        }
    }
    if(Request::is('*/testimonials*')) {
        $activeMenu = 'testimonials';
    }
    if(Request::is('*/articles*')) {
        $activeMenu = 'articles';
    }
    if(Request::is('*/users*')) {
        $activeMenu = 'users';
    }
    if(Request::is('*/education*')) {
        $activeParentMenu = 'education';
        if(Request::is('*education/education-degrees*')) {
            $activeMenu = 'education-degrees';
        }
        if(Request::is('*education/education-fields*')) {
            $activeMenu = 'education-fields';
        }
    }
    if(Request::is('*/question*')) {
        $activeParentMenu = 'question';
        if(Request::is('*question/english-questions*')) {
            $activeMenu = 'english-questions';
        }
    }
    if(Request::is('*/static-blocks*')) {
        $activeMenu = 'static_blocks';
    }
    if(Request::is('*/menus*')) {
        $activeMenu = 'menus';
    }
    if(Request::is('*/banners*')) {
        $activeParentMenu = 'banners';
        if(Request::is('*/banners/main-banners*')) {
            $activeMenu = 'main_banners';
        }
    }
    if(Request::is('*/admins*')) {
        $activeMenu = 'admins';
    }
    if(Request::is('*/configs*')) {
        $activeMenu = 'configs';
    }

    $admin = auth()->guard('admin')->user();
    $permissions = $admin->permissions->pluck('name')->toArray();
    $adminResource = new \App\Http\Resources\AdminResource($admin);
@endphp

<sidebar
    :admin="{{ json_encode($adminResource) }}"
    :permissions="{{ json_encode($permissions) }}"
    :app_name="'{{ $siteName }}'"
    :logo_url="'{{ $logoUrl }}'"
    :home_url="'{{ route('admin.home') }}'"
    {{-- :categories_url="'{{ route('admin.categories.index') }}'" --}}
    :pages_url="'{{ route('admin.pages.index') }}'"
    :employers_url="'{{ route('admin.employers.index') }}'"
    :head_huntings_url="'{{ route('admin.head-huntings.index') }}'"
    :companies_url="'{{ route('admin.companies.index') }}'"
    :company_reviews_url="'{{ route('admin.company-reviews.index') }}'"
    :industries_url="'{{ route('admin.industries.index') }}'"
    :jobs_url="'{{ route('admin.jobs.index') }}'"
    :job_applications_url="'{{ route('admin.job-applications.index') }}'"
    :skills_url="'{{ route('admin.skills.index') }}'"
    :job_functions_url="'{{ route('admin.job-functions.index') }}'"
    :job_roles_url="'{{ route('admin.job-roles.index') }}'"
    :jap_rejection_messages_url="'{{ route('admin.jap-rejection-messages.index') }}'"
    :testimonials_url="'{{ route('admin.testimonials.index') }}'"
    :articles_url="'{{ route('admin.articles.index') }}'"
    :users_url="'{{ route('admin.users.index') }}'"
    :education_degrees_url="'{{ route('admin.education-degrees.index') }}'"
    :education_fields_url="'{{ route('admin.education-fields.index') }}'"
    :english_questions_url="'{{ route('admin.english-questions.index') }}'"
    :static_blocks_url="'{{ route('admin.static-blocks.index') }}'"
    :menus_url="'{{ route('admin.menus.index') }}'"
    :main_banners_url="'{{ route('admin.main-banners.index') }}'"
    :admins_url="'{{ route('admin.admins.index') }}'"
    :configs_url="'{{ route('admin.configs.general') }}'"
    :active_menu="'{{ $activeMenu }}'"
    :active_parent_menu="'{{ $activeParentMenu }}'"
></sidebar>