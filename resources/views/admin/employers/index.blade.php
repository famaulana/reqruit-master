@extends('admin.layouts.app')

@section('content')
    <Employers :data="{{ json_encode($data) }}" />
@endsection
