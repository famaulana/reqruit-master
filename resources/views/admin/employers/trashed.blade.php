@extends('admin.layouts.app')

@section('content')
    <employers-trashed :data="{{ json_encode($data) }}"></employers-trashed>
@endsection
