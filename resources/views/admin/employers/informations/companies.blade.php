@extends('admin.employers.informations.employers-layout')

@section('employer-information')
    <employer-companies 
        :data="{{ json_encode($data) }}"
    ></employer-companies>
@endsection
