@extends('admin.employers.informations.employers-layout')

@section('employer-information')
    <b-card header="Employer Info">
        <div class="row">
            <div class="col-md-12 col-lg-6">
                <table class="table table-sm table-borderless table--width-auto">
                    <tbody>
                        <tr>
                            <th class="label">Identifier</th>
                            <td class="data">{{ $employer->identifier }}</td>
                        </tr>
                        <tr>
                            <th class="label">Name</th>
                            <td class="data">{{ $employer->name }}</td>
                        </tr>
                        <tr>
                            <th class="label">Email</th>
                            <td class="data">{{ $employer->email }}</td>
                        </tr>
                        <tr>
                            <th class="label">Phone</th>
                            <td class="data">{{ $employer->phone ?? '' }}</td>
                        </tr>
                        <tr>
                            <th class="label">Gender</th>
                            <td class="data">{{ $employer->gender ?? '' }}</td>
                        </tr>
                        <tr>
                            <th class="label">Job Position</th>
                            <td class="data">{{ $employer->job_position ?? '' }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-12 col-lg-6">
                <table class="table table-sm table-borderless table--width-auto">
                    <tbody>
                        <tr>
                            <th class="label">Date of Birth</th>
                            <td class="data">{{ is_null($employer->dob) ? '' : $employer->dob->format('Y-m-d') }}</td>
                        </tr>
                        <tr>
                            <th class="label">Role</th>
                            <td class="data">{{ $employer->role->description }}</td>
                        </tr>
                        <tr>
                            <th class="label">Created At</th>
                            <td class="data">{{ $employer->created_at }}</td>
                        </tr>
                        <tr>
                            <th class="label">Updated At</th>
                            <td class="data">{{ $employer->updated_at }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        
    </b-card>

    @if (!is_null($company))
        <div class="card mt-4">
            <div class="card-header">Company</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-md-4 col-lg-3">
                        <img src="{{ $company->logo_url }}" alt="{{ $company->name }}" class="img-fluid">
                    </div>
                    <div class="col-12 col-md-8 col-lg-9">
                        <dl>
                            <dt>Name</dt>
                            <dd>{{ $company->name }}</dd>
    
                            <dt>Address</dt>
                            <dd>{{ $company->city->name }}, <br>{{ $company->province->name }}</dd>
                        </dl>
                        <div class="mt-2">
                            <a href="{{ route('company.index', ['slug' => $company->slug]) }}" title="Company page" target="_blank">Company page <i class="fas fa-external-link-alt"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
