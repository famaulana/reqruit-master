@extends('admin.layouts.app')

@section('content')
<b-container fluid>
    <b-row>
        <b-col md="5" lg="3" class="mb-3 mb-md-3">
            <employer-card :employer="{{ json_encode($employer) }}"></employer-card>
            
            {{-- <b-card class="card-navigation">
                <a 
                    class="nav-link {{ (Request::is('*/employers/'.$employer->id)) ? 'active' : '' }}" 
                    href="{{ route('admin.employers.show', $employer->id) }}"
                >
                    <span class="icon"><i class="far fa-dot-circle"></i></span>
                    <span class="text">Overview</span>
                </a>
    
                <a 
                    class="nav-link {{ (Request::is('*/employers/*/companies')) ? 'active' : '' }}" 
                    href="{{ route('admin.employers.companies', $employer->id) }}"
                >
                    <span class="icon"><i class="far fa-building"></i></span>
                    <span class="text">Companies</span>
                </a>
            </b-card> --}}
        </b-col>
        <div class="col-md-7 col-lg-9">
            @yield('employer-information')
        </div>
    </b-row>
</b-container>
@endsection
