@extends('admin.layouts.app')

@section('content')
    <Jobfunctions :data="{{ json_encode($data) }}" />
@endsection
