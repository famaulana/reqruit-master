@extends('admin.layouts.app')

@section('content')
    <jobfunctions-trashed :data="{{ json_encode($data) }}"></jobfunctions-trashed>
@endsection
