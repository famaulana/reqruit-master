@extends('admin.layouts.app')

@section('content')
    <jobfunction-edit :job_function="{{ json_encode($jobFunction) }}"></jobfunction-edit>
@endsection