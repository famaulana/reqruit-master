@extends('admin.layouts.app')

@section('content')
    <job-applications :data="{{ json_encode($data) }}"></job-applications>
@endsection
