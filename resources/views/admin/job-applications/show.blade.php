@extends('admin.layouts.app')

@section('content')
    <b-container>
        <h3 class="mb-4">Status: {{ $jobApplication->status->description }}</h3>

        <b-row>
            <b-col lg="6">
                <b-card header="Job">
                    <b-row>
                        <b-col lg="3">
                            <b-img rounded src="{{ $company->logo_url }}" alt="{{ $company->name }}" class="img-fluid"></b-img>
                        </b-col>
                        <b-col lg="9">
                            <p class="h5"><strong>{{ $job->title }}</strong></p>
                            <div class="my-2">
                                <a href="{{ route('company.index', $company->slug) }}" title="{{ $company->name }}"><strong>{{ $company->name }}</strong></a>
                            </div>
                            <a href="{{ route('workjob.index', ['slug' => $job->slug, 'identifier' => $job->identifier]) }}" title="Job page" target="_blank">Job page <i class="fas fa-external-link-alt"></i></a>
                        </b-col>
                    </b-row>

                    <hr>

                    <b-row>
                        <b-col lg="6">
                            <dl>
                                <dt>Location</dt>
                                <dd>{{ $job->city_name }}</dd>
                            
                                <dt>Tipe Pekerjaan</dt>
                                <dd>{{ $job->type->description }}</dd>
                            </dl>
                        </b-col>
                        <b-col lg="6">
                            <dl>
                                <dt>Job Function</dt>
                                <dd>{{ $job->job_function->name }}</dd>
    
                                <dt>Experience</dt>
                                <dd>{{ $job->work_experience->description }}</dd>
                            </dl>
                        </b-col>
                    </b-row>

                    <hr>

                    <div>
                        <h5 class="mb-3"><strong><i class="fas fa-star"></i> <span class="ml-1">Skills</span></strong></h5>
                        <b-row>
                            <b-col lg="6">
                                <p>Required Skills:</p>
                                @foreach ($job->must_skills as $skill)
                                    <b-badge variant="secondary">{{ $skill->name }}</b-badge>
                                @endforeach
                            </b-col>
                            <b-col lg="6">
                                <p>Optional Skills:</p>
                                @foreach ($job->nice_skills as $skill)
                                    <b-badge variant="secondary">{{ $skill->name }}</b-badge>
                                @endforeach
                            </b-col>
                        </b-row>
                    </div>

                    <hr>

                    <div>
                        <h5 class="mb-3"><strong><i class="fas fa-money-bill-wave"></i> <span class="ml-1">Salary</span></strong></h5>
                        <b-row>
                            <b-col lg="6">
                                <dl>
                                    <dt>Main Salary</dt>
                                    <dd>{{ $job->salary_currency }} {{ $job->formatted_salary_min }} - {{ $job->formatted_salary_max }}</dd>

                                    <dt>Durasi Gaji</dt>
                                    <dd>{{ $job->salary_duration->description }}</dd>
                                </dl>
                            </b-col>
                            <b-col lg="6">
                                <dl>
                                    <dt>Bonus Salary</dt>
                                    <dd>
                                        @if ($job->bonus_salary)
                                            {{ $job->salary_currency }} {{ $job->formatted_salary_min }} - {{ $job->formatted_salary_max }}
                                        @else
                                            Tidak ada
                                        @endif
                                    </dd>

                                    @if ($job->bonus_salary)
                                        <dt>Durasi Gaji Bonus</dt>
                                        <dd>{{ $job->bonus_salary_duration->description }}</dd>
                                    @endif
                                </dl>
                            </b-col>
                        </b-row>
                    </div>

                    <hr>

                    <div>
                        <h5 class="mb-3"><strong><i class="far fa-file-alt"></i> <span class="ml-1">Description</span></strong></h5>
                        <div>{!! $job->description !!}</div>
                    </div>
                </b-card>
            </b-col>

            <b-col lg="6">
                <b-card header="User">
                    <b-row>
                        <b-col lg="3">
                            <b-avatar src="{{ $user->avatar_url }}" size="6rem"></b-avatar>
                        </b-col>
                        <b-col lg="9">
                            <p class="h5"><strong>{{ $user->name }}</strong> <small class="text-muted">({{ $user->age }} years old)</small></p>
                            <div class="mb-2">{{ $user->city->name }},<br> {{ $user->province->name }}</div>
                            <a href="{{ route('profile', ['identifier' => $user->identifier]) }}" title="Profile page" target="_blank">Profile page <i class="fas fa-external-link-alt"></i></a>
                        </b-col>
                    </b-row>

                    <hr>

                    <b-row>
                        <b-col lg="6">
                            <dl>
                                <dt>Email</dt>
                                <dd>{{ $user->email }}</dd>
        
                                <dt>Gender</dt>
                                <dd>{{ $user->gender_text }}</dd>
                            </dl>
                        </b-col>
                        <b-col lg="6">
                            <dl>
                                <dt>Phone</dt>
                                <dd>{{ $user->phone }}</dd>
        
                                <dt>DOB</dt>
                                <dd>{{ is_null($user->dob) ? '' : $user->dob->format('Y-m-d') }}</dd>
                            </dl>
                        </b-col>
                    </b-row>
                    
                    <hr>

                    <div>
                        <h5 class="mb-3"><strong><i class="fas fa-user"></i> <span class="ml-1">About</span></strong></h5>
                        <div class="pre-line">{{ $user->about }}</div>
                    </div>

                    <hr>

                    <div>
                        <h5 class="mb-3"><strong><i class="fas fa-briefcase"></i> <span class="ml-1">Experience</span></strong></h5>
                       
                        <b-list-group>
                            @foreach ($user->work_experiences as $exp)
                                <b-list-group-item>
                                    <h5>{{ $exp->company }} <small class="text-muted">{{ $exp->position }}</small></h5>
                                    <div class="my-2 pre-line">{{ $exp->more_info }}</div>
                                    <div class="font-italic text-muted small">
                                        {{ is_null($exp->start_time) ? '' : $exp->start_time->format('d F Y') }} -
                                        {{ is_null($exp->start_time) ? '' : $exp->start_time->format('d F Y'), $exp->still_working }}
                                    </div>
                                </b-list-group-item>
                            @endforeach
                        </b-list-group>
                    </div>

                    <hr>

                    <div>
                        <h5 class="mb-3"><strong><i class="fas fa-graduation-cap"></i> <span class="ml-1">Education</span></strong></h5>
                        @foreach ($user->educations as $edu)
                            <b-list-group-item>
                                <h5>{{ $edu->institution }}</h5>
                                <div>{{ $edu->degree->name }} - {{ $edu->education_field->name }}</div>
                                <div class="my-2 pre-line">{{ $edu->more_info }}</div>
                                <div class="font-italic text-muted small">
                                    {{ is_null($edu->start_time) ? '' : $edu->start_time->format('d F Y'), }} -
                                    {{ is_null($edu->end_time) ? '' : $edu->end_time->format('d F Y'), $edu->still_studying }}
                                </div>
                            </b-list-group-item>
                        @endforeach
                    </div>

                    <hr>

                    <div>
                        <h5 class="mb-3"><strong><i class="fas fa-star"></i> <span class="ml-1">Skills</span></strong></h5>
                        @foreach ($user->skills as $skill)
                            <b-badge variant="secondary">{{ $skill->name }}</b-badge>
                        @endforeach
                    </div>
                </b-card>
            </b-col>
        </b-row>
    </b-container>
@endsection
