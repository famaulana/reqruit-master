@extends('admin.layouts.app')

@section('content')
    <company-reviews-trashed :data="{{ json_encode($data) }}"></company-reviews-trashed>
@endsection
