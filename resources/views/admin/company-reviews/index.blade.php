@extends('admin.layouts.app')

@section('content')
    <company-reviews :data="{{ json_encode($data) }}"></company-reviews>
@endsection
