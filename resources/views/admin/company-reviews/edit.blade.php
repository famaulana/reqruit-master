@extends('admin.layouts.app')

@section('content')
    <company-review-edit :review="{{ json_encode($review) }}"></company-review-edit>
@endsection
