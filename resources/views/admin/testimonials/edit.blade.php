@extends('admin.layouts.app')

@section('content')
    <testimonial-edit 
        :testimonial="{{ json_encode($testimonial) }}"
    ></testimonial-edit>
@endsection