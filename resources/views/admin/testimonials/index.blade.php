@extends('admin.layouts.app')

@section('content')
    <Testimonials :data="{{ json_encode($data) }}" />
@endsection
