<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('admin/js/admin.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('admin/css/admin.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <main id="auth">
            <div class="container">
                <div class="row justify-content-center align-content-center">
                    <div class="col-sm-12 col-lg-4">
                        <div class="auth-form">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif

                            @include('share.flash-alert')
                            
                            @yield('content')

                            @if (!Request::is('*auth/panel-login*'))
                                <div class="small mt-4">
                                    <a href="{{ route('auth.admin.login.form') }}" title="Back to login">&larr; Back to login</a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</body>
</html>
