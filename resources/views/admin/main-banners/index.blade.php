@extends('admin.layouts.app')

@section('content')
    <main-banners :data="{{ json_encode($data) }}"></main-banners>
@endsection
