@extends('admin.layouts.app')

@section('content')
    <main-banner-edit :main_banner="{{ json_encode($mainBanner) }}"></main-banner-edit>
@endsection