<div class="form-group">
    <label for="name">Name</label>
    <input type="text" id="name" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ old('name', (isset($educationField)) ? $educationField->name : '') }}">
    {!! $errors->first('name', '<span class="invalid-feedback" role="alert">:message</span>') !!}
</div>