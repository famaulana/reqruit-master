@extends('admin.layouts.app')

@section('content')
    <education-fields :data="{{ json_encode($data) }}"></education-fields>
@endsection
