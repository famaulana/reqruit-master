@extends('admin.layouts.app')

@section('content')
    <education-fields-trashed :data="{{ json_encode($data) }}"></education-fields-trashed>
@endsection
