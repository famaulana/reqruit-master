@extends('admin.layouts.app')

@section('content')
    <skill-edit :skill="{{ json_encode($skill) }}"></skill-edit>
@endsection