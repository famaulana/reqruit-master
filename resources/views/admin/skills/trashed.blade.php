@extends('admin.layouts.app')

@section('content')
    <skills-trashed :data="{{ json_encode($data) }}"></skills-trashed>
@endsection
