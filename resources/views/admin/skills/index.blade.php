@extends('admin.layouts.app')

@section('content')
    <Skills :data="{{ json_encode($data) }}" />
@endsection
