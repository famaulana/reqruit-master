@extends('admin.layouts.app')

@section('content')
<b-container>
    <b-row>
        <b-col md="5" lg="3" class="mb-3 mb-md-3">
            <div class="card mb-3">
                <div class="card-body">
                    <div class="mx-auto mb-3" style="width: 150px;">
                        <img src="{{ $company->logo_url }}" alt="{{ $company->name }}" class="img-fluid" />
                    </div>
                    <h4 class="h5 bold text-center mb-2"><strong>{{ $company->name }}</strong></h4>
                    <div class="mt-3 small text-center">
                        <a
                            href="{{ config('frontend.frontend_url') }}/company/{{ $company->slug }}"
                            title="Company Page"
                            target="_blank"
                            >Company Page <i class="fas fa-external-link-alt"></i
                        ></a>
                    </div>
                </div>
            </div>
        </b-col>
        <div class="col-md-7 col-lg-9">
            <b-card header="Company Main Info" class="mb-3">
                <table class="table table-sm table-borderless table--width-auto">
                    <tbody>
                        <tr>
                            <th class="label">Name</th>
                            <td class="data">{{ $company->name }}</td>
                        </tr>
                        <tr>
                            <th class="label">Short Description</th>
                            <td class="data">{{ $company->short_description }}</td>
                        </tr>
                        <tr>
                            <th class="label">Description</th>
                            <td class="data">{!! $company->description !!}</td>
                        </tr>
                        <tr>
                            <th class="label">Industries</th>
                            <td class="data">
                                <ul>
                                    @foreach($company->industries as $ind)
                                        <li>{{ $ind->name }}</li>
                                    @endforeach
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <th class="label">Address</th>
                            <td class="data">
                                {{ $company->address }},<br>
                                {{ $company->subdistrict->name }}, {{ $company->city->name }}<br>
                                {{ $company->province->name }}, {{ $company->country->name }}<br>
                                {{ $company->pos_code }}
                            </td>
                        </tr>
                        <tr>
                            <th class="label">Phone</th>
                            <td class="data">
                                <a href="tel:{{ $company->phone }}">{{ $company->phone }}</a>
                            </td>
                        </tr>
                        <tr>
                            <th class="label">Email</th>
                            <td class="data">
                                <a href="mailto:{{ $company->email }}">{{ $company->email }}</a>
                            </td>
                        </tr>
                        <tr>
                            <th class="label">Website</th>
                            <td class="data">
                                <a href="{{ $company->website_url }}" target="_blank">{{ $company->website_url }}</a>
                            </td>
                        </tr>
                        <tr>
                            <th class="label">Instagram</th>
                            <td class="data">
                                <a href="{{ $company->instagram }}" target="_blank">{{ $company->instagram }}</a>
                            </td>
                        </tr>
                        <tr>
                            <th class="label">Facebook</th>
                            <td class="data">
                                <a href="{{ $company->facebook }}" target="_blank">{{ $company->facebook }}</a>
                            </td>
                        </tr>
                        <tr>
                            <th class="label">Linkedin</th>
                            <td class="data">
                                <a href="{{ $company->linkedin }}" target="_blank">{{ $company->linkedin }}</a>
                            </td>
                        </tr>
                        <tr>
                            <th class="label">Twitter</th>
                            <td class="data">
                                <a href="{{ $company->twitter }}" target="_blank">{{ $company->twitter }}</a>
                            </td>
                        </tr>
                        <tr>
                            <th class="label">Created At</th>
                            <td class="data">{{ $company->created_at->format('d M Y H:i:s') }}</td>
                        </tr>
                    </tbody>
                </table>
            </b-card>

            <b-card header="Company Gallery">
                <div class="row">
                    @foreach ($company->company_images as $img)
                        <div class="col-12 col-md-4">
                            <img src="{{ $img['src'] }}" alt="{{ $img['description'] }}" class="img-fluid mb-2" />
                            <p class="small text-muted">{{ $img['description'] }}</p>
                        </div>
                    @endforeach
                </div>
            </b-card>
        </div>
    </b-row>
</b-container>
@endsection
