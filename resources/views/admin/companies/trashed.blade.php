@extends('admin.layouts.app')

@section('content')
    <companies-trashed :data="{{ json_encode($data) }}"></companies-trashed>
@endsection
