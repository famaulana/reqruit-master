@extends('admin.layouts.app')

@section('content')
    <Companies :data="{{ json_encode($data) }}" />
@endsection
