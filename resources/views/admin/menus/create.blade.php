@extends('admin.layouts.app')

@section('content')
    <menu-create 
        :pages="{{ json_encode($pages) }}"
    ></menu-create>
@endsection