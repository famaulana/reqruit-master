@extends('admin.layouts.app')

@section('content')
    <Menus :data="{{ json_encode($data) }}" />
@endsection
