@extends('admin.layouts.app')

@section('content')
    <menu-edit 
        :pages="{{ json_encode($pages) }}" 
        :menu="{{ json_encode($menu) }}"
    ></menu-edit>
@endsection