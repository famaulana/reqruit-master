@extends('admin.layouts.app')

@section('content')
    <Categories :categories_data="{{ json_encode($categories) }}" />
@endsection
