@extends('admin.layouts.app')

@section('content')
    <categories-trashed :data="{{ json_encode($data) }}"></categories-trashed>
@endsection
