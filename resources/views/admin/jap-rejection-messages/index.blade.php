@extends('admin.layouts.app')

@section('content')
    <b-container>
        <jap-rejection-messages :messages="{{ json_encode($messages) }}"></jap-rejection-messages>
    </b-container>
@endsection
