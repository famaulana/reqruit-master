@extends('admin.layouts.app')

@section('content')
    <jobrole-edit 
        :job_function_select="{{ json_encode($jobFunctionSelect) }}"
        :job_role="{{ json_encode($jobRole) }}"
    ></jobrole-edit>
@endsection