@extends('admin.layouts.app')

@section('content')
    <jobrole-create :job_function_select="{{ json_encode($jobFunctionSelect) }}"></jobrole-create>
@endsection
