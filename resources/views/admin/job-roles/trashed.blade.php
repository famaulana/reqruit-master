@extends('admin.layouts.app')

@section('content')
    <jobroles-trashed :data="{{ json_encode($data) }}"></jobroles-trashed>
@endsection
