@extends('admin.layouts.app')

@section('content')
    <article-edit :article="{{ json_encode($article) }}"></article-edit>
@endsection