@extends('admin.layouts.app')

@section('content')
    <articles-trashed :data="{{ json_encode($data) }}"></articles-trashed>
@endsection
