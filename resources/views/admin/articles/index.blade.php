@extends('admin.layouts.app')

@section('content')
    <Articles :data="{{ json_encode($data) }}" />
@endsection
