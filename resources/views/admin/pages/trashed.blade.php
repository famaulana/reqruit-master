@extends('admin.layouts.app')

@section('content')
    <pages-trashed :data="{{ json_encode($data) }}"></pages-trashed>
@endsection
