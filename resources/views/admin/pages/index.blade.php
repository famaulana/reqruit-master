@extends('admin.layouts.app')

@section('content')
    <Pages :data="{{ json_encode($data) }}" />
@endsection
