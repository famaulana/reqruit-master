@extends('admin.layouts.app')

@section('content')
    <page-edit :page="{{ json_encode($page) }}"></page-edit>
@endsection