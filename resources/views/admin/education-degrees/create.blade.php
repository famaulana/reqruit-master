@extends('admin.layouts.app')

@section('content')
<b-container>
    <b-row>
        <b-col lg="6" class="m-auto">
            <b-card header="New Education Degree">
                <form action="{{ route('admin.education-degrees.store') }}" method="POST">
                    @csrf
                    @include('admin.education-degrees._form')
                    <button type="submit" class="btn btn-primary float-right">Save</button>
                    <div class="clearfix"></div>
                </form>
            </b-card>
        </b-col>
    </b-row>
</b-container>
@endsection
