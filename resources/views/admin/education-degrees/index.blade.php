@extends('admin.layouts.app')

@section('content')
    <education-degrees :data="{{ json_encode($data) }}"></education-degrees>
@endsection
