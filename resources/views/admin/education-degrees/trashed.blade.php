@extends('admin.layouts.app')

@section('content')
    <education-degrees-trashed :data="{{ json_encode($data) }}"></education-degrees-trashed>
@endsection
