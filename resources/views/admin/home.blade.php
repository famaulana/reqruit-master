@extends('admin.layouts.app')

@section('content')
    <b-container fluid>
        <b-row>
            <b-col lg="3" md="6" class="mb-4">
                <b-card bg-variant="primary" text-variant="white" header="Companies" class="text-center">
                    <b-card-text><div class="display-4">{{ $data['stats']['total_companies'] }}</div></b-card-text>
                </b-card>
            </b-col>
            <b-col lg="3" md="6" class="mb-4">
                <b-card bg-variant="success" text-variant="white" header="Jobs" class="text-center">
                    <b-card-text><div class="display-4">{{ $data['stats']['total_jobs'] }}</div></b-card-text>
                </b-card>
            </b-col>
            <b-col lg="3" md="6" class="mb-4">
                <b-card bg-variant="info" text-variant="white" header="Info" header="Users" class="text-center">
                    <b-card-text><div class="display-4">{{ $data['stats']['total_users'] }}</div></b-card-text>
                </b-card>
            </b-col>
            <b-col lg="3" md="6" class="mb-4">
                <b-card bg-variant="secondary" text-variant="white" header="Job Applications" class="text-center">
                    <b-card-text><div class="display-4">{{ $data['stats']['total_job_applications'] }}</div></b-card-text>
                </b-card>
            </b-col>
        </b-row>

        <b-row class="mt-4">
            <b-col lg="6">
                <b-card header="Companies">
                    <table class="table table-sm">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data['companies'] as $comp)
                                <tr>
                                    <td>{{ $comp->id }}</td>
                                    <td>{{ $comp->name }}</td>
                                </tr> 
                            @endforeach
                        </tbody>
                    </table>
                </b-card>
            </b-col>
            <b-col lg="6">
                <b-card header="Jobs">
                    <table class="table table-sm">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Title</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data['jobs'] as $job)
                                <tr>
                                    <td>{{ $job->id }}</td>
                                    <td>{{ $job->title }}</td>
                                </tr> 
                            @endforeach
                        </tbody>
                    </table>
                </b-card>
            </b-col>
            <b-col lg="6" class="mt-4">
                <b-card header="Users">
                    <table class="table table-sm">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data['users'] as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->name }}</td>
                                </tr> 
                            @endforeach
                        </tbody>
                    </table>
                </b-card>
            </b-col>
            <b-col lg="6" class="mt-4">
                <b-card header="Job Applications">
                    <table class="table table-sm">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Job Title</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data['job_applications'] as $jap)
                                @if (!is_null($jap->workjob))
                                    <tr>
                                        <td>{{ $jap->id }}</td>
                                        <td>{{ $jap->workjob->title }}</td>
                                        <td>{{ $jap->status->description }}</td>
                                    </tr> 
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </b-card>
            </b-col>
        </b-row>
    </b-container>
@endsection
