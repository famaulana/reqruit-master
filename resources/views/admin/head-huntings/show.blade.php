@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <h1 class="page-title">Head Hunting Details</h1>
        <b-row>
            <b-col lg="6">
                <b-card header="Job Info" class="mb-4">
                    <dl>
                        <dt>Company</dt>
                        <dd>{{ $headHunting->company }}</dd>

                        <dt>Job Position</dt>
                        <dd>{{ $headHunting->job_position }}</dd>

                        <dt>Salary Min</dt>
                        <dd>Rp {{ \App\Helpers\AppHelper::formatNumber($headHunting->salary_min, 'id-ID') }}</dd>

                        <dt>Salary Max</dt>
                        <dd>Rp {{ \App\Helpers\AppHelper::formatNumber($headHunting->salary_max, 'id-ID') }}</dd>

                        <dt>Location</dt>
                        <dd>{{ $headHunting->location }}</dd>

                        <dt>Description</dt>
                        <dd>{{ $headHunting->description }}</dd>

                        <dt>Contacted</dt>
                        <dd>{{ $headHunting->contacted ? 'Yes' : 'No' }}</dd>

                        <dt>Created At</dt>
                        <dd>{{ $headHunting->created_at }}</dd>
                    </dl>
                </b-card>
            </b-col>
            <b-col lg="6">
                <b-card header="Employer Info" class="mb-4">
                    <table class="table table-sm table-borderless table--width-auto">
                        <tbody>
                            {{-- <tr>
                                <th class="label">Identifier</th>
                                <td class="data">{{ $headHunting->employer->identifier }}</td>
                            </tr> --}}
                            <tr>
                                <th class="label">Name</th>
                                <td class="data">{{ $headHunting->name }}</td>
                            </tr>
                            <tr>
                                <th class="label">Email</th>
                                <td class="data">{{ $headHunting->email }}</td>
                            </tr>
                            <tr>
                                <th class="label">Phone</th>
                                <td class="data">{{ $headHunting->phone ?? '' }}</td>
                            </tr>
                        </tbody>
                    </table>
                </b-card>
                <b-card header="Action">
                    <form action="{{ route('admin.head-huntings.update', $headHunting->id) }}" method="POST">
                        @method('patch')
                        @csrf
                        <div class="form-group">
                            <label for="contacted">Contacted?</label>
                            <select name="contacted" id="contacted" class="form-control{{ $errors->has('contacted') ? ' is-invalid' : '' }}">
                                <option value="0" {{ !$headHunting->contacted ? 'selected' : '' }}>No</option>
                                <option value="1" {{ $headHunting->contacted ? 'selected' : '' }}>Yes</option>
                            </select>
                            {!! $errors->first('contacted', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </b-card>
            </b-col>
        </b-row>
    </div>
@endsection
