@extends('admin.layouts.app')

@section('content')
    <head-huntings :data="{{ json_encode($data) }}"></head-huntings>
@endsection
