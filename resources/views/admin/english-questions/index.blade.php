@extends('admin.layouts.app')

@section('content')
    <english-questions :data="{{ json_encode($data) }}"></english-questions>
@endsection
