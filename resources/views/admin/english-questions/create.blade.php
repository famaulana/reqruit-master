@extends('admin.layouts.app')

@section('content')
<b-container>
    <b-row>
        <b-col lg="8" class="m-auto">
            <b-card header="New English Assessment">
                <form action="{{ route('admin.english-questions.store') }}" method="POST">
                    @csrf
                    @include('admin.english-questions._form')
                    <button type="submit" class="btn btn-primary float-right">Save</button>
                    <div class="clearfix"></div>
                </form>
            </b-card>
        </b-col>
    </b-row>
</b-container>
@endsection
