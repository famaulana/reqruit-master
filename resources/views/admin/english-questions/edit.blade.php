@extends('admin.layouts.app')

@section('content')
<b-container>
    <b-row>
        <b-col lg="8" class="m-auto">
            <b-card header="Edit English Assessment">
                <form action="{{ route('admin.english-questions.update', $englishQuestion) }}" method="POST">
                    @method('patch')
                    @csrf
                    @include('admin.english-questions._form')
                    <button type="submit" class="btn btn-primary float-right">Save</button>
                    <div class="clearfix"></div>
                </form>
            </b-card>
        </b-col>
    </b-row>
</b-container>
@endsection
