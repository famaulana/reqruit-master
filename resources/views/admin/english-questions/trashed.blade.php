@extends('admin.layouts.app')

@section('content')
    <english-questions-trashed :data="{{ json_encode($data) }}"></english-questions-trashed>
@endsection
