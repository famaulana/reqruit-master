<table class="table">
    <thead>
        <tr>
            <th>Question</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <div class="form-group">
                    <textarea 
                        rows="4" 
                        id="question" 
                        name="question" 
                        class="form-control{{ $errors->has('question') ? ' is-invalid' : '' }}"
                    >{{ old('question', (isset($englishQuestion)) ? $englishQuestion->question : '') }}</textarea>
                    {!! $errors->first('question', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                </div>
            </td>
        </tr>
    </tbody>
</table>

<table class="table">
    <thead>
        <tr>
            <th>Timer</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <div class="form-group">
                    <input
                        type="number" 
                        id="timer" 
                        name="timer" 
                        class="form-control{{ $errors->has('timer') ? ' is-invalid' : '' }}"
                        value="{{ old('timer', (isset($englishQuestion)) ? $englishQuestion->timer : '') }}"
                    />
                    {!! $errors->first('timer', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                </div>
            </td>
        </tr>
    </tbody>
</table>

<table class="table">
    <colgroup>
        <col width="90%">
        <col width="10%">
    </colgroup>
    <thead>
        <tr>
            <th>Answers</th>
            <th>Correct</th>
        </tr>
    </thead>
    <tbody>
        @for ($i = 0; $i < 4; $i++) 
            <tr>
                <td>
                    <input 
                        type="text" 
                        name="answers[]" 
                        id="answer-1" 
                        class="form-control{{ $errors->has('answers.'.$i) ? ' is-invalid' : '' }}" 
                        value="{{ old('answers.'.$i, $answers[$i]['answer']) }}"
                    >
                    {!! $errors->first('answers.'.$i, '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    <input 
                        type="hidden" 
                        name="answer_ids[]" 
                        id="answer_id-{{ $i }}" 
                        class="form-control{{ $errors->has('answer_ids.'.$i) ? ' is-invalid' : '' }}" 
                        value="{{ $answers[$i]['id'] }}"
                    >
                    {!! $errors->first('answer_ids.'.$i, '<span class="invalid-feedback" role="alert">:message</span>') !!}
                </td>
                <td class="d-flex justify-content-center">
                    <div class="custom-control custom-radio">
                        <input 
                            type="radio" 
                            class="custom-control-input" 
                            id="correct_answer-{{ $i }}" 
                            name="correct_answer" 
                            value="{{ $answers[$i]['id'] }}" 
                            {{ (isset($englishQuestion) && $englishQuestion->correct_answer === $answers[$i]['id']) ? 'checked' : '' }}
                        >
                        <label class="custom-control-label" for="correct_answer-{{ $i }}"></label>
                    </div>
                </td>
            </tr>
        @endfor
    </tbody>
</table>
{!! $errors->first('answers', '<span class="small text-danger" role="alert">:message</span>') !!}
{!! $errors->first('answer_ids', '<span class="small text-danger" role="alert">:message</span>') !!}
{!! $errors->first('correct_answer', '<span class="small text-danger" role="alert">:message</span>') !!}