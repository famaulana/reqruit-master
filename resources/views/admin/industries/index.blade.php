@extends('admin.layouts.app')

@section('content')
    <industries :data="{{ json_encode($data) }}"></industries>
@endsection
