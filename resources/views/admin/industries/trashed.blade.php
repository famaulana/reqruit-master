@extends('admin.layouts.app')

@section('content')
    <industries-trashed :data="{{ json_encode($data) }}"></industries-trashed>
@endsection
