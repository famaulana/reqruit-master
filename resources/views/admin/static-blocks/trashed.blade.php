@extends('admin.layouts.app')

@section('content')
    <static-blocks-trashed :data="{{ json_encode($data) }}"></static-blocks-trashed>
@endsection
