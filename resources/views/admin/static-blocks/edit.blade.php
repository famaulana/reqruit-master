@extends('admin.layouts.app')

@section('content')
    <static-block-edit :static_block="{{ json_encode($staticBlock) }}"></static-block-edit>
@endsection