@extends('admin.layouts.app')

@section('content')
    <static-blocks :data="{{ json_encode($data) }}"></static-blocks>
@endsection
