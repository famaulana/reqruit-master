@extends('admin.layouts.app')

@section('content')
    <workjobs-trashed :data="{{ json_encode($data) }}"></workjobs-trashed>
@endsection
