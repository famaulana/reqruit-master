@extends('admin.layouts.app')

@section('content')
    <b-container>
        <b-row>
            <b-col lg="3">
                <b-card>
                    <div class="mb-4">
                        <img src="{{ $job->company->logo_url }}" alt="{{ $job->company->name }}" class="img-fluid">
                    </div>
                    <div class="text-center"><strong>{{ $job->company->name }}</strong></div>
                    <hr>
                    <dl class="job-meta-info">
                        <dt>Job Posting Status</dt>
                        <dd>{{ $job->job_posting_status->description }}</dd>

                        <dt>Status</dt>
                        <dd>{{ $job->status->description }}</dd>

                        <dt>Created By</dt>
                        <dd>
                            {{ $job->creator->name }}<br />
                            at {{ $job->created_at->format('Y-m-d H:i:s') }}
                        </dd>

                        <dt>Updated By</dt>
                        <dd>
                            {{ $job->editor->name }}<br />
                            at {{ $job->updated_at->format('Y-m-d H:i:s') }}
                        </dd>
                    </dl>
                    <hr>
                    <form action="{{ route('admin.jobs.destroy', $job->id) }}" method="post" class="mb-3">
                        @method('delete')
                        @csrf
                        <button type="submit" class="btn btn-danger btn-block btn-submit-confirm" data-msg="Send this job to trash.">
                            <i class="far fa-trash-alt"></i> Trash
                        </button>
                    </form>

                    <div class="text-center">
                        <a href="{{ config('frontend.frontend_url') }}/job/{{ $job->slug }}/{{ $job->identifier }}" title="View page" target="_blank">View page <i class="fas fa-external-link-alt"></i></a>
                    </div>
                </b-card>
            </b-col>
            <b-col lg="9">
                <b-card header="{{ $job->title }}">
                    <b-row>
                        <b-col lg="3">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <a class="nav-link active" id="v-pills-details-tab" data-toggle="pill" href="#v-pills-details" role="tab" aria-controls="v-pills-details" aria-selected="true">Job Details</a>
                                <a class="nav-link" id="v-pills-req-tab" data-toggle="pill" href="#v-pills-req" role="tab" aria-controls="v-pills-req" aria-selected="false">Job Requirements</a>
                                <a class="nav-link" id="v-pills-salary-tab" data-toggle="pill" href="#v-pills-salary" role="tab" aria-controls="v-pills-salary" aria-selected="false">Salary</a>
                            </div>
                        </b-col>
                        <b-col lg="9">
                            <div class="tab-content" id="v-pills-tabContent">
                                <div class="tab-pane fade show active" id="v-pills-details" role="tabpanel" aria-labelledby="v-pills-details-tab">
                                    <table class="table table-sm table-borderless">
                                        <colgroup>
                                            <col width="150px">
                                            <col>
                                        </colgroup>
                                        <tr>
                                            <th>Identifier</th>
                                            <td>{{ $job->identifier }}</td>
                                        </tr>
                                        <tr>
                                            <th>Job Type</th>
                                            <td>{{ $job->type->description }}</td>
                                        </tr>
                                        <tr>
                                            <th>Province</th>
                                            <td>{{ $job->province->name }}</td>
                                        </tr>
                                        <tr>
                                            <th>City</th>
                                            <td>{{ !is_null($job->city) ? $job->city->name : $job->city_name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Vacancies</th>
                                            <td>{{ $job->number_of_vacancies }}</td>
                                        </tr>
                                        <tr>
                                            <th>Job Function</th>
                                            <td>{{ $job->job_function->name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Job Roles</th>
                                            <td>{{ implode(', ', $job->job_roles()->pluck('name')->toArray()) }}</td>
                                        </tr>
                                    </table>
                                    <div class="my-3"><strong>Description</strong></div>
                                    <div>
                                        {!! $job->description !!}
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="v-pills-req" role="tabpanel" aria-labelledby="v-pills-req-tab">
                                    <table class="table table-sm table-borderless">
                                        <colgroup>
                                            <col width="150px">
                                            <col>
                                        </colgroup>
                                        <tr>
                                            <th>Work Experience</th>
                                            <td>{{ $job->work_experience->description }}</td>
                                        </tr>
                                        <tr>
                                            <th>Must Have Skills</th>
                                            <td>{{ implode(', ', $job->must_skills()->pluck('name')->toArray()) }}</td>
                                        </tr>
                                        <tr>
                                            <th>Nice To Have Skills</th>
                                            <td>{{ implode(', ', $job->nice_skills()->pluck('name')->toArray()) }}</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="tab-pane fade" id="v-pills-salary" role="tabpanel" aria-labelledby="v-pills-salary-tab">
                                    <table class="table table-sm table-borderless">
                                        <colgroup>
                                            <col width="150px">
                                            <col>
                                        </colgroup>
                                        <tr>
                                            <th>Salary</th>
                                            <td>
                                                {{ $job->salary_currency }} {{ $job->formatted_salary_min }} - {{ $job->salary_currency }} {{ $job->formatted_salary_max }} {{ \App\Enums\WorkjobSalaryDuration::getDescription($job->salary_duration) }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Bonus Salary</th>
                                            <td>
                                                @if ($job->bonus_salary)
                                                    <div class="mb-2">Includes Bonus Salary</div>
                                                    <div>
                                                        {{ $job->salary_currency }} {{ $job->formatted_bonus_salary_min }} - {{ $job->salary_currency }} {{ $job->formatted_bonus_salary_max }} {{ \App\Enums\WorkjobSalaryDuration::getDescription($job->bonus_salary_duration) }}
                                                    </div>
                                                @else
                                                    <span>Without Bonus Salary</span>
                                                @endif
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </b-col>
                    </b-row>
                </b-card>
            </b-col>
        </b-row>
        
    </b-container>
@endsection