@extends('admin.layouts.app')

@section('content')
    <Workjobs :data="{{ json_encode($data) }}" />
@endsection
