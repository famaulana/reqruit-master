@extends('admin.layouts.app')

@section('content')
    <Users :data="{{ json_encode($data) }}" />
@endsection
