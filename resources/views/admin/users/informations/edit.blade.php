@extends('admin.users.informations.users-layout')

@section('user-information')
    <user-edit-details :user="{{ json_encode($user) }}"></user-edit-details>
    <user-edit-password :user="{{ json_encode($user) }}"></user-edit-password>
@endsection
