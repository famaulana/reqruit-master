@extends('admin.users.informations.users-layout')

@section('user-information')
    <user-addresses :data="{{ json_encode($data) }}"></user-addresses>
@endsection
