@extends('admin.layouts.app')

@section('content')
<b-container fluid>
    <b-row>
        <b-col md="5" lg="3" class="mb-3 mb-md-3">
            <user-card :user="{{ json_encode($user) }}"></user-card>
            
            {{-- <b-card class="card-navigation">
                <a 
                    class="nav-link {{ (Request::is('*/users/'.$user->id)) ? 'active' : '' }}" 
                    href="{{ route('admin.users.show', $user->id) }}"
                >
                    <span class="icon"><i class="far fa-dot-circle"></i></span>
                    <span class="text">Overview</span>
                </a>
    
                <a 
                    class="nav-link {{ (Request::is('*/users/*/edit')) ? 'active' : '' }}" 
                    href="{{ route('admin.users.edit', $user->id) }}"
                >
                    <span class="icon"><i class="fas fa-user-edit"></i></span>
                    <span class="text">Edit Profile</span>
                </a>
            </b-card> --}}
        </b-col>
        <div class="col-md-7 col-lg-9">
            @yield('user-information')
        </div>
    </b-row>
</b-container>
@endsection
