@extends('admin.users.informations.users-layout')

@section('user-information')
    <user-overview 
        :user="{{ json_encode($user) }}"
        :personality_test="{{ json_encode($personalityTest) }}"
    ></user-overview>
@endsection
