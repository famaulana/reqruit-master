@extends('admin.layouts.app')

@section('content')
<div class="container">
    <b-row>
        <b-col lg="8" class="m-auto">
            <user-create></user-create>
        </b-col>
    </b-row>
</div>
@endsection
