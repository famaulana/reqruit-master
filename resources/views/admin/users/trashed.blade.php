@extends('admin.layouts.app')

@section('content')
    <users-trashed :data="{{ json_encode($data) }}"></users-trashed>
@endsection
