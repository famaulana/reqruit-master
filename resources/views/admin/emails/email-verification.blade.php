Halo {{ $admin->name }},<br>
Klik link dibawah ini untuk verifikasi email admin Anda.<br>
<a href="{{ $link = route('auth.admin.verify', ['token' => $token]).'?email='.$admin->email }}"> {{ $link }} </a>
<br><br>
Sincerely,<br>
{{ config('app.name') }}