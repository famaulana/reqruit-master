@extends('admin.layouts.app')

@section('content')
    <Admins :data="{{ json_encode($data) }}" />
@endsection
