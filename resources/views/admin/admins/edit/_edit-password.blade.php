<div class="card">
    <div class="card-header">Update Password</div>
    <div class="card-body">
        <form action="{{ route('admin.admins.update-password', $admin->id) }}" method="POST" class="needs-validation" novalidate>
            @csrf
            <div class="form-group row">
                <label for="new_password" class="col-lg-3 col-form-label">
                    New Password
                    <span class="required">*</span>
                </label>
                <div class="col-lg-5">
                    <input
                        type="password"
                        class="form-control{{ $errors->has('new_password') ? ' is-invalid' : '' }}"
                        name="new_password"
                        autocomplete="new-password"
                        required
                    >
                    {!! $errors->first('new_password', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                </div>
            </div>
            <div class="form-group row">
                <label
                    for="new_password_confirmation"
                    class="col-lg-3 col-form-label"
                >
                    Confirm New Password
                    <span class="required">*</span>
                </label>
                <div class="col-lg-5">
                    <input
                        type="password"
                        class="form-control{{ $errors->has('new_password_confirmation') ? ' is-invalid' : '' }}"
                        name="new_password_confirmation"
                        autocomplete="off"
                        required
                    >
                    {!! $errors->first('new_password_confirmation', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                </div>
            </div>
            <div class="form-group mt-3">
                <button type="submit" class="btn btn-primary float-right">Update</button>
            </div>
        </form>
    </div>
</div>