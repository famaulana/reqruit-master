@extends('admin.layouts.app')

@section('content')
    <admins-trashed :data="{{ json_encode($data) }}"></admins-trashed>
@endsection
