@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <admin-card 
            :admin="{{ json_encode($admin) }}" 
            :current_admin_id="{{ Auth::guard('admin')->user()->id }}"
        ></admin-card>
        <div class="col-md-7 col-lg-9">
            @include('admin.admins.edit._edit-details')
            @include('admin.admins.edit._edit-password')
        </div>
    </div>
</div>
@endsection
