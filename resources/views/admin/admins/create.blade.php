@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">New Administrator</div>
        <div class="card-body">
            <form method="POST" action="{{ route('admin.admins.store') }}" class="needs-validation" enctype="multipart/form-data" novalidate>
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <h5>Details</h5>
                        <hr>

                        <div class="form-group row">
                            <label for="name" class="col-sm-4 col-form-label">
                                Name
                                <span class="required">*</span>
                            </label>
                            <div class="col-sm-8">
                                <input
                                    type="text"
                                    class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                    name="name"
                                    value="{{ old('name', (isset($admin)) ? $admin->name : '') }}"
                                    required
                                >
                                {!! $errors->first('name', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label">
                                Email
                                <span class="required">*</span>
                            </label>
                            <div class="col-sm-8">
                                <input
                                    type="text"
                                    class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                    name="email"
                                    value="{{ old('email', (isset($admin)) ? $admin->email : '') }}"
                                    required
                                >
                                {!! $errors->first('email', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="photo" class="col-sm-4 col-form-label">Photo</label>
                            <div class="col-sm-8 input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="photo" name="photo" placeholder="Drop file here...">
                                    <label class="custom-file-label" for="photo">Choose a file</label>
                                </div>
                                {!! $errors->first('photo', '<span class="text-danger small" role="alert">:message</span>') !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-4">Primary <span class="required">*</span></div>
                            <div class="col-sm-8">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="primary" name="primary" value="true" {{ (old('primary') == 'true' || (isset($admin) && $admin->primary)) ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="primary"></label>
                                </div>
                                {!! $errors->first('primary', '<span class="text-danger small" role="alert">:message</span>') !!}
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-4">
                                Password
                                <span class="required">*</span>
                            </div>
                            <div class="col-sm-8">
                                <input
                                    type="password"
                                    class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                    name="password"
                                    autocomplete="new-password"
                                    required
                                >
                                {!! $errors->first('password', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-4">
                                Confirm Password
                                <span class="required">*</span>
                            </div>
                            <div class="col-sm-8">
                                <input
                                    type="password"
                                    class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}"
                                    name="password_confirmation"
                                    autocomplete="off"
                                    required
                                >
                                {!! $errors->first('password_confirmation', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h5>Permissions</h5>
                        <hr>

                        @foreach ($permissions as $pm)
                            <div class="col-12 mb-2">
                                <div class="custom-control custom-switch">
                                    <input 
                                        type="checkbox" 
                                        class="custom-control-input" 
                                        name="permissions[]" 
                                        id="pm-{{ $pm->id }}" 
                                        value="{{ $pm->id }}"
                                        {{ (is_array(old('permissions')) && in_array($pm->id, old('permissions')) || (isset($admin) && (in_array($pm->id, $currentPms)))) ? 'checked' : '' }}
                                    >
                                    <label class="custom-control-label" for="pm-{{ $pm->id }}">{{ $pm->display_name }}</label>
                                </div>
                                {!! $errors->first('permissions', '<span class="text-danger small" role="alert">:message</span>') !!}
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="form-group mt-3">
                    <button type="submit" class="btn btn-primary float-right">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
