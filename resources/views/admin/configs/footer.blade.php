@extends('admin.configs.config-layout')

@section('config-form')
    <footer-config 
        :footer_content="{{ json_encode($footer_content) }}"
        :footer_bg_image_enable="'{{ $footer_bg_image_enable }}'"
        :footer_bg_image="'{{ $footer_bg_image }}'"
        :footer_bg_image_url="'{{ $footer_bg_image_url }}'"
    ></footer-config>
@endsection
