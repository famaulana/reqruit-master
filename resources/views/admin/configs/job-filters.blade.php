@extends('admin.configs.config-layout')

@section('config-form')
    <job-filters-config :jf_companies="'{{ json_encode($jf_companies) }}'" :jf_companies_data="{{ $companies }}">
    </job-filters-config>
@endsection
