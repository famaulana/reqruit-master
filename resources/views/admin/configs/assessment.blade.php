@extends('admin.configs.config-layout')

@section('config-form')
    <assessment-config 
        :english_test_q_count="'{{ $english_test_q_count }}'"
        :english_test_can_retest_after_days="'{{ $english_test_can_retest_after_days }}'"
        :english_test_session_2_timer="'{{ $english_test_session_2_timer }}'"
        :personal_test_can_retest_after_days="'{{ $personal_test_can_retest_after_days }}'"
    ></assessment-config>
@endsection
