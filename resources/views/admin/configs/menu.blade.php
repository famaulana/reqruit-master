@extends('admin.configs.config-layout')

@section('config-form')
    <menu-config 
        :menus="{{ json_encode($menus) }}"
        :top_menu="'{{ $top_menu }}'"
        :footer_menu="'{{ $footer_menu }}'"
    ></menu-config>
@endsection
