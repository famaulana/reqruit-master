@extends('admin.layouts.app')

@section('content')
    <b-container>
        <b-card>
            <b-row>
                <b-col md="3">
                    <div
                         class="nav flex-column nav-pills"
                         role="tablist"
                         aria-orientation="vertical">
                        <a
                           class="nav-link {{ Request::is('*/configs/general*') ? 'active' : '' }}"
                           href="{{ route('admin.configs.general') }}"
                           role="tab"
                           aria-selected="true">General</a>
                        <a
                           class="nav-link {{ Request::is('*/configs/logo*') ? 'active' : '' }}"
                           href="{{ route('admin.configs.logo') }}"
                           role="tab"
                           aria-selected="false">Logo</a>
                        <a
                           class="nav-link {{ Request::is('*/configs/homepage*') ? 'active' : '' }}"
                           href="{{ route('admin.configs.homepage') }}"
                           role="tab"
                           aria-selected="false">Homepage</a>
                        <a
                           class="nav-link {{ Request::is('*/configs/blog*') ? 'active' : '' }}"
                           href="{{ route('admin.configs.blog') }}"
                           role="tab"
                           aria-selected="false">Blog</a>
                        <a
                           class="nav-link {{ Request::is('*/configs/auth*') ? 'active' : '' }}"
                           href="{{ route('admin.configs.auth') }}"
                           role="tab"
                           aria-selected="false">Authentication</a>
                        <a
                           class="nav-link {{ Request::is('*/configs/menu*') ? 'active' : '' }}"
                           href="{{ route('admin.configs.menu') }}"
                           role="tab"
                           aria-selected="false">Menu</a>
                        <a
                           class="nav-link {{ Request::is('*/configs/assessment*') ? 'active' : '' }}"
                           href="{{ route('admin.configs.assessment') }}"
                           role="tab"
                           aria-selected="false">Assessment</a>
                        <a
                           class="nav-link {{ Request::is('*/configs/job-filters*') ? 'active' : '' }}"
                           href="{{ route('admin.configs.job-filters') }}"
                           role="tab"
                           aria-selected="false">Job Filters</a>
                        <a
                           class="nav-link {{ Request::is('*/configs/footer*') ? 'active' : '' }}"
                           href="{{ route('admin.configs.footer') }}"
                           role="tab"
                           aria-selected="false">Footer</a>
                        <a
                           class="nav-link {{ Request::is('*/configs/social*') ? 'active' : '' }}"
                           href="{{ route('admin.configs.social') }}"
                           role="tab"
                           aria-selected="false">Social</a>
                        <a
                           class="nav-link {{ Request::is('*/configs/locale*') ? 'active' : '' }}"
                           href="{{ route('admin.configs.locale') }}"
                           role="tab"
                           aria-selected="false">Locale</a>
                        <a
                           class="nav-link {{ Request::is('*/configs/javascript*') ? 'active' : '' }}"
                           href="{{ route('admin.configs.javascript') }}"
                           role="tab"
                           aria-selected="false">Javascript Code</a>
                    </div>
                </b-col>
                <b-col md="9">
                    <div class="tab-content" id="config-pills-tabContent">
                        @yield('config-form')
                    </div>
                </b-col>
            </b-row>
        </b-card>
    </b-container>
@endsection
