@extends('admin.configs.config-layout')

@section('config-form')
    <locale-config 
        :configs="{{ json_encode($configs) }}"
        :locales="{{ json_encode($locales) }}"
    ></locale-config>
@endsection
