@extends('admin.configs.config-layout')

@section('config-form')
    <shipment-config :configs="{{ json_encode($configs) }}"></shipment-config>
@endsection
