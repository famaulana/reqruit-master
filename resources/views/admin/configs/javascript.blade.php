@extends('admin.configs.config-layout')

@section('config-form')
    <javascript-config :configs="{{ json_encode($configs) }}"></javascript-config>
@endsection
