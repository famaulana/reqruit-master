@extends('admin.configs.config-layout')

@section('config-form')
    <homepage-config 
        :home_meta_title="'{{ $home_meta_title }}'"
        :home_meta_description="'{{ $home_meta_description }}'"
        :home_banner="'{{ $home_banner }}'"
        :home_banner_url="'{{ $home_banner_url }}'"
        :home_banner_mobile="'{{ $home_banner_mobile }}'"
        :home_banner_mobile_url="'{{ $home_banner_mobile_url }}'"
        :intro_right_image="'{{ $intro_right_image }}'"
        :intro_right_image_url="'{{ $intro_right_image_url }}'"
        :intro_left_image="'{{ $intro_left_image }}'"
        :intro_left_image_url="'{{ $intro_left_image_url }}'"
        :section_about_btn_link="'{{ $section_about_btn_link }}'"
        :section_about_yt_link="'{{ $section_about_yt_link }}'"
        :section_about_video_image="'{{ $section_about_video_image }}'"
        :section_about_video_image_url="'{{ $section_about_video_image_url }}'"
        :home_join_banner="'{{ $home_join_banner }}'"
        :home_join_banner_url="'{{ $home_join_banner_url }}'"
        :home_count_1_value="'{{ $home_count_1_value }}'"
        :home_count_1_label="'{{ $home_count_1_label }}'"
        :home_count_2_value="'{{ $home_count_2_value }}'"
        :home_count_2_label="'{{ $home_count_2_label }}'"
        :home_count_3_value="'{{ $home_count_3_value }}'"
        :home_count_3_label="'{{ $home_count_3_label }}'"
        :home_count_4_value="'{{ $home_count_4_value }}'"
        :home_count_4_label="'{{ $home_count_4_label }}'"
    ></homepage-config>
@endsection
