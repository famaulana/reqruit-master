@extends('admin.configs.config-layout')

@section('config-form')
    <logo-config 
        :logo1="'{{ $logo1 }}'"
        :logo2="'{{ $logo2 }}'"
        :logo3="'{{ $logo3 }}'"
        :logo_square="'{{ $logo_square }}'"
        :favicon="'{{ $favicon }}'"
        :admin_logo="'{{ $admin_logo }}'"
        :frontend_logo="'{{ $frontend_logo }}'"
        :email_logo="'{{ $email_logo }}'"
        :invoice_logo="'{{ $invoice_logo }}'"
        :light_logo="'{{ $light_logo }}'"
        :dark_logo="'{{ $dark_logo }}'"
        :logo1_url="'{{ $logo1_url }}'"
        :logo2_url="'{{ $logo2_url }}'"
        :logo3_url="'{{ $logo3_url }}'"
        :logo_square_url="'{{ $logo_square_url }}'"
        :favicon_url="'{{ $favicon_url }}'"
    ></logo-config>
@endsection
