@extends('admin.configs.config-layout')

@section('config-form')
    <blog-config 
        :ft_posts_data="{{ $ft_posts_data }}" 
        :featured_post_ids="'{{ json_encode($featured_post_ids) }}'"
        :pop_posts_data="{{ $pop_posts_data }}" 
        :pop_post_ids="'{{ json_encode($pop_post_ids) }}'"
    ></blog-config>
@endsection
