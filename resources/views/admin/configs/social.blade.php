@extends('admin.configs.config-layout')

@section('config-form')
    <social-config 
        :youtube_link="'{{ $youtube_link }}'"
        :instagram_link="'{{ $instagram_link }}'"
        :twitter_link="'{{ $twitter_link }}'"
        :facebook_link="'{{ $facebook_link }}'"
    ></social-config>
@endsection
