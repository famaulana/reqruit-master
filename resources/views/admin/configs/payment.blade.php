@extends('admin.configs.config-layout')

@section('config-form')
    <payment-config 
        :configs="{{ json_encode($configs) }}"
        :bank_logo_urls="{{ json_encode($logoUrls) }}"
    ></payment-config>
@endsection
