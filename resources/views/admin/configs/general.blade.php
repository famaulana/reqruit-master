@extends('admin.configs.config-layout')

@section('config-form')
    <general-config 
        :site_name="'{{ $site_name }}'"
        :site_email="'{{ $site_email }}'"
        :site_contact_email="'{{ $site_contact_email }}'"
    ></general-config>
@endsection
