@extends('admin.configs.config-layout')

@section('config-form')
    <auth-config 
        :auth_image="'{{ $auth_image }}'"
        :auth_image_url="'{{ $auth_image_url }}'"
        :auth_image_employer="'{{ $auth_image_employer }}'"
        :auth_image_employer_url="'{{ $auth_image_employer_url }}'"
    ></auth-config>
@endsection
