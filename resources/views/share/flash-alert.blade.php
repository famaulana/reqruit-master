@if (session()->has('flash_alert'))
    <div class="alert alert-{{ session()->get('flash_alert.level') }} alert-dismissible fade show" role="alert">
        {!! session()->get('flash_alert.message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif