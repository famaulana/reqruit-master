<table class="action" align="center" width="100%" cellpadding="0" cellspacing="0" role="presentation">
<tr>
<td align="center">
<table width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation">
<tr>
<td align="center">
@if (isset($full) && $full)
<table class="button-block-wrapper" border="0" cellpadding="0" cellspacing="0" role="presentation">
@else
<table border="0" cellpadding="0" cellspacing="0" role="presentation">
@endif
<tr>
<td>
<a href="{{ $url }}" class="button {{ $class ?? 'button-primary' }}" target="_blank" rel="noopener">{{ $slot }}</a>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
