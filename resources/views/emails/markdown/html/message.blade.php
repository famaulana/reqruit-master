@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header')
@if ($logo_url == '')
<a href="{{ url('/') }}" title="{{ $app_name }}" target="_blank">
{{ $app_name }}
</a>
@else
<a href="{{ url('/') }}" title="{{ $app_name }}" target="_blank">
<img src="{{ $logo_url }}" alt="{{ $app_name }}" class="logo">
</a>
@endif
@endcomponent
@endslot

{{-- Body --}}
{{ $slot }}

{{-- Subcopy --}}
@isset($subcopy)
@slot('subcopy')
@component('mail::subcopy')
{{ $subcopy }}
@endcomponent
@endslot
@endisset

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
© {{ date('Y') }} {{ $app_name }}. @lang('All rights reserved.')
@endcomponent
@endslot
@endcomponent
