@component('mail::message', ['app_name' => config('app_configs.site_name'), 'logo_url' => $logoUrl])
Hello, {{ $user->name }}

Click the following link or button to reset your password:

<a href="{{ $link }}" target="_blank">{{ $link }}</a>

@component('mail::button', ['url' => $link, 'class' => 'button-primary'])
Reset Password
@endcomponent

Thank you,<br>
{{ config('app_configs.site_name') }}
@endcomponent
