@component('mail::message', ['app_name' => $configs->getConfig('site_name'), 'logo_url' => $logoUrl])
Halo, {{ $newEmployer->name }}

Anda telah diundang oleh {{ $employer->name }} sebagai tim dari {{ $company->name }}.

Berikut ini detail akun Anda:<br>
<strong>User/Email:</strong> {{ $newEmployer->email }}<br>
<strong>Password:</strong> {{ $password }}<br>

@component('mail::button', ['url' => config('frontend.employer_url'), 'class' => 'button-primary'])
Login
@endcomponent

Ganti password Anda di <a href="{{ config('frontend.employer_url').'/account/edit' }}" title="Edit akun" target="_blank">halaman edit akun</a>.

Terima kasih,<br>
{{ $configs->getConfig('site_name') }}
@endcomponent
