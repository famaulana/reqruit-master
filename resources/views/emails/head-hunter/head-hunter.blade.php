@component('mail::message', ['app_name' => $configs->getConfig('site_name'), 'logo_url' => $logoUrl])
Message from head hunter form:

Sender details:<br>
<strong>Name:</strong> {{ $emailData['name'] }}<br>
<strong>Email:</strong> {{ $emailData['email'] }}<br>
<strong>Phone:</strong> {{ $emailData['phone'] }}<br>
<strong>Position:</strong> {{ $emailData['position'] }}<br>
<strong>Company:</strong> {{ $emailData['company'] }}<br>
<strong>Job Position:</strong> {{ $emailData['job_position'] }}<br>
<strong>Salary Min:</strong> Rp {{ \App\Helpers\AppHelper::formatNumber($emailData['salary_min'], 'id-ID') }}<br>
<strong>Salary Max:</strong> Rp {{ \App\Helpers\AppHelper::formatNumber($emailData['salary_max'], 'id-ID') }}<br>
<strong>Location:</strong> {{ $emailData['location'] }}<br>
<strong>Description:</strong><br>
{{ $emailData['description'] }}
@endcomponent
