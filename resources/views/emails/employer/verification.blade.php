@component('mail::message', ['app_name' => $siteName, 'logo_url' => $logoUrl])
Halo, {{ $employer->name }}

Klik link atau tombol dibawah ini untuk verifikasi email Kamu.

<a href="{{ $link }}" target="_blank">{{ $link }}</a>

@component('mail::button', ['url' => $link, 'class' => 'button-primary'])
Verifikasi Email
@endcomponent

Terima kasih,<br>
{{ $siteName }}
@endcomponent
