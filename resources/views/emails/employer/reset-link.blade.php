@component('mail::message', ['app_name' => $siteName, 'logo_url' => $logoUrl])
Hello, {{ $employer->name }}

Click the following link or button to reset your password:

<a href="{{ $link }}" target="_blank">{{ $link }}</a>

@component('mail::button', ['url' => $link, 'class' => 'button-primary'])
Reset Password
@endcomponent

Thank you,<br>
{{ $siteName }}
@endcomponent
