@component('mail::message', ['app_name' => $siteName, 'logo_url' => $logoUrl])
@if ($jobApplication->status->value === 'interview')
{{ $emailTemplate }}

<strong>Waktu: </strong> {{ $jobApplication->interview_time->format('d F Y H:i') }}<br />
<strong>Tempat: </strong> {{ $jobApplication->interview_location }}

@component('mail::button', [
    'url' => config('frontend.frontend_url').'/account/interview-action?jap='.$jobApplication->id.'&action=accept', 
    'class' => 'button-block button-primary',
    'full' => true
])
Terima wawancara
@endcomponent

@component('mail::button', [
    'url' => config('frontend.frontend_url').'/account/interview-action?jap='.$jobApplication->id.'&action=decline', 
    'class' => 'button-block button-error',
    'full' => true
])
Tolak wawancara
@endcomponent

@component('mail::button', [
    'url' => config('frontend.frontend_url').'/account/interview-action?jap='.$jobApplication->id.'&action=time-not-suit', 
    'class' => 'button-block button-outline-error',
    'full' => true
])
Waktu tidak sesuai
@endcomponent
@else
Halo, {{ $user->name }}

Status lamaran kerja Anda pada {{ $company->name }} telah berubah menjadi <strong>{{ $jobApplication->status->description }}</strong>.

@if ($jobApplication->status->value == 'unsuitable' && isset($jobApplication->data['rejection_message']))
<strong>Alasan Penolakan:</strong>
{{ $jobApplication->data['rejection_message'] }}
@endif

Terima kasih,<br>
{{ $siteName }}
@endif

@endcomponent
