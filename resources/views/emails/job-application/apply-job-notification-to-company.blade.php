@component('mail::message', ['app_name' => $configs->getConfig('site_name'), 'logo_url' => $logoUrl])
Hello,

{{ $userName }} has been applied to this job: <strong>{{ $jobTitle }}</strong>.

<strong>Name:</strong> {{ $userName }}<br>
<strong>Application Letter:</strong><br>
{{ $jobApplication->cover_letter }}

@component('mail::button', ['url' => config('frontend.employer_url').'/job-applications', 'class' => 'button-primary'])
See More Details
@endcomponent

Thank you,<br>
{{ $configs->getConfig('site_name') }}
@endcomponent
