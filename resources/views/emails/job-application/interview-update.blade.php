@component('mail::message', ['app_name' => $siteName, 'logo_url' => $logoUrl])
Hello,

@if ($status == 'accept')
User {{ $jobApplication->user->name }} accepted the interview for job {{ $jobApplication->workjob->title }}.
@elseif($status == 'decline')
User {{ $jobApplication->user->name }} denied the interview for job {{ $jobApplication->workjob->title }}.
@elseif($status == 'time-not-suit')
User {{ $jobApplication->user->name }} feels that interview time is not suitable for job {{ $jobApplication->workjob->title }}.
@endif

<strong>Interview time:</strong> {{ $jobApplication->interview_time }}
<br />
<strong>Interview location:</strong> {{ $jobApplication->interview_location }}

Thank you,<br>
{{ $siteName }}
@endcomponent
