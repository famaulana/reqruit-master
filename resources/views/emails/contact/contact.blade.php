@component('mail::message', ['app_name' => $configs->getConfig('site_name'), 'logo_url' => $logoUrl])
Message from contact us form:

Sender details:<br>
<strong>Name:</strong> {{ $senderName }}<br>
<strong>Email:</strong> {{ $senderEmail }}<br>
<strong>Phone:</strong> {{ $senderPhone }}<br>
<strong>Message:</strong><br>
{{ $message }}
@endcomponent
