import Swal from "sweetalert2";

$(document).ready(function() {
    $(".btn-submit-confirm").on("click", function(e) {
        e.preventDefault();
        Swal.fire({
            title: "Are you sure?",
            text: $(this).data("msg"),
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, do it!"
        }).then(result => {
            if (result.isConfirmed) {
                $(this)
                    .parent("form")
                    .submit();
            }
        });
    });
});
