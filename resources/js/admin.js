/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");
require("./helpers/helpers");
import Snotify, { SnotifyPosition } from "vue-snotify";
import VueSweetalert2 from "vue-sweetalert2";
import draggable from "vuedraggable";
import VJstree from "vue-jstree";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import DatePick from "vue-date-pick";
import Vuelidate from "vuelidate";
import CKEditor from "@ckeditor/ckeditor5-vue";

window.Vue = require("vue");

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
// lodash
Vue.prototype._ = _;
Vue.use(Snotify, { toast: { position: SnotifyPosition.rightTop } });
Vue.use(VueSweetalert2);
Vue.use(Vuelidate);
Vue.use(CKEditor);
Vue.component("draggable", draggable);
Vue.component("v-jstree", VJstree);
Vue.component("date-pick", DatePick);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Components
require("./components/admin");

// Filters
require("./filters");

const app = new Vue({
    el: "#app"
});
