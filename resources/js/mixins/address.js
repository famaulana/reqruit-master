import Vue from "vue";

const AddressMixin = {
    install(Vue, options) {
        Vue.mixin({
            data() {
                return {
                    city_keyword: "",
                    provinces: [],
                    cities: [],
                    cities_search_result: [],
                    subdistricts: []
                };
            },
            methods: {
                getProvinces() {
                    axios
                        .get(`${APP_BASE_URL}/api/provinces`)
                        .then(res => {
                            var data = res.data;
                            this.provinces = data.data;
                        })
                        .catch(err => {
                            console.log(err);
                        });
                },
                getCities(province_id) {
                    axios
                        .get(`${APP_BASE_URL}/api/cities/${province_id}`)
                        .then(res => {
                            var data = res.data;
                            this.cities = data.data;
                            this.subdistricts = [];
                        })
                        .catch(err => {
                            console.log(err);
                        });
                },
                searchCities(search, loading) {
                    loading(true);

                    axios
                        .get(
                            `${APP_BASE_URL}/api/cities-search/?s=${escape(
                                search
                            )}`
                        )
                        .then(res => {
                            var data = res.data;
                            this.cities_search_result = data.data;
                        })
                        .catch(err => {
                            console.log(err);
                        })
                        .finally(() => {
                            loading(false);
                        });
                },
                getSubdistricts(city_id) {
                    axios
                        .get(`${APP_BASE_URL}/api/subdistricts/${city_id}`)
                        .then(res => {
                            var data = res.data;
                            this.subdistricts = data.data;
                        })
                        .catch(err => {
                            console.log(err);
                        });
                }
            }
        });
    }
};

Vue.use(AddressMixin);
