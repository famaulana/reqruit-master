import Vue from "vue";

const SwalMixin = {
    install(Vue, options) {
        Vue.mixin({
            methods: {
                swalToast(icon, title) {
                    this.$swal({
                        toast: true,
                        icon,
                        title,
                        position: "center",
                        showConfirmButton: false,
                        timer: 3000
                    });
                },
                swalSuccess(text = "") {
                    this.$swal({
                        icon: "success",
                        title: "Success",
                        text: text == "" ? "Success!" : text
                    });
                },
                swalWrong(text = "") {
                    this.$swal({
                        icon: "error",
                        title: "Oops...",
                        text: text == "" ? "Something went wrong!" : text
                    });
                },
                swalWarning(text = "") {
                    this.$swal({
                        icon: "warning",
                        title: "Warning...",
                        text: text == "" ? "Something went wrong!" : text
                    });
                },
                swalErrorValidation(error_data) {
                    let errors = "<ul>";
                    for (var property in error_data) {
                        if (error_data.hasOwnProperty(property)) {
                            errors +=
                                "<li>" + error_data[property][0] + "</li>";
                        }
                    }
                    errors += "</ul>";

                    this.$swal({
                        title: "Oops...",
                        icon: "error",
                        html: errors,
                        showCloseButton: true
                    });
                }
            }
        });
    }
};

Vue.use(SwalMixin);
