import Vue from "vue";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";

const CKEditorMixin = {
    install(Vue, options) {
        Vue.mixin({
            data() {
                return {
                    editor_type: ClassicEditor,
                    editorConfig: {
                        toolbar: {
                            items: [
                                "heading",
                                "|",
                                "bold",
                                "italic",
                                "underline",
                                "strikethrough",
                                "link",
                                "alignment",
                                "bulletedList",
                                "numberedList",
                                "|",
                                "indent",
                                "outdent",
                                "|",
                                "imageUpload",
                                "blockQuote",
                                "insertTable",
                                "mediaEmbed",
                                "|",
                                "undo",
                                "redo"
                                // "|",
                                // "code",
                            ]
                        },
                        image: {
                            styles: [
                                "full",
                                "alignLeft",
                                "alignCenter",
                                "alignRight"
                            ],
                            toolbar: [
                                "imageTextAlternative",
                                "|",
                                "imageStyle:full",
                                "imageStyle:alignLeft",
                                "imageStyle:alignCenter",
                                "imageStyle:alignRight"
                            ]
                        },
                        table: {
                            contentToolbar: [
                                "tableColumn",
                                "tableRow",
                                "mergeTableCells"
                            ]
                        },
                        simpleUpload: {
                            uploadUrl: `${DASHBOARD_BASE_URL}`,
                            withCredentials: true,
                            headers: {
                                "X-CSRF-TOKEN": document.querySelector(
                                    'meta[name="csrf-token"]'
                                ).content
                            }
                        }
                    }
                };
            },
            created() {
                this.editorConfig.simpleUpload.uploadUrl = this.ckeditor_upload_image_url;
            }
        });
    }
};

Vue.use(CKEditorMixin);
