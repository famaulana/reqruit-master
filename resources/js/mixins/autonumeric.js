import Vue from "vue";
import VueAutonumeric from "vue-autonumeric";

const AutoNumericMixin = {
    install(Vue, options) {
        Vue.mixin({
            data() {
                return {
                    autonumeric: {
                        digitGroupSeparator: this.locale == "id-ID" ? "." : ",",
                        decimalCharacter: this.locale == "id-ID" ? "," : ".",
                        decimalCharacterAlternative:
                            this.locale == "id-ID" ? "." : ",",
                        minimumValue: "0",
                        allowDecimalPadding:
                            this.locale == "id-ID" ? false : true,
                        decimalPlaces: this.locale == "id-ID" ? 0 : 2
                    }
                };
            },
            components: {
                VueAutonumeric
            }
        });
    }
};

Vue.use(AutoNumericMixin);
