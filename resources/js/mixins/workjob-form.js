import Vue from "vue";

const WorkjobFormMixin = {
    install(Vue, options) {
        Vue.mixin({
            data() {
                return {
                    form_steps: [
                        "details",
                        "roles",
                        "salary",
                        "description",
                        "job_question"
                    ],
                    form_step_fields: {
                        details: [
                            "title",
                            "type",
                            "province_id",
                            "city_id",
                            "number_of_vacancies",
                            "job_posting_status"
                        ],
                        roles: [
                            "jobfunction_id",
                            "jobrole_ids",
                            "must_skill_ids",
                            "nice_skill_ids",
                            "work_experience"
                        ],
                        salary: [
                            "salary_min",
                            "salary_max",
                            "salary_duration",
                            "bonus_salary",
                            "bonus_salary_min",
                            "bonus_salary_max",
                            "bonus_salary_duration"
                        ],
                        description: ["description"],
                        job_question: ["has_question", "questions"]
                    }
                };
            },
            methods: {
                goTo(type) {
                    if (type == "next" || type == "prev") {
                        const total_steps = this.form_steps.length - 1;
                        let num = 0;
                        for (let index = 0; index < total_steps; index++) {
                            if (this.form_steps[index] === this.active_form) {
                                break;
                            }
                            num++;
                        }

                        let key = 0;
                        if (type == "prev") {
                            key = num === 0 ? 0 : num - 1;
                        } else {
                            key = num === total_steps ? total_steps : num + 1;
                        }

                        this.active_form = this.form_steps[key];
                    } else {
                        this.active_form = type;
                    }
                },
                jobDetailUpdate(form) {
                    this.form.title = form.title;
                    this.form.type = form.type;
                    this.form.province_id = form.province_id;
                    this.form.city_id = form.city_id;
                    this.form.number_of_vacancies = form.number_of_vacancies;
                    this.form.job_posting_status = form.job_posting_status;
                },
                rolesUpdate(form) {
                    this.form.jobfunction_id = form.jobfunction_id;
                    this.form.jobrole_ids = form.jobrole_ids;
                    this.form.must_skill_ids = form.must_skill_ids;
                    this.form.nice_skill_ids = form.nice_skill_ids;
                    this.form.work_experience = form.work_experience;
                },
                salaryUpdate(form) {
                    this.form.salary_min = form.salary_min;
                    this.form.salary_max = form.salary_max;
                    this.form.salary_duration = form.salary_duration;
                    this.form.bonus_salary = form.bonus_salary;
                    this.form.bonus_salary_min = form.bonus_salary_min;
                    this.form.bonus_salary_max = form.bonus_salary_max;
                    this.form.bonus_salary_duration =
                        form.bonus_salary_duration;
                },
                descriptionUpdate(form) {
                    this.form.description = form.description;
                },
                jobQuestionUpdate(form) {
                    this.form.has_question = form.has_question;
                    this.form.questions = form.questions;
                },
                actionUpdate(form) {
                    this.form.status = form.status;
                },
                save() {
                    this.saving = true;

                    if (this.form.id == "") {
                        axios
                            .post(`${APP_BASE_URL}/employer/workjob`, this.form)
                            .then(res => {
                                this.handlingSaveSuccess();
                            })
                            .catch(err => {
                                this.handlingSaveFail(err);
                            })
                            .finally(() => {
                                this.saving = false;
                            });
                    } else {
                        axios
                            .patch(
                                `${APP_BASE_URL}/employer/workjob/${this.form.id}`,
                                this.form
                            )
                            .then(res => {
                                this.handlingSaveSuccess();
                            })
                            .catch(err => {
                                this.handlingSaveFail(err);
                            })
                            .finally(() => {
                                this.saving = false;
                            });
                    }
                },
                handlingSaveSuccess() {
                    window.location.href = `${APP_BASE_URL}/employer/workjob`;
                },
                handlingSaveFail(err) {
                    console.log(err.response);
                    let response = err.response;
                    if (response.status == 422) {
                        this.errors = response.data.errors;
                        let first_error = Object.keys(this.errors)[0];

                        Object.keys(this.form_step_fields).forEach(step => {
                            this.form_step_fields[step].forEach(field => {
                                if (field === first_error) {
                                    this.active_form = step;
                                }
                            });
                        });

                        this.swalWrong("Beberapa data tidak valid!");
                    } else {
                        this.swalWrong();
                    }
                }
            }
        });
    }
};

Vue.use(WorkjobFormMixin);
