import Vue from "vue";

const SlugifyMixin = {
    install(Vue, options) {
        Vue.mixin({
            methods: {
                slugify(string) {
                    var slugify = require("slugify");

                    return slugify(string.toLowerCase(), {
                        remove: /[#^{}?/;,=`\[\]*+~.()'"!:@\\]/g
                    });
                },
                slugifyOnSlugEdit(string) {
                    this.form_data.slug = this.slugify(string);
                }
            }
        });
    }
};

Vue.use(SlugifyMixin);
