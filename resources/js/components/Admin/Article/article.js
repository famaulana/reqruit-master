Vue.component("Articles", require("./Articles.vue").default);
Vue.component("ArticleCreate", require("./ArticleCreate.vue").default);
Vue.component("ArticleEdit", require("./ArticleEdit.vue").default);
Vue.component("ArticlesTrashed", require("./ArticlesTrashed.vue").default);
