Vue.component("StaticBlocks", require("./StaticBlocks.vue").default);
Vue.component("StaticBlockCreate", require("./StaticBlockCreate.vue").default);
Vue.component("StaticBlockEdit", require("./StaticBlockEdit.vue").default);
Vue.component(
    "StaticBlocksTrashed",
    require("./StaticBlocksTrashed.vue").default
);
