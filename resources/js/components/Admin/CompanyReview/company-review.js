Vue.component("CompanyReviews", require("./CompanyReviews.vue").default);
Vue.component(
    "CompanyReviewsTrashed",
    require("./CompanyReviewsTrashed.vue").default
);
Vue.component("CompanyReviewEdit", require("./CompanyReviewEdit.vue").default);
Vue.component("CompanyReviewForm", require("./CompanyReviewForm.vue").default);
