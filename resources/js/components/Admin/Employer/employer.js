Vue.component("Employers", require("./Employers.vue").default);
Vue.component("EmployersTrashed", require("./EmployersTrashed.vue").default);
Vue.component(
    "EmployerCard",
    require("./Informations/EmployerCard.vue").default
);
Vue.component(
    "EmployerCompanies",
    require("./Informations/EmployerCompanies.vue").default
);
