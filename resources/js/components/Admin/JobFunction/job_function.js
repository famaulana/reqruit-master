Vue.component("Jobfunctions", require("./Jobfunctions.vue").default);
Vue.component(
    "JobfunctionsTrashed",
    require("./JobfunctionsTrashed.vue").default
);
Vue.component("JobfunctionCreate", require("./JobfunctionCreate.vue").default);
Vue.component("JobfunctionEdit", require("./JobfunctionEdit.vue").default);
