Vue.component("Users", require("./Users.vue").default);
Vue.component("UsersTrashed", require("./UsersTrashed.vue").default);
Vue.component("UserCreate", require("./UserCreate.vue").default);

Vue.component("UserEditDetails", require("./Edit/UserEditDetails.vue").default);
Vue.component(
    "UserEditPassword",
    require("./Edit/UserEditPassword.vue").default
);

Vue.component("UserCard", require("./Informations/UserCard.vue").default);
Vue.component(
    "UserOverview",
    require("./Informations/UserOverview.vue").default
);
Vue.component(
    "UserAddresses",
    require("./Informations/UserAddresses.vue").default
);
