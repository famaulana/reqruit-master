// Share
require("./Share/share");

// Layout
require("./Admin/Layout/layout");

// Home
require("./Admin/Home/home");

// Category
require("./Admin/Category/category");

// Page
require("./Admin/Page/page");

// Employer
require("./Admin/Employer/employer");

// Head Hunting
require("./Admin/HeadHunting/head-hunting");

// Company
require("./Admin/Company/company");

// Company review
require("./Admin/CompanyReview/company-review");

// Industry
require("./Admin/Industry/industry");

// Work Job
require("./Admin/WorkJob/work_job");

// Job application
require("./Admin/JobApplication/job-application");

// Skill
require("./Admin/Skill/skill");

// Job Function
require("./Admin/JobFunction/job_function");

// Job Role
require("./Admin/JobRole/job_role");

// Testimonial
require("./Admin/Testimonial/testimonial");

// Article
require("./Admin/Article/article");

// User
require("./Admin/User/user");

// Education degree
require("./Admin/EducationDegree/education-degree");

// Education field
require("./Admin/EducationField/education-field");

// English question
require("./Admin/EnglishQuestion/english-question");

// MainBanner
require("./Admin/MainBanner/main-banner");

// StaticBlock
require("./Admin/StaticBlock/static-block");

// Menu
require("./Admin/Menu/menu");

// Admin
require("./Admin/Admin/admin");

// Config
require("./Admin/Config/config");

// Job application rejection message
require("./Admin/JapRejectionMessage/jap-rejection-message");
